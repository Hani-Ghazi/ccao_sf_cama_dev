
# This program demonstrates R connectivity to a Postgresql database

rm(list=ls())

# Controls ----
# Who is running this code?
user <- "rross"

# What packages will you need?
libs <- c("DBI", "odbc","dplyr","sqldf", "rgdal", "sf", "xlsx", "maptools","sp","foreign","fields", "mgcv", "car","RPostgreSQL"
          , "plyr","dplyr", "DBI", "RODBC", "RStata", "lattice", "RColorBrewer", 
          "ggplot2", "scales", "zoo", "leaflet", "htmlwidgets", "htmltools", "sjPlot"
          ,"gbm", "progress", "knitr", "kableExtra","Hmisc", "quantreg","randomForest","geosphere", "ape", "SpatialNP")

wd <- paste0("C:/Users/",user,"/Documents/CCAO_SF_CAMA_DEV")
setwd(wd)
common.drive <- "O:/CCAODATA"
dirs <- list(code=paste0(wd,"/code.r/")
             , data=paste0(common.drive,"/data/")
             , results=paste0(wd, "/results/")
             , spatial_data=paste0(common.drive,"/data/spatial/")
             , parcels=paste0(common.drive,"/data/spatial/", "2018_parcels.shp")
             , raw_parcels=paste0(common.drive,"/data/spatial/", "TY2018_DefaultParcels.shp")
             , raw_tracts=paste0(common.drive,"/data/spatial/", "tl_2018_17_tract.shp")
             , bad_data=paste0(common.drive,"/data/bad_data/")
             , raw_data=paste0(common.drive,"/data/raw_data/"))

# Load utilities
source(paste0(dirs$code, "99_utility_2.r"))
# Refresh packages
check.packages(libs)
rm(libs)
# Open DB connection ----
CCAODATA <- dbConnect(odbc(),
                      Driver = "SQL Server",
                      Server = "10.128.57.154",
                      Database = "CCAODATA",
                      UID = "generalUser",
                      PWD = "generalUser" ,
                      Port = 1433)

# Once a connection is made, you will be able to view the tables and fields available in the connections tab.

# Query from Mike:
  # Want: CLASS, FORMER CLASS, SALE DATE, PIN, ADDRESS, and SALE AMOUNT
  # For:  properties that were 100-class at any point following the sale
  # Sales after 2016
  # look for 100 class in 17 or 18

# # ALL 100 class properties in 2018
# (SELECT HD_PIN as PIN, HD_CLASS 
#   FROM HEAD 
#   WHERE TAX_YEAR IN (2018) AND HD_CLASS IN (100)
# ) as A
# 
# # ALL sales in 2016 NOT 100 class
# (SELECT PIN, REC_DATE, ADDR, CLASS as PTAX_CLASS, SALE_PRICE 
#   FROM PTAX203 
#   WHERE CONVERT(INT, RIGHT(CONVERT(varchar, REC_DATE),4)) IN (2016) 
#   AND CLASS NOT IN (100)
# ) as B
# 
# INNER JOIN ---> "INNER" means all matching


test <- dbGetQuery(CCAODATA,"
SELECT '100 class in 18, sold as not 100 class in 16' as DECRIPTION,
A.*, REC_DATE, ADDR, PTAX_CLASS_2016, SALE_PRICE
  FROM
  (SELECT HD_PIN as PIN, HD_TOWN, HD_CLASS as HD_CLASS_2018 FROM HEAD 
   WHERE TAX_YEAR IN (2018) AND HD_CLASS IN (100) 
  AND CONVERT(INT, SUBSTRING(CONVERT(CHARACTER, HD_TOWN),1,2)) IN
      (17, 23, 10, 16, 18, 20, 22, 24, 25, 26, 29, 30, 26 )
  ) as A
  INNER JOIN
(SELECT PIN, REC_DATE, ADDR, CLASS as PTAX_CLASS_2016, SALE_PRICE 
  FROM PTAX203 
  WHERE CONVERT(INT, RIGHT(CONVERT(varchar, REC_DATE),4)) IN (2016) 
  AND CLASS NOT IN (100)
  ) as B
  ON A.PIN=B.PIN
   ")

test2 <- dbGetQuery(CCAODATA,"
SELECT '100 class in 17, sold as not 100 class in 16' as DECRIPTION,
A.*, REC_DATE, ADDR, PTAX_CLASS_2016, SALE_PRICE FROM
  (SELECT HD_PIN as PIN, HD_TOWN, HD_CLASS as HD_CLASS_2017 FROM HEAD 
  WHERE TAX_YEAR IN (2017) AND HD_CLASS IN (100) 
  AND CONVERT(INT, SUBSTRING(CONVERT(CHARACTER, HD_TOWN),1,2)) IN
      (17, 23, 10, 16, 18, 20, 22, 24, 25, 26, 29, 30, 26 )
  ) as A
  INNER JOIN
  (SELECT PIN, REC_DATE, ADDR, CLASS as PTAX_CLASS_2016, SALE_PRICE 
  FROM PTAX203 
  WHERE CONVERT(INT, RIGHT(CONVERT(varchar, REC_DATE),4)) IN (2016) 
  AND CLASS NOT IN (100)
  ) as B
  ON A.PIN=B.PIN
  ")
test3 <- dbGetQuery(CCAODATA,"
SELECT  '100 class in 18, sold as not 100 class in 17' as DECRIPTION,
A.*, REC_DATE, ADDR, PTAX_CLASS_2017, SALE_PRICE FROM
  (SELECT HD_PIN as PIN, HD_TOWN, HD_CLASS as HD_CLASS_2017 FROM HEAD 
  WHERE TAX_YEAR IN (2018) AND HD_CLASS IN (100) 
  AND CONVERT(INT, SUBSTRING(CONVERT(CHARACTER, HD_TOWN),1,2)) IN
  (17, 23, 10, 16, 18, 20, 22, 24, 25, 26, 29, 30, 26 )
) as A
  INNER JOIN
  (SELECT PIN, REC_DATE, ADDR, CLASS as PTAX_CLASS_2017, SALE_PRICE 
  FROM PTAX203 
  WHERE CONVERT(INT, RIGHT(CONVERT(varchar, REC_DATE),4)) IN (2017) 
  AND CLASS NOT IN (100)
  ) as B
  ON A.PIN=B.PIN
")
mikes_data_query <- rbind.fill(test, test2, test3)
write.xlsx(mikes_data_query, file="C:/Users/wicabal/Downloads/mikes_data.xlsx")
rm(test, test2, test3, mikes_data_query)


test <- dbGetQuery(CCAODATA," SELECT COUNT(PIN) FROM PTAX203" )
write.csv(test, file=paste0(dirs$data, "test.csv"))

# query the data from postgreSQL 
df_postgres <- dbGetQuery(con, "
    SELECT DISTINCT pin14, taxyear
	, senex
	, CASE WHEN senex>0 THEN 1 ELSE 0 END as flag_senex
	, senfrzex
	, rate
	, homeex
	, CASE WHEN homeex>0 THEN 1 ELSE 0 END as flag_homeex
	, class
	, CASE WHEN senfrzex>0 THEN 1 ELSE 0 END as flag_senfrzex
	, totaltax 
	, CASE WHEN pin2017 IS NULL THEN 0 ELSE 1 END as flag_addr
	, CASE WHEN pin14 IS NULL THEN 0 ELSE 1 END as flag_cmf
	FROM ccao_working.cmf_all_taxes AS B 
	LEFT JOIN ccao_working.pin_addresses_2017 AS A
	ON A.pin2017=B.pin14
	WHERE taxyear>2014 OR taxyear IS NULL
	ORDER BY pin14, taxyear
                          ")

rm(pw) # removes the password

# check for the cartable
dbExistsTable(con, "cartable")
=======

# This program demonstrates R connectivity to a Postgresql database


#install.packages("RPostgreSQL")
#install.packages("odbc")
#install.packages("DBI")
#install.packages("dbplyr")
#install.packages("RODBC")

require("RPostgreSQL")
library(odbc)
library(DBI)
library(dbplyr)
library(RODBC)

drv <- dbDriver("PostgreSQL")
pw <- "password"

con <- dbConnect(drv, dbname = "postgres",
                 host = "localhost", port = 5432,
                 user = "postgres", password = pw)

# Once a connection is made, you will be able to view the tables and fields available in the connections tab.

# query the data from postgreSQL 
df_postgres <- dbGetQuery(con, "
    SELECT DISTINCT pin14, taxyear
	, senex
	, CASE WHEN senex>0 THEN 1 ELSE 0 END as flag_senex
	, senfrzex
	, rate
	, homeex
	, CASE WHEN homeex>0 THEN 1 ELSE 0 END as flag_homeex
	, class
	, CASE WHEN senfrzex>0 THEN 1 ELSE 0 END as flag_senfrzex
	, totaltax 
	, CASE WHEN pin2017 IS NULL THEN 0 ELSE 1 END as flag_addr
	, CASE WHEN pin14 IS NULL THEN 0 ELSE 1 END as flag_cmf
	FROM ccao_working.cmf_all_taxes AS B 
	LEFT JOIN ccao_working.pin_addresses_2017 AS A
	ON A.pin2017=B.pin14
	WHERE taxyear>2009 OR taxyear IS NULL
	ORDER BY pin14, taxyear
                          ")

exemptions_sums <- dbGetQuery(con, "
SELECT 
taxyear, count(DISTINCT pin) as pins
, SUM(flag_vetex) as veterans_exemptions 
, SUM(flag_longex) as longtime_exemptions
, SUM(flag_retvetex) as retired_veterans_exemptions
, SUM(flag_disabpersex) as disabled_exemptions
, SUM(flag_disabvetex) as disabled_veterans_exemptions
, SUM(flag_homeex) as homeowners_exemptions
, SUM(flag_senex) as senior_exemptions
, SUM(flag_senfrzex) as senior_freeze_exemptions
FROM (
  SELECT taxyear, pin14 as pin
  , CASE WHEN vetex>0 THEN 1 ELSE 0 END as flag_vetex
  , CASE WHEN longex>0 THEN 1 ELSE 0 END as flag_longex
  , CASE WHEN retvetex>0 THEN 1 ELSE 0 END as flag_retvetex
  , CASE WHEN disabpersex>0 THEN 1 ELSE 0 END as flag_disabpersex
  , CASE WHEN disabvetex>0 THEN 1 ELSE 0 END as flag_disabvetex
  , CASE WHEN homeex>0 THEN 1 ELSE 0 END as flag_homeex
  , CASE WHEN senex>0 THEN 1 ELSE 0 END as flag_senex
  , CASE WHEN senfrzex>0 THEN 1 ELSE 0 END as flag_senfrzex
  FROM ccao_working.cmf_all_taxes
  WHERE 
    taxyear IN (2013, 2014) 
    AND maj_descr IN ('Reg Residential')
) AS A
GROUP BY taxyear
")

bad_data <- dbGetQuery(CCAODATA, "
SELECT TOP 100 PIN, FBATH, HD_CLASS as CLASS
FROM CCAOSFCHARS as C
LEFT JOIN
HEAD AS H
ON HD_PIN=PIN AND H.TAX_YEAR=C.TAX_YEAR
WHERE FBATH IN (0) AND C.TAX_YEAR IN (2018) AND MULTI_IND=0 
")
write.csv(bad_data, file="C:/Users/rross/Downloads/bad_data.csv")


bad_pin <- dbGetQuery(CCAODATA, "
SELECT TOP 100 PIN, FBATH, HD_CLASS as CLASS, C.TAX_YEAR
FROM CCAOSFCHARS as C
LEFT JOIN
HEAD AS H
ON HD_PIN=PIN AND H.TAX_YEAR=C.TAX_YEAR
WHERE PIN IN (11201020320000)
ORDER BY TAX_YEAR
")
write.csv(bad_data, file="C:/Users/rross/Downloads/bad_data.csv")


# Accurate CHARS?



write.csv(exemptions_sums, "C:/Users/Robert A Ross/Downloads/Don's Data.csv")
rm(pw) # removes the password

# check for the cartable
dbExistsTable(con, "cartable")
>>>>>>> 7e71fba93e6c557cdd5d902b8fb72f73bebd998b
# TRUE