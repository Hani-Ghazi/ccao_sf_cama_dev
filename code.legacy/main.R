# Header -----
# Open-source Computer Assisted Mass Appraisal System, 
# plus ancillary  functions.
# Developed by the Transition Team for Fritz Kaegi
# for testing, refinement, and deployment in the Cook County Assessors' Office
# 
# Version: 0.3.1
# See accompanying technical documentation for detailed walkthrough
# 

rm(list=ls())

# Users
# JJM Productions
# Robert Ross
# Ray Gottner
# A new user

user <- "Robert Ross"

# Set working directory ------
if(user=="JJM Productions"){setwd("C:/Users/JJM Productions/Desktop/CCAO");}
if(user=="Robert Ross"){setwd("C:/Users/Robert A Ross/Documents/CCAO_CAMA_SF");}
if(user=="Ray Gottner"){setwd("----- RAY PUT DIRECTORY HERE ------");}
if(user=="A new user"){setwd("----- PUT DIRECTORY HERE ------");}
dirs <- list(data="",code="code.r/",data_pilot="data/Assessor Information from Hal/",
             data_shp="data/SHP/shp2016.Rdata",data_shp_raw="data/SHP/ccgisdata_-_Parcels_2016.csv");
options(scipen=20);

# Refresh code ----
# system.time(source("C:/Users/JJM Productions/Desktop/CCAO/code.r/main.R"));

# Initial block of setup code
if (TRUE) {
	dirs <- list(data="",code="code.r/",data_pilot="data/Assessor Information from Hal/",
				data_shp="data/SHP/shp2016.Rdata",data_shp_raw="data/SHP/ccgisdata_-_Parcels_2016.csv");
	options(scipen=20);
	# Read in global shapefile
	if (TRUE) {
		load(dirs$data_shp);
		if (FALSE) {	# RUN THIS BLOCK OF CODE ONCE TO COMPILE THE CSV INTO RDATA
			SHP <- read.csv(dirs$data_shp_raw,as.is=TRUE);
			SHP <- subset(SHP,select=c(Name,the_geom));
			save(SHP,file=dirs$data_shp);
		}
	}

	# This is a function that refreshes all code files
	.r <- function(recode=TRUE) {
		require(xlsx);
		
		# Load in data constants
		if (recode) {
			cat("Loading in codebook...");flush.console();
			fn_codebook <- paste0(dirs$code,"codebook.xlsx");
			fn_worksheets <- c("TOWNSHIPS","IQR","Scope_Objects","Setup_Packages","Setup_Library");
			code <- lapply(fn_worksheets,function(wk) read.xlsx(fn_codebook,sheetName=wk,stringsAsFactors=FALSE));
			names(code) <- fn_worksheets;
			code$IQR$IQRm <- as.numeric(code$IQR$IQRm);	# Special coding of infinity
			CODE <<- code;
			cat("loaded as CODE$\n",paste(paste0("\t",names(CODE)),collapse="\n"),"\n",sep="");flush.console();
		}
		
		# Load packages
		cat("Refreshing R-packages...");flush.console();
		this <- CODE$Setup_Packages;
		
		# Check to see if packages are installed. Install them if they are not, then load them into the R session.
		check.packages <- function(pkg){
		  new.pkg <- pkg[!(pkg %in% installed.packages()[, "Package"])]
		  if (length(new.pkg)) 
		    install.packages(new.pkg, dependencies = TRUE)
		  sapply(pkg, require, character.only = TRUE)
		}
		for (iter in 1:nrow(this))
		  check.packages(this$Package[iter]);

		# Load source files
		cat("Refreshing R-source files...\n");flush.console();
		this <- CODE$Setup_Library;
		for (iter in 1:nrow(this)) {
			cat("\t",this$File[iter],"...",sep="");flush.console();
			source(paste0(dirs$code,this$File[iter]));
			cat("[OK]\n");flush.console();
		}
	}
	.r();
}

# Tyler-replication -----
# Create example projects, and load data
s1 <- S_new(townships=c("Hyde Park"));

# Wrapper function to run all 'Tyler replicated' stuffs
compile <- function(scope) {
	Sdat <- DT_clip(scope);
	Sdat <- DT_createVariables(Sdat);
	Sdat <- DT_createLOCF(Sdat);

	Smod <- M_runBaseline(Sdat);
	Smod <- M_diagnostics(Smod);
	return(Smod);
}

S1 <- compile(s1);
rm(s1);
#s2 <- S_new(townships=c("Rogers Park","Jefferson"));	S2 <- compile(s2);
#s3 <- S_new(townships=c("Lakeview"));					S3 <- compile(s3);
#s4 <- S_new(townships=c("Hyde Park"));				S4 <- compile(s4);

# Sandbox ----
# For ongoing model development, below is a sandbox.

# Rip out Rogers Park data
modeldat <- S1$modeldat;
if (TRUE) {
	library(gbm);
	# Some variables need to be interpretable
	modeldat$rooms <- as.numeric(as.character(modeldat$ROOMS));
	modeldat$USE <- factor(modeldat$use);

	# Flatten some characteristics which might be useful
	# THESE ARE GUESSES!!! DON'T KNOW WHAT THE CODES MEAN EXACTLY!!!
	modeldat$NUM6 <- as.numeric(modeldat$NUM==6);
	modeldat$basment1 <- as.numeric(modeldat$basment==1);
	modeldat$ceiling2 <- as.numeric(modeldat$ceiling==2);
	modeldat$garNOT7 <- as.numeric(modeldat$gar!=7);
	
	# Remove more "outliers"
	modeldat <- subset(modeldat,saleamt>1000 & saleamt<1e6);
		modeldat <- subset(modeldat,saleamt/rooms>5000);
		#modeldat <- subset(modeldat,pmax(amount1/saleamt,saleamt/amount1)<2);	# WHY IS THERE A DISCREPANCY BETWEEN THESE TWO 'AMOUNT' VARIABLES?
		
	form_1 <- log(saleamt)~log(BSF)+log(LSF)+log(FULLBATH)+log(1+halfbath)+
				rqos+I(rqos^2)+
				rooms+NUM6+EXTCON+FIREPL+log(LOCF)+
				bedrooms+basment1+heat+aircond+ceiling2+garNOT7;
	linear_f1 <- lm(form_1,data=modeldat);
	tree_f1 <- gbm(form_1,distribution="gaussian",interaction.depth=2,n.trees=10000,data=modeldat);
		ntree <- gbm.perf(tree_f1,plot.it=FALSE);
	
	# Apply predicted values
	modeldat$linear_f1 <- exp(linear_f1$fitted.values);
	modeldat$tree_f1 <- exp(predict(tree_f1,newdata=modeldat,n.trees=ntree));
	modeldat$weighted_w1_f1 <- (modeldat$linear_f1+modeldat$tree_f1)/2;
	
	# Sort by what's really off
		modeldat$eratio <- modeldat$linear_f1/modeldat$saleamt-1;
		modeldat <- modeldat[order(-abs(modeldat$eratio)),];
	
	out <- M_diagnostics(modeldat,c("linear_f1","tree_f1","weighted_w1_f1"));
}
	
# Create a sandbox object to access diagnostic functions
test <- S_sandbox(S1,
			newmodeldat=modeldat,
			newmodelobjects=list(linear_f1=linear_f1,tree_f1=tree_f1,weighted_w1_f1=NA),
			newdiagnostics=out);

# Example outputs
if (FALSE) {
	S1;				# print out full object
	plot(S1);		# Menu-based way to access diagnostics
	V_stddiagnostics(S1);	# Within menu
	
	# Direct access to plots
	V_LOCF(S1,drawparcels=TRUE);
	V_stddiagnostics(S1,modelname="MM",panels=3);
	V_stddiagnostics(S1,modelname="MM",panels=c(1,3));

	V_spatialresid(S1,modelname="COMPS",drawparcels=TRUE);
	V_pinreport(S1,pin=10253010300000);
}
save(dirs,.r,S1,file="codebase_v01_build_S1.Rdata");
load("codebase_v01_build_S1.Rdata")
V_stddiagnostics(test);	# Within menu

V_stddiagnostics(S1);	# Within menu

