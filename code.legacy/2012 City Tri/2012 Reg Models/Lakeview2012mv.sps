                *SPSS Regression.
		*LAKEVIEW AND NORTH REGRESSION  2012.

Get file='C:\Program Files\IBM\SPSS\statistics\19\regt73andregt74mergefcl2.sav'.

*select if (amount1>250000).
*select if (amount1<3000000).
*select if (multi<1).
*select if sqftb<9000.

Compute year1=0.
If  (amount1>0) year1=1900 + yr1. 
If  (yr1=11 and amount1>0) year1=2011.
If  (yr1=10 and amount1>0) year1=2010.
If  (yr1=9 and amount1>0)  year1=2009.
If  (yr1=8 and amount1>0)  year1=2008.
If  (yr1=7 and amount1>0)  year1=2007.  
If  (yr1=6 and amount1>0)  year1=2006. 
If  (yr1=5 and amount1>0)  year1=2005.
If  (yr1=4 and amount1>0)  year1=2004.  
If  (yr1=3 and amount1>0)  year1=2003. 
If  (yr1=2 and amount1>0)  year1=2002.  
If  (yr1=1 and amount1>0)  year1=2001.   
If  (yr1=0 and amount1>0)  year1=2000.

 *NEW - ONLY USE 5 YEARS OF SALES.                                           
*select if (year1>2006). 

                                              
*NEW - ONLY SELECT PURE SALES(CREATE PURESALE VARIABLE IN REGT).                                        
*Select if puremarket=1.                                               

Compute bs=0.
*if age<10 and (AMOUNT1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.
if (pin=	14061080050000
or pin=	14072100050000
or pin=	14072270150000
or pin=	14073060280000
or pin=	14073060380000
or pin=	14073140280000
or pin=	14051010190000
or pin=	14051030220000
or pin=	14052130170000
or pin=	14053260030000
or pin=	14064110040000
or pin=	14072090270000
or pin=	14074220060000
or pin=	14074220400000
or pin=	14083050410000
or pin=	14083080370000
or pin=	14083100060000
or pin=	14171130140000
or pin=	14171140160000
or pin=	14171200020000
or pin=	14182050150000
or pin=	14182050270000
or pin=	14182050290000
or pin=	14184200240000
or pin=	14081100280000
or pin=	14181210350000
or pin=	14181220160000
or pin=	14181230200000
or pin=	14183020100000
or pin=	14183090130000
or pin=	14183150360000
or pin=	14183160340000
or pin=	14183180170000
or pin=	14183260180000
or pin=	14183260300000
or pin=	14181270050000
or pin=	14181310170000
or pin=	14181320080000
or pin=	14181340180000
or pin=	14181340280000
or pin=	14183050240000
or pin=	14183130210000
or pin=	14184060380000
or pin=	14184120050000
or pin=	14184160290000
or pin=	14184190240000
or pin=	14184230070000
or pin=	14173020240000
or pin=	14173050010000
or pin=	14173100170000
or pin=	14171160050000
or pin=	14172280050000
or pin=	14281120280000
or pin=	14283090070000
or pin=	14283110040000
or pin=	14283110050000
or pin=	14283120400000
or pin=	14283150310000
or pin=	14283210280000
or pin=	14283210290000
or pin=	14191100050000
or pin=	14191120280000
or pin=	14191200070000
or pin=	14191200110000
or pin=	14191200180000
or pin=	14191240140000
or pin=	14193000060000
or pin=	14193010020000
or pin=	14193090390000
or pin=	14193150030000
or pin=	14193180320000
or pin=	14193190240000
or pin=	14193240180000
or pin=	14193310160000
or pin=	14194010280000
or pin=	14194100140000
or pin=	14194110230000
or pin=	14194180150000
or pin=	14194180240000
or pin=	14194190160000
or pin=	14192040080000
or pin=	14192270120000
or pin=	14194160060000
or pin=	14194340420000
or pin=	14302050090000
or pin=	14302060150000
or pin=	14302060280000
or pin=	14302080160000
or pin=	14302110330000
or pin=	14302110510000
or pin=	14302130150000
or pin=	14302200100000
or pin=	14304070120000
or pin=	14304090250000
or pin=	14201010200000
or pin=	14201010380000
or pin=	14201110220000
or pin=	14201130050000
or pin=	14201210190000
or pin=	14201230380000
or pin=	14201240310000
or pin=	14202140080000
or pin=	14202290310000
or pin=	14203110040000
or pin=	14203110100000
or pin=	14203160260000
or pin=	14203180390000
or pin=	14203190330000
or pin=	14203210280000
or pin=	14203210290000
or pin=	14203230040000
or pin=	14203230070000
or pin=	14203230100000
or pin=	14203230360000
or pin=	14203250310000
or pin=	14203260130000
or pin=	14203260340000
or pin=	14203290380000
or pin=	14204130530000
or pin=	14293000160000
or pin=	14293000900000
or pin=	14293120110000
or pin=	14293150440000
or pin=	14293160010000
or pin=	14293170050000
or pin=	14293170070000
or pin=	14293180360000
or pin=	14293210180000
or pin=	14294210380000
or pin=	14301090030000
or pin=	14301200270000
or pin=	14291050200000
or pin=	14291120270000
or pin=	14291120320000
or pin=	14291140090000
or pin=	14291140190000
or pin=	14281190130000
or pin=	14291150140000
or pin=	14291230150000
or pin=	14292080320000
or pin=	14292150260000
or pin=	14292160110000
or pin=	14302220310000
or pin=	17041210400000
or pin=	17041210440000
or pin=	17042191350000
or pin=	14321010500000
or pin=	14321010510000
or pin=	14321020230000
or pin=	14321020330000
or pin=	14321030190000
or pin=	14321040270000
or pin=	14321070420000
or pin=	14321070450000
or pin=	14321080330000
or pin=	14321090010000
or pin=	14321270160000
or pin=	14322140110000
or pin=	14322210060000
or pin=	14324080140000
or pin=	14324120460000
or pin=	14324130120000
or pin=	14324130160000
or pin=	14324220180000
or pin=	14331040820000
or pin=	14331210830000
or pin=	14331300410000
or pin=	14331300450000
or pin=	14333080530000
or pin=	14333090060000
or pin=	14333280130000
or pin=	14334010280000
or pin=	17031030020000
or pin=	17032020320000
or pin=	17042170380000
or pin=	17042170390000
or pin=	17102210620000)    bs=1.
*select if bs=0.

compute bsf=sqftb.

Compute N=1.

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.

compute bsf=sqftb.

compute bsf02=0.
compute bsf03=0.
compute bsf04=0.
compute bsf05=0.
compute bsf06=0.
compute bsf07=0.
compute bsf08=0.
compute bsf09=0.
compute bsf10=0.
compute bsf11=0.
compute bsf12=0.
compute bsf34=0.
compute bsf78=0.
compute bsf95=0.

if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.


compute bsf02=bsf*class02.
compute bsf03=bsf*class03.
compute bsf04=bsf*class04.
compute bsf05=bsf*class05.
compute bsf06=bsf*class06.
compute bsf07=bsf*class07.
compute bsf08=bsf*class08.
compute bsf09=bsf*class09.
Compute bsf10=bsf*class10.
compute bsf11=bsf*class11.
compute bsf12=bsf*class12.
compute bsf34=bsf*class34.
compute bsf78=bsf*class78.
compute bsf95=bsf*class95.

compute tnb=(town*1000) + nghcde.

if (tnb= 74011 ) n=6.50.
if (tnb= 74012 ) n=9.50.
if (tnb= 74013 ) n=4.50.
if (tnb= 74022 ) n=9.85.
if (tnb= 74030 ) n=10.60.
if (tnb= 73011 ) n=3.88.
if (tnb= 73012 ) n=4.62.
if (tnb= 73022 ) n=4.55.
if (tnb= 73031 ) n=5.10.
if (tnb= 73032 ) n=5.95.
if (tnb= 73034 ) n=7.30.
if (tnb= 73041 ) n=5.38.
if (tnb= 73042 ) n=5.40.
if (tnb= 73044 ) n=5.98.
if (tnb= 73050 ) n=4.48.
if (tnb= 73060 ) n=6.88.
if (tnb= 73062 ) n=7.92.
if (tnb= 73063 ) n=10.32.
if (tnb= 73070 ) n=6.15.
if (tnb= 73081 ) n=6.54.
if (tnb= 73084 ) n=7.15.
if (tnb= 73092 ) n=9.30.
if (tnb= 73093 ) n=7.25.
if (tnb= 73110 ) n=5.55.
if (tnb= 73120 ) n=5.81.
if (tnb= 73150 ) n=7.55.
if (tnb= 73200 ) n=5.52.



Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

compute cl10=0.
if class=10 cl10=1.
Compute nbsf10=n*cl10*sqrt(bsf).
compute cl95=0.
if class=95 cl95=1.
Compute nbsf95=n*cl95*sqrt(bsf).

compute cl34=0.
if class=34 cl34=1.
Compute nbsf34=n*cl34*sqrt(bsf).  

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).


*CREATE NEW TOWN AND NEIGHBORHOOD VARIABLES(FOR BUILDING SQUARE FOOT). 
compute sb74011=0.                              
compute sb74012=0.
compute sb74013=0.
compute sb74022=0.
compute sb74030=0.
compute sb73011=0.
compute sb73012=0.
compute sb73022=0.
compute sb73031=0.
compute sb73032=0.
compute sb73034=0.
compute sb73041=0.
compute sb73042=0.
compute sb73044=0.
compute sb73050=0.
compute sb73060=0.
compute sb73062=0.
compute sb73063=0.
compute sb73070=0.
compute sb73081=0.
compute sb73084=0.
compute sb73092=0.
compute sb73093=0.
compute sb73110=0.
compute sb73120=0.
compute sb73150=0.
compute sb73200=0.

compute n74011=0.
compute n74012=0.
compute n74013=0.
compute n74022=0.
compute n74030=0.
compute n73011=0.
compute n73012=0.
compute n73022=0.
compute n73031=0.
compute n73032=0.
compute n73034=0.
compute n73041=0.
compute n73042=0.
compute n73044=0.
compute n73050=0.
compute n73060=0.
compute n73062=0.
compute n73063=0.
compute n73070=0.
compute n73081=0.
compute n73084=0.
compute n73092=0.
compute n73093=0.
compute n73110=0.
compute n73120=0.
compute n73150=0.
compute n73200=0.

if (tnb= 74011 ) n74011=1.                
if (tnb= 74012 ) n74012=1.
if (tnb= 74013 ) n74013=1.
if (tnb= 74022 ) n74022=1.
if (tnb= 74030 ) n74030=1.
if (tnb= 73011 ) n73011=1.
if (tnb= 73012 ) n73012=1.
if (tnb= 73022 ) n73022=1.
if (tnb= 73031 ) n73031=1.
if (tnb= 73032 ) n73032=1.
if (tnb= 73034 ) n73034=1.
if (tnb= 73041 ) n73041=1.
if (tnb= 73042 ) n73042=1.
if (tnb= 73044 ) n73044=1.
if (tnb= 73050 ) n73050=1.
if (tnb= 73060 ) n73060=1.
if (tnb= 73062 ) n73062=1.
if (tnb= 73063 ) n73063=1.
if (tnb= 73070 ) n73070=1.
if (tnb= 73081 ) n73081=1.
if (tnb= 73084 ) n73084=1.
if (tnb= 73092 ) n73092=1.
if (tnb= 73093 ) n73093=1.
if (tnb= 73110 ) n73110=1.
if (tnb= 73120 ) n73120=1.
if (tnb= 73150 ) n73150=1.
if (tnb= 73200 ) n73200=1.


*NEW - CREATE TOWN AND NEIGHBORHOOD COMBINATION VARIABLES FOR SQRT*BUILDING SQUARE FOOT.
if (tnb= 74011 ) sb74011=sqrt(bsf).              
if (tnb= 74012 ) sb74012=sqrt(bsf).
if (tnb= 74013 ) sb74013=sqrt(bsf).
if (tnb= 74022 ) sb74022=sqrt(bsf).
if (tnb= 74030 ) sb74030=sqrt(bsf).
if (tnb= 73011 ) sb73011=sqrt(bsf).
if (tnb= 73012 ) sb73012=sqrt(bsf).
if (tnb= 73022 ) sb73022=sqrt(bsf).
if (tnb= 73031 ) sb73031=sqrt(bsf).
if (tnb= 73032 ) sb73032=sqrt(bsf).
if (tnb= 73034 ) sb73034=sqrt(bsf).
if (tnb= 73041 ) sb73041=sqrt(bsf).
if (tnb= 73042 ) sb73042=sqrt(bsf).
if (tnb= 73044 ) sb73044=sqrt(bsf).
if (tnb= 73050 ) sb73050=sqrt(bsf).
if (tnb= 73060 ) sb73060=sqrt(bsf).
if (tnb= 73062 ) sb73062=sqrt(bsf).
if (tnb= 73063 ) sb73063=sqrt(bsf).
if (tnb= 73070 ) sb73070=sqrt(bsf).
if (tnb= 73081 ) sb73081=sqrt(bsf).
if (tnb= 73084 ) sb73084=sqrt(bsf).
if (tnb= 73092 ) sb73092=sqrt(bsf).
if (tnb= 73093 ) sb73093=sqrt(bsf).
if (tnb= 73110 ) sb73110=sqrt(bsf).
if (tnb= 73120 ) sb73120=sqrt(bsf).
if (tnb= 73150 ) sb73150=sqrt(bsf).
if (tnb= 73200 ) sb73200=sqrt(bsf).

*******************************************************************************************************************.

***** Compute Block-Level Filings.
* There are two variations of this variable created below:.
* 1) FX includes the own property when counting "block-level filings".
* 2) FX2 excludes the own property when counting "block-level filings".
* Either variable can be included one at a time in the regression; they should not both be included in the same regression.
* Either variable yields similar regression results.
*****.
*********** We have combined the filings so that we are now using 2006, 2007 and 2008 block filings.COMPUTE FX = cumfile789.

COMPUTE FX = cumfile789101112.
RECODE FX (SYSMIS=0).


********************************************************************************************.
************* The next section computes low , mid and high foreclosure zones  for Lakeview.
********************************************************************************************.

compute midzonelakeview=0.
if town=73 and (nghcde=11 or nghcde=110) midzonelakeview=1.

compute lowzonelakeview=0.
if town=73 and (midzonelakeview=0) lowzonelakeview=1.


compute fxlowblocklakeview=0.
if lowzonelakeview=1 fxlowblocklakeview=fx*lowzonelakeview.

compute fxmidblocklakeview=0.
if midzonelakeview=1  fxmidblocklakeview=fx*midzonelakeview.


*******************************************************************************************.
*************** This section computes low, mid and high foreclosure zones for North.
*******************************************************************************************.


compute lowzonenorth=0.
if town=74 and (nghcde=11 or nghcde=12 or nghcde=22 or nghcde=30) lowzonenorth=1.

compute midzonenorth=0.
if town=74 and (nghcde=13)  midzonenorth=1.

 
compute fxlowblocknorth=0.
if lowzonenorth=1 fxlowblocknorth=fx*lowzonenorth.

compute fxmidblocknorth=0.
if midzonenorth=1 fxmidblocknorth=fx*midzonenorth.


*******************************************************************************************************************.
******.


*NEW - CREATE NEW  TIME DUMMY VARIABLES BROKEN DOWN INTO 6 MONTH TIME PERIODS, BY TIME OF YEAR(SUMMER AND WINTER) FOR LAST 5 YEARS.   
Compute winter0708=0.
if (mos > 9 and yr=7) or (mos <= 3 and yr=8) winter0708=1.
Compute winter0809=0.
if (mos > 9 and yr=8) or (mos <= 3 and yr=9) winter0809=1.
Compute winter0910=0.
if (mos > 9 and yr=9) or (mos <= 3 and yr=10) winter0910=1.
Compute winter1011=0.
if (mos > 9 and yr=10) or (mos <= 3 and yr=11) winter1011=1.
Compute summer07=0.
if (mos > 3 and yr=7) and (mos <= 9 and yr=7) summer07=1.
Compute summer08=0.
if (mos > 3 and yr=8) and (mos <= 9 and yr=8) summer08=1. 
Compute summer09=0.
if (mos > 3 and yr=9) and (mos <= 9 and yr=9) summer09=1.
Compute summer10=0.
if (mos > 3 and yr=10) and (mos <= 9 and yr=10) summer10=1.
Compute summer11=0.
if (mos > 3 and yr=11) and (mos <= 9 and yr=11) summer11=1.
Compute jantmar07=0.
if (year1=2007 and (mos>=1 and mos<=3)) jantmar07=1. 
Compute octtdec11=0.
if (year1=2011 and (mos>=10 and mos<=12)) octtdec11=1.


Compute jantmar07cl234=jantmar07*cl234.
Compute winter0708cl234=winter0708*cl234.
Compute winter0809cl234=winter0809*cl234.
Compute winter0910cl234=winter0910*cl234.
Compute winter1011cl234=winter1011*cl234.
Compute summer07cl234=summer07*cl234.
Compute summer08cl234=summer08*cl234.
Compute summer09cl234=summer09*cl234.
Compute summer10cl234=summer10*cl234.
Compute summer11cl234=summer11*cl234.
Compute octtdec11cl234=octtdec11*cl234.

Compute jantmar07cl56=jantmar07*cl56.
Compute winter0708cl56=winter0708*cl56.
Compute winter0809cl56=winter0809*cl56.
Compute winter0910cl56=winter0910*cl56.
Compute winter1011cl56=winter1011*cl56.
Compute summer07cl56=summer07*cl56.
Compute summer08cl56=summer08*cl56.
Compute summer09cl56=summer09*cl56.
Compute summer10cl56=summer10*cl56.
Compute summer11cl56=summer11*cl56.
Compute octtdec11cl56=octtdec11*cl56.

Compute jantmar07cl778=jantmar07*cl778.
Compute winter0708cl778=winter0708*cl778.
Compute winter0809cl778=winter0809*cl778.
Compute winter0910cl778=winter0910*cl778.
Compute winter1011cl778=winter1011*cl778.
Compute summer07cl778=summer07*cl778.
Compute summer08cl778=summer08*cl778.
Compute summer09cl778=summer09*cl778.
Compute summer10cl778=summer10*cl778.
Compute summer11cl778=summer11*cl778.
Compute octtdec11cl778=octtdec11*cl778.

Compute jantmar07cl89=jantmar07*cl89.
Compute winter0708cl89=winter0708*cl89.
Compute winter0809cl89=winter0809*cl89.
Compute winter0910cl89=winter0910*cl89.
Compute winter1011cl89=winter1011*cl89.
Compute summer07cl89=summer07*cl89.
Compute summer08cl89=summer08*cl89.
Compute summer09cl89=summer09*cl89.
Compute summer10cl89=summer10*cl89.
Compute summer11cl89=summer11*cl89.
Compute octtdec11cl89=octtdec11*cl89.


Compute jantmar07cl1112=jantmar07*cl1112.
Compute winter0708cl1112=winter0708*cl1112.
Compute winter0809cl1112=winter0809*cl1112.
Compute winter0910cl1112=winter0910*cl1112.
Compute winter1011cl1112=winter1011*cl1112.
Compute summer07cl1112=summer07*cl1112.
Compute summer08cl1112=summer08*cl1112.
Compute summer09cl1112=summer09*cl1112.
Compute summer10cl1112=summer10*cl1112.
Compute summer11cl1112=summer11*cl1112.
Compute octtdec11cl1112=octtdec11*cl1112.


Compute jantmar07cl1095=jantmar07*cl1095.
Compute winter0708cl1095=winter0708*cl1095.
Compute winter0809cl1095=winter0809*cl1095.
Compute winter0910cl1095=winter0910*cl1095.
Compute winter1011cl1095=winter1011*cl1095.
Compute summer07cl1095=summer07*cl1095.
Compute summer08cl1095=summer08*cl1095.
Compute summer09cl1095=summer09*cl1095.
Compute summer10cl1095=summer10*cl1095.
Compute summer11cl1095=summer11*cl1095.
Compute octtdec11cl1095=octtdec11*cl1095.

Compute jantmar07clsplt=jantmar07*cl34.
Compute winter0708clsplt=winter0708*cl34.
Compute winter0809clsplt=winter0809*cl34.
Compute winter0910clsplt=winter0910*cl34.
Compute winter1011clsplt=winter1011*cl34.
Compute summer07clsplt=summer07*cl34.
Compute summer08clsplt=summer08*cl34.
Compute summer09clsplt=summer09*cl34.
Compute summer10clsplt=summer10*cl34.
Compute summer11clsplt=summer11*cl34.
Compute octtdec11clsplt=octtdec11*cl34.


Compute cathdral=0.
If ceiling=1 cathdral=1.

Compute LV=0.
If town=73 LV=1.
Compute NO=0.
If town=74 NO=1.

If sqftl >= 3250  sqftl=3250. 
Compute lsf=sqftl.
Compute nlsf=n*lsf.
Compute lvlsf=lv*sqrt(lsf).
compute nolsf=no*sqrt(lsf).

if tnb=73011 and class=95   lsf=1373.
if tnb=73012 and class=95   lsf=1213.
if tnb=73031 and class=95   lsf=1070.
if tnb=73032 and class=95   lsf=1339.
if tnb=73042 and class=95   lsf=1091.
if tnb=73044 and class=95   lsf=1048.
if tnb=73050 and class=95   lsf=1238.
if tnb=73062 and class=95   lsf=1804.
if tnb=73081 and class=95   lsf=1182.
if tnb=73084 and class=95   lsf=982.
if tnb=73093 and class=95   lsf=1187.
if tnb=73120 and class=95   lsf=1028.
if tnb=73150 and class=95   lsf=1368.
if tnb=73200 and class=95   lsf=1260.
if tnb=74011 and class=95   lsf=1167.
if tnb=74012 and class=95   lsf=1137.
if tnb=74013 and class=95   lsf=891.
if tnb=74022 and class=95   lsf=1362.
if tnb=74030 and class=95   lsf=1231.

Compute srbsf=sqrt(bsf).
Compute nsrbsf=n*sqrt(bsf).
Compute nbsf=n*bsf.


Compute srage=sqrt(age).
Compute nsrage=n*srage.
Compute bsfage=bsf*age.
Compute srbsfage=srbsf*srage.

Compute nclass95=n*class95.
Compute nclass10=n*class10.
Compute no95=0.
If class95=0 no95=1.

Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.

Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute frabsf=frame*bsf.
compute masbsf=mason*bsf.
Compute stubsf=stucco*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.
Compute bathsum=fullbath + 0.25*halfbath.



Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.
Compute prembsf=premrf*bsf.

If firepl>=2 firepl=2.
compute nfirepl=0.
compute nfirepl=n*firepl.

Compute gar1car=0.
Compute gar1hcar=0.
Compute gar2car=0.
Compute gar2hcar=0.
Compute gar3car=0.
Compute gar3hcar=0.
Compute gar4car=0.	

Compute garnogar=0.
Compute garage=0.
If gar=1 gar1car=1.
If gar=2 gar1hcar=1.
If gar=3 gar2car=1.
If gar=4 gar2hcar=1.
If gar=5 gar3car=1.
If gar=6 gar3hcar=1.
If gar=7 garnogar=1.
If gar=8 gar4car=1. 
If gar ne 7 garage=1.
compute garage1=0.
if gar=1 or gar=2 garage1=1.
compute garage2=0.
if gar=3 or gar=4 garage2=1.
compute biggar=0.
if gar=5 or gar=6 or gar=8 biggar=1.

Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
Compute basement=0.
If basment=1 or basment=2 basement=1.
Compute nbasfull=n*basefull.
Compute nbaspart=n*basepart.
Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 and (class=2 or class=3 or class=4 or class=5 or class=6 or class=7 or class=8 or class=9 or class=10 or class=34
or class=78 or class=95)  bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.
Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
Compute nnobase=n*nobase.


Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
Compute planarch=0.
Compute planstoc=0.
If plan=1 planarch=1.
If plan=2 planstoc=1.
Compute archbsf=planarch*bsf.
Compute narchbsf=n*planarch*bsf.
Compute deluxbsf=qualdlux*bsf.
Compute poorbsf=qualpoor*bsf.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute sitedbsf=sitedetr*bsf.

Compute repabove=0.
Compute repbelow=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.

Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.


Compute noapt=0.
Compute apt=class11 + class12.
if apt<1 noapt=1.

compute nsrbsfs=nsrbsf*noapt.
compute napsrbsf=nsrbsf*apt.

compute srlsf=sqrt(lsf).
compute nsrlsf=n*srlsf.

compute nage=n*age.
compute nsrage=n*srage.


compute rs1519sf=0.
if sqftb>1200 rs1519sf=rs1519*sqftb.
compute nmasbsf=n*masbsf.
compute aptmas=masbsf*apt.
compute frmsbsf=sqftb*framas.

compute lfrno=0.
if(14332000000000<pin and pin<14332999999999) or (14334000000000 <pin and pin<14334999999999) or 
(17031000000000<pin and pin <17033999999999) lfrno=1.
compute lake=0.
if ((14332000000000<pin and pin<14332999999999) or (14334000000000 <pin and pin<14334999999999) or 
  tnb=73062 or tnb= 73063 or tnb=74030 or tnb=74022) lake=1.
compute good95=0.
if ((14331230520000<pin and pin<14331230570000) or (14331240380000 <pin and pin<14331240490000) or 
   (17044240270000<pin and pin<14044240490000) or (17044310240000 <pin and pin<17044310270000)) good95=1.

compute lakel=lake*lsf.
compute lakesrl=lake*srlsf.
compute aptlakel=apt*lake*lsf.
compute aptnlsf=nlsf*apt.
compute aptlux=apt*nlux.
compute aptlsrl=apt*lake*srlsf.

compute lsf95=class95*lsf.
compute nlsf95=n*lsf95.
compute lux95=nlux*class95.
compute good95b=good95*srbsf.

compute nfulbath=n*fullbath.
compute nhafbath=n*halfbath.

compute nrenbsf=n*renbsf.
compute nbathsum=n*bathsum.

compute singlef=0.
if class11=0 and class12=0 singlef=1.
compute nbsfs=nbsf*singlef.

If num=6 bnum=0.
If num=0 bnum=0.
If num=1 bnum=2.
If num=2 bnum=3.
If num=3 bnum=4.
If num=4 bnum=5.
If num=5 bnum=6.
Compute nbnum=n*bnum.
Compute ncomm=n*comm.
Compute totunit=bnum + comm.
Compute totuntb=totunit*sqrt(bsf).

select if town=73.

compute mv =-363005.0789	
+ 6921.068041*nbathsum
+ 3268.04405*nbsf778
+ 1232.648903*nbsf89
+ 2096.512102*nbsf56
- 7.948287854*masbsf
-  21750.43122*nbasfull
+ 5421.035*sb74022
+ 2.634324223*nbsfair
- 24734.2998*nbaspart
- 4055.324285*sb74030
+ 2347.34562*nbsf234
+ 123155.1171*biggar
+ 23780.16687*garage2
+ 11863.09459*nolsf
+ 10978.73453*lvlsf
+ 1697.860556*nbsf1095
+ 1788.711228*nbsf1112
- 48351.81584*fxlowblocknorth
- 4986.167884*sb73092
+ 1123.780564*sb73120
- 1741.960004*sb73081
+ 3183.914594*nfirepl
- 4860.2663*sb73062
- 2325.344962*sb73150
+ 4.18932321*nrenbsf
+ 8577.085549*npremrf
- 46889.1187*	lowzonenorth
+ 1543.000633*sb73044
+ 2850.35442*sb74011
- 1367.582983*sb73031
- 105.1651211*srbsfage
- 2371.809485*sb73034
- 2977.789698*sb73060
- 20.3627561*frmsbsf
- 1558.330113*sb73032
- 1166.607499*sb73070
- 802.6600944*sb73084
- 9744.212993*fxlowblocklakeview
- 99868.27319*midzonelakeview
+ 1154.905602*sb73200
+ 1299.128169*nbsf34
- 21048.59391*garage1
- 36850.6279*nnobase
- 17.8689354*stubsf
- 32.93901468*frastbsf
+ 15194.37413*nsiteben
- 662.6758226*sb73012
+  6817.74359*fxmidblocklakeview
+ 302550.5285*midzonenorth
- 24713.9863*fxmidblocknorth.

  
