
# This example comes to you from https://rpubs.com/nabilabd/118172
# Thanks Nabile A.

rm(list=ls())

# Controls ----
# Who is running this code?
user <- "rross"

# What packages will you need?
libs <- c("DBI", "odbc","dplyr","sqldf", "rgdal", "sf", "stringr", "xlsx", "maptools","sp","foreign","fields", "mgcv", "car","RPostgreSQL"
          , "plyr","dplyr", "DBI", "RODBC", "RStata", "lattice", "RColorBrewer", 
          "ggplot2", "scales", "zoo", "leaflet", "htmlwidgets", "htmltools", "sjPlot", "gdata"
          ,"gbm", "progress", "knitr", "kableExtra","Hmisc", "quantreg","randomForest","geosphere", "ape", "SpatialNP", "spdep", "fts"
          , "nnet", "systemfit", "foreign", "Rcpp", "gstat", "gridExtra")

# Where are your files stored?
wd <- paste0("C:/Users/",user,"/Documents/CCAO_SF_CAMA_DEV")
setwd(wd)
common.drive <- "O:/CCAODATA"
dirs <- list(code=paste0(wd,"/code.r/")
             , data=paste0(common.drive,"/data/")
             , results=paste0(common.drive, "/results/")
             , documentation=paste0(common.drive, "/documentation/")
             , results_sandbox=paste0(common.drive, "/results/sandbox/")
             , results_desk_review=paste0(common.drive, "/output_data/desktop_review/")
             , results_modeling=paste0(common.drive, "/results/modeling/")
             , results_ad_hoc=paste0(common.drive, "/results/ad_hoc/")
             , spatial_data=paste0(common.drive,"/data/spatial/")
             , parcels=paste0(common.drive,"/data/spatial/", "2018_parcels.shp")
             , raw_parcels=paste0(common.drive,"/data/spatial/", "TY2018_DefaultParcels.shp")
             , raw_tracts=paste0(common.drive,"/data/spatial/", "tl_2018_17_tract.shp")
             , bad_data=paste0(common.drive,"/data/bad_data/")
             , raw_data=paste0(common.drive,"/data/raw_data/"))

rm(common.drive, wd)
options(scipen=999)
# Load utilities
source(paste0(dirs$code, "99_utility_2.r"))
# Refresh packages
check.packages(libs)
# Open ODBC connection (you stil have to manually connect)
pw <-"generalUser"
odbc.connect()

# Example data - zinc in the meuse river
data(meuse)
glimpse(meuse)

?meuse

meuse %>% as.data.frame %>% 
  ggplot(aes(x, y)) + geom_point(aes(size=zinc), color="blue", alpha=3/4) + 
  ggtitle("Zinc Concentration (ppm)") + coord_equal() + theme_bw()

# Step 1 - Convert to an SPDF
class(meuse)
coordinates(meuse) <- ~ x + y
class(meuse)


# Step 2 - Fit a variogram
lzn.vgm <- variogram(log(zinc)~1, meuse) # calculates sample variogram values 
lzn.fit <- fit.variogram(lzn.vgm, model=vgm(1, "Sph", 900, 1)) # fit model

# Step 2.5 - 

data("meuse.grid")
?meuse.grid

# Step 3 - Krig

data("meuse.grid")


