                *SPSS Regression.
		*LAKEVIEW AND NORTH REGRESSION  2009.

Get file='C:\Program Files\SPSS\spssa\regt73andregt74mergefcl2.sav'.

select if (amount>250000).
select if (amount<3000000).
select if (multi<1).
select if sqftb<9000.

Compute year1=0.
If  (amount>0) year1=1900 + yr. 
If  (yr=8 and amount>0)  year1=2008.
If  (yr=7 and amount>0)  year1=2007.  
If  (yr=6 and amount>0)  year1=2006. 
If  (yr=5 and amount>0)  year1=2005.
If  (yr=4 and amount>0)  year1=2004.  
If  (yr=3 and amount>0)  year1=2003. 
If  (yr=2 and amount>0)  year1=2002.  
If  (yr=1 and amount>0)  year1=2001.   
If  (yr=0 and amount>0)  year1=2000.

*Compute aos=12*(2008-year1) + (13-mos).

 *NEW - ONLY USE 5 YEARS OF SALES.                                           
select if (year1>2003). 
                                               
*NEW - ONLY SELECT PURE SALES(CREATE PURESALE VARIABLE IN REGT).                                        
Select if puresale=1 or (yr=4 or yr=5).                                               

Compute bs=0.
if age<10 and (AMOUNT<1600000 and (amount/sqftb)<75 and class<95) bs=1.
if(pin=	14061150180000
or pin=	14062160280000
or pin=	14071130110000
or pin=	14071140280000
or pin=	14073010160000
or pin=	14073040310000
or pin=	14073060280000
or pin=	14073060380000
or pin=	14073140280000
or pin=	14073170310000
or pin=	14074060030000
or pin=	14051010280000
or pin=	14051170200000
or pin=	14052140040000
or pin=	14053150380000
or pin=	14054000190000
or pin=	14072130210000
or pin=	14081150140000
or pin=	14074210070000
or pin=	14074210080000
or pin=	14083010350000
or pin=	14083050410000
or pin=	14171070030000
or pin=	14171140160000
or pin=	14171190030000
or pin=	14171200020000
or pin=	14182050150000
or pin=	14182050270000
or pin=	14081270200000
or pin=	14181240350000
or pin=	14183000150000
or pin=	14183000290000
or pin=	14183040180000
or pin=	14183090300000
or pin=	14183110270000
or pin=	14183110280000
or pin=	14183150220000
or pin=	14183150430000
or pin=	14183160330000
or pin=	14183170140000
or pin=	14183170400000
or pin=	14183180040000
or pin=	14183180070000
or pin=	14183210320000
or pin=	14181050190000
or pin=	14181080190000
or pin=	14181090090000
or pin=	14181200110000
or pin=	14182190050000
or pin=	14182190090000
or pin=	14183140120000
or pin=	14184010130000
or pin=	14184060390000
or pin=	14184230070000
or pin=	14173040330000
or pin=	14173050010000
or pin=	14173100170000
or pin=	14174040120000
or pin=	14213010240000
or pin=	14213010270000
or pin=	14281030240000
or pin=	14281180350000
or pin=	14283090070000
or pin=	14283170040000
or pin=	14191010120000
or pin=	14191110110000
or pin=	14191190230000
or pin=	14191200070000
or pin=	14191200110000
or pin=	14191200270000
or pin=	14191200350000
or pin=	14191240140000
or pin=	14191310180000
or pin=	14193090300000
or pin=	14193110290000
or pin=	14193110340000
or pin=	14193130100000
or pin=	14193130190000
or pin=	14193140350000
or pin=	14193160280000
or pin=	14193170360000
or pin=	14193190200000
or pin=	14193190240000
or pin=	14193190260000
or pin=	14193190360000
or pin=	14193280160000
or pin=	14193310090000
or pin=	14194000330000
or pin=	14194030200000
or pin=	14194030220000
or pin=	14194090070000
or pin=	14194270190000
or pin=	14194280310000
or pin=	14194330340000
or pin=	14194330390000
or pin=	14192060190000
or pin=	14192080250000
or pin=	14192110260000
or pin=	14192120110000
or pin=	14192140250000
or pin=	14192140280000
or pin=	14192150340000
or pin=	14192210110000
or pin=	14192220200000
or pin=	14192310260000
or pin=	14194060030000
or pin=	14194080200000
or pin=	14194130010000
or pin=	14194290160000
or pin=	14302020170000
or pin=	14302050140000
or pin=	14302070270000
or pin=	14302070300000
or pin=	14302100210000
or pin=	14302100220000
or pin=	14302140230000
or pin=	14302150140000
or pin=	14302180090000
or pin=	14302180120000
or pin=	14302210170000
or pin=	14304040330000
or pin=	14304070110000
or pin=	14304090020000
or pin=	14304100050000
or pin=	14304100060000
or pin=	14201000390000
or pin=	14201010380000
or pin=	14201050100000
or pin=	14201090060000
or pin=	14201130050000
or pin=	14201190240000
or pin=	14201210190000
or pin=	14201220080000
or pin=	14201230380000
or pin=	14201260080000
or pin=	14202280150000
or pin=	14203020030000
or pin=	14203070210000
or pin=	14203110060000
or pin=	14203110100000
or pin=	14203180360000
or pin=	14203230040000
or pin=	14203230100000
or pin=	14203230160000
or pin=	14203240300000
or pin=	14204260390000
or pin=	14213040020000
or pin=	14213110340000
or pin=	14281040320000
or pin=	14281200140000
or pin=	14293000160000
or pin=	14293010480000
or pin=	14293010610000
or pin=	14293020320000
or pin=	14293023470000
or pin=	14293080400000
or pin=	14293150100000
or pin=	14293170050000
or pin=	14293190170000
or pin=	14294120020000
or pin=	14294160320000
or pin=	14294160770000
or pin=	14294210380000
or pin=	14301020300000
or pin=	14301030300000
or pin=	14301050150000
or pin=	14301060230000
or pin=	14301060650000
or pin=	14301110030000
or pin=	14301200170000
or pin=	14301200270000
or pin=	14291010220000
or pin=	14291050160000
or pin=	14291100110000
or pin=	14291120270000
or pin=	14291120320000
or pin=	14291160160000
or pin=	14291160290000
or pin=	14291210270000
or pin=	14291210400000
or pin=	14291240250000
or pin=	14291300100000
or pin=	14281190140000
or pin=	14291150140000
or pin=	14291190390000
or pin=	14291190400000
or pin=	14291230120000
or pin=	14292080150000
or pin=	14292080320000
or pin=	14292150260000
or pin=	14292160110000
or pin=	14302220310000
or pin=	14302231420000
or pin=	14302231470000
or pin=	14304031640000
or pin=	17041220100000
or pin=	17041430120000
or pin=	17042020280000
or pin=	17042200800000
or pin=	14321110160000
or pin=	14321260160000
or pin=	14322150380000
or pin=	14322200180000
or pin=	14322210170000
or pin=	14324030080000
or pin=	14324100550000
or pin=	14324220180000
or pin=	14324230440000
or pin=	17044360280000
or pin=	14331040820000
or pin=	14331070420000
or pin=	14331140130000
or pin=	14333031050000
or pin=	14333070330000
or pin=	14333180360000
or pin=	14333310230000
or pin=	14333310240000
or pin=	14334080200000
or pin=	17031030020000
or pin=	17032020320000
or pin=	17032230260000
or pin=	17042170380000
or pin=	17092030090000
or pin=	17092030140000
or pin=	17102210230000
or pin=	17102210270000
or pin=	17102210280000
or pin=	17102210300000
or pin=	17102210330000
or pin=	17102210510000
or pin=	17102210580000
or pin=	17102210610000
or pin=	17102210620000
or pin=	17102210630000)   bs=1.
select if bs=0.

compute bsf=sqftb.

Compute N=1.

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.

compute bsf=sqftb.

compute bsf02=0.
compute bsf03=0.
compute bsf04=0.
compute bsf05=0.
compute bsf06=0.
compute bsf07=0.
compute bsf08=0.
compute bsf09=0.
compute bsf10=0.
compute bsf11=0.
compute bsf12=0.
compute bsf34=0.
compute bsf78=0.
compute bsf95=0.

if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.




compute bsf02=bsf*class02.
compute bsf03=bsf*class03.
compute bsf04=bsf*class04.
compute bsf05=bsf*class05.
compute bsf06=bsf*class06.
compute bsf07=bsf*class07.
compute bsf08=bsf*class08.
compute bsf09=bsf*class09.
Compute bsf10=bsf*class10.
compute bsf11=bsf*class11.
compute bsf12=bsf*class12.
compute bsf34=bsf*class34.
compute bsf78=bsf*class78.
compute bsf95=bsf*class95.

compute tnb=(town*1000) + nghcde.

if (tnb= 74011 ) n=7.80.
if (tnb= 74012 ) n=11.00.
if (tnb= 74013 ) n=7.12.
if (tnb= 74022 ) n=10.25.
if (tnb= 74030 ) n=11.45.
if (tnb= 73011 ) n=4.50.
if (tnb= 73012 ) n=5.25.
if (tnb= 73022 ) n=5.25.
if (tnb= 73031 ) n=5.48.
if (tnb= 73032 ) n=5.95.
if (tnb= 73034 ) n=7.88.
if (tnb= 73041 ) n=6.90.
if (tnb= 73042 ) n=6.45.
if (tnb= 73044 ) n=7.40.
if (tnb= 73050 ) n=5.58.
if (tnb= 73060 ) n=8.80.
if (tnb= 73062 ) n=5.92.
if (tnb= 73063 ) n=10.58.
if (tnb= 73070 ) n=6.78.
if (tnb= 73081 ) n=7.92.
if (tnb= 73084 ) n=8.25.
if (tnb= 73092 ) n=10.32.
if (tnb= 73093 ) n=8.25.
if (tnb= 73110 ) n=6.35.
if (tnb= 73120 ) n=7.68.
if (tnb= 73150 ) n=7.60.
if (tnb= 73200 ) n=6.16.



Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).
compute cl10=0.
if class=10 cl10=1.
Compute nbsf10=n*cl10*sqrt(bsf).
compute cl95=0.
if class=95 cl95=1.
Compute nbsf95=n*cl95*sqrt(bsf).

compute clsplt=0.
if class=34 clsplt=1.
Compute nbsfclsplt=n*clsplt*sqrt(bsf).  

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).


*CREATE NEW TOWN AND NEIGHBORHOOD VARIABLES(FOR BUILDING SQUARE FOOT). 
compute sb74011=0.                              
compute sb74012=0.
compute sb74013=0.
compute sb74022=0.
compute sb74030=0.
compute sb73011=0.
compute sb73012=0.
compute sb73022=0.
compute sb73031=0.
compute sb73032=0.
compute sb73034=0.
compute sb73041=0.
compute sb73042=0.
compute sb73044=0.
compute sb73050=0.
compute sb73060=0.
compute sb73062=0.
compute sb73063=0.
compute sb73070=0.
compute sb73081=0.
compute sb73084=0.
compute sb73092=0.
compute sb73093=0.
compute sb73110=0.
compute sb73120=0.
compute sb73150=0.
compute sb73200=0.

compute n74011=0.
compute n74012=0.
compute n74013=0.
compute n74022=0.
compute n74030=0.
compute n73011=0.
compute n73012=0.
compute n73022=0.
compute n73031=0.
compute n73032=0.
compute n73034=0.
compute n73041=0.
compute n73042=0.
compute n73044=0.
compute n73050=0.
compute n73060=0.
compute n73062=0.
compute n73063=0.
compute n73070=0.
compute n73081=0.
compute n73084=0.
compute n73092=0.
compute n73093=0.
compute n73110=0.
compute n73120=0.
compute n73150=0.
compute n73200=0.

if (tnb= 74011 ) n74011=1.                
if (tnb= 74012 ) n74012=1.
if (tnb= 74013 ) n74013=1.
if (tnb= 74022 ) n74022=1.
if (tnb= 74030 ) n74030=1.
if (tnb= 73011 ) n73011=1.
if (tnb= 73012 ) n73012=1.
if (tnb= 73022 ) n73022=1.
if (tnb= 73031 ) n73031=1.
if (tnb= 73032 ) n73032=1.
if (tnb= 73034 ) n73034=1.
if (tnb= 73041 ) n73041=1.
if (tnb= 73042 ) n73042=1.
if (tnb= 73044 ) n73044=1.
if (tnb= 73050 ) n73050=1.
if (tnb= 73060 ) n73060=1.
if (tnb= 73062 ) n73062=1.
if (tnb= 73063 ) n73063=1.
if (tnb= 73070 ) n73070=1.
if (tnb= 73081 ) n73081=1.
if (tnb= 73084 ) n73084=1.
if (tnb= 73092 ) n73092=1.
if (tnb= 73093 ) n73093=1.
if (tnb= 73110 ) n73110=1.
if (tnb= 73120 ) n73120=1.
if (tnb= 73150 ) n73150=1.
if (tnb= 73200 ) n73200=1.


*NEW - CREATE TOWN AND NEIGHBORHOOD COMBINATION VARIABLES FOR SQRT*BUILDING SQUARE FOOT.
if (tnb= 74011 ) sb74011=sqrt(bsf).              
if (tnb= 74012 ) sb74012=sqrt(bsf).
if (tnb= 74013 ) sb74013=sqrt(bsf).
if (tnb= 74022 ) sb74022=sqrt(bsf).
if (tnb= 74030 ) sb74030=sqrt(bsf).
if (tnb= 73011 ) sb73011=sqrt(bsf).
if (tnb= 73012 ) sb73012=sqrt(bsf).
if (tnb= 73022 ) sb73022=sqrt(bsf).
if (tnb= 73031 ) sb73031=sqrt(bsf).
if (tnb= 73032 ) sb73032=sqrt(bsf).
if (tnb= 73034 ) sb73034=sqrt(bsf).
if (tnb= 73041 ) sb73041=sqrt(bsf).
if (tnb= 73042 ) sb73042=sqrt(bsf).
if (tnb= 73044 ) sb73044=sqrt(bsf).
if (tnb= 73050 ) sb73050=sqrt(bsf).
if (tnb= 73060 ) sb73060=sqrt(bsf).
if (tnb= 73062 ) sb73062=sqrt(bsf).
if (tnb= 73063 ) sb73063=sqrt(bsf).
if (tnb= 73070 ) sb73070=sqrt(bsf).
if (tnb= 73081 ) sb73081=sqrt(bsf).
if (tnb= 73084 ) sb73084=sqrt(bsf).
if (tnb= 73092 ) sb73092=sqrt(bsf).
if (tnb= 73093 ) sb73093=sqrt(bsf).
if (tnb= 73110 ) sb73110=sqrt(bsf).
if (tnb= 73120 ) sb73120=sqrt(bsf).
if (tnb= 73150 ) sb73150=sqrt(bsf).
if (tnb= 73200 ) sb73200=sqrt(bsf).

*******************************************************************************************************************.

***** Compute Block-Level Filings.
* There are two variations of this variable created below:.
* 1) FX includes the own property when counting "block-level filings".
* 2) FX2 excludes the own property when counting "block-level filings".
* Either variable can be included one at a time in the regression; they should not both be included in the same regression.
* Either variable yields similar regression results.
*****.
*********** We have combined the filings so that we are now using 2006, 2007 and 2008 block filings.COMPUTE FX = cumfile789.

COMPUTE FX = cumfile789.
RECODE FX (SYSMIS=0).


********************************************************************************************.
************* The next section computes low , mid and high foreclosure zones  for Lakeview.
********************************************************************************************.

compute midzonelakeview=0.
if town=73 and (nghcde=12 or nghcde=31 or nghcde=41 or nghcde=44 or nghcde=50) midzonelakeview=1.

compute highzonelakeview=0.
if town=73 and (nghcde=11 or nghcde=110) highzonelakeview=1.

compute lowzonelakeview=0.
if town=73 and (midzonelakeview=0 or highzonelakeview=0) lowzonelakeview=1.


compute fxlowblocklakeview=0.
if lowzonelakeview=1 fxlowblocklakeview=fx*lowzonelakeview.

compute fxmidblocklakeview=0.
if midzonelakeview=1  fxmidblocklakeview=fx*midzonelakeview.

compute fxhighblocklakeview=0.
if highzonelakeview=1 fxhighblocklakeview=fx*highzonelakeview.


*******************************************************************************************.
*************** This section computes low, mid and high foreclosure zones for North.
*******************************************************************************************.

compute lowzonenorth=0.
if town=74 and (nghcde=12 ) lowzonenorth=1.

compute midzonenorth=0.
if town=74 and (nghcde=11 or nghcde=22 or nghcde=30) midzonenorth=1.

compute highzonenorth=0.
if town=74 and (nghcde=13)  highzonenorth=1.

 
compute fxlowblocknorth=0.
if lowzonenorth=1 fxlowblocknorth=fx*lowzonenorth.

compute fxmidblocknorth=0.
if midzonenorth=1 fxmidblocknorth=fx*midzonenorth.

compute fxhighblocknorth=0.
if highzonenorth=1 fxhighblocknorth=fx*highzonenorth.


*******************************************************************************************************************.
******.
select if puresale=1 or (yr=4 or yr=5).


*NEW - CREATE NEW  TIME DUMMY VARIABLES BROKEN DOWN INTO 6 MONTH TIME PERIODS, BY TIME OF YEAR(SUMMER AND WINTER) FOR LAST 5 YEARS.   
Compute winter0405=0.
if (mos > 9 and yr=4) or (mos <= 3 and yr=5) winter0405=1.
Compute winter0506=0.
if (mos > 9 and yr=5) or (mos <= 3 and yr=6) winter0506=1.
Compute winter0607=0.
if (mos > 9 and yr=6) or (mos <= 3 and yr=7) winter0607=1.
Compute winter0708=0.
if (mos > 9 and yr=7) or (mos <= 3 and yr=8) winter0708=1.
Compute summer04=0.
if (mos > 3 and yr=4) and (mos <= 9 and yr=4) summer04=1.
Compute summer05=0.
if (mos > 3 and yr=5) and (mos <= 9 and yr=5) summer05=1. 
Compute summer06=0.
if (mos > 3 and yr=6) and (mos <= 9 and yr=6) summer06=1.
Compute summer07=0.
if (mos > 3 and yr=7) and (mos <= 9 and yr=7) summer07=1.
Compute summer08=0.
if (mos > 3 and yr=8) and (mos <= 9 and yr=8) summer08=1.
Compute jantmar04=0.
if (year1=2004 and (mos>=1 and mos<=3)) jantmar04=1. 
Compute octtdec08=0.
if (year1=2008 and (mos>=10 and mos<=12)) octtdec08=1.


*NEW - CREATE NEW TIME DUMMY VARIABLES BROKEN DOWN INTO  6 MONTH TIME PERIODS(SUMMER AND WINTER) FOR LAST 5 YEARS, FOR EACH CLASS COMBINATION.    
Compute jantmar04cl234=jantmar04*cl234.
Compute winter0405cl234=winter0405*cl234.
Compute winter0506cl234=winter0506*cl234.
Compute winter0607cl234=winter0607*cl234.
Compute winter0708cl234=winter0708*cl234.
Compute summer04cl234=summer04*cl234.
Compute summer05cl234=summer05*cl234.
Compute summer06cl234=summer06*cl234.
Compute summer07cl234=summer07*cl234.
Compute summer08cl234=summer08*cl234.
Compute octtdec08cl234=octtdec08*cl234.

*NEW.
Compute jantmar04cl56=jantmar04*cl56.
Compute winter0405cl56=winter0405*cl56.
Compute winter0506cl56=winter0506*cl56.
Compute winter0607cl56=winter0607*cl56.
Compute winter0708cl56=winter0708*cl56.
Compute summer04cl56=summer04*cl56.
Compute summer05cl56=summer05*cl56.
Compute summer06cl56=summer06*cl56.
Compute summer07cl56=summer07*cl56.
Compute summer08cl56=summer08*cl56.
Compute octtdec08cl56=octtdec08*cl56.

*NEW.
Compute jantmar04cl778=jantmar04*cl778.
Compute winter0405cl778=winter0405*cl778.
Compute winter0506cl778=winter0506*cl778.
Compute winter0607cl778=winter0607*cl778.
Compute winter0708cl778=winter0708*cl778.
Compute summer04cl778=summer04*cl778.
Compute summer05cl778=summer05*cl778.
Compute summer06cl778=summer06*cl778.
Compute summer07cl778=summer07*cl778.
Compute summer08cl778=summer08*cl778.
Compute octtdec08cl778=octtdec08*cl778.

*NEW.
Compute jantmar04cl89=jantmar04*cl89.
Compute winter0405cl89=winter0405*cl89.
Compute winter0506cl89=winter0506*cl89.
Compute winter0607cl89=winter0607*cl89.
Compute winter0708cl89=winter0708*cl89.
Compute summer04cl89=summer04*cl89.
Compute summer05cl89=summer05*cl89.
Compute summer06cl89=summer06*cl89.
Compute summer07cl89=summer07*cl89.
Compute summer08cl89=summer08*cl89.
Compute octtdec08cl89=octtdec08*cl89.

*NEW.
Compute jantmar04cl1112=jantmar04*cl1112. 
Compute winter0405cl1112=winter0405*cl1112.
Compute winter0506cl1112=winter0506*cl1112.
Compute winter0607cl1112=winter0607*cl1112.
Compute winter0708cl1112=winter0708*cl1112.
Compute summer04cl1112=summer04*cl1112.
Compute summer05cl1112=summer05*cl1112.
Compute summer06cl1112=summer06*cl1112.
Compute summer07cl1112=summer07*cl1112.
Compute summer08cl1112=summer08*cl1112.
Compute octtdec08cl1112=octtdec08*cl1112.

*NEW.
Compute jantmar04cl1095=jantmar04*cl1095.
Compute winter0405cl1095=winter0405*cl1095.
Compute winter0506cl1095=winter0506*cl1095.
Compute winter0607cl1095=winter0607*cl1095.
Compute winter0708cl1095=winter0708*cl1095.
Compute summer04cl1095=summer04*cl1095.
Compute summer05cl1095=summer05*cl1095.
Compute summer06cl1095=summer06*cl1095.
Compute summer07cl1095=summer07*cl1095.
Compute summer08cl1095=summer08*cl1095.
Compute octtdec08cl1095=octtdec08*cl1095.

*NEW.
Compute jantmar04clsplt=jantmar04*clsplt.
Compute winter0405clsplt=winter0405*clsplt.
Compute winter0506clsplt=winter0506*clsplt.
Compute winter0607clsplt=winter0607*clsplt.
Compute winter0708clsplt=winter0708*clsplt.
Compute summer04clsplt=summer04*clsplt.
Compute summer05clsplt=summer05*clsplt.
Compute summer06clsplt=summer06*clsplt.
Compute summer07clsplt=summer07*clsplt.
Compute summer08clsplt=summer08*clsplt.
Compute octtdec08clsplt=octtdec08*clsplt.

Compute cathdral=0.
If ceiling=1 cathdral=1.

Compute LV=0.
If town=73 LV=1.
Compute NO=0.
If town=74 NO=1.

Compute lsf=sqftl.
Compute nlsf=n*lsf.
if lsf > 3500 lsf = 3500 + ((lsf - 3500)/4).
if lsf <= 3500 lsf = lsf + ((3500 - lsf)/2).  
Compute lvlsf=lv*sqrt(lsf).
compute nolsf=no*sqrt(lsf).

Compute srbsf=sqrt(bsf).
Compute nsrbsf=n*sqrt(bsf).
Compute nbsf=n*bsf.
Compute srage=sqrt(age).
Compute bsfage=bsf*age.

Compute nclass95=n*class95.
Compute nclass10=n*class10.
Compute no95=0.
If class95=0 no95=1.

Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.

Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute frabsf=frame*bsf.
compute masbsf=mason*bsf.
Compute stubsf=stucco*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.
Compute bathsum=fullbath + 0.25*halfbath.



Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.
Compute prembsf=premrf*bsf.

If firepl>=2 firepl=2.
compute nfirepl=0.
compute nfirepl=n*firepl.

Compute gar1car=0.
Compute gar1hcar=0.
Compute gar2car=0.
Compute gar2hcar=0.
Compute gar3car=0.
Compute gar3hcar=0.
Compute gar4car=0.	

Compute garnogar=0.
Compute garage=0.
If gar=1 gar1car=1.
If gar=2 gar1hcar=1.
If gar=3 gar2car=1.
If gar=4 gar2hcar=1.
If gar=5 gar3car=1.
If gar=6 gar3hcar=1.
If gar=7 garnogar=1.
If gar=8 gar4car=1. 
If gar ne 7 garage=1.
compute garage1=0.
if gar=1 or gar=2 garage1=1.
compute garage2=0.
if gar=3 or gar=4 garage2=1.
compute biggar=0.
if gar=5 or gar=6 or gar=8 biggar=1.

Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
Compute basement=0.
If basment=1 or basment=2 basement=1.
Compute nbasfull=n*basefull.
Compute nbaspart=n*basepart.
Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 and (class=2 or class=3 or class=4 or class=5 or class=6 or class=7 or class=8 or class=9 or class=10 or class=34
or class=78 or class=95)  bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.
Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
Compute nnobase=n*nobase.


Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
Compute planarch=0.
Compute planstoc=0.
If plan=1 planarch=1.
If plan=2 planstoc=1.
Compute archbsf=planarch*bsf.
Compute narchbsf=n*planarch*bsf.
Compute deluxbsf=qualdlux*bsf.
Compute poorbsf=qualpoor*bsf.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute sitedbsf=sitedetr*bsf.

Compute repabove=0.
Compute repbelow=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.

Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.


Compute noapt=0.
Compute apt=class11 + class12.
if apt<1 noapt=1.

compute nsrbsfs=nsrbsf*noapt.
compute napsrbsf=nsrbsf*apt.

compute srlsf=sqrt(lsf).
compute nsrlsf=n*srlsf.

compute nage=n*age.
compute nsrage=n*srage.


compute rs1519sf=0.
if sqftb>1200 rs1519sf=rs1519*sqftb.
compute nmasbsf=n*masbsf.
compute aptmas=masbsf*apt.
compute frmsbsf=sqftb*framas.

compute srbsf11=sqrt(bsf)*class11.
compute srbsf12=sqrt(bsf)*class12.

compute framapt=frame*apt.
compute ahbath=apt*halfbath.
compute pbath=(sqftb/bathsum)*noapt.
compute aptbase=apt*basefull.
compute sbase=basefull*noapt.
compute apartb=apt*basepart.
compute sbprt=basepart*noapt.


compute lfrno=0.
if(14332000000000<pin and pin<14332999999999) or (14334000000000 <pin and pin<14334999999999) or 
(17031000000000<pin and pin <17033999999999) lfrno=1.
compute lake=0.
if ((14332000000000<pin and pin<14332999999999) or (14334000000000 <pin and pin<14334999999999) or 
  tnb=73062 or tnb= 73063 or tnb=74030 or tnb=74022) lake=1.
compute good95=0.
if ((14331230520000<pin and pin<14331230570000) or (14331240380000 <pin and pin<14331240490000) or 
   (17044240270000<pin and pin<14044240490000) or (17044310240000 <pin and pin<17044310270000)) good95=1.

compute lakel=lake*lsf.
compute lakesrl=lake*srlsf.
compute aptlakel=apt*lake*lsf.
compute aptnlsf=nlsf*apt.
compute aptlux=apt*nlux.
compute aptlsrl=apt*lake*srlsf.

compute lsf95=class95*lsf.
compute nlsf95=n*lsf95.
compute lux95=nlux*class95.
compute good95b=good95*srbsf.

compute nfulbath=n*fullbath.
compute nhafbath=n*halfbath.

compute nrenbsf=n*renbsf.
compute nbathsum=n*bathsum.

compute singlef=0.
if class11=0 and class12=0 singlef=1.
compute nbsfs=nbsf*singlef.

If num=6 bnum=0.
If num=0 bnum=0.
If num=1 bnum=2.
If num=2 bnum=3.
If num=3 bnum=4.
If num=4 bnum=5.
If num=5 bnum=6.
Compute nbnum=n*bnum.
Compute ncomm=n*comm.
Compute totunit=bnum + comm.
Compute totuntb=totunit*sqrt(bsf).


compute b=1.

*NEW - ONLY USE YEAR 2008 TO CREATE N FACTORS.
*Select if (year1=2008 and amount > 0).           
*Table observation = b
                amount
               /table = tnb by 
                           amount 
                             + b      
	/title = 'Ave & Med Sales'
     	/statistics =  median (amount 'MED SP')
              mean (amount 'MEAN SP')
                         validn (b '# PROPS').


reg des=defaults cov
      	/var=amount lvlsf nolsf nsrbsf nbsfair frabsf masbsf 
		  nbsf234 nbsf56 nbsf778 nbsf1095 nbsfclsplt nbsf89 stubsf frastbsf prembsf
		  garage1 garage2 garnogar biggar nbasfull nbaspart nluxbsf nrenbsf narchbsf nsiteben
		  srage npremrf nfirepl deluxbsf framapt lux95 good95b cathdral sb74011 sb74012
		  sb74013 sb74022 sb74030 sb73011 sb73012 sb73022 sb73031 sb73032 sb73200
		  sb73034 sb73041 sb73042 sb73044 sb73050 sb73060 sb73062 sb73070 sb73081 sb73084
		  sb73092 sb73110 sb73120 sb73150 qualdlux frmsbsf nlsf95 nbathsum winter0405cl234
		  winter0506cl234 winter0607cl234 winter0708cl234 summer04cl234 summer05cl234 summer06cl234
		  summer07cl234 jantmar04cl234 octtdec08cl234 winter0405cl56 winter0506cl56 winter0607cl56
		  winter0708cl56 summer04cl56 summer05cl56 summer06cl56 summer07cl56 jantmar04cl56 octtdec08cl56  	
           winter0405cl778 winter0506cl778 winter0607cl778 winter0708cl778 summer04cl778 summer05cl778
		  summer06cl778 summer07cl778 jantmar04cl778 octtdec08cl778 winter0405cl89 winter0506cl89
		  winter0607cl89 winter0708cl89 summer04cl89 summer05cl89 summer06cl89 summer07cl89 jantmar04cl89
		  octtdec08cl89 winter0405cl1112 winter0506cl1112 winter0607cl1112 winter0708cl1112 summer04cl1112
		  summer05cl1112 summer06cl1112 summer07cl1112 jantmar04cl1112 octtdec08cl1112 winter0405clsplt 
		  winter0506clsplt winter0607clsplt winter0708clsplt summer04clsplt summer05clsplt summer06clsplt
		  summer07clsplt jantmar04clsplt octtdec08clsplt winter0405cl1095 winter0506cl1095 winter0607cl1095
		  winter0708cl1095 summer04cl1095 summer05cl1095 summer06cl1095 summer07cl1095 jantmar04cl1095 
		  octtdec08cl1095 lowzonenorth midzonenorth highzonenorth fxlowblocknorth fxmidblocknorth fxhighblocknorth 
            /dep=amount
           /method=stepwise 
		  /method=enter lowzonenorth midzonenorth  fxlowblocknorth fxhighblocknorth fxmidblocknorth   
           /method=enter winter0405cl234 winter0506cl234 winter0607cl234 winter0708cl234
		  summer04cl234 summer05cl234 summer06cl234 summer07cl234 jantmar04cl234 
           winter0405cl56 winter0506cl56 winter0607cl56 winter0708cl56 summer04cl56
		  summer05cl56 summer06cl56 summer07cl56 jantmar04cl56 octtdec08cl56 octtdec08cl234
 	       winter0405cl778 winter0506cl778 winter0607cl778 winter0708cl778 summer04cl778
		  summer05cl778 summer06cl778 summer07cl778 jantmar04cl778 octtdec08cl778  	
           winter0405cl89 winter0506cl89 winter0607cl89 winter0708cl89 summer04cl89
		  summer05cl89 summer06cl89 summer07cl89 jantmar04cl89 octtdec08cl89  	
		  winter0405cl1112 winter0506cl1112 winter0607cl1112 winter0708cl1112 summer04cl1112
		  summer05cl1112 summer06cl1112 summer07cl1112 jantmar04cl1112 octtdec08cl1112  	
           winter0405clsplt winter0506clsplt winter0607clsplt winter0708clsplt summer04clsplt
		  summer05clsplt summer06clsplt summer07clsplt jantmar04clsplt octtdec08clsplt  	
           winter0405cl1095 winter0506cl1095 winter0607cl1095 winter0708cl1095 summer04cl1095
		  summer05cl1095 summer06cl1095 summer07cl1095 jantmar04cl1095 octtdec08cl1095 
		/method=enter garage2 garnogar biggar nbasfull nbaspart nbsf89 npremrf sb74012
		/save pred (pred) resid (resid).
         sort cases by tnb pin.	
         value labels extcon 1 'Frame' 2 'Mason' 3 'Framas' 4 'Stucco'
          /basment 1 'Full' 2 'Slab' 3 'Part' 4 'Crwl'
          /bsfn 1 'Rec' 2 'Apt' 3 'Unf'
          /gar 1 '1' 2 '1.5' 3 '2' 4 '2.5' 5 '3' 6 '3.5' 7 'non'
          /class 2 '2-02' 3 '2-03' 4 '2-04' 5 '2-05' 6 '2-06' 7 '2-07' 8 '2-08' 9 '2-09' 10 '2-10' 11'2-11' 12 '2-12' 34 '2-34' 78 '2-78' 95 '2-95'.

compute perdif=(resid)/(amount)*100.
formats pred(COMMA9.0)
          /resid (f6.0).

*plot
/vertical='Sales Price' MIN(50000) MAX(1000000)
/horizontal='RESID'
/vsize=25
/hsize=140
/plot=amount with RESID.
compute badsal=0.
if perdif>75 badsal=1.
if perdif<-75 badsal=1.
select if badsal=1.
set wid=125.
set len=59.

*REPORT FORMAT=automatic list(1)
/title='Office of the Assessor'
        'Residential Regression Report'
        'Town is Lakeview and North'
    /ltitle 'Report Ran On)Date' 
    /rtitle='PAGE)PAGE'
    /string=prop(area(2) '-' subarea(2) '-' block(3) '-' parcel(3))
          date(mos(2) '-'yr(2))
   /var=prop 'Prop' 'Index Number'(13)
       class 'Class' (label) (5)
       nghcde 'nbhb'(5)
       amount 'Price'(7)
       pred 'Predicted'(9)
       resid 'Resid'(7)
       perdif '% DIF' (5)
       sqftb 'Sqftb'(5)
       age 'Age'(4)
       extcon 'Extcon' (label)(6)
       sqftl 'Land'(6).
	
*REMOVE ALL THE ORIGINAL AOS VARIABLES FROM THE INDEPENDENT VARIABLE LIST - ONLY INCLUDE NEW  TIME DUMMY VARIABLES THAT WERE CREATED. 
*ENTER ALL TIME DUMMY VARIABLES. 
*aos lvaos noaos sraos bsfaos lsfaos aos112 aos612 aos1224 aos1824 naos. 

	