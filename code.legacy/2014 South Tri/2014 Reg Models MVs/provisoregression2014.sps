                               		*SPSS Regression.
		                               		* Proviso 2011.

Get file='C:\Program Files\IBM\SPSS\Statistics\19\1\regt31mergefcl2a.sav'.
select if (amount1>85000).
select if (amount1<600000).
select if (multi<1).
select if sqftb<6000.
Compute year1=0.
If  (amount1>0) year1=1900 + yr.
if  (yr=13 and amount1>0)  year1=2013.
if  (yr=12 and amount1>0)  year1=2012.
if  (yr=11 and amount1>0)  year1=2011.  
If  (yr=10 and amount1>0) year1=2010.
If  (yr=9 and amount1>0)  year1=2009.
If  (yr=8 and amount1>0)  year1=2008.  
If  (yr=7 and amount1>0)  year1=2007.
If  (yr=6 and amount1>0)  year1=2006.
If  (yr=5 and amount1>0)  year1=2005.
If  (yr=4 and amount1>0)  year1=2004.
If  (yr=3 and amount1>0)  year1=2003.   
If  (yr=2 and amount1>0)  year1=2002.
If  (yr=1 and amount1>0)  year1=2001.
If  (yr=0 and amount1>0)  year1=2000.

COMPUTE FX = cumfile11121314.
If FX > 6 FX=6.
COMPUTE SRFX = sqrt(fx).
RECODE FX (SYSMIS=0).
*******************************************************************************************************************.

select if (year1 > 2008). 
select if puremarket=1.

set mxcells=2500000.
compute bs=0.
if age<10 and (amount1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.
if  (pin=	15091020110000
or pin=	15191001120000
or pin=	15151260260000
or pin=	15271000590000
or pin=	15271080280000
or pin=	15143030120000
or pin=	15143120160000
or pin=	15154270610000
or pin=	15023340010000
or pin=	15031130220000
or pin=	15031260740000
or pin=	15033080150000
or pin=	15102150250000
or pin=	15021110520000
or pin=	15021110530000
or pin=	15023410020000
or pin=	15032180070000
or pin=	15032210050000
or pin=	15103300190000
or pin=	15181000050000
or pin=	15182270080000
or pin=	15124020090000
or pin=	15124070140000
or pin=	15124090190000
or pin=	15124150180000
or pin=	15124160110000
or pin=	15124220060000
or pin=	15124260280000
or pin=	15124260300000
or pin=	15124270090000
or pin=	15124300100000
or pin=	15132150140000
or pin=	15133020050000
or pin=	15133150200000
or pin=	15134010090000
or pin=	15134020410000
or pin=	15134060090000
or pin=	15134080090000
or pin=	15134090080000
or pin=	15134100090000
or pin=	15134220270000
or pin=	15134230020000
or pin=	15242020460000
or pin=	15242020530000
or pin=	15242060620000
or pin=	15242120150000
or pin=	15242150350000
or pin=	15293150190000
or pin=	15284180220000
or pin=	15203050060000
or pin=	15203070060000
or pin=	15203130220000
or pin=	15204060310000
or pin=	15201020370000
or pin=	15201050420000
or pin=	15201070370000
or pin=	15201180380000
or pin=	15211110610000
or pin=	15213040050000
or pin=	15213120080000
or pin=	15273110110000
or pin=	15273170020000
or pin=	15273260160000
or pin=	15284150180000
or pin=	15332210070000
or pin=	15334050190000
or pin=	15334130200000
or pin=	15314070170000
or pin=	15333040260000
or pin=	15333140030000
or pin=	15333190100000
or pin=	15333280080000
or pin=	15333300180000
or pin=	15333310230000
or pin=	15333330080000
or pin=	15331190150000
or pin=	15331250240000
or pin=	15341120010000
or pin=	15341190300000
or pin=	15341200470000
or pin=	15341220520000
or pin=	15341300510000
or pin=	15343060080000
or pin=	15343080290000
or pin=	15343160120000
or pin=	15343290180000
or pin=	15344030230000
or pin=	15344080060000
or pin=	15344100230000
or pin=	15344120130000
or pin=	15344180160000
or pin=	15344200440000
or pin=	15274170320000
or pin=	15342000220000
or pin=	15342030340000
or pin=	15342200110000
or pin=	15342240210000
or pin=	15342250110000
or pin=	15342260410000)  bs=1.

if (pin=	15051090070000
or pin=	15051110130000
or pin=	15051110140000
or pin=	15051160240000
or pin=	15052080420000
or pin=	15052080470000
or pin=	15052140070000
or pin=	15052180040000
or pin=	15052180100000
or pin=	15052190070000
or pin=	15052200050000
or pin=	15082120370000
or pin=	15082140390000
or pin=	15082220280000
or pin=	15082330250000
or pin=	15083040310000
or pin=	15083120560000
or pin=	15083120690000
or pin=	15084020580000
or pin=	15084100460000
or pin=	15084100600000
or pin=	15084120210000
or pin=	15091030470000
or pin=	15091090010000
or pin=	15091170170000
or pin=	15091180430000
or pin=	15091180460000
or pin=	15091180540000
or pin=	15092020390000
or pin=	15092180170000
or pin=	15093170550000
or pin=	15191001380000
or pin=	15191010860000
or pin=	15191010940000
or pin=	15191060210000
or pin=	15191070130000
or pin=	15191080180000
or pin=	15221010780000
or pin=	15221080480000
or pin=	15221110550000
or pin=	15221110660000
or pin=	15221150160000
or pin=	15221230140000
or pin=	15222010150000
or pin=	15222080250000
or pin=	15222130190000
or pin=	15222130200000
or pin=	15222140110000
or pin=	15222280560000
or pin=	15222290240000
or pin=	15224170110000
or pin=	15151230090000
or pin=	15152170100000
or pin=	15152220230000
or pin=	15152320040000
or pin=	15152340210000
or pin=	15093200550000
or pin=	15151060110000
or pin=	15151130110000
or pin=	15151190100000
or pin=	15161040130000
or pin=	15161050080000
or pin=	15162120560000
or pin=	15271000110000
or pin=	15271000230000
or pin=	15143190240000
or pin=	15143200140000
or pin=	15153240190000
or pin=	15154070130000
or pin=	15023030130000
or pin=	15023310140000
or pin=	15111000150000
or pin=	15111130110000
or pin=	15111300020000
or pin=	15031050340000
or pin=	15031090390000
or pin=	15031100450000
or pin=	15031110200000
or pin=	15031110690000
or pin=	15031130510000
or pin=	15031130530000
or pin=	15031150270000
or pin=	15031190790000
or pin=	15031220770000
or pin=	15031220840000
or pin=	15031240490000
or pin=	15031270480000
or pin=	15031310280000
or pin=	15032090350000
or pin=	15032100140000
or pin=	15033110090000
or pin=	15033220030000
or pin=	15034150110000
or pin=	15034190290000
or pin=	15034280040000
or pin=	15034330220000
or pin=	15034440060000
or pin=	15034500060000
or pin=	15034580010000
or pin=	15034600100000
or pin=	15101030290000
or pin=	15101060280000
or pin=	15021050270000
or pin=	15021060100000
or pin=	15021070550000
or pin=	15021070600000
or pin=	15021150140000
or pin=	15022000550000
or pin=	15023420060000
or pin=	15032150070000
or pin=	15032210170000
or pin=	15032240110000
or pin=	15102290350000
or pin=	15103020220000
or pin=	15103100030000
or pin=	15103180050000
or pin=	15103260100000
or pin=	15103280240000
or pin=	15103300080000
or pin=	15104010220000
or pin=	15111470350000
or pin=	15071020090000
or pin=	15072140550000
or pin=	15072141580000
or pin=	15072160280000
or pin=	15073010030000
or pin=	15073040530000
or pin=	15073070110000
or pin=	15073110660000
or pin=	15073110920000
or pin=	15073111070000
or pin=	15074010240000
or pin=	15074010380000
or pin=	15074030210000
or pin=	15074060160000
or pin=	15074160120000
or pin=	15074160330000
or pin=	15074170140000
or pin=	15074190050000
or pin=	15074210070000
or pin=	15074220250000
or pin=	15081140430000
or pin=	15083080260000
or pin=	15083190300000
or pin=	15083280120000
or pin=	15083300230000
or pin=	15084300120000
or pin=	15171090180000
or pin=	15171100070000
or pin=	15174060140000
or pin=	15174060380000
or pin=	15181000020000
or pin=	15181000360000
or pin=	15181000430000
or pin=	15181030100000
or pin=	15182010220000
or pin=	15182140300000
or pin=	15182270210000
or pin=	15124060220000
or pin=	15124130040000
or pin=	15124130080000
or pin=	15124150120000
or pin=	15124150160000
or pin=	15124220190000
or pin=	15124260060000
or pin=	15124270250000
or pin=	15124280060000
or pin=	15124280300000
or pin=	15124280380000
or pin=	15124330170000
or pin=	15124350110000
or pin=	15124360390000
or pin=	15124360400000
or pin=	15131030100000
or pin=	15131030210000
or pin=	15132010110000
or pin=	15132020230000
or pin=	15132040160000
or pin=	15132040240000
or pin=	15132050210000
or pin=	15132060290000
or pin=	15132100150000
or pin=	15132100200000
or pin=	15132110140000
or pin=	15132120270000
or pin=	15132130170000
or pin=	15132170130000
or pin=	15132180040000
or pin=	15132200240000
or pin=	15132210170000
or pin=	15132220050000
or pin=	15132230330000
or pin=	15133020070000
or pin=	15133020120000
or pin=	15133020150000
or pin=	15133030290000
or pin=	15133030320000
or pin=	15133040120000
or pin=	15133090260000
or pin=	15133100290000
or pin=	15133120050000
or pin=	15134000100000
or pin=	15134030170000
or pin=	15134030290000
or pin=	15134040220000
or pin=	15134040410000
or pin=	15134050180000
or pin=	15134050230000
or pin=	15134060210000
or pin=	15134070050000
or pin=	15134130050000
or pin=	15134130110000
or pin=	15134140280000
or pin=	15134160220000
or pin=	15134170240000
or pin=	15134180320000
or pin=	15134200020000
or pin=	15134200290000
or pin=	15134200360000
or pin=	15134200560000
or pin=	15134210220000
or pin=	15134210230000
or pin=	15134210260000
or pin=	15134210470000
or pin=	15134220120000
or pin=	15134230090000
or pin=	15134230320000
or pin=	15134240270000
or pin=	15134250130000
or pin=	15134260270000
or pin=	15134310020000
or pin=	15242020570000
or pin=	15242030130000
or pin=	15242070330000
or pin=	15242100360000
or pin=	15242100590000
or pin=	15202060130000
or pin=	15291150030000
or pin=	15291150090000
or pin=	15291160220000
or pin=	15293020280000
or pin=	15293050350000
or pin=	15293110220000
or pin=	15293170030000
or pin=	15293210060000
or pin=	15293220010000
or pin=	15293220050000
or pin=	15293220380000
or pin=	15293220510000
or pin=	15293230050000
or pin=	15293290110000
or pin=	15293290170000
or pin=	15294120520000
or pin=	15282030600000
or pin=	15283010320000
or pin=	15283120110000
or pin=	15283130390000
or pin=	15283140500000
or pin=	15283150560000
or pin=	15283160010000
or pin=	15284000310000
or pin=	15284010380000
or pin=	15284020160000
or pin=	15284030180000
or pin=	15284030230000
or pin=	15284040040000
or pin=	15284110340000
or pin=	15284120170000
or pin=	15284160300000
or pin=	15284270160000
or pin=	15203030030000
or pin=	15203070230000
or pin=	15203110470000
or pin=	15203130070000
or pin=	15203160270000
or pin=	15203160380000
or pin=	15203190080000
or pin=	15203190340000
or pin=	15204000790000
or pin=	15204001060000
or pin=	15204010510000
or pin=	15204030060000
or pin=	15204030390000
or pin=	15204040220000
or pin=	15204100500000
or pin=	15204110420000
or pin=	15204110490000
or pin=	15204140420000
or pin=	15204140500000
or pin=	15204160720000
or pin=	15204171090000
or pin=	15291050260000
or pin=	15291070190000
or pin=	15291130190000
or pin=	15292010600000
or pin=	15292180140000
or pin=	15292230340000
or pin=	15292240400000
or pin=	15292250520000
or pin=	15292260130000
or pin=	15292260150000
or pin=	15292260540000
or pin=	15292270070000
or pin=	15292280290000
or pin=	15292290420000
or pin=	15173040530000
or pin=	15201010420000
or pin=	15201060380000
or pin=	15201060500000
or pin=	15201070400000
or pin=	15201080500000
or pin=	15201110530000
or pin=	15201110540000
or pin=	15201120470000
or pin=	15201140510000
or pin=	15201170420000
or pin=	15201170510000
or pin=	15201180510000
or pin=	15201200360000
or pin=	15201220300000
or pin=	15211000440000
or pin=	15211011550000
or pin=	15211012680000
or pin=	15211050650000
or pin=	15211050660000
or pin=	15211060680000
or pin=	15211080450000
or pin=	15211090600000
or pin=	15211100680000
or pin=	15211120480000
or pin=	15211130360000
or pin=	15211140460000
or pin=	15211140540000
or pin=	15211180510000
or pin=	15211200400000
or pin=	15212041200000
or pin=	15212060330000
or pin=	15212060480000
or pin=	15212070280000
or pin=	15212070370000
or pin=	15212090280000
or pin=	15212110370000
or pin=	15212140450000
or pin=	15213011200000
or pin=	15213011300000
or pin=	15213020400000
or pin=	15213040130000
or pin=	15213050120000
or pin=	15213060550000
or pin=	15213080530000
or pin=	15213100010000
or pin=	15213120160000
or pin=	15213150110000
or pin=	15213170110000
or pin=	15213170130000
or pin=	15213170190000
or pin=	15213210430000
or pin=	15213220450000
or pin=	15213220490000
or pin=	15213230160000
or pin=	15214100220000
or pin=	15214160340000
or pin=	15214180140000
or pin=	15214220260000
or pin=	15291050500000
or pin=	15163020540000
or pin=	15163050860000
or pin=	15163080500000
or pin=	15163090210000
or pin=	15163170490000
or pin=	15163170560000
or pin=	15163180010000
or pin=	15163180440000
or pin=	15163190490000
or pin=	15163200410000
or pin=	15163270430000
or pin=	15164070180000
or pin=	15273000140000
or pin=	15273010030000
or pin=	15273010040000
or pin=	15273070130000
or pin=	15273080110000
or pin=	15273110170000
or pin=	15273140240000
or pin=	15273150090000
or pin=	15273160130000
or pin=	15273160210000
or pin=	15273190140000
or pin=	15273210200000
or pin=	15273230030000
or pin=	15273230060000
or pin=	15273240080000
or pin=	15273270020000
or pin=	15273290050000
or pin=	15273310050000
or pin=	15273310430000
or pin=	15284070060000
or pin=	15284080010000
or pin=	15284080100000
or pin=	15284080220000
or pin=	15284150140000
or pin=	15284210010000
or pin=	15284230110000
or pin=	15284300060000
or pin=	15331090260000
or pin=	15331100070000
or pin=	15331120220000
or pin=	15331200130000
or pin=	15331280050000
or pin=	15331280150000
or pin=	15331280180000
or pin=	15332010020000
or pin=	15332030230000
or pin=	15332050100000
or pin=	15332060120000
or pin=	15332080140000
or pin=	15332090130000
or pin=	15332090160000
or pin=	15332090170000
or pin=	15332100020000
or pin=	15332140090000
or pin=	15332150030000
or pin=	15332230200000
or pin=	15332250140000
or pin=	15332270230000
or pin=	15332300150000
or pin=	15333070400000
or pin=	15334020250000
or pin=	15334020290000
or pin=	15334030210000
or pin=	15334050120000
or pin=	15334060030000
or pin=	15334100020000
or pin=	15334100190000
or pin=	15334110790000
or pin=	15314030150000
or pin=	15323060060000
or pin=	15333010070000
or pin=	15333010220000
or pin=	15333020140000
or pin=	15333040080000
or pin=	15333040120000
or pin=	15333040300000
or pin=	15333040310000
or pin=	15333050190000
or pin=	15333080040000
or pin=	15333110040000
or pin=	15333110080000
or pin=	15333120150000
or pin=	15333130120000
or pin=	15333210110000
or pin=	15333220010000
or pin=	15333230030000
or pin=	15333230230000
or pin=	15333280160000
or pin=	15333280190000
or pin=	15333300050000
or pin=	15333300100000
or pin=	15333310160000
or pin=	15333310170000
or pin=	15333340030000
or pin=	15331130150000
or pin=	15331140190000
or pin=	15331150070000
or pin=	15331170170000
or pin=	15331170240000
or pin=	15331190050000
or pin=	15331230040000
or pin=	15331240100000
or pin=	15331250050000
or pin=	15324080170000
or pin=	15324100310000
or pin=	15324160160000
or pin=	15324160170000
or pin=	15341000580000
or pin=	15341020570000
or pin=	15341020610000
or pin=	15341040520000
or pin=	15341060620000
or pin=	15341090200000
or pin=	15341100550000
or pin=	15341110710000
or pin=	15341140530000
or pin=	15341160610000
or pin=	15341170570000
or pin=	15341210810000
or pin=	15341220530000
or pin=	15341220610000
or pin=	15341240590000
or pin=	15341280620000
or pin=	15343030060000
or pin=	15343030120000
or pin=	15343040020000
or pin=	15343100100000
or pin=	15343150150000
or pin=	15343180240000
or pin=	15343200150000
or pin=	15343230240000
or pin=	15343240150000
or pin=	15343240160000
or pin=	15343270230000
or pin=	15344020010000
or pin=	15344030260000
or pin=	15344030440000
or pin=	15344040100000
or pin=	15344090090000
or pin=	15344120020000
or pin=	15344150510000
or pin=	15344160050000
or pin=	15344170380000
or pin=	15344180070000
or pin=	15344190090000
or pin=	15344230090000
or pin=	15344230190000
or pin=	15344240280000
or pin=	15344250050000
or pin=	15344300100000
or pin=	15344300250000
or pin=	15344310420000
or pin=	15344320320000
or pin=	15274020460000
or pin=	15274030270000
or pin=	15274100130000
or pin=	15274110490000
or pin=	15274180530000
or pin=	15274220330000
or pin=	15274220350000
or pin=	15274220510000
or pin=	15274230260000
or pin=	15272010510000
or pin=	15272020340000
or pin=	15272050370000
or pin=	15272060400000
or pin=	15272100070000
or pin=	15272100180000
or pin=	15272100260000
or pin=	15272130020000
or pin=	15272170410000
or pin=	15272180030000
or pin=	15302010620000
or pin=	15304180330000
or pin=	15304180580000
or pin=	15302060170000
or pin=	15041170980000
or pin=	15041190100000
or pin=	15041210420000
or pin=	15041210650000
or pin=	15041210660000
or pin=	15041220600000
or pin=	15342000160000
or pin=	15342000190000
or pin=	15342000240000
or pin=	15342030150000
or pin=	15342050050000
or pin=	15342050070000
or pin=	15342050380000
or pin=	15342070330000
or pin=	15342070440000
or pin=	15342090160000
or pin=	15342100010000
or pin=	15342120300000
or pin=	15342120450000
or pin=	15342130110000
or pin=	15342140310000
or pin=	15342150040000
or pin=	15342150120000
or pin=	15342170140000
or pin=	15342170270000
or pin=	15342230170000
or pin=	15342240250000
or pin=	15342240340000
or pin=	15342250220000
or pin=	15342260140000
or pin=	15342260350000
or pin=	15342260400000)   bs=1.
select if bs=0.

Compute N=1.
compute b=1.
compute bsf=sqftb.
compute lsf=sqftl.
compute srbsf=sqrt(sqftb).
compute srage=sqrt(age).
compute srlsf=sqrt(sqftl).

compute tnb=(town*1000) + nghcde.

if (tnb= 31010 ) n=1.27.
if (tnb= 31020 ) n=1.14.
if (tnb= 31021 ) n=1.24.
if (tnb= 31030 ) n=1.26.
if (tnb= 31031 ) n=1.18.
if (tnb= 31032 ) n=1.25.
if (tnb= 31033 ) n=1.79.
if (tnb= 31034 ) n=1.25.
if (tnb= 31040 ) n=1.29.
if (tnb= 31041 ) n=1.20.
if (tnb= 31042 ) n=1.31.
if (tnb= 31050 ) n=1.89.
if (tnb= 31060 ) n=1.21.
if (tnb= 31080 ) n=1.43.
if (tnb= 31091 ) n=2.73.
if (tnb= 31092 ) n=2.05.
if (tnb= 31102 ) n=2.51.
if (tnb= 31103 ) n=2.80.
if (tnb= 31104 ) n=1.91.
if (tnb= 31110 ) n=1.86.
if (tnb= 31120 ) n=1.42.
if (tnb= 31141 ) n=2.42.
if (tnb= 31150 ) n=3.85.
if (tnb= 31151 ) n=3.58.
if (tnb= 31152 ) n=3.42.
if (tnb= 31160 ) n=1.95.
if (tnb= 31161 ) n=2.27.
if (tnb= 31170 ) n=2.05.
if (tnb= 31173 ) n=2.72.
if (tnb= 31174 ) n=3.85.
if (tnb= 31190 ) n=1.41.
if (tnb= 31200 ) n=1.43.
if (tnb= 31210 ) n=1.96.


Compute n31010=0.
Compute n31020=0.
Compute n31021=0.
Compute n31030=0.
Compute n31031=0.
Compute n31032=0.
Compute n31033=0.
Compute n31034=0.
Compute n31040=0.
Compute n31041=0.
compute n31042=0.
compute n31050=0.
compute n31060=0.
compute n31080=0.
compute n31091=0.
compute n31092=0.
compute n31102=0.
compute n31103=0.
compute n31104=0.
compute n31110=0.
compute n31120=0.
compute n31141=0.
compute n31150=0.
compute n31151=0.
compute n31152=0.
compute n31160=0.
compute n31161=0.
compute n31170=0.
compute n31173=0.
compute n31174=0.
compute n31190=0.
compute n31200=0.
compute n31210=0.


Compute b31010=0.
Compute b31020=0.
Compute b31021=0.
Compute b31030=0.
Compute b31031=0.
Compute b31032=0.
Compute b31033=0.
Compute b31034=0.
Compute b31040=0.
Compute b31041=0.
compute b31042=0.
compute b31050=0.
compute b31060=0.
compute b31080=0.
compute b31091=0.
compute b31092=0.
compute b31102=0.
compute b31103=0.
compute b31104=0.
compute b31110=0.
compute b31120=0.
compute b31141=0.
compute b31150=0.
compute b31151=0.
compute b31152=0.
compute b31160=0.
compute b31161=0.
compute b31170=0.
compute b31172=0.
compute b31173=0.
compute b31174=0.
compute b31190=0.
compute b31200=0.
compute b31210=0.

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.

compute bsf=sqftb.
compute bsf02=0.
compute bsf03=0.
compute bsf04=0.
compute bsf05=0.
compute bsf06=0.
compute bsf07=0.
compute bsf08=0.
compute bsf09=0.
compute bsf10=0.
compute bsf11=0.
compute bsf12=0.
compute bsf34=0.
compute bsf78=0.
compute bsf95=0.
if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.


if (tnb=31010)    n31010=1.
if (tnb=31020)    n31020=1.
if (tnb=31021)    n31021=1.
if (tnb=31030)    n31030=1.
if (tnb=31031)    n31031=1.
if (tnb=31032)    n31032=1.
if (tnb=31033)    n31033=1.
if (tnb=31034)    n31034=1.
if (tnb=31040)    n31040=1.
if (tnb=31041)    n31041=1.
if (tnb=31042)    n31042=1.
if (tnb=31050)    n31050=1.
if (tnb=31060)    n31060=1.
if (tnb=31080)    n31080=1.
if (tnb=31091)    n31091=1.
if (tnb=31092)    n31092=1.
if (tnb=31102)    n31102=1.
if (tnb=31103)    n31103=1.
if (tnb=31104)    n31104=1.
if (tnb=31110)    n31110=1.
if (tnb=31120)    n31120=1.
if (tnb=31141)    n31141=1.
if (tnb=31150)    n31150=1.
if (tnb=31151)    n31151=1.
if (tnb=31152)    n31152=1.
if (tnb=31160)    n31160=1.
if (tnb=31161)    n31161=1.
if (tnb=31170)    n31170=1.
if (tnb=31173)    n31173=1.
if (tnb=31174)    n31174=1.
if (tnb=31190)    n31190=1.
if (tnb=31200)    n31200=1.
if (tnb=31210)    n31210=1.

compute bathsum= fullbath + .25*halfbath.
compute nbathsum=n*bathsum.
compute nbsf=n*bsf. 
compute nsrbsf=n*srbsf.

compute sb31010=0.
compute sb31020=0.
compute sb31021=0.
compute sb31030=0.
compute sb31031=0.
compute sb31032=0.
compute sb31033=0.
compute sb31034=0.
compute sb31040=0.
compute sb31041=0.
compute sb31042=0.
compute sb31050=0.
compute sb31060=0.
compute sb31080=0.
compute sb31091=0.
compute sb31092=0.
compute sb31102=0.
compute sb31103=0.
compute sb31104=0.
compute sb31110=0.
compute sb31120=0.
compute sb31141=0.
compute sb31150=0.
compute sb31151=0.
compute sb31152=0.
compute sb31160=0.
compute sb31161=0.
compute sb31170=0.
compute sb31173=0.
compute sb31174=0.
compute sb31190=0.
compute sb31200=0.
compute sb31210=0.

if (tnb=31010)   sb31010=sqrt(bsf).
if (tnb=31020)   sb31020=sqrt(bsf).
if (tnb=31021)   sb31021=sqrt(bsf).
if (tnb=31030)   sb31030=sqrt(bsf).
if (tnb=31031)   sb31031=sqrt(bsf).
if (tnb=31032)   sb31032=sqrt(bsf).
if (tnb=31033)   sb31033=sqrt(bsf).
if (tnb=31034)   sb31034=sqrt(bsf).
if (tnb=31040)   sb31040=sqrt(bsf).
if (tnb=31041)   sb31041=sqrt(bsf).
if (tnb=31042)   sb31042=sqrt(bsf).
if (tnb=31050)   sb31050=sqrt(bsf).
if (tnb=31060)   sb31060=sqrt(bsf).
if (tnb=31080)   sb31080=sqrt(bsf).
if (tnb=31091)   sb31091=sqrt(bsf).
if (tnb=31092)   sb31092=sqrt(bsf).
if (tnb=31102)   sb31102=sqrt(bsf).
if (tnb=31103)   sb31103=sqrt(bsf).
if (tnb=31104)   sb31104=sqrt(bsf).
if (tnb=31110)   sb31110=sqrt(bsf).
if (tnb=31120)   sb31120=sqrt(bsf).
if (tnb=31141)   sb31141=sqrt(bsf).
if (tnb=31150)   sb31150=sqrt(bsf).
if (tnb=31151)   sb31151=sqrt(bsf).
if (tnb=31152)   sb31152=sqrt(bsf).
if (tnb=31160)   sb31160=sqrt(bsf).
if (tnb=31161)   sb31161=sqrt(bsf).
if (tnb=31170)   sb31170=sqrt(bsf).
if (tnb=31173)   sb31173=sqrt(bsf).
if (tnb=31174)   sb31174=sqrt(bsf).
if (tnb=31190)   sb31190=sqrt(bsf).
if (tnb=31200)   sb31200=sqrt(bsf).
if (tnb=31210)   sb31210=sqrt(bsf).


compute cenair=0.
if aircond=1 cenair=1.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.

Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute frabsf=frame*bsf.
Compute stubsf=stucco*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.
Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
compute frabsf=0.
compute masbsf=0.
compute frmsbsf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.
if extcon=1 frabsf=frame*bsf.
if extcon=2 masbsf=mason*bsf.
if extcon=3 frmsbsf=framas*bsf.

If firepl>0 firepl=1.
compute nfirepl=n*firepl.
compute nsrage=n*srage.
compute frast=frame + stucco.
compute frastbsf=frast*sqftb.

compute nbsf=n*bsf.
compute nlsf=n*lsf.
compute nsrbsf=n*srbsf.
compute nsrlsf=n*srlsf.

compute nbsf02=nbsf*class02.
compute nbsf03=nbsf*class03.
compute nbsf04=nbsf*class04.
compute nbsf05=nbsf*class05.
compute nbsf06=nbsf*class06.
compute nbsf07=nbsf*class07.
compute nbsf08=nbsf*class08.
compute nbsf09=nbsf*class09.
compute nbsf10=nbsf*class10.
compute nbsf11=nbsf*class11.
compute nbsf12=nbsf*class12.
compute nbsf34=nbsf*class34.
compute nbsf78=nbsf*class78.
compute nbsf95=nbsf*class95.

if class=95 and nghcde=32  lsf=1000.
if class=95 and nghcde=41  lsf=1750.
if class=95 and nghcde=91  lsf=1300.
if class=95 and nghcde=92  lsf=1300.
if class=95 and nghcde=102 lsf=3000.
if class=95 and nghcde=160 lsf=2800.
if class=95 and nghcde=173 lsf=4000.
if class=95 and nghcde=174 lsf=5000.  


compute nsrage=n*srage.

if num=6 bnum=0.
if num=0 bnum=0.
if num=1 bnum=2.
if num=2 bnum=3.
if num=3 bnum=4.
if num=4 bnum=5.
if num=5 bnum=6.

compute res=bnum - comm.
compute bnumb=bnum*bsf.
compute nbnum=n*bnum.
compute nres=n*res.
compute nbsfair=n*bsfair.

Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
compute nbasefull=n*basefull.
compute nbasepart=n*basepart.
Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
compute nnobase=n*nobase.

Compute lowzoneproviso=0.
if town=31 and  (nghcde=91 or nghcde=102  or nghcde=103  or nghcde=104 
or nghcde=110 or nghcde=141 or nghcde=150 or nghcde=151
or nghcde=152 or nghcde=160 or nghcde=161 or nghcde=170 or nghcde=173
or nghcde=174 or nghcde=210)  lowzoneproviso=1.                         	

Compute midzoneproviso=0.
if town=31 and (nghcde=21 or nghcde=30 or nghcde=33 
or nghcde=42 or nghcde=50 or nghcde=80    
or nghcde=92 or nghcde=120)   midzoneproviso=1. 

Compute highzoneproviso=0.
if town=31 and (nghcde=10 or nghcde=20 or nghcde=31 
or nghcde=32 or nghcde=34  or nghcde=40 or nghcde=41
or nghcde=60 or nghcde=190 or nghcde=200)   highzoneproviso=1.

Compute srfxlowblockproviso=0.
if lowzoneproviso=1 srfxlowblockproviso=srfx*lowzoneproviso.

Compute srfxmidblockproviso=0.
if midzoneproviso=1 srfxmidblockproviso=srfx*midzoneproviso.

Compute srfxhighblockproviso=0.
if highzoneproviso=1 srfxhighblockproviso=srfx*highzoneproviso.

Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).

Compute winter0910=0.
if (mos > 9 and yr=9) or (mos <= 3 and yr=10) winter0910=1.
Compute winter1011=0.
if (mos > 9 and yr=10) or (mos <= 3 and yr=11) winter1011=1.
Compute winter1112=0.
if (mos > 9 and yr=11) or (mos <= 3 and yr=12) winter1112=1.
Compute winter1213=0.
if (mos > 9 and yr=12) or (mos <= 3 and yr=13) winter1213=1.
Compute summer09=0.
if (mos > 3 and yr=9) and (mos <= 9 and yr=9) summer09=1. 
Compute summer10=0.
if (mos > 3 and yr=10) and (mos <= 9 and yr=10) summer10=1.
Compute summer11=0.
if (mos > 3 and yr=11) and (mos <= 9 and yr=11) summer11=1.
Compute summer12=0.
if (mos > 3 and yr=12) and (mos <= 9 and yr=12) summer12=1.
Compute summer13=0.
if (mos > 3 and yr=13) and (mos <= 9 and yr=13) summer13=1.
Compute jantmar09=0.
if (year1=2009 and (mos>=1 and mos<=3)) jantmar09=1. 
Compute octtdec13=0.
if (year1=2013 and (mos>=11 and mos<=13)) octtdec13=1.

Compute jantmar09cl234=jantmar09*cl234.
Compute winter0910cl234=winter0910*cl234.
Compute winter1011cl234=winter1011*cl234.
Compute winter1112cl234=winter1112*cl234.
Compute winter1213cl234=winter1213*cl234.
Compute summer09cl234=summer09*cl234.
Compute summer10cl234=summer10*cl234.
Compute summer11cl234=summer11*cl234.
Compute summer12cl234=summer12*cl234.
Compute summer13cl234=summer13*cl234.
Compute octtdec13cl234=octtdec13*cl234.

Compute jantmar09cl56=jantmar09*cl56.
Compute winter0910cl56=winter0910*cl56.
Compute winter1011cl56=winter1011*cl56.
Compute winter1112cl56=winter1112*cl56.
Compute winter1213cl56=winter1213*cl56.
Compute summer09cl56=summer09*cl56.
Compute summer10cl56=summer10*cl56.
Compute summer11cl56=summer11*cl56.
Compute summer12cl56=summer12*cl56.
Compute summer13cl56=summer13*cl56.
Compute octtdec13cl56=octtdec13*cl56.

Compute jantmar09cl778=jantmar09*cl778.
Compute winter0910cl778=winter0910*cl778.
Compute winter1011cl778=winter1011*cl778.
Compute winter1112cl778=winter1112*cl778.
Compute winter1213cl778=winter1213*cl778.
Compute summer09cl778=summer09*cl778.
Compute summer10cl778=summer10*cl778.
Compute summer11cl778=summer11*cl778.
Compute summer12cl778=summer12*cl778.
Compute summer13cl778=summer13*cl778.
Compute octtdec13cl778=octtdec13*cl778.

Compute jantmar09cl89=jantmar09*cl89.
Compute winter0910cl89=winter0910*cl89.
Compute winter1011cl89=winter1011*cl89.
Compute winter1112cl89=winter1112*cl89.
Compute winter1213cl89=winter1213*cl89.
Compute summer09cl89=summer09*cl89.
Compute summer10cl89=summer10*cl89.
Compute summer11cl89=summer11*cl89.
Compute summer12cl89=summer12*cl89.
Compute summer13cl89=summer13*cl89.
Compute octtdec13cl89=octtdec13*cl89.

Compute jantmar09cl1112=jantmar09*cl1112.
Compute winter0910cl1112=winter0910*cl1112.
Compute winter1011cl1112=winter1011*cl1112.
Compute winter1112cl1112=winter1112*cl1112.
Compute winter1213cl1112=winter1213*cl1112.
Compute summer09cl1112=summer09*cl1112.
Compute summer10cl1112=summer10*cl1112.
Compute summer11cl1112=summer11*cl1112.
Compute summer12cl1112=summer12*cl1112.
Compute summer13cl1112=summer13*cl1112.
Compute octtdec13cl1112=octtdec13*cl1112.

Compute jantmar09cl1095=jantmar09*cl1095.
Compute winter0910cl1095=winter0910*cl1095.
Compute winter1011cl1095=winter1011*cl1095.
Compute winter1112cl1095=winter1112*cl1095.
Compute winter1213cl1095=winter1213*cl1095.
Compute summer09cl1095=summer09*cl1095.
Compute summer10cl1095=summer10*cl1095.
Compute summer11cl1095=summer11*cl1095.
Compute summer12cl1095=summer12*cl1095.
Compute summer13cl1095=summer13*cl1095.
Compute octtdec13cl1095=octtdec13*cl1095.

Compute jantmar09clsplt=jantmar09*clsplt.
Compute winter0910clsplt=winter0910*clsplt.
Compute winter1011clsplt=winter1011*clsplt.
Compute winter1112clsplt=winter1112*clsplt.
Compute winter1213clsplt=winter1213*clsplt.
Compute summer09clsplt=summer09*clsplt.
Compute summer10clsplt=summer10*clsplt.
Compute summer11clsplt=summer11*clsplt.
Compute summer12clsplt=summer12*clsplt.
Compute summer13clsplt=summer13*clsplt.
Compute octtdec13clsplt=octtdec13*clsplt.


Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
compute nluxbsf=0.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.

Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute repabove=0.
Compute repbelow=0.
Compute repave=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabsf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.

Compute garage1=0.
Compute garage2=0.
Compute garage3=0.
Compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1.  
Compute biggar=0.
if garage3=1 or garage4=1 biggar=1.

compute b=1.
*select if year1 = 2013.
*select if puremarket=1.
*Table observation = b
                amount1
               /table = nghcde by 
                           amount1 
                             + b      
	/title = 'Ave & Med Sales'
     	/statistics =  median (amount1 'MED SP')
              mean (amount1 'MEAN SP')
                         validn (b '# PROPS').


reg des=defaults cov
     /var=amount1 nsrlsf nbasefull nbasepart nnobase nbnum
     nbsf234 nbsf56 nbsf778 nbsf1095 nbsf1112 nbsf34 nbsf89
     npremrf frabsf masbsf frastbsf frmsbsf nsrage nbsfair nbathsum 
     garage1 garage2 biggar nres nfirepl sb31010 sb31020 sb31021 sb31030 sb31031 sb31032 
    sb31033 sb31040 sb31041 sb31042 sb31050 sb31060 sb31080 sb31091 sb31092 
    sb31102 sb31103 sb31104 sb31110 sb31120 sb31141 sb31150 sb31152 
   	sb31160 sb31161 sb31170 sb31173 sb31174 sb31190 sb31200 sb31210
    jantmar09cl234 winter0910cl234 winter1011cl234 winter1112cl234 winter1213cl234 summer09cl234
   summer10cl234 summer11cl234 summer12cl234 summer13cl234 octtdec13cl234 jantmar09cl56
   winter0910cl56 winter1011cl56 winter1112cl56 winter1213cl56 summer09cl56 summer10cl56
   summer11cl56 summer12cl56 summer13cl56 octtdec13cl56  jantmar09cl778 winter0910cl778
   winter1011cl778 winter1112cl778 winter1213cl778 summer09cl778 summer10cl778 summer11cl778
   summer12cl778 summer13cl778 octtdec13cl778 jantmar09cl89 winter0910cl89 winter1011cl89
   winter1112cl89 winter1213cl89 summer09cl89 summer10cl89 summer11cl89 summer12cl89
   summer13cl89 octtdec13cl89 jantmar09cl1112 winter0910cl1112 winter1011cl1112
   winter1112cl1112 winter1213cl1112 summer09cl1112 summer10cl1112 summer11cl1112
   summer12cl1112 summer13cl1112 octtdec13cl1112 jantmar09cl1095 winter0910cl1095
   winter1011cl1095 winter1112cl1095 winter1213cl1095 summer09cl1095
   summer10cl1095 summer11cl1095 summer12cl1095 summer13cl1095 octtdec13cl1095
   jantmar09clsplt winter0910clsplt winter1011clsplt winter1112clsplt winter1213clsplt
   summer09clsplt summer10clsplt summer11clsplt summer12clsplt summer13clsplt octtdec13clsplt
	  midzoneproviso highzoneproviso srfxlowblockproviso srfxmidblockproviso srfxhighblockproviso  
	/dep=amount1	
 /method=stepwise 
	/method=enter jantmar09cl234 winter0910cl234 winter1011cl234 winter1112cl234 winter1213cl234 summer09cl234
  summer10cl234 summer11cl234 summer12cl234 summer13cl234 octtdec13cl234 jantmar09cl56
  winter0910cl56 winter1011cl56 winter1112cl56 winter1213cl56 summer09cl56 summer10cl56
  summer11cl56 summer12cl56 summer13cl56 octtdec13cl56  jantmar09cl778 winter0910cl778
  winter1011cl778 winter1112cl778 winter1213cl778 summer09cl778 summer10cl778 summer11cl778
  summer12cl778 summer13cl778 octtdec13cl778 jantmar09cl89 winter0910cl89 winter1011cl89
  winter1112cl89 winter1213cl89 summer09cl89 summer10cl89 summer11cl89 summer12cl89
  summer13cl89 octtdec13cl89 jantmar09cl1112 winter0910cl1112 winter1011cl1112
  winter1112cl1112 winter1213cl1112 summer09cl1112 summer10cl1112 summer11cl1112
  summer12cl1112 summer13cl1112 octtdec13cl1112 jantmar09cl1095 winter0910cl1095
  winter1011cl1095 winter1112cl1095 winter1213cl1095 summer09cl1095
  summer10cl1095 summer11cl1095 summer12cl1095 summer13cl1095 octtdec13cl1095
  jantmar09clsplt winter0910clsplt winter1011clsplt winter1112clsplt winter1213clsplt
  summer09clsplt summer10clsplt summer11clsplt summer12clsplt summer13clsplt octtdec13clsplt
	/method=enter midzoneproviso highzoneproviso srfxlowblockproviso srfxmidblockproviso srfxhighblockproviso 
	/method=enter  nsrage garage1 biggar nbasefull sb31210 sb31110 sb31040 sb31161 
 /method=enter  nbasepart nbsf1112 npremrf
	/save pred (pred) resid (resid).
     sort cases by nghcde pin.	
     value labels extcon 1 'Frame' 2 'Mason' 3 'Framas' 4 'Stucco'
     /basment 1 'Full' 2 'Slab' 3 'Part' 4 'Crwl'
     /bsfn 1 'Rec' 2 'Apt' 3 'Unf'
     /gar 1 '1' 2 '1.5' 3 '2' 4 '2.5' 5 '3' 6 '3.5' 7 'non'   
     /class 2 '2-02' 3 '2-03' 4 '2-04' 5 '2-05' 6 '2-06' 7 '2-07' 8 '2-08' 9 '2-09' 10 '2-10' 11'2-11' 12 '2-12' 34 '2-34' 78 '2-78' 95 '2-95'.
compute perdif=(resid)/(amount1)*100.
formats pred(COMMA9.0)
          /resid (f6.0).
exe.
*plot
/vertical='Sales Price' MIN(50000) MAX(1000000)
/horizontal='RESID'
/vsize=25
/hsize=140
/plot=amount1 with RESID.
*compute badsal=0.
*if perdif>20 badsal=1.
*if perdif<-20 badsal=1.	
*select if badsal=1.
*set wid=125.
*set len=59.
*REPORT FORMAT=automatic list(1)
/title='Office of the Assessor'
        'Residential Regression Report'
        'Town is Proviso'
    /ltitle 'Report Ran On)Date' 
    /rtitle='PAGE)PAGE'
    /string=prop(area(2) '-' subarea(2) '-' block(3) '-' parcel(3))
          date(mos(2) '-'yr(2))
   /var=prop 'Prop' 'Index Number'(13)
       class 'Class' (label) (5)
       nghcde 'nbhb'(5)
       amount1 'Price'(7)
       pred 'Predicted'(9)
       resid 'Resid'(7)
       perdif '% DIF' (5)
       aos 'AOS' (3)
       sqftb 'Sqftb'(5)
       age 'Age'(4)
       extcon 'Extcon' (label)(6)
       sqftl 'Land'(6).
	
exe. 	


