                     *SPSS Regression.
		           *BLOOM REGRESSION  2014.


Get file='C:\Program Files\IBM\SPSS\Statistics\19\1\regt12mergefcl2.sav'.
select if (amount1>120000).
select if (amount1<760000).
select if (multi<1).

Compute year1=0.
If  (amount1>0) year1=1900 + yr.
if  (yr=13 and amount1>0) year1=2013.
if  (yr=12 and amount1>0) year1=2012.
if  (yr=11 and amount1>0) year1=2011. 
If  (yr=10 and amount1>0) year1=2010.
If  (yr=9 and amount1>0)  year1=2009.
If  (yr=8 and amount1>0)  year1=2008. 
If  (yr=7 and amount1>0)  year1=2007.
If  (yr=6 and amount1>0)  year1=2006. 
If  (yr=5 and amount1>0)  year1=2005.
If  (yr=4 and amount1>0)  year1=2004.
If  (yr=3 and amount1>0)  year1=2003. 

select if sqftb<6000.
select if (year1>2008).
select if puremarket=1.

COMPUTE FX = cumfile11121314.
If FX >=7 FX=7.
COMPUTE SRFX = sqrt(fx).
RECODE FX (SYSMIS=0).
*******************************************************************************************************************.



Compute bs=0.
if age<10 and (amount1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.

if (pin=	32051010140000
or pin=	32051100180000
or pin=	32051100400000
or pin=	32061100100000
or pin=	32061200120000
or pin=	32061260310000
or pin=	32062100070000
or pin=	32062110170000
or pin=	32062130080000
or pin=	32062160320000
or pin=	32062170050000
or pin=	32062180410000
or pin=	32062270130000
or pin=	32064030560000
or pin=	32041050370000
or pin=	32041110280000
or pin=	32052200230000
or pin=	32051150200000
or pin=	32051180340000
or pin=	32051190160000
or pin=	32051190230000
or pin=	32052030510000
or pin=	32052120240000
or pin=	32052120470000
or pin=	32052120490000
or pin=	32053100070000
or pin=	32053110100000
or pin=	32053190040000
or pin=	32053220250000
or pin=	32053220330000
or pin=	32054010020000
or pin=	32054010070000
or pin=	32054010190000
or pin=	32054050060000
or pin=	32054100190000
or pin=	32081060090000
or pin=	32033020080000
or pin=	32033070080000
or pin=	32033250060000
or pin=	32042000180000
or pin=	32042000230000
or pin=	32042000340000
or pin=	32044030130000
or pin=	32044030310000
or pin=	32102100090000
or pin=	33051020220000
or pin=	33054000340000
or pin=	33064020080000
or pin=	33064150030000
or pin=	33071041080000
or pin=	33071050450000
or pin=	33071100030000
or pin=	33051170120000
or pin=	33053070120000
or pin=	33053200190000
or pin=	33054000560000
or pin=	33054000640000
or pin=	32063000140000
or pin=	32063000270000
or pin=	32063000280000
or pin=	32063000490000
or pin=	32063010280000
or pin=	32063030080000
or pin=	32063030090000
or pin=	32063040230000
or pin=	32063060090000
or pin=	32063060300000
or pin=	32063090170000
or pin=	32063090320000
or pin=	32063090430000
or pin=	32063090470000
or pin=	32063120030000
or pin=	32063120160000
or pin=	32063140040000
or pin=	32063140350000
or pin=	32063140370000
or pin=	32063140470000
or pin=	32063150140000
or pin=	32063160020000
or pin=	32063160150000
or pin=	32063160280000
or pin=	32063170090000
or pin=	32063180040000
or pin=	32063190110000
or pin=	32071080110000
or pin=	32071090120000
or pin=	32071030050000
or pin=	32071050260000
or pin=	32071050300000
or pin=	32071060080000
or pin=	32071070030000
or pin=	32072000130000
or pin=	32072000380000
or pin=	32074030040000
or pin=	32181020170000
or pin=	32181030020000
or pin=	32181040110000
or pin=	32181040130000
or pin=	32072030630000
or pin=	32074010190000
or pin=	32074010570000
or pin=	32074080060000
or pin=	32074080260000
or pin=	32074100180000
or pin=	32082160170000
or pin=	32083360190000
or pin=	32182030270000
or pin=	32182090260000
or pin=	32182100110000
or pin=	32182100200000
or pin=	32182120110000
or pin=	32022010440000
or pin=	32182180050000
or pin=	32114030180000
or pin=	32114030220000
or pin=	32124150080000
or pin=	32124150140000
or pin=	32132030140000
or pin=	33062000520000
or pin=	33184000070000
or pin=	33073110270000
or pin=	33074020050000
or pin=	33074080070000
or pin=	32183020150000
or pin=	32191030010000
or pin=	32184040090000
or pin=	32201070090000
or pin=	32201070200000
or pin=	32201040030000
or pin=	32201040880000
or pin=	32183080010000
or pin=	32183090040000
or pin=	32183090280000
or pin=	32321030100000
or pin=	32321060030000
or pin=	32321110350000
or pin=	32321150170000
or pin=	32321160050000
or pin=	32322230050000
or pin=	32333270150000
or pin=	32334170070000
or pin=	32342050020000
or pin=	33311200070000
or pin=	32342060080000
or pin=	32253130220000)     bs=1.
select if bs=0.

Compute bsf=sqftb.
Compute N=1.
Compute tnb=(town*1000)+nghcde.


If nghcde=10 N=1.36.
If nghcde=21 N=1.22.
If nghcde=22 N=1.45.
If nghcde=31 N=.95.
If nghcde=32 N=.92.
If nghcde=33 N=1.32.
If nghcde=41 N=1.05.
If nghcde=42 N=1.22.
If nghcde=43 N=1.35.
If nghcde=44 N=1.25. 
If nghcde=45 N=1.18.
If nghcde=46 N=1.51.
If nghcde=51 N=1.82.
If nghcde=52 N=2.55.
If nghcde=53 N=2.27.
If nghcde=54 N=2.12.
If nghcde=61 N=1.17.
If nghcde=62 N=1.17.
If nghcde=63 N=.80.
If nghcde=64 N=2.28.
If nghcde=65 N=.70.
If nghcde=71 N=2.37.
If nghcde=72 N=1.19.
If nghcde=73 N=1.44.
If nghcde=81 N=.97.
If nghcde=82 N=1.25.
If nghcde=84 N=.96.
If nghcde=85 N=.95.
If nghcde=86 N=.92.
If nghcde=87 N=1.01.
If nghcde=88 N=.98.
If nghcde=91 N=.84.
If nghcde=92 N=.88.
If nghcde=93 N=.88.
If nghcde=101 N=.86.
If nghcde=111 N=.85.
If nghcde=112 N=1.05.
If nghcde=121 N=.58.
If nghcde=122 N=.59.
If nghcde=131 N=.86.
If nghcde=132 N=1.13.
If nghcde=141 N=.95.
If nghcde=142 N=.98.
If nghcde=150 N=1.32.
If nghcde=151 N=1.52.
If nghcde=162 N=1.16.
If nghcde=180 N=.85.
If nghcde=190 N=1.35.
If nghcde=191 N=1.54.


Compute n10=0.
If nghcde=10 n10=1.
Compute n21=0.
If nghcde=21 n21=1.
Compute n22=0.
If nghcde=22 n22=1.
Compute n31=0.
If nghcde=31 n31=1.
Compute n32=0.
If nghcde=32 n32=1.
Compute n33=0.
If nghcde=33 n33=1.
Compute n41=0.
If nghcde=41 n41=1.
Compute n42=0.
If nghcde=42 n42=1.
Compute n43=0.
If nghcde=43 n43=1.
Compute n44=0.
If nghcde=44 n44=1.
Compute n45=0.
If nghcde=45 n45=1.
Compute n46=0.
If nghcde=46 n46=1.
Compute n51=0.
If nghcde=51 n51=1.
Compute n52=0.
If nghcde=52 n52=1.
Compute n53=0.
If nghcde=53 n53=1.
Compute n54=0.
If nghcde=54 n54=1.
Compute n61=0.
If nghcde=61 n61=1.
Compute n62=0.
If nghcde=62 n62=1.
Compute n63=0.
If nghcde=63 n63=1.
Compute n64=0.
If nghcde=64 n64=1.
Compute n65=0.
If nghcde=65 n65=1.
Compute n71=0.
If nghcde=71 n71=1.
Compute n72=0.
If nghcde=72 n72=1.
Compute n73=0.
If nghcde=73 n73=1.
Compute n81=0.
If nghcde=81 n81=1.
Compute n82=0.
If nghcde=82 n82=1.
Compute n84=0.
If nghcde=84 n84=1.
Compute n85=0.
If nghcde=85 n85=1.
Compute n86=0.
If nghcde=86 n86=1.
Compute n87=0.
If nghcde=87 n87=1.
Compute n88=0.
If nghcde=88 n88=1.
Compute n91=0.
If nghcde=91 n91=1.
Compute n92=0.
If nghcde=92 n92=1.
Compute n93=0.
If nghcde=93 n93=1.
Compute n101=0.
If nghcde=101 n101=1.
Compute n111=0.
If nghcde=111 n111=1.
Compute n112=0.
If nghcde=112 n112=1.
Compute n121=0.
If nghcde=121 n121=1.
Compute n122=0.
If nghcde=122 n122=1.
Compute n131=0.
If nghcde=131 n131=1.
Compute n132=0.
If nghcde=132 n132=1.
Compute n141=0.
If nghcde=141 n141=1.
Compute n142=0.
If nghcde=142 n142=1.
Compute n150=0.
If nghcde=150 n150=1.
Compute n151=0.
If nghcde=151 n151=1.
Compute n162=0.
If nghcde=162 n162=1.
Compute n180=0.
If nghcde=180 n180=1.
Compute n190=0.
If nghcde=190 n190=1.
compute n191=0.
if nghcde=191 n191=1.

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.
compute bsf=sqftb.
compute bsf02=0.
compute bsf03=0.
compute bsf04=0.
compute bsf05=0.
compute bsf06=0.
compute bsf07=0.
compute bsf08=0.
compute bsf09=0.
compute bsf10=0.
compute bsf11=0.
compute bsf12=0.
compute bsf34=0.
compute bsf78=0.
compute bsf95=0.
if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.

Compute sb10=sqrt(bsf)*n10.
Compute sb21=sqrt(bsf)*n21.
Compute sb22=sqrt(bsf)*n22.
Compute sb31=sqrt(bsf)*n31.
Compute sb32=sqrt(bsf)*n32.
Compute sb33=sqrt(bsf)*n33.
Compute sb41=sqrt(bsf)*n41.
Compute sb42=sqrt(bsf)*n42.
Compute sb43=sqrt(bsf)*n43.
Compute sb44=sqrt(bsf)*n44.
Compute sb45=sqrt(bsf)*n45.
Compute sb46=sqrt(bsf)*n46.
Compute sb51=sqrt(bsf)*n51.
Compute sb52=sqrt(bsf)*n52.
Compute sb53=sqrt(bsf)*n53.
Compute sb54=sqrt(bsf)*n54.
Compute sb61=sqrt(bsf)*n61.
Compute sb62=sqrt(bsf)*n62.
Compute sb63=sqrt(bsf)*n63.
Compute sb64=sqrt(bsf)*n64.
Compute sb65=sqrt(bsf)*n65.
Compute sb71=sqrt(bsf)*n71.
Compute sb72=sqrt(bsf)*n72.
Compute sb73=sqrt(bsf)*n73.
Compute sb81=sqrt(bsf)*n81.
Compute sb82=sqrt(bsf)*n82.
Compute sb84=sqrt(bsf)*n84.
Compute sb85=sqrt(bsf)*n85.
Compute sb86=sqrt(bsf)*n86.
Compute sb87=sqrt(bsf)*n87.
Compute sb88=sqrt(bsf)*n88.
Compute sb91=sqrt(bsf)*n91.
Compute sb92=sqrt(bsf)*n92.
Compute sb93=sqrt(bsf)*n93.
Compute sb101=sqrt(bsf)*n101.
Compute sb111=sqrt(bsf)*n111.
Compute sb112=sqrt(bsf)*n112.
Compute sb121=sqrt(bsf)*n121.
Compute sb122=sqrt(bsf)*n122.
Compute sb131=sqrt(bsf)*n131.
Compute sb132=sqrt(bsf)*n132.
Compute sb141=sqrt(bsf)*n141.
Compute sb142=sqrt(bsf)*n142.
Compute sb150=sqrt(bsf)*n150.
Compute sb151=sqrt(bsf)*n151.
Compute sb162=sqrt(bsf)*n162.
Compute sb180=sqrt(bsf)*n180.
Compute sb190=sqrt(bsf)*n190.
compute sb191=sqrt(bsf)*n191.

Compute nsrbsf=n*sqrt(bsf).
Compute lsf=sqftl.

If firepl>1 firepl=1.  

Compute bathsum=fullbath + 0.25*halfbath.
Compute nbathsum=n*bathsum.

*Cutpoints for land square foot are 1.75 * median lsf.
Compute lsf=sqftl.
If lsf > 15120  lsf = 15120 + ((lsf - 15120)/3).
If (class=8 or class=9) lsf=sqftl.

if nghcde=43 and class=95  lsf=4500.
if nghcde=53 and class=95  lsf=5000.
if nghcde=162 and class=95 lsf=3500.  

Compute nsrlsf=n*sqrt(lsf).

Compute nbsf=n*bsf.
Compute srage=sqrt(age).
Compute nsrage=n*srage.
Compute bsfage=bsf*age.

Compute nclass95=n*class95.
Compute nclass10=n*class10.
Compute no95=0.
If class95=0 no95=1.

Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).

Compute winter0910=0.
if (mos > 9 and yr=9) or (mos <= 3 and yr=10) winter0910=1.
Compute winter1011=0.
if (mos > 9 and yr=10) or (mos <= 3 and yr=11) winter1011=1.
Compute winter1112=0.
if (mos > 9 and yr=11) or (mos <= 3 and yr=12) winter1112=1.
Compute winter1213=0.
if (mos > 9 and yr=12) or (mos <= 3 and yr=13) winter1213=1.
Compute summer09=0.
if (mos > 3 and yr=9) and (mos <= 9 and yr=9) summer09=1. 
Compute summer10=0.
if (mos > 3 and yr=10) and (mos <= 9 and yr=10) summer10=1.
Compute summer11=0.
if (mos > 3 and yr=11) and (mos <= 9 and yr=11) summer11=1.
Compute summer12=0.
if (mos > 3 and yr=12) and (mos <= 9 and yr=12) summer12=1.
Compute summer13=0.
if (mos > 3 and yr=13) and (mos <= 9 and yr=13) summer13=1.
Compute jantmar09=0.
if (year1=2009 and (mos>=1 and mos<=3)) jantmar09=1. 
Compute octtdec13=0.
if (year1=2013 and (mos>=11 and mos<=13)) octtdec13=1.

Compute jantmar09cl234=jantmar09*cl234.
Compute winter0910cl234=winter0910*cl234.
Compute winter1011cl234=winter1011*cl234.
Compute winter1112cl234=winter1112*cl234.
Compute winter1213cl234=winter1213*cl234.
Compute summer09cl234=summer09*cl234.
Compute summer10cl234=summer10*cl234.
Compute summer11cl234=summer11*cl234.
Compute summer12cl234=summer12*cl234.
Compute summer13cl234=summer13*cl234.
Compute octtdec13cl234=octtdec13*cl234.

Compute jantmar09cl56=jantmar09*cl56.
Compute winter0910cl56=winter0910*cl56.
Compute winter1011cl56=winter1011*cl56.
Compute winter1112cl56=winter1112*cl56.
Compute winter1213cl56=winter1213*cl56.
Compute summer09cl56=summer09*cl56.
Compute summer10cl56=summer10*cl56.
Compute summer11cl56=summer11*cl56.
Compute summer12cl56=summer12*cl56.
Compute summer13cl56=summer13*cl56.
Compute octtdec13cl56=octtdec13*cl56.

Compute jantmar09cl778=jantmar09*cl778.
Compute winter0910cl778=winter0910*cl778.
Compute winter1011cl778=winter1011*cl778.
Compute winter1112cl778=winter1112*cl778.
Compute winter1213cl778=winter1213*cl778.
Compute summer09cl778=summer09*cl778.
Compute summer10cl778=summer10*cl778.
Compute summer11cl778=summer11*cl778.
Compute summer12cl778=summer12*cl778.
Compute summer13cl778=summer13*cl778.
Compute octtdec13cl778=octtdec13*cl778.

Compute jantmar09cl89=jantmar09*cl89.
Compute winter0910cl89=winter0910*cl89.
Compute winter1011cl89=winter1011*cl89.
Compute winter1112cl89=winter1112*cl89.
Compute winter1213cl89=winter1213*cl89.
Compute summer09cl89=summer09*cl89.
Compute summer10cl89=summer10*cl89.
Compute summer11cl89=summer11*cl89.
Compute summer12cl89=summer12*cl89.
Compute summer13cl89=summer13*cl89.
Compute octtdec13cl89=octtdec13*cl89.

Compute jantmar09cl1112=jantmar09*cl1112.
Compute winter0910cl1112=winter0910*cl1112.
Compute winter1011cl1112=winter1011*cl1112.
Compute winter1112cl1112=winter1112*cl1112.
Compute winter1213cl1112=winter1213*cl1112.
Compute summer09cl1112=summer09*cl1112.
Compute summer10cl1112=summer10*cl1112.
Compute summer11cl1112=summer11*cl1112.
Compute summer12cl1112=summer12*cl1112.
Compute summer13cl1112=summer13*cl1112.
Compute octtdec13cl1112=octtdec13*cl1112.

Compute jantmar09cl1095=jantmar09*cl1095.
Compute winter0910cl1095=winter0910*cl1095.
Compute winter1011cl1095=winter1011*cl1095.
Compute winter1112cl1095=winter1112*cl1095.
Compute winter1213cl1095=winter1213*cl1095.
Compute summer09cl1095=summer09*cl1095.
Compute summer10cl1095=summer10*cl1095.
Compute summer11cl1095=summer11*cl1095.
Compute summer12cl1095=summer12*cl1095.
Compute summer13cl1095=summer13*cl1095.
Compute octtdec13cl1095=octtdec13*cl1095.

Compute jantmar09clsplt=jantmar09*clsplt.
Compute winter0910clsplt=winter0910*clsplt.
Compute winter1011clsplt=winter1011*clsplt.
Compute winter1112clsplt=winter1112*clsplt.
Compute winter1213clsplt=winter1213*clsplt.
Compute summer09clsplt=summer09*clsplt.
Compute summer10clsplt=summer10*clsplt.
Compute summer11clsplt=summer11*clsplt.
Compute summer12clsplt=summer12*clsplt.
Compute summer13clsplt=summer13*clsplt.
Compute octtdec13clsplt=octtdec13*clsplt.


Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
compute nbsfair=n*bsfair.

Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
Compute basement=0.
If basment=1 or basment=2 basement=1.
Compute nbaseful=n*basefull.
Compute nbaseprt=n*basepart.
Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
Compute nnobase=n*nobase.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf
Compute nluxbsf=n*luxbsf.
Compute deluxbsf = qualdlux*bsf.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute repabove=0.
Compute repbelow=0.
Compute repave=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.
Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.
Compute twosty=0.
If class07=1 or class08=1 or class78=1 twosty=1.
compute b=1.

Compute lowzonebloom=0.
if town=12 and  (nghcde=10 or nghcde=41  or nghcde=42  or nghcde=51 
or nghcde=81 or nghcde=82 or nghcde=112 or nghcde=122
or nghcde=141 or nghcde=142) lowzonebloom=1.                         	

Compute midzonebloom=0.
if town=12 and (nghcde=21 or nghcde=22 or nghcde=31 or nghcde=33
or nghcde=44 or nghcde=46 or nghcde=52 or nghcde=53 or nghcde=54
or nghcde=61 or nghcde=63 or nghcde=64 or nghcde=71 or nghcde=86
or nghcde=87 or nghcde=88 or nghcde=91 or nghcde=111 or nghcde=132
or nghcde=150 or nghcde=151)  midzonebloom=1. 

Compute highzonebloom=0.
if town=12 and (nghcde=32 or nghcde=43 or nghcde=65  or nghcde=72 or nghcde=73 
or nghcde=84 or nghcde=85 or nghcde=93 or nghcde=101 or nghcde=121 or nghcde=162
or nghcde=190 or nghcde=191)  highzonebloom=1.


Compute srfxlowblockbloom=0.
if lowzonebloom=1 srfxlowblockbloom=srfx*lowzonebloom.

Compute srfxmidblockbloom=0.
if midzonebloom=1 srfxmidblockbloom=srfx*midzonebloom.

Compute srfxhighblockbloom=0.
if highzonebloom=1 srfxhighblockbloom=srfx*highzonebloom.

if num=6 bnum=0.
if num=0 bnum=0.
if num=1 bnum=2.
if num=2 bnum=3.
if num=3 bnum=4.
if num=4 bnum=5.
if num=5 bnum=6.

compute res = bnum - comm.
compute bnumb = bnum*bsf.
compute nres = n*res.

Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute masbsf=mason*bsf.
Compute frmsbsf=framas*bsf.
Compute frabsf=frame*bsf.
Compute stubsf=stucco*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.
Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.
Compute nfirepl=n*firepl.


compute cathdral=0.
if ceiling=1 cathdral=1.

Compute gar1car=0.
Compute gar1hcar=0.
Compute gar2car=0.
Compute gar2hcar=0.
Compute gar3car=0.
Compute gar3hcar=0.
Compute gar4car=0.	

Compute garnogar=0.
Compute garage=0.
Compute biggar=0.
If gar=1 gar1car=1.
If gar=2 gar1hcar=1.
If gar=3 gar2car=1.
If gar=4 gar2hcar=1.
If gar=5 gar3car=1.
If gar=6 gar3hcar=1.
If gar=7 garnogar=1.
If gar=8 gar4car=1. 
compute garage1=0.
compute garage2=0.
compute garage3=0.
compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1.
Compute biggar=0.
if garage3=1 or garage4=1 biggar=1.

compute b=1.
*select if year1 = 2013.
*select if puremarket=1.
*Table observation = b
                amount1
               /table = nghcde by 
                           amount1 
                             + b      
	/title = 'Ave & Med Sales'
     	/statistics =  median (amount1 'MED SP')
              mean (amount1 'MEAN SP')
                         validn (b '# PROPS').
   
  

reg des=defaults cov
     /var=amount1 nbsf nsrlsf nsrage nbaseful nbaseprt nnobase nbathsum nfirepl masbsf frmsbsf frabsf stubsf
  nbsfair srfx garnogar garage1 garage2  nrenbsf nres nbsf234 nbsf34 nbsf56 nbsf778 nbsf89 nbsf1095 nbsf1112
 sb10 sb21 sb22 sb31 sb32 sb33 sb41 sb42 sb43 sb44 sb45 sb46 sb51 sb52 sb53 sb54
	sb61 sb62 sb63 sb64 sb65 sb71 sb72 sb73 sb81 sb82 sb84 sb85 sb86 sb87 sb88 sb91
	sb92 sb93 sb101 sb111 sb112 sb121 sb122 sb131 sb132 sb141 sb142 sb150 sb151 sb162
	sb180 sb190 sb191  jantmar09cl234 winter0910cl234 winter1011cl234 winter1112cl234 winter1213cl234 summer09cl234
 summer10cl234 summer11cl234 summer12cl234 summer13cl234 octtdec13cl234 jantmar09cl56
 winter0910cl56 winter1011cl56 winter1112cl56 winter1213cl56 summer09cl56 summer10cl56
 summer11cl56 summer12cl56 summer13cl56 octtdec13cl56  jantmar09cl778 winter0910cl778
 winter1011cl778 winter1112cl778 winter1213cl778 summer09cl778 summer10cl778 summer11cl778
 summer12cl778 summer13cl778 octtdec13cl778 jantmar09cl89 winter0910cl89 winter1011cl89
 winter1112cl89 winter1213cl89 summer09cl89 summer10cl89 summer11cl89 summer12cl89
 summer13cl89 octtdec13cl89 jantmar09cl1112 winter0910cl1112 winter1011cl1112
 winter1112cl1112 winter1213cl1112 summer09cl1112 summer10cl1112 summer11cl1112
 summer12cl1112 summer13cl1112 octtdec13cl1112 jantmar09cl1095 winter0910cl1095
 winter1011cl1095 winter1112cl1095 winter1213cl1095 summer09cl1095
 summer10cl1095 summer11cl1095 summer12cl1095 summer13cl1095 octtdec13cl1095
 jantmar09clsplt winter0910clsplt winter1011clsplt winter1112clsplt winter1213clsplt
 summer09clsplt summer10clsplt summer11clsplt summer12clsplt summer13clsplt octtdec13clsplt
	midzonebloom highzonebloom srfxlowblockbloom srfxmidblockbloom srfxhighblockbloom  
	/dep=amount1
/method=stepwise
/method=enter jantmar09cl234 winter0910cl234 winter1011cl234 winter1112cl234 winter1213cl234 summer09cl234
 summer10cl234 summer11cl234 summer12cl234 summer13cl234 octtdec13cl234 jantmar09cl56
 winter0910cl56 winter1011cl56 winter1112cl56 winter1213cl56 summer09cl56 summer10cl56
 summer11cl56 summer12cl56 summer13cl56 octtdec13cl56  jantmar09cl778 winter0910cl778
 winter1011cl778 winter1112cl778 winter1213cl778 summer09cl778 summer10cl778 summer11cl778
 summer12cl778 summer13cl778 octtdec13cl778 jantmar09cl89 winter0910cl89 winter1011cl89
 winter1112cl89 winter1213cl89 summer09cl89 summer10cl89 summer11cl89 summer12cl89
 summer13cl89 octtdec13cl89 jantmar09cl1112 winter0910cl1112 winter1011cl1112
 winter1112cl1112 winter1213cl1112 summer09cl1112 summer10cl1112 summer11cl1112
 summer12cl1112 summer13cl1112 octtdec13cl1112 jantmar09cl1095 winter0910cl1095
 winter1011cl1095 winter1112cl1095 winter1213cl1095 summer09cl1095
 summer10cl1095 summer11cl1095 summer12cl1095 summer13cl1095 octtdec13cl1095
 jantmar09clsplt winter0910clsplt winter1011clsplt winter1112clsplt winter1213clsplt
 summer09clsplt summer10clsplt summer11clsplt summer12clsplt summer13clsplt octtdec13clsplt
 /method=enter nbsfair nres sb141 sb82 nbaseful nbaseprt nfirepl
 /method=enter  nbsf234 nbsf34  nbsf778  nbsf1112
	/method=enter midzonebloom highzonebloom srfxlowblockbloom srfxmidblockbloom srfxhighblockbloom  
 /method=enter garnogar nnobase sb73 sb122 sb190 sb54
/method=enter nbathsum garnogar garage2 masbsf frabsf nbsf56 nbsf1112
	/save pred (pred) resid (resid).
           sort cases by tnb pin.	
           value labels extcon 1 'Frame' 2 'Mason' 3 'Framas' 4 'Stucco'
          /basment 1 'Full' 2 'Slab' 3 'Part' 4 'Crwl'
          /b 1 'Rec' 2 'Apt' 3 'Unf'
          /gar 1 '1' 2 '1.5' 3 '2' 4 '2.5' 5 '3' 6 '3.5' 7 'non'
          /class 2 '2-02' 3 '2-03' 4 '2-04' 5 '2-05' 6 '2-06' 7 '2-07' 8 '2-08' 9 '2-09' 10 '2-10' 11'2-11' 12 '2-12' 34 '2-34' 78 '2-78' 95 '2-95'.
*compute perdif=(resid)/(amount1)*100.
*formats pred(COMMA9.0)
          /resid (f6.0).
exe.
*plot
/vertical='Sales Price' MIN(50000) MAX(1000000)
/horizontal='RESID'
/vsize=25
/hsize=140
/plot=amount1 with RESID.
*compute badsal=0.
*if perdif>20 badsal=1.
*if perdif<-20 badsal=1.
*select if badsal=1.
*set wid=125.
*set len=59.
*REPORT FORMAT=automatic list(1)
/title='Office of the Assessor'
        'Residential Regression Report'
        'Town is Bloom'
    /ltitle 'Report Ran On)Date' 
    /rtitle='PAGE)PAGE'
    /string=prop(area(2) '-' subarea(2) '-' block(3) '-' parcel(3))
          date(mos(2) '-'yr(2))
   /var=prop 'Prop' 'Index Number'(13)
       class 'Class' (label) (5)
       nghcde 'nbhb'(5)
       amount1 'Price'(7)
       pred 'Predicted'(9)
       resid 'Resid'(7)
       perdif '% DIF' (5)
       aos 'AOS' (3)
       sqftb 'Sqftb'(5)
       age 'Age'(4)
       extcon 'Extcon' (label)(6)
       sqftl 'Land'(6).
	
 	

