                                        *SPSS Regression.
		                		                   * Bremen 2014.



Get file='C:\Program Files\IBM\SPSS\Statistics\19\1\regt13mergefcl2a.sav'.
*select if (amount1>65000).
*select if (amount1<650000).
*select if (multi<1).
*select if sqftb<6000.

Compute year1=0.
If  (amount1>0) year1=1900 + yr.
if  (yr=13 and amount1>0) year1=2013.
if  (yr=12 and amount1>0) year1=2012.
if  (yr=11 and amount1>0) year1=2011. 
if  (yr=10 and amount1>0) year1=2010.
if  (yr=9 and amount1>0)  year1=2009.
if  (yr=8 and amount1>0)  year1=2008.    
if  (yr=7 and amount1>0)  year1=2007. 
If  (yr=6 and amount1>0)  year1=2006.
If  (yr=5 and amount1>0)  year1=2005.
If  (yr=4 and amount1>0)  year1=2004.
If  (yr=3 and amount1>0)  year1=2003.   
If  (yr=2 and amount1>0)  year1=2002.
If  (yr=1 and amount1>0)  year1=2001.
If  (yr=0 and amount1>0)  year1=2000.

*select if (year1>2008).
*select if puremarket=1.


COMPUTE FX = cumfile11121314.
if FX > 8 FX=8.
COMPUTE SRFX = sqrt(fx).
RECODE FX (SYSMIS=0).
*******************************************************************************************************************.

Compute bs=0.
*if age<10 and (amount1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.
if (pin=	28041120090000
or pin=	28042040410000
or pin=	28042070160000
or pin=	28083010120000
or pin=	28083030240000
or pin=	28084070270000
or pin=	28084120120000
or pin=	28092010270000
or pin=	28092030140000
or pin=	28092040170000
or pin=	28093120010000
or pin=	28093160120000
or pin=	28094000650000
or pin=	28094010470000
or pin=	28101040400000
or pin=	28101170160000
or pin=	28101200510000
or pin=	28101200550000
or pin=	28102010130000
or pin=	28102080190000
or pin=	28104060210000
or pin=	28104130480000
or pin=	28104160130000
or pin=	28122020310000
or pin=	28122170210000
or pin=	28123000540000
or pin=	28142120090000
or pin=	28144110030000
or pin=	28151170290000
or pin=	28161040080000
or pin=	28161050070000
or pin=	28161090100000
or pin=	28162150120000
or pin=	28163080240000
or pin=	28163130010000
or pin=	28164070060000
or pin=	28171110010000
or pin=	28171210130000
or pin=	28171250170000
or pin=	28172060040000
or pin=	28172070270000
or pin=	28172160160000
or pin=	28172300100000
or pin=	28174040130000
or pin=	28182050370000
or pin=	28182060070000
or pin=	28182180120000
or pin=	28193000670000
or pin=	28193120150000
or pin=	28193120200000
or pin=	28194020160000
or pin=	28194020210000
or pin=	28194020410000
or pin=	28194170220000
or pin=	28211050210000
or pin=	28211140110000
or pin=	28211210150000
or pin=	28211210360000
or pin=	28223130270000
or pin=	28223180160000
or pin=	28231250060000
or pin=	28234110510000
or pin=	28234230190000
or pin=	28274090330000
or pin=	28282150190000
or pin=	28283000340000
or pin=	28291050130000
or pin=	28294040040000
or pin=	28301000230000
or pin=	28301020230000
or pin=	28301110030000
or pin=	28302030060000
or pin=	28302070360000
or pin=	28302080090000
or pin=	28303070050000
or pin=	28303090260000
or pin=	28303100080000
or pin=	28303110140000
or pin=	28304030150000
or pin=	28304120870000
or pin=	28304170050000
or pin=	28304170060000
or pin=	28311130230000
or pin=	28312050040000
or pin=	28312070020000
or pin=	28312170070000
or pin=	28313060260000
or pin=	28314150120000
or pin=	28321000510000
or pin=	28321030200000
or pin=	28341030060000
or pin=	28341090180000
or pin=	28342170210000
or pin=	28344140270000
or pin=	28352050110000
or pin=	28352070080000
or pin=	28353020420000
or pin=	28354130100000
or pin=	28361130060000
or pin=	28363080010000
or pin=	28363140150000
or pin=	28364150040000)   bs=1.
*select if bs=0.

Compute N=1.
compute bsf=sqftb.
compute lsf=sqftl.

*Cutpoints for land square foot are 1.75 * median lsf.
*If lsf > 9200 lsf = 9200 + ((lsf - 9200)/3).
*If (class=8 or class=9) lsf=sqftl.

compute srbsf=sqrt(sqftb).
compute srage=sqrt(age).
compute srlsf=sqrt(sqftl).
compute apt=0.
compute noapt=0.
if class=11 or class=12 apt=1.
if apt=0 noapt=1.
compute nontown=0.
compute c1095=0.
if class=10 or class=95 c1095=1.
if c1095=0 nontown=1.
compute townlsf=0.
compute twnsrlsf=0.
if class=95 townlsf=lsf.
if class=95 twnsrlsf=srlsf.
compute srages=noapt*srage.

if class=95 and nghcde=20   lsf=4000.
if class=95 and nghcde=170  lsf=2850.
if class=95 and nghcde=171  lsf=2000.
if class=95 and nghcde=341  lsf=2400.
if class=95 and nghcde=342  lsf=2500.

compute n10=0.
compute n20=0.
compute n32=0.
compute n50=0.
compute n60=0.
compute n70=0.
compute n80=0.
compute n90=0.
compute n100=0.
compute n110=0.
compute n120=0.
compute n130=0.
compute n140=0.
compute n151=0.
compute n152=0.
compute n160=0.
compute n170=0.
compute n171=0.
compute n180=0.
compute n182=0.
compute n210=0.
compute n240=0.
compute n250=0.
compute n260=0.
compute n270=0.
compute n280=0.
compute n310=0.
compute n312=0.
compute n314=0.
compute n315=0.
compute n316=0.
compute n320=0.
compute n330=0.
compute n340=0.
compute n341=0.
compute n342=0.
compute n344=0.
compute n345=0.
compute n400=0.

compute sb10=0.
compute sb20=0.
compute sb32=0.
compute sb50=0.
compute sb60=0.
compute sb70=0.
compute sb80=0.
compute sb90=0.
compute sb100=0.
compute sb110=0.
compute sb120=0.
compute sb130=0.
compute sb140=0.
compute sb151=0.
compute sb152=0.
compute sb160=0.
compute sb170=0.
compute sb171=0.
compute sb180=0.
compute sb182=0.
compute sb210=0.
compute sb240=0.
compute sb250=0.
compute sb260=0.
compute sb270=0.
compute sb280=0.
compute sb310=0.
compute sb312=0.
compute sb314=0.
compute sb315=0.
compute sb316=0.
compute sb320=0.
compute sb330=0.
compute sb340=0.
compute sb341=0.
compute sb342=0.
compute sb344=0.
compute sb345=0.
compute sb400=0.

if (nghcde=10)   n10=1.
if (nghcde=20)   n20=1.
if (nghcde=32)   n32=1.
if (nghcde=50)   n50=1.
if (nghcde=60)   n60=1.
if (nghcde=70)   n70=1.
if (nghcde=80)   n80=1.
if (nghcde=90)   n90=1.
if (nghcde=100)  n100=1.
if (nghcde=110)  n110=1.
if (nghcde=120)  n120=1.
if (nghcde=130)  n130=1.
if (nghcde=140)  n140=1.
if (nghcde=151)  n151=1.
if (nghcde=152)  n152=1.
if (nghcde=160)  n160=1.
if (nghcde=170)  n170=1.
if (nghcde=171)  n171=1.
if (nghcde=180)  n180=1.
if (nghcde=182)  n182=1.
if (nghcde=210)  n210=1.
if (nghcde=240)  n240=1.
if (nghcde=250)  n250=1.
if (nghcde=260)  n260=1.
if (nghcde=270)  n270=1.
if (nghcde=280)  n280=1.
if (nghcde=310)  n310=1.
if (nghcde=312)  n312=1.
if (nghcde=314)  n314=1.
if (nghcde=315)  n315=1.
if (nghcde=316)  n316=1.
if (nghcde=320)  n320=1.
if (nghcde=330)  n330=1.
if (nghcde=340)  n340=1.
if (nghcde=341)  n341=1.
if (nghcde=342)  n342=1.
if (nghcde=344)  n344=1.
if (nghcde=345)  n345=1.
if (nghcde=400)  n400=1.

if (nghcde=10)  sb10=sqrt(bsf).
if (nghcde=20)  sb20=sqrt(bsf).
if (nghcde=32)  sb32=sqrt(bsf).
if (nghcde=50)  sb50=sqrt(bsf).
if (nghcde=60)  sb60=sqrt(bsf).
if (nghcde=70)  sb70=sqrt(bsf).
if (nghcde=80)  sb80=sqrt(bsf).
if (nghcde=90)  sb90=sqrt(bsf).
if (nghcde=100) sb100=sqrt(bsf).
if (nghcde=110) sb110=sqrt(bsf).
if (nghcde=120) sb120=sqrt(bsf).
if (nghcde=121) sb121=sqrt(bsf).
if (nghcde=130) sb130=sqrt(bsf).
if (nghcde=140) sb140=sqrt(bsf).
if (nghcde=151) sb151=sqrt(bsf).
if (nghcde=152) sb152=sqrt(bsf).
if (nghcde=160) sb160=sqrt(bsf).
if (nghcde=170) sb170=sqrt(bsf).
if (nghcde=171) sb171=sqrt(bsf).
if (nghcde=180) sb180=sqrt(bsf).
if (nghcde=182) sb182=sqrt(bsf).
if (nghcde=210) sb210=sqrt(bsf).
if (nghcde=240) sb240=sqrt(bsf).
if (nghcde=250) sb250=sqrt(bsf).
if (nghcde=260) sb260=sqrt(bsf).
if (nghcde=270) sb270=sqrt(bsf).
if (nghcde=280) sb280=sqrt(bsf).
if (nghcde=310) sb310=sqrt(bsf).
if (nghcde=312) sb312=sqrt(bsf).
if (nghcde=314) sb314=sqrt(bsf).
if (nghcde=315) sb315=sqrt(bsf).
if (nghcde=316) sb316=sqrt(bsf).
if (nghcde=320) sb320=sqrt(bsf).
if (nghcde=330) sb330=sqrt(bsf).
if (nghcde=340) sb340=sqrt(bsf).
if (nghcde=341) sb341=sqrt(bsf).
if (nghcde=342) sb342=sqrt(bsf).
if (nghcde=344) sb344=sqrt(bsf).
if (nghcde=345) sb345=sqrt(bsf).
if (nghcde=400) sb400=sqrt(bsf).

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.

compute bsf02=0.
compute bsf03=0.
compute bsf04=0.
compute bsf05=0.
compute bsf06=0.
compute bsf07=0.
compute bsf08=0.
compute bsf09=0.
compute bsf10=0.
compute bsf11=0.
compute bsf12=0.
compute bsf34=0.
compute bsf78=0.
compute bsf95=0.

if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.

compute bsf=sqftb.

If firepl>1 firepl=1.  
compute nfirepl=n*firepl.

Compute bathsum=fullbath + 0.25*halfbath.
Compute nbathsum=n*bathsum.

compute cenair=0.
if aircond =1 cenair=1.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.
Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute frabsf=frame*bsf.
Compute frmsbsf=framas*bsf.
Compute stubsf=stucco*bsf.
Compute masbsf=mason*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.
Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.


Compute garage1=0.
Compute garage2=0.
Compute garage3=0.
Compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1.  
Compute biggar=0.
if garage3=1 or garage4=1 biggar=1.

compute n=1.0.
if (nghcde=10)  n= 1.15.
if (nghcde=20)  n= 1.08.
if (nghcde=32)  n= 1.51.
if (nghcde=50)  n= 1.07.
if (nghcde=60)  n= 1.64.
if (nghcde=70)  n= 1.19.
if (nghcde=80)  n= 1.25.
if (nghcde=90)  n= 1.87.
if (nghcde=100) n= 1.89.
if (nghcde=110) n= 1.29.
if (nghcde=120) n= 1.49.
if (nghcde=130) n= 1.66.
if (nghcde=140) n= 1.87.
if (nghcde=151) n= 1.19.
if (nghcde=152) n= 1.74.
if (nghcde=160) n= 1.90.
if (nghcde=170) n= 1.57.
if (nghcde=171) n= 1.77.
if (nghcde=180) n= 1.99.
if (nghcde=182) n= 2.79.
if (nghcde=210) n= 1.82.
if (nghcde=240) n= 1.14.
if (nghcde=250) n= 1.38.
if (nghcde=260) n= 1.08.
if (nghcde=270) n= 1.31.
if (nghcde=280) n= 1.78.
if (nghcde=310) n= 1.98.
if (nghcde=312) n= 1.55.
if (nghcde=314) n= 1.23.
if (nghcde=315) n= 2.35.
if (nghcde=316) n= 2.29.
if (nghcde=320) n= 1.45.
if (nghcde=330) n= 1.21.
if (nghcde=340) n= 1.27.
if (nghcde=341) n= 1.92.   
if (nghcde=342) n= 2.97.
if (nghcde=344) n= 1.97.
if (nghcde=345) n= 2.10. 
if (nghcde=400) n= 1.52.

compute nbsf=n*bsf.
compute nlsf=n*lsf.
compute nsrbsf=n*srbsf.
compute nsrlsf=n*srlsf.
compute nbsfs=noapt*nbsf.
compute nsrbsfs=n*srbsf*noapt.
compute nsrage=n*srage.
compute nsrages=n*srage*noapt.

Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).


Compute winter0910=0.
if (mos > 9 and yr=9) or (mos <= 3 and yr=10) winter0910=1.
Compute winter1011=0.
if (mos > 9 and yr=10) or (mos <= 3 and yr=11) winter1011=1.
Compute winter1112=0.
if (mos > 9 and yr=11) or (mos <= 3 and yr=12) winter1112=1.
Compute winter1213=0.
if (mos > 9 and yr=12) or (mos <= 3 and yr=13) winter1213=1.
Compute summer09=0.
if (mos > 3 and yr=9) and (mos <= 9 and yr=9) summer09=1. 
Compute summer10=0.
if (mos > 3 and yr=10) and (mos <= 9 and yr=10) summer10=1.
Compute summer11=0.
if (mos > 3 and yr=11) and (mos <= 9 and yr=11) summer11=1.
Compute summer12=0.
if (mos > 3 and yr=12) and (mos <= 9 and yr=12) summer12=1.
Compute summer13=0.
if (mos > 3 and yr=13) and (mos <= 9 and yr=13) summer13=1.
Compute jantmar09=0.
if (year1=2009 and (mos>=1 and mos<=3)) jantmar09=1. 
Compute octtdec13=0.
if (year1=2013 and (mos>=11 and mos<=13)) octtdec13=1.

Compute jantmar09cl234=jantmar09*cl234.
Compute winter0910cl234=winter0910*cl234.
Compute winter1011cl234=winter1011*cl234.
Compute winter1112cl234=winter1112*cl234.
Compute winter1213cl234=winter1213*cl234.
Compute summer09cl234=summer09*cl234.
Compute summer10cl234=summer10*cl234.
Compute summer11cl234=summer11*cl234.
Compute summer12cl234=summer12*cl234.
Compute summer13cl234=summer13*cl234.
Compute octtdec13cl234=octtdec13*cl234.

Compute jantmar09cl56=jantmar09*cl56.
Compute winter0910cl56=winter0910*cl56.
Compute winter1011cl56=winter1011*cl56.
Compute winter1112cl56=winter1112*cl56.
Compute winter1213cl56=winter1213*cl56.
Compute summer09cl56=summer09*cl56.
Compute summer10cl56=summer10*cl56.
Compute summer11cl56=summer11*cl56.
Compute summer12cl56=summer12*cl56.
Compute summer13cl56=summer13*cl56.
Compute octtdec13cl56=octtdec13*cl56.

Compute jantmar09cl778=jantmar09*cl778.
Compute winter0910cl778=winter0910*cl778.
Compute winter1011cl778=winter1011*cl778.
Compute winter1112cl778=winter1112*cl778.
Compute winter1213cl778=winter1213*cl778.
Compute summer09cl778=summer09*cl778.
Compute summer10cl778=summer10*cl778.
Compute summer11cl778=summer11*cl778.
Compute summer12cl778=summer12*cl778.
Compute summer13cl778=summer13*cl778.
Compute octtdec13cl778=octtdec13*cl778.

Compute jantmar09cl89=jantmar09*cl89.
Compute winter0910cl89=winter0910*cl89.
Compute winter1011cl89=winter1011*cl89.
Compute winter1112cl89=winter1112*cl89.
Compute winter1213cl89=winter1213*cl89.
Compute summer09cl89=summer09*cl89.
Compute summer10cl89=summer10*cl89.
Compute summer11cl89=summer11*cl89.
Compute summer12cl89=summer12*cl89.
Compute summer13cl89=summer13*cl89.
Compute octtdec13cl89=octtdec13*cl89.

Compute jantmar09cl1112=jantmar09*cl1112.
Compute winter0910cl1112=winter0910*cl1112.
Compute winter1011cl1112=winter1011*cl1112.
Compute winter1112cl1112=winter1112*cl1112.
Compute winter1213cl1112=winter1213*cl1112.
Compute summer09cl1112=summer09*cl1112.
Compute summer10cl1112=summer10*cl1112.
Compute summer11cl1112=summer11*cl1112.
Compute summer12cl1112=summer12*cl1112.
Compute summer13cl1112=summer13*cl1112.
Compute octtdec13cl1112=octtdec13*cl1112.

Compute jantmar09cl1095=jantmar09*cl1095.
Compute winter0910cl1095=winter0910*cl1095.
Compute winter1011cl1095=winter1011*cl1095.
Compute winter1112cl1095=winter1112*cl1095.
Compute winter1213cl1095=winter1213*cl1095.
Compute summer09cl1095=summer09*cl1095.
Compute summer10cl1095=summer10*cl1095.
Compute summer11cl1095=summer11*cl1095.
Compute summer12cl1095=summer12*cl1095.
Compute summer13cl1095=summer13*cl1095.
Compute octtdec13cl1095=octtdec13*cl1095.

Compute jantmar09clsplt=jantmar09*clsplt.
Compute winter0910clsplt=winter0910*clsplt.
Compute winter1011clsplt=winter1011*clsplt.
Compute winter1112clsplt=winter1112*clsplt.
Compute winter1213clsplt=winter1213*clsplt.
Compute summer09clsplt=summer09*clsplt.
Compute summer10clsplt=summer10*clsplt.
Compute summer11clsplt=summer11*clsplt.
Compute summer12clsplt=summer12*clsplt.
Compute summer13clsplt=summer13*clsplt.
Compute octtdec13clsplt=octtdec13*clsplt.


Compute lowzonebremen=0.
if town=13 and  (nghcde=342 or nghcde=140  or nghcde=180  or nghcde=182 
or nghcde=170 or nghcde=210 or nghcde=310 or nghcde=100 or nghcde=341
or nghcde=315 or nghcde=130 or nghcde=316 )  lowzonebremen=1.                         	

Compute midzonebremen=0.
if town=13 and (nghcde=171 or nghcde=10 or nghcde=32 or nghcde=312
or nghcde=152 or nghcde=20 or nghcde=70 or nghcde=80 or nghcde=280
or nghcde=400 or nghcde=160)   midzonebremen=1. 

Compute highzonebremen=0.
if town=13 and (nghcde=90 or nghcde=314 or nghcde=60 
or nghcde=151 or nghcde=344 or nghcde=330  or nghcde=50  or nghcde=320
or nghcde=270 or nghcde=120  or nghcde=110 or nghcde=240 or nghcde=340 or nghcde=260 or nghcde=250 or nghcde=345)   highzonebremen=1.


Compute srfxlowblockbremen=0.
if lowzonebremen=1 srfxlowblockbremen=srfx*lowzonebremen.

Compute srfxmidblockbremen=0.
if midzonebremen=1 srfxmidblockbremen=srfx*midzonebremen.

Compute srfxhighblockbremen=0.
if highzonebremen=1 srfxhighblockbremen=srfx*highzonebremen.


if num=6 bnum=0.
if num=0 bnum=0.
if num=1 bnum=2.
if num=2 bnum=3.
if num=3 bnum=4.
if num=4 bnum=5.
if num=5 bnum=6.

compute res=bnum - comm.
compute bnumb=bnum*bsf.
compute nres=n*res.

Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
compute nbasfull=n*basefull.
compute nbaspart=n*basepart.

Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.
Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
Compute nnobase=n*nobase.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
compute nluxbsf=0.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.

Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute repabove=0.
Compute repbelow=0.
Compute repave=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabsf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.
Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.
Compute twosty=0.
If class07=1 or class08=1 or class78=1 twosty=1.

compute b=1.

compute mv = 166545.9485	
+ 10495.53711*nbathsum
+ 168.9547321*nsrlsf
+ 0.339619318*nbsfair
- 76864.96351*highzonebremen
+ 4522.56342*nbasfull
+ 35.97827311*frmsbsf
- 36078.69567*midzonebremen
- 5973.837436*nsrage
+ 1060.074305*sb171
- 2168.168678*sb20
- 345.3313263*sb130
+ 315.2205584*nbsf1095
+ 6034.050699*nfirepl
- 500.3665447*sb250
+ 39.85150992*masbsf
- 344.82929*sb110
+ 32.51234488*frabsf
+ 598.6197294*sb152
+ 961.7182304*sb151
+ 4248.192079*garage2
+ 1775.26543*sb90
+ 314.9281573*sb312
- 468.2896177*sb341
+ 63.74624151*nbsf234
- 1048.134478*nbaspart
- 1820.6805993*sb10
- 682.4157051*sb70
+ 937.82737*nbsf89
+ 759.004217*sb182
+ 597.3538594*sb314
+ 241.4802905*sb342
+ 358.6515368*sb180
+ 402.2521542*nbsf56
+ 349.5591181*nbsf34
+ 20.79788178*stubsf
+ 9842.382828*biggar
- 5193.126958*nnobase
- 442.8190218*srfxlowblockbremen
- 1212.106884*srfxmidblockbremen
- 833.997061*srfxhighblockbremen
- 2121.221924*garage1
+ 520.0729682*nbsf1112
+ 29.36316819*sb100
+ 95.65086692*sb140
+ 129.7295007*sb170
+ 39.60292582*sb210
+ 690.0683481*sb330
- 279.6522173*sb270.


save outfile='C:\Program Files\IBM\SPSS\Statistics\19\1\mv13.sav'/keep town pin mv.


	
 
