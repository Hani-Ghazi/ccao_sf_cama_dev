                             *SPSS Regression.
		 	        *Lemont, Orland, and Palos Regression 2014.                                                            


Get file='C:\Program Files\IBM\SPSS\Statistics\19\1\lempalorlmergefcl2a.sav'.
*select if (amount1>145000).
*select if (amount1<890000).
*select if (multi<1).
*select if sqftb<6000.
Compute year1=0.
If  (amount1>0) year1=1900 + yr.
If  (yr=13 and amount1>0)  year1=2013.
If  (yr=12 and amount1>0)  year1=2012.
if  (yr=11 and amount1>0)  year1=2011. 
If  (yr=10 and amount1>0)  year1=2010.
If  (yr=9 and amount1>0)  year1=2009.
If  (yr=8 and amount1>0)  year1=2008. 
If  (yr=7 and amount1>0)  year1=2007. 
If  (yr=6 and amount1>0)  year1=2006.
If  (yr=5 and amount1>0)  year1=2005.
If  (yr=4 and amount1>0)  year1=2004.
If  (yr=3 and amount1>0)  year1=2003.   
If  (yr=2 and amount1>0)  year1=2002.
If  (yr=1 and amount1>0)  year1=2001.
If  (yr=0 and amount1>0)  year1=2000.

COMPUTE FX = cumfile11121314.
If FX > 6 FX = 6.
COMPUTE SRFX = sqrt(fx).
RECODE FX (SYSMIS=0).
*******************************************************************************************************************.

*select if (year1>2008). 
*select if puremarket=1.

set mxcells=2000500.
Compute bs=0.
if age<10 and (amount1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.

if (pin=	22204000100000
or pin=	22204410080000
or pin=	22212000460000
or pin=	22291010010000
or pin=	22291030050000
or pin=	22291040050000
or pin=	22291090440000
or pin=	22291100250000
or pin=	22291140270000
or pin=	22291210110000
or pin=	22291210140000
or pin=	22292020410000
or pin=	22292150220000
or pin=	22323000420000
or pin=	22323010130000
or pin=	22233010050000
or pin=	22281041070000
or pin=	22292330380000
or pin=	22282040070000
or pin=	22293040010000
or pin=	22293110050000
or pin=	22294090110000
or pin=	22353020130000
or pin=	22241010130000
or pin=	22241010330000
or pin=	22241010340000
or pin=	22243010110000
or pin=	22311000100000
or pin=	22311120080000
or pin=	22321070060000
or pin=	22321080050000
or pin=	22322030020000
or pin=	22322030180000
or pin=	22332030110000
or pin=	22343040110000
or pin=	22274090110000
or pin=	27181050260000
or pin=	27184030060000
or pin=	27184250120000
or pin=	27082000160000
or pin=	27082010090000
or pin=	27082010270000
or pin=	27082030150000
or pin=	27082030220000
or pin=	27082100040000
or pin=	27082110160000
or pin=	27082140080000
or pin=	27084060150000
or pin=	27084070080000
or pin=	27203020970000
or pin=	27203270070000
or pin=	27311050160000
or pin=	27314090480000
or pin=	27092000180000
or pin=	27101090070000
or pin=	27101090100000
or pin=	27102060120000
or pin=	27161070220000
or pin=	27033010070000
or pin=	27033080080000
or pin=	27034020050000
or pin=	27034020240000
or pin=	27024050080000
or pin=	27111140160000
or pin=	27104040250000
or pin=	27134030740000
or pin=	27141080060000
or pin=	27141090010000
or pin=	27091020050000
or pin=	27091120150000
or pin=	27091140370000
or pin=	27091180040000
or pin=	27091210050000
or pin=	27092170130000
or pin=	27094060010000
or pin=	27222060060000
or pin=	27222070130000
or pin=	27233040040000
or pin=	27253030190000
or pin=	27271040430000
or pin=	27271050120000
or pin=	27271060030000
or pin=	27272040060000
or pin=	27241130170000
or pin=	27262100070000
or pin=	27263210050000
or pin=	27353110380000
or pin=	27223140080000
or pin=	27272010230000
or pin=	27272080110000
or pin=	27272160040000
or pin=	27272190090000
or pin=	27272240100000
or pin=	27352210010000
or pin=	27243130270000
or pin=	27244070120000
or pin=	27252210120000
or pin=	27254120400000
or pin=	27212030820000
or pin=	27212050350000
or pin=	27053070040000
or pin=	27081020040000
or pin=	27171010130000
or pin=	27174080140000
or pin=	27291000380000
or pin=	27291110090000
or pin=	27302020040000
or pin=	27302030290000
or pin=	27022030110000
or pin=	27253000070000
or pin=	27253170060000
or pin=	27343080170000
or pin=	27344070140000
or pin=	27344140020000
or pin=	23021030100000
or pin=	23141000270000
or pin=	23141050590000
or pin=	23021050110000
or pin=	23022010660000
or pin=	23022070240000
or pin=	23024000220000
or pin=	23024100160000
or pin=	23024180150000
or pin=	23114011210000
or pin=	23114060260000
or pin=	23114120420000
or pin=	23114120440000
or pin=	23114130120000
or pin=	23123060310000
or pin=	23142050220000
or pin=	23142180300000
or pin=	23142240020000
or pin=	23132040020000
or pin=	23132050120000
or pin=	23132050170000
or pin=	23011080330000
or pin=	23352060220000
or pin=	23023030850000
or pin=	23023040010000
or pin=	23344070090000
or pin=	23332000270000
or pin=	23304010230000
or pin=	23104110080000
or pin=	23353140170000
or pin=	23233010490000
or pin=	23234110100000
or pin=	23234130210000
or pin=	23243002450000
or pin=	23261020030000
or pin=	23261120050000
or pin=	23262030060000
or pin=	23262090080000
or pin=	23264080150000
or pin=	23264100140000
or pin=	23264120080000
or pin=	23351030220000
or pin=	23351040660000
or pin=	23351090020000
or pin=	23352020030000
or pin=	23352040110000
or pin=	23352050030000
or pin=	23263040030000
or pin=	23263090070000
or pin=	23271080020000
or pin=	23271160050000
or pin=	23272020040000
or pin=	23272050080000
or pin=	23272050190000
or pin=	23251110080000
or pin=	23251120140000
or pin=	23252040050000
or pin=	23252230310000
or pin=	23252270060000
or pin=	23253000120000
or pin=	23253011090000
or pin=	23254130060000
or pin=	23341000850000)	bs=1.

*select if bs=0.

Compute N=1.

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.

compute bsf=sqftb.

compute bsf02=0.
compute bsf03=0.
compute bsf04=0.
compute bsf05=0.
compute bsf06=0.
compute bsf07=0.
compute bsf08=0.
compute bsf09=0.
compute bsf10=0.
compute bsf11=0.
compute bsf12=0.
compute bsf34=0.
compute bsf78=0.
compute bsf95=0.

if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.

compute tnb=(town*1000)+nghcde.

if (tnb= 19010 ) n=2.30.
if (tnb= 19011 ) n=3.55.
if (tnb= 19020 ) n=2.82.
if (tnb= 19025 ) n=3.52.
if (tnb= 19030 ) n=3.39.
if (tnb= 19032 ) n=2.18.
if (tnb= 19038 ) n=3.84.
if (tnb= 19040 ) n=5.48.
if (tnb= 19050 ) n=2.67.
if (tnb= 19060 ) n=4.42.
if (tnb= 19061 ) n=3.83.
if (tnb= 19100 ) n=5.33.

if (tnb= 28011 ) n=2.73.
if (tnb= 28012 ) n=5.48.
if (tnb= 28013 ) n=3.17.
if (tnb= 28014 ) n=3.12.
if (tnb= 28015 ) n=2.85.
if (tnb= 28022 ) n=1.97.
if (tnb= 28030 ) n=3.38.
if (tnb= 28031 ) n=2.45. 
if (tnb= 28032 ) n=2.30.
if (tnb= 28034 ) n=4.44.
if (tnb= 28036 ) n=2.45.
if (tnb= 28037 ) n=3.40.
if (tnb= 28039 ) n=2.31.
if (tnb= 28040 ) n=1.83.
if (tnb= 28043 ) n=2.32.
if (tnb= 28045 ) n=2.90.
if (tnb= 28046 ) n=2.25.
if (tnb= 28047 ) n=2.75.
if (tnb= 28050 ) n=1.93.
if (tnb= 28061 ) n=1.91.
if (tnb= 28088 ) n=2.49.
if (tnb= 28099 ) n=2.18.
if (tnb= 28100 ) n=4.30.
if (tnb= 28101 ) n=3.88.
if (tnb= 28102 ) n=3.20.
if (tnb= 28103 ) n=2.46.
if (tnb= 28104 ) n=3.05. 

if (tnb= 30010 ) n=2.30.
if (tnb= 30011 ) n=2.73.
if (tnb= 30012 ) n=2.39.
if (tnb= 30020 ) n=2.60.
if (tnb= 30022 ) n=2.10.
if (tnb= 30030 ) n=1.98.
if (tnb= 30031 ) n=2.63.
if (tnb= 30040 ) n=2.83.
if (tnb= 30041 ) n=3.46.
if (tnb= 30042 ) n=1.55.
if (tnb= 30050 ) n=3.05.
if (tnb= 30051 ) n=3.30.
if (tnb= 30052 ) n=4.08.
if (tnb= 30053 ) n=2.89.
if (tnb= 30054 ) n=3.21.
if (tnb= 30060 ) n=2.77.
if (tnb= 30061 ) n=2.87.
if (tnb= 30062 ) n=4.17.
if (tnb= 30063 ) n=4.95.
if (tnb= 30070 ) n=2.81.
if (tnb= 30071 ) n=4.30.
if (tnb= 30080 ) n=2.90.
if (tnb= 30082 ) n=7.53.
if (tnb= 30092 ) n=3.75.

compute n19010=0.
compute n19011=0.
compute n19020=0.
compute n19025=0.
compute n19030=0.
compute n19032=0.
compute n19038=0.
compute n19040=0.
compute n19050=0.
compute n19060=0.
compute n19061=0.
compute n19071=0.
compute n19100=0.

compute n28011=0.
compute n28012=0.
compute n28013=0.
compute n28014=0.
compute n28015=0.
compute n28022=0.
compute n28030=0.
compute n28031=0. 
compute n28032=0.
compute n28034=0.
compute n28036=0.
compute n28037=0.
compute n28039=0.
compute n28040=0.
compute n28043=0.
compute n28045=0.
compute n28046=0.
compute n28047=0.
compute n28050=0.
compute n28061=0.
compute n28088=0.
compute n28099=0.
compute n28100=0.
compute n28101=0.
compute n28102=0.
compute n28103=0.
compute n28104=0. 

compute n30010=0.
compute n30011=0.
compute n30012=0.
compute n30020=0.
compute n30022=0.
compute n30030=0.
compute n30031=0.
compute n30038=0.
compute n30040=0. 
compute n30041=0.
compute n30042=0.
compute n30050=0.
compute n30051=0.
compute n30052=0.
compute n30053=0.
compute n30054=0.
compute n30055=0.
compute n30060=0.
compute n30061=0.
compute n30062=0.
compute n30063=0.
compute n30070=0.
compute n30071=0.
compute n30080=0.
compute n30082=0.
compute n30092=0.

compute b19010=0.
compute b19011=0.
compute b19020=0.
compute b19025=0.
compute b19030=0.
compute b19032=0.
compute b19040=0.
compute b19050=0.
compute b19038=0.
compute b19060=0.
compute b19061=0.
compute b19071=0.
compute b19100=0.

compute b28011=0.
compute b28012=0.
compute b28013=0.
compute b28014=0.
compute b28015=0.
compute b28022=0.
compute b28030=0.
compute b28031=0. 
compute b28032=0.
compute b28034=0.
compute b28036=0.
compute b28037=0.
compute b28039=0.
compute b28040=0.
compute b28043=0.
compute b28045=0.
compute b28046=0.
compute b28047=0.
compute b28050=0.
compute b28061=0.
compute b28088=0.
compute b28099=0.
compute b28100=0.
compute b28101=0.
compute b28102=0.
compute b28103=0.
compute b28104=0. 

compute b30010=0.
compute b30011=0.
compute b30012=0.
compute b30020=0.
compute b30022=0.
compute b30030=0.
compute b30031=0.
compute b30040=0. 
compute b30041=0.
compute b30042=0.
compute b30050=0.
compute b30051=0.
compute b30052=0.
compute b30053=0.
compute b30054=0.
compute b30060=0.
compute b30061=0.
compute b30062=0.
compute b30063=0.
compute b30070=0.
compute b30071=0.
compute b30080=0.
compute b30082=0.
compute b30092=0.


compute sb19010=0.
compute sb19011=0.
compute sb19020=0.
compute sb19025=0.
compute sb19030=0.
compute sb19032=0.
compute sb19040=0.
compute sb19050=0.
compute sb19038=0.
compute sb19060=0.
compute sb19061=0.
compute sb19071=0.
compute sb19100=0.

compute sb28011=0.
compute sb28012=0.
compute sb28013=0.
compute sb28014=0.
compute sb28015=0.
compute sb28022=0.
compute sb28030=0.
compute sb28031=0. 
compute sb28032=0.
compute sb28034=0.
compute sb28036=0.
compute sb28037=0.
compute sb28039=0.
compute sb28040=0.
compute sb28043=0.
compute sb28045=0.
compute sb28046=0.
compute sb28047=0.
compute sb28050=0.
compute sb28061=0.
compute sb28088=0.
compute sb28099=0.
compute sb28100=0.
compute sb28101=0.
compute sb28102=0.
compute sb28103=0.
compute sb28104=0. 

compute sb30010=0.
compute sb30011=0.
compute sb30012=0.
compute sb30020=0.
compute sb30022=0.
compute sb30030=0.
compute sb30031=0.
compute sb30040=0. 
compute sb30041=0.
compute sb30042=0.
compute sb30050=0.
compute sb30051=0.
compute sb30052=0.
compute sb30053=0.
compute sb30054=0.
compute sb30060=0.
compute sb30061=0.
compute sb30062=0.
compute sb30063=0.
compute sb30070=0.
compute sb30071=0.
compute sb30080=0.
compute sb30082=0.
compute sb30092=0.



if (tnb= 19010 ) n19010=1.
if (tnb= 19011 ) n19011=1.
if (tnb= 19020 ) n19020=1.
if (tnb= 19025 ) n19025=1.
if (tnb= 19030 ) n19030=1.
if (tnb= 19032 ) n19032=1.
if (tnb= 19038 ) n19038=1.
if (tnb= 19040 ) n19040=1.
if (tnb= 19050 ) n19050=1.
if (tnb= 19060 ) n19060=1.
if (tnb= 19061 ) n19061=1.
if (tnb= 19071 ) n19071=1.
if (tnb= 19100 ) n19100=1.

if (tnb= 28011 ) n28011=1.
if (tnb= 28012 ) n28012=1.
if (tnb= 28013 ) n28013=1.
if (tnb= 28014 ) n28014=1.
if (tnb= 28015 ) n28015=1.
if (tnb= 28022 ) n28022=1.
if (tnb= 28030 ) n28030=1.
if (tnb= 28031 ) n28031=1. 
if (tnb= 28032 ) n28032=1.
if (tnb= 28034 ) n28034=1.
if (tnb= 28036 ) n28036=1.
if (tnb= 28037 ) n28037=1.
if (tnb= 28039 ) n28039=1.
if (tnb= 28040 ) n28040=1.
if (tnb= 28043 ) n28043=1.
if (tnb= 28045 ) n28045=1.
if (tnb= 28046 ) n28046=1.
if (tnb= 28047 ) n28047=1.
if (tnb= 28050 ) n28050=1.
if (tnb= 28061 ) n28061=1.
if (tnb= 28088 ) n28088=1.
if (tnb= 28099 ) n28099=1.
if (tnb= 28100 ) n28100=1.
if (tnb= 28101 ) n28101=1.
if (tnb= 28102 ) n28102=1.
if (tnb= 28103 ) n28103=1.
if (tnb= 28104 ) n28104=1. 

if (tnb= 30010 ) n30010=1.
if (tnb= 30011 ) n30011=1.
if (tnb= 30012 ) n30012=1.
if (tnb= 30020 ) n30020=1.
if (tnb= 30022 ) n30022=1.
if (tnb= 30030 ) n30030=1.
if (tnb= 30031 ) n30031=1.
if (tnb= 30040 ) n30040=1. 
if (tnb= 30041 ) n30041=1.
if (tnb= 30042 ) n30042=1.
if (tnb= 30050 ) n30050=1.
if (tnb= 30051 ) n30051=1.
if (tnb= 30052 ) n30052=1.
if (tnb= 30053 ) n30053=1.
if (tnb= 30054 ) n30054=1.
if (tnb= 30055 ) n30055=1.
if (tnb= 30060 ) n30060=1.
if (tnb= 30061 ) n30061=1.
if (tnb= 30062 ) n30062=1.
if (tnb= 30063 ) n30063=1.
if (tnb= 30070 ) n30070=1.
if (tnb= 30080 ) n30080=1.
if (tnb= 30082 ) n30082=1.
if (tnb= 30092 ) n30092=1.

if (tnb= 19010 ) sb19010=sqrt(bsf).
if (tnb= 19011 ) sb19011=sqrt(bsf).
if (tnb= 19020 ) sb19020=sqrt(bsf).
if (tnb= 19025 ) sb19025=sqrt(bsf).
if (tnb= 19030 ) sb19030=sqrt(bsf).
if (tnb= 19032 ) sb19032=sqrt(bsf).
if (tnb= 19040 ) sb19040=sqrt(bsf).
if (tnb= 19050 ) sb19050=sqrt(bsf).
if (tnb= 19038 ) sb19038=sqrt(bsf).
if (tnb= 19060 ) sb19060=sqrt(bsf).
if (tnb= 19061 ) sb19061=sqrt(bsf).
if (tnb= 19071 ) sb19071=sqrt(bsf).
if (tnb= 19100 ) sb19100=sqrt(bsf).

if (tnb= 28011 ) sb28011=sqrt(bsf).
if (tnb= 28012 ) sb28012=sqrt(bsf).
if (tnb= 28013 ) sb28013=sqrt(bsf).
if (tnb= 28014 ) sb28014=sqrt(bsf).
if (tnb= 28015 ) sb28015=sqrt(bsf).
if (tnb= 28022 ) sb28022=sqrt(bsf).
if (tnb= 28030 ) sb28030=sqrt(bsf).
if (tnb= 28031 ) sb28031=sqrt(bsf). 
if (tnb= 28032 ) sb28032=sqrt(bsf).
if (tnb= 28034 ) sb28034=sqrt(bsf).
if (tnb= 28036 ) sb28036=sqrt(bsf).
if (tnb= 28037 ) sb28037=sqrt(bsf).
if (tnb= 28039 ) sb28039=sqrt(bsf).
if (tnb= 28040 ) sb28040=sqrt(bsf).
if (tnb= 28043 ) sb28043=sqrt(bsf).
if (tnb= 28045 ) sb28045=sqrt(bsf).
if (tnb= 28046 ) sb28046=sqrt(bsf).
if (tnb= 28047 ) sb28047=sqrt(bsf).
if (tnb= 28050 ) sb28050=sqrt(bsf).
if (tnb= 28061 ) sb28061=sqrt(bsf).
if (tnb= 28088 ) sb28088=sqrt(bsf).
if (tnb= 28099 ) sb28099=sqrt(bsf).
if (tnb= 28100 ) sb28100=sqrt(bsf).
if (tnb= 28101 ) sb28101=sqrt(bsf).
if (tnb= 28102 ) sb28102=sqrt(bsf).
if (tnb= 28103 ) sb28103=sqrt(bsf).
if (tnb= 28104 ) sb28104=sqrt(bsf). 

if (tnb= 30010 ) sb30010=sqrt(bsf).
if (tnb= 30011 ) sb30011=sqrt(bsf).
if (tnb= 30012 ) sb30012=sqrt(bsf).
if (tnb= 30020 ) sb30020=sqrt(bsf).
if (tnb= 30022 ) sb30022=sqrt(bsf).
if (tnb= 30030 ) sb30030=sqrt(bsf).
if (tnb= 30031 ) sb30031=sqrt(bsf).
if (tnb= 30040 ) sb30040=sqrt(bsf). 
if (tnb= 30041 ) sb30041=sqrt(bsf).
if (tnb= 30042 ) sb30042=sqrt(bsf).
if (tnb= 30050 ) sb30050=sqrt(bsf).
if (tnb= 30051 ) sb30051=sqrt(bsf).
if (tnb= 30052 ) sb30052=sqrt(bsf).
if (tnb= 30053 ) sb30053=sqrt(bsf).
if (tnb= 30054 ) sb30054=sqrt(bsf).
if (tnb= 30060 ) sb30060=sqrt(bsf).
if (tnb= 30061 ) sb30061=sqrt(bsf).
if (tnb= 30062 ) sb30062=sqrt(bsf).
if (tnb= 30063 ) sb30063=sqrt(bsf).
if (tnb= 30070 ) sb30070=sqrt(bsf).
if (tnb= 30071 ) sb30071=sqrt(bsf).
if (tnb= 30080 ) sb30080=sqrt(bsf).
if (tnb= 30082 ) sb30082=sqrt(bsf).
if (tnb= 30092 ) sb30092=sqrt(bsf).

compute lem=0.
compute pal=0.
compute orl=0.
if town=19 lem=1.
if town=28 orl=1.
if town=30 pal=1.

compute bsf=sqftb.
compute srbsf=sqrt(sqftb).
compute srage=sqrt(age).
compute nsrbsf=n*sqrt(bsf).

Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).

Compute winter0910=0.
if (mos > 9 and yr=9) or (mos <= 3 and yr=10) winter0910=1.
Compute winter1011=0.
if (mos > 9 and yr=10) or (mos <= 3 and yr=11) winter1011=1.
Compute winter1112=0.
if (mos > 9 and yr=11) or (mos <= 3 and yr=12) winter1112=1.
Compute winter1213=0.
if (mos > 9 and yr=12) or (mos <= 3 and yr=13) winter1213=1.
Compute summer09=0.
if (mos > 3 and yr=9) and (mos <= 9 and yr=9) summer09=1. 
Compute summer10=0.
if (mos > 3 and yr=10) and (mos <= 9 and yr=10) summer10=1.
Compute summer11=0.
if (mos > 3 and yr=11) and (mos <= 9 and yr=11) summer11=1.
Compute summer12=0.
if (mos > 3 and yr=12) and (mos <= 9 and yr=12) summer12=1.
Compute summer13=0.
if (mos > 3 and yr=13) and (mos <= 9 and yr=13) summer13=1.
Compute jantmar09=0.
if (year1=2009 and (mos>=1 and mos<=3)) jantmar09=1. 
Compute octtdec13=0.
if (year1=2013 and (mos>=11 and mos<=13)) octtdec13=1.

Compute jantmar09cl234=jantmar09*cl234.
Compute winter0910cl234=winter0910*cl234.
Compute winter1011cl234=winter1011*cl234.
Compute winter1112cl234=winter1112*cl234.
Compute winter1213cl234=winter1213*cl234.
Compute summer09cl234=summer09*cl234.
Compute summer10cl234=summer10*cl234.
Compute summer11cl234=summer11*cl234.
Compute summer12cl234=summer12*cl234.
Compute summer13cl234=summer13*cl234.
Compute octtdec13cl234=octtdec13*cl234.

Compute jantmar09cl56=jantmar09*cl56.
Compute winter0910cl56=winter0910*cl56.
Compute winter1011cl56=winter1011*cl56.
Compute winter1112cl56=winter1112*cl56.
Compute winter1213cl56=winter1213*cl56.
Compute summer09cl56=summer09*cl56.
Compute summer10cl56=summer10*cl56.
Compute summer11cl56=summer11*cl56.
Compute summer12cl56=summer12*cl56.
Compute summer13cl56=summer13*cl56.
Compute octtdec13cl56=octtdec13*cl56.

Compute jantmar09cl778=jantmar09*cl778.
Compute winter0910cl778=winter0910*cl778.
Compute winter1011cl778=winter1011*cl778.
Compute winter1112cl778=winter1112*cl778.
Compute winter1213cl778=winter1213*cl778.
Compute summer09cl778=summer09*cl778.
Compute summer10cl778=summer10*cl778.
Compute summer11cl778=summer11*cl778.
Compute summer12cl778=summer12*cl778.
Compute summer13cl778=summer13*cl778.
Compute octtdec13cl778=octtdec13*cl778.

Compute jantmar09cl89=jantmar09*cl89.
Compute winter0910cl89=winter0910*cl89.
Compute winter1011cl89=winter1011*cl89.
Compute winter1112cl89=winter1112*cl89.
Compute winter1213cl89=winter1213*cl89.
Compute summer09cl89=summer09*cl89.
Compute summer10cl89=summer10*cl89.
Compute summer11cl89=summer11*cl89.
Compute summer12cl89=summer12*cl89.
Compute summer13cl89=summer13*cl89.
Compute octtdec13cl89=octtdec13*cl89.

Compute jantmar09cl1112=jantmar09*cl1112.
Compute winter0910cl1112=winter0910*cl1112.
Compute winter1011cl1112=winter1011*cl1112.
Compute winter1112cl1112=winter1112*cl1112.
Compute winter1213cl1112=winter1213*cl1112.
Compute summer09cl1112=summer09*cl1112.
Compute summer10cl1112=summer10*cl1112.
Compute summer11cl1112=summer11*cl1112.
Compute summer12cl1112=summer12*cl1112.
Compute summer13cl1112=summer13*cl1112.
Compute octtdec13cl1112=octtdec13*cl1112.

Compute jantmar09cl1095=jantmar09*cl1095.
Compute winter0910cl1095=winter0910*cl1095.
Compute winter1011cl1095=winter1011*cl1095.
Compute winter1112cl1095=winter1112*cl1095.
Compute winter1213cl1095=winter1213*cl1095.
Compute summer09cl1095=summer09*cl1095.
Compute summer10cl1095=summer10*cl1095.
Compute summer11cl1095=summer11*cl1095.
Compute summer12cl1095=summer12*cl1095.
Compute summer13cl1095=summer13*cl1095.
Compute octtdec13cl1095=octtdec13*cl1095.

Compute jantmar09clsplt=jantmar09*clsplt.
Compute winter0910clsplt=winter0910*clsplt.
Compute winter1011clsplt=winter1011*clsplt.
Compute winter1112clsplt=winter1112*clsplt.
Compute winter1213clsplt=winter1213*clsplt.
Compute summer09clsplt=summer09*clsplt.
Compute summer10clsplt=summer10*clsplt.
Compute summer11clsplt=summer11*clsplt.
Compute summer12clsplt=summer12*clsplt.
Compute summer13clsplt=summer13*clsplt.
Compute octtdec13clsplt=octtdec13*clsplt.


Compute lowzoneorland=0.
if town=28 and  (nghcde=11 or nghcde=12  or nghcde=14  or nghcde=15 
or nghcde=22 or nghcde=30 or nghcde=31 or nghcde=32 or nghcde=34
or nghcde=36 or nghcde=37 or nghcde=39 or nghcde=43 or nghcde=45
or nghcde=46 or nghcde=47 or nghcde=61 or nghcde=88
or nghcde=99 or nghcde=100 or nghcde=101 or nghcde=102 
or nghcde=103 or nghcde=104)  lowzoneorland=1.                 	

Compute midzoneorland=0.
if town=28 and (nghcde=13 or nghcde=40 or nghcde=50) midzoneorland=1. 

Compute srfxlowblockorland=0.
if lowzoneorland=1 srfxlowblockorland=srfx*lowzoneorland.

Compute srfxmidblockorland=0.
if midzoneorland=1 srfxmidblockorland=srfx*midzoneorland.

Compute lowzonepalos=0.
if town=30 and  (nghcde=10 or nghcde=11  or nghcde=12  or nghcde=20 
or nghcde=31 or nghcde=40 or nghcde=41 or nghcde=50
or nghcde=51 or nghcde=52 or nghcde=53 or nghcde=54 or nghcde=62 
or nghcde=63 or nghcde=70 or nghcde=71 or nghcde=80
or nghcde=82 or nghcde=92 or nghcde=100) lowzonepalos=1.
                              	
Compute midzonepalos=0.
if town=30 and (nghcde=22 or nghcde=30 or nghcde=42 or nghcde=60 or nghcde=61 )  midzonepalos=1. 

Compute srfxlowblockpalos=0.
if lowzonepalos=1 srfxlowblockpalos=srfx*lowzonepalos.

Compute srfxmidblockpalos=0.
if midzonepalos=1 srfxmidblockpalos=srfx*midzonepalos.

Compute lowzonelemont=0.
if town=19 and  (nghcde=10 or nghcde=11  or nghcde=20  or nghcde=25 
or nghcde=30 or nghcde=32 or nghcde=38 or nghcde=40  or nghcde=50    
or nghcde=60 or nghcde=61 or nghcde=100) lowzonelemont=1.                         	

Compute srfxlowblocklemont=0.
if lowzonelemont=1 srfxlowblocklemont=srfx*lowzonelemont.


if num=6 bnum=0.
if num=0 bnum=0.
if num=1 bnum=2.
if num=2 bnum=3.
if num=3 bnum=4.
if num=4 bnum=5.
if num=5 bnum=6.

compute res=bnum - comm.
compute bnumb=bnum*bsf.
compute nres=n*res.

Compute nsrbsf=n*sqrt(bsf).
Compute nbsf=n*bsf.
Compute srage=sqrt(age).
Compute nsrage=n*srage.

Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.

Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute nframas=n*framas.
Compute nmason=n*mason.
Compute frabsf=frame*bsf.
Compute stubsf=stucco*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute nfrast=n*frast.
Compute frastbsf=frast*bsf.
Compute masonbsf=mason*bsf.
Compute frmsbsf=framas*bsf.

Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
Compute prembsf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.
Compute nprembsf=n*premrf*bsf.


compute attcfull=0.
compute attcpart=0.
compute attcnone=0.
if attc=1 attcfull=1.
if attc=2 attcpart=1.
if attc=3 attcnone=1.
compute atfnliv=0.
compute atfnapt=0.
if atfn=1 atfnliv=1.
if atfn=2 atfnapt=1.

Compute garnogar=0.
Compute garage=0.
Compute biggar=0.
compute garage1=0.
compute garage2=0.
compute garage3=0.
compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1.
Compute biggar=0.
if garage3=1 or garage4=1 biggar=1.


Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
Compute nbasfull=n*basefull.
Compute nbaspart=n*basepart.

Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.

Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
compute nnobase=n*nobase.


If firepl>1 firepl=1. 
compute nfirepl=n*firepl.
 
Compute bathsum=fullbath + 0.25*halfbath.
Compute nbathsum=n*bathsum.


*Cutpoints for land square foot are 1.75 * median lsf.
Compute lsf=sqftl.
*If town=28 and lsf > 17500  lsf = 17500 + ((lsf - 17500)/3).
*If town=19 and lsf > 19785  lsf = 19785 + ((lsf - 19785)/3).
*If town=30 and lsf > 18037  lsf = 18037 + ((lsf - 18037)/3).
*If (class=8 or class=9) lsf=sqftl.

compute nbsfair=n*bsfair.
compute nmasbsf=n*masonbsf.
compute nfrmsbsf=n*frmsbsf.
compute nfrstbsf=n*frastbsf.
compute lemlsf=lem*lsf.
compute orllsf=orl*lsf.
compute lemage=lem*age.
compute orlage=orl*age.
compute srlsf=sqrt(lsf).
compute nsrlsf=n*srlsf.

compute nsragpal=nsrage*pal.
compute nsra1930=nsrage*(lem+pal).
compute nsraglem=nsrage*lem.
compute nsragorl=nsrage*orl.
compute srlsforl=srlsf*orl.
compute nsrlorl=nsrlsf*orl.
compute nsrlem=nsrlsf*lem.
compute nsrlpal=nsrlsf*pal.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute repabove=0.
Compute repbelow=0.
Compute repave=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.

Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.
Compute twosty=0.
If class07=1 or class08=1 or class78=1 twosty=1.

compute cathdral=0.
if ceiling=1 cathdral=1.
if ceiling=2 cathdral=0.
  
compute b=1.
sel if town=19.
compute mv = (165340.4708	
+ 14.92282784*nbsf
+ 27691.23144*biggar
+ 160.3365356*nbsf1095
- 7334.79304*nsrage
+ 2004.423277*sb28014
+ 139.201566*nbsf234
+ 5684.684967*nbathsum
+ 1425.0727*sb19061
+ 973.2998571*sb30070
+ 207.6931787*nsrlpal
+ 197.2110958*nsrlorl
+ 130.5546874*nsrlem
- 1968.354549*sb28012
+ 1319.777237*sb19032
+ 2.07776777*nmasbsf
+ 345.89299*nbsf1112
+ 1036.388839*sb19010
+ 1.6559339*nfrmsbsf
+ 1323.953675*sb28039
+ 730.6551398*sb30080
- 2154.745402*sb30063
+ 0.8992922*nfrstbsf
+ 913.53939*sb19030
+ 9528.638492*nbasfull
+ 6714.822145*nbaspart
+ 691.5063255*sb28022
+ 305.4989287*nbsf778
+ 739.3700998*sb19050
+ 863.290111*nbsf89
- 495.7864702*sb28045
- 1195.646186*sb28034
+ 5136.598728*garage2
+ 567.1302259*sb30051
- 764.0054255*sb19060
+ 901.1392034*sb30031
+ 401.192418*sb28031
- 5194.19042*srfxlowblockorland
+ 292.712065*sb28036
+ 761.3479339*sb28099
- 6555.349221*garnogar
+ 350.5726151*sb28032
- 818.1219101*sb19040
- 5300.276525*srfxlowblocklemont
- 1729.770635*midzoneorland
- 589.3358153*srfxmidblockorland
- 1810.8861*midzonepalos
- 3034.464195*srfxlowblockpalos
- 220.0730814*srfxmidblockpalos
+ 0.745555*nbsfair
+ 210.217047*nbsf34
+ 573.7371273*nfirepl
- 1254.206566*garage1)*1.0.


save outfile='C:\Program Files\IBM\SPSS\Statistics\19\1\mv19.sav'/keep town pin mv.
	
 
 
