                                                     *SPSS Regression.
						           *OAK PARK RIVERSIDE RIVER FOREST REGRESSION  2011.


Get file='C:\Program Files\SPSS\SPSSA\regts273334mergefcl2.sav'.
select if (amount1>160000).
select if (amount1<1500000).
select if (multi<1).
select if sqftb<10000.
Compute year1=0.
If  (amount1>0) year1=1900 + yr. 
If  (yr=10 and amount1>0)  year1=2010.
If  (yr=9 and amount1>0)  year1=2009.
if  (yr=8 and amount1>0)  year1=2008.
If  (yr=7 and amount1>0)  year1=2007.
If  (yr=6 and amount1>0)  year1=2006.
If  (yr=5 and amount1>0)  year1=2005.   
If  (yr=4 and amount1>0)  year1=2004
If  (yr=3 and amount1>0)  year1=2003.   
If  (yr=2 and amount1>0)  year1=2002.
If  (yr=1 and amount1>0)  year1=2001.
If  (yr=0 and amount1>0)  year1=2000.

COMPUTE FX = cumfile7891011.
COMPUTE SRFX = sqrt(fx).
RECODE FX (SYSMIS=0).
*******************************************************************************************************************.

select if puremarket=1. 
exe.
select if (year1>2006).
set mxcells=2500000.
Compute bs=0.
if age<10 and (amount1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.
if (pin=	16061010150000
or pin=	16061020140000
or pin=	16061040160000
or pin=	16061100120000
or pin=	16062220320000
or pin=	16062270240000
or pin=	16062290040000
or pin=	16063090360000
or pin=	16063130330000
or pin=	16063180310000
or pin=	16064050260000
or pin=	16064120070000
or pin=	16051040210000
or pin=	16051120130000
or pin=	16051130120000
or pin=	16051130210000
or pin=	16051170300000
or pin=	16051180190000
or pin=	16051240300000
or pin=	16053030040000
or pin=	16053050050000
or pin=	16053210050000
or pin=	16073231060000
or pin=	16074030140000
or pin=	16074080290000
or pin=	16074090090000
or pin=	16074140240000
or pin=	16181140090000
or pin=	16182110140000
or pin=	16182110380000
or pin=	16182150110000
or pin=	16182270170000
or pin=	16183040050000
or pin=	16183060250000
or pin=	16184090260000
or pin=	16184110440000
or pin=	16184240180000
or pin=	16184260070000
or pin=	16071020270000
or pin=	16171020060000
or pin=	16171030370000
or pin=	16171160130000
or pin=	16171190230000
or pin=	16171230040000
or pin=	16171240060000
or pin=	16171290240000
or pin=	16171300030000
or pin=	16081080100000
or pin=	16081180180000
or pin=	16081220080000
or pin=	16083020050000
or pin=	16083220060000
or pin=	16173180160000
or pin=	15012040180000
or pin=	15012060190000
or pin=	15012110370000
or pin=	15014040140000
or pin=	15014150130000
or pin=	15024030070000
or pin=	15024040070000
or pin=	15112060070000
or pin=	15121010320000
or pin=	15121070360000
or pin=	15122140250000
or pin=	15123050090000
or pin=	15123100170000
or pin=	15123220140000
or pin=	15011010200000
or pin=	15011010360000
or pin=	15011100050000
or pin=	15251000420000
or pin=	15261030300000
or pin=	15264010860000
or pin=	15253060260000
or pin=	15253090320000
or pin=	15254030160000
or pin=	15254080240000
or pin=	15254100500000
or pin=	15254100550000
or pin=	15364090550000
or pin=	15354180180000
or pin=	15363050220000
or pin=	15353110040000
or pin=	15352050020000)  bs=1.
select if bs=0.


Compute OP=0.
If town=27 OP=1.
Compute RF=0.
If town=33 RF=1.
Compute RS=0.
If town=34 RS=1.

Compute N=1.
Compute tnb=(town*1000)+nghcde.

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.
compute bsf=sqftb.
compute bsf02=0.
compute bsf03=0.
compute bsf04=0.
compute bsf05=0.
compute bsf06=0.
compute bsf07=0.
compute bsf08=0.
compute bsf09=0.
compute bsf10=0.
compute bsf11=0.
compute bsf12=0.
compute bsf34=0.
compute bsf78=0.
compute bsf95=0.
if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.


compute n27010=0.
compute n27020=0.
compute n27030=0.
compute n27040=0.
compute n27041=0.
compute n27050=0.
compute n27060=0.
compute n27070=0.
compute n27080=0.
compute n27090=0.
compute n27100=0.

compute n33015=0.
compute n33020=0.
compute n33030=0.
compute n33040=0.
compute n33050=0.

compute n34010=0.
compute n34020=0.
compute n34040=0.
compute n34050=0.
compute n34070=0.
compute n34080=0.

If tnb=27010 n27010=1.
If tnb=27020 n27020=1. 
If tnb=27030 n27030=1.
If tnb=27040 n27040=1.
If tnb=27041 n27041=1.
If tnb=27050 n27050=1.
If tnb=27060 n27060=1.
If tnb=27070 n27070=1.
If tnb=27080 n27080=1.
If tnb=27090 n27090=1. 
If tnb=27100 n27100=1.

If tnb=33015 n33015=1.
If tnb=33020 n33020=1.  
If tnb=33030 n33030=1. 
If tnb=33040 n33040=1.
If tnb=33050 n33050=1.


If tnb=34010 n34010=1.
If tnb=34020 n34020=1.
If tnb=34040 n34040=1.
If tnb=34050 n34050=1.
If tnb=34070 n34070=1.
If tnb=34080 n34080=1.

compute b27010=0.
compute b27020=0.
compute b27030=0.
compute b27040=0.
compute b27041=0.
compute b27050=0.
compute b27060=0.
compute b27070=0.
compute b27080=0.
compute b27090=0.
compute b27100=0.

compute b33015=0.
compute b33020=0.
compute b33030=0.
compute b33040=0.
compute b33050=0.

compute b34010=0.
compute b34020=0.
compute b34040=0.
compute b34050=0.
compute b34070=0.
compute b34080=0.

compute sb27010=0.
compute sb27020=0.
compute sb27030=0.
compute sb27040=0.
compute sb27041=0.
compute sb27050=0.
compute sb27060=0.
compute sb27070=0.
compute sb27080=0.
compute sb27090=0.
compute sb27100=0.

compute sb33015=0.
compute sb33020=0.
compute sb33030=0.
compute sb33040=0.
compute sb33050=0.

compute sb34010=0.
compute sb34020=0.
compute sb34040=0.
compute sb34050=0.
compute sb34070=0.
compute sb34080=0.



If tnb=27010 sb27010=sqrt(bsf).
If tnb=27020 sb27020=sqrt(bsf). 
If tnb=27030 sb27030=sqrt(bsf).
If tnb=27040 sb27040=sqrt(bsf).
If tnb=27041 sb27041=sqrt(bsf).
If tnb=27050 sb27050=sqrt(bsf).
If tnb=27060 sb27060=sqrt(bsf).
If tnb=27070 sb27070=sqrt(bsf).
If tnb=27080 sb27080=sqrt(bsf).
If tnb=27090 sb27090=sqrt(bsf). 
If tnb=27100 sb27100=sqrt(bsf).

If tnb=33015 sb33015=sqrt(bsf).
If tnb=33020 sb33020=sqrt(bsf).  
If tnb=33030 sb33030=sqrt(bsf). 
If tnb=33040 sb33040=sqrt(bsf).
If tnb=33050 sb33050=sqrt(bsf).

If tnb=34010 sb34010=sqrt(bsf).
If tnb=34020 sb34020=sqrt(bsf).
If tnb=34040 sb34040=sqrt(bsf).
If tnb=34050 sb34050=sqrt(bsf).
If tnb=34070 sb34070=sqrt(bsf).
If tnb=34080 sb34080=sqrt(bsf).

If tnb=27010 n=5.40.
If tnb=27020 n=5.05. 
If tnb=27030 n=3.02.
If tnb=27040 n=3.74.
If tnb=27041 n=4.72.
If tnb=27050 n=4.00.
If tnb=27060 n=3.70.
If tnb=27070 n=6.59.
If tnb=27080 n=3.30.
If tnb=27090 n=4.32. 
If tnb=27100 n=2.91.

If tnb=33015 n=5.50.
If tnb=33020 n=6.50.  
If tnb=33030 n=6.07. 
If tnb=33040 n=4.25.
If tnb=33050 n=5.75.
	
If tnb=34010 n=2.29.
If tnb=34020 n=3.70.
If tnb=34040 n=2.61.
If tnb=34050 n=6.51.
If tnb=34070 n=3.13.
If tnb=34080 n=2.28.

Compute lsf=sqftl.
If town=27 and lsf > 8990  lsf = 8990 + ((lsf - 8990)/3).
If town=33 and lsf > 16450  lsf=16450 + ((lsf -16450)/3).
If town=34 and lsf > 12561  lsf=12561 + ((lsf -12561)/3).
If (class=8 or class=9) lsf=sqftl.
Compute nlsf=n*lsf.
Compute nsrlsf=n*sqrt(lsf).


Compute opage=0.
If OP=1 opage=OP*age.
Compute rfage=0.
If RF=1 rfage=RF*age.
Compute rsage=0.
If RS=1 rsage=RS*age.

Compute oplsf=0.
If OP=1 oplsf=OP*lsf.
Compute rflsf=0.
If RF=1 rflsf=RF*lsf.
Compute rslsf=0.
If RS=1 rslsf=RS*lsf.

compute onharlem=0.
if (15014120130000<=pin and pin<=15014120240000) or
   (15014180100000<=pin and pin<=15014180170000) or
   (16061200240000<=pin and pin<=16061200350000) or
pin=15122110190000 or
pin=16061070280000 or
pin=16061070340000 or
pin=16061070830000 or
pin=16061070540000 or
pin=16061070600000 or
pin=16061130260000 or
pin=16061130300000 or
pin=16061130320000 or
pin=16063000080000 or
pin=16063000110000 or
pin=16063000120000 or
pin=16063000130000 or
pin=16063000340000 or
pin=16063000350000 or
pin=16063000470000 or
pin=16063070030000 or
pin=16063070040000 or
pin=16063070060000 or
pin=16063070070000 or
pin=16063070080000 or
pin=16063070290000 or
pin=16063140080000 or
pin=16063140220000 or
pin=16063150010000 or
pin=16063150210000 or
pin=16063150280000 or
pin=16063170250000 or 
pin=16073000050000 or
pin=16073000070000 or
pin=16073000080000 or
pin=16073070110000 or
pin=16073140130000 or
pin=16073140140000 or
pin=16073140150000 or
pin=16073140160000 or
pin=16073140060000 or
pin=16073140070000 or
pin=16073140090000 or
pin=16181000080000 or
pin=16181090010000 or
pin=16181090020000 or
pin=16181090030000 or
pin=16181090040000 or
pin=16181090070000 or
pin=16181090120000 or
(16181260040000<=pin and pin<=16181260160000) onharlem=1.

Compute archlist=0.
If pin=16063210320000
or pin=16063210380000
or pin=16063230210000
or pin=16063250140000
or pin=16064030120000
or pin=16064040210000
or pin=16064070240000
or pin=16064080030000
or pin=16064080070000
or pin=16064080080000
or pin=16064090060000
or pin=16064090070000
or pin=16064090160000
or pin=16064140140000
or pin=16064150190000
or pin=16064160030000
or pin=16064160120000
or pin=16064160130000
or pin=16064160210000
or pin=16064170060000
or pin=16064170200000
or pin=16064170210000
or pin=16064170230000
or pin=16064200030000
or pin=16064210030000
or pin=16064220030000
or pin=16064220050000
or pin=16064220090000
or pin=16064220170000
or pin=16064230010000
or pin=16071020050000
or pin=16071020060000
or pin=16071020080000
or pin=16071020180000
or pin=16071030010000
or pin=16071030080000
or pin=16071040050000
or pin=16071040080000
or pin=16071040170000
or pin=16071040430000
or pin=16071080260000
or pin=16071080320000
or pin=16071150060000
or pin=16071150110000
or pin=16071150200000
or pin=16072010030000
or pin=16072010130000
or pin=16072030130000
or pin=16072030140000
or pin=16072050010000
or pin=16072050020000
or pin=16072050030000
or pin=16072060100000
or pin=16072070060000
or pin=16072120040000
or pin=16072120050000
or pin=16072130030000
or pin=16072160080000
or pin=16073240030000
or pin=16073240310000
or pin=16071030070000
or pin=16071040100000
or pin=16072060090000
or pin=15112080060000
or pin=15121060060000
or pin=15121110110000
or pin=15361030220000 archlist=1.

Compute lowzoneoakpark=0.
if town=27 and  (nghcde=10 or nghcde=20  or nghcde=30  or nghcde=40 
 or nghcde=41 or nghcde=50 or nghcde=60 or nghcde=70 or
 nghcde=80 or nghcde=90 or nghcde=100) lowzoneoakpark=1.                         	
Compute srfxlowblockoakpark=0.
if lowzoneoakpark=1 srfxlowblockoakpark=srfx*lowzoneoakpark.

Compute lowzoneriver=0.
if town=34 and  (nghcde=10 or nghcde=20  or nghcde=40  or nghcde=50 
or nghcde=70 or nghcde=80)  lowzoneriver=1.                         	
Compute srfxlowblockriver=0.
if lowzoneriver=1 srfxlowblockriver=srfx*lowzoneriver.

Compute lowzonerf=0.
if town=33 and  (nghcde=15 or nghcde=20  or nghcde=30  or nghcde=40 
or nghcde=50)  lowzonerf=1.                         	
Compute srfxlowblockrf=0.
if lowzonerf=1 srfxlowblockrf=srfx*lowzonerf.


Compute nsrbsf=n*sqrt(bsf).
Compute nbsf=n*bsf.
Compute srage=sqrt(age).
Compute nsrage=n*srage.
Compute bsfage=bsf*age.
Compute nclass95=n*class95.
Compute nclass10=n*class10.
Compute no95=0.
If class95=0 no95=1.
Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.

Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl5=0.
compute bsf5=0.
if class=5 cl5=1.
compute nbsf5=n*cl5*sqrt(bsf).

compute cl6=0.
compute bsf6=0.
if class=6 cl6=1.
compute nbsf6=n*cl6*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

Compute cl95=0.
if class=95 cl95=1.
Compute nbsf95=n*cl95*sqrt(bsf).


compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).

Compute winter0607=0.
if (mos > 9 and yr=6) or (mos <= 3 and yr=7) winter0607=1.
Compute winter0708=0.
if (mos > 9 and yr=7) or (mos <= 3 and yr=8) winter0708=1.
Compute winter0809=0.
if (mos > 9 and yr=8) or (mos <= 3 and yr=9) winter0809=1.
Compute winter0910=0.
if (mos > 9 and yr=9) or (mos <= 3 and yr=10) winter0910=1.
Compute summer06=0.
if (mos > 3 and yr=6) and (mos <= 9 and yr=6) summer06=1.
Compute summer07=0.
if (mos > 3 and yr=7) and (mos <= 9 and yr=7) summer07=1. 
Compute summer08=0.
if (mos > 3 and yr=8) and (mos <= 9 and yr=8) summer08=1.
Compute summer09=0.
if (mos > 3 and yr=9) and (mos <= 9 and yr=9) summer09=1.
Compute summer10=0.
if (mos > 3 and yr=10) and (mos <= 9 and yr=10) summer10=1.
Compute jantmar06=0.
if (year1=2006 and (mos>=1 and mos<=3)) jantmar06=1. 
Compute octtdec10=0.
if (year1=2010 and (mos>=10 and mos<=12)) octtdec10=1.


Compute jantmar06cl234=jantmar06*cl234.
Compute winter0607cl234=winter0607*cl234.
Compute winter0708cl234=winter0708*cl234.
Compute winter0809cl234=winter0809*cl234.
Compute winter0910cl234=winter0910*cl234.
Compute summer06cl234=summer06*cl234.
Compute summer07cl234=summer07*cl234.
Compute summer08cl234=summer08*cl234.
Compute summer09cl234=summer09*cl234.
Compute summer10cl234=summer10*cl234.
Compute octtdec10cl234=octtdec10*cl234.

Compute jantmar06cl56=jantmar06*cl56.
Compute winter0607cl56=winter0607*cl56.
Compute winter0708cl56=winter0708*cl56.
Compute winter0809cl56=winter0809*cl56.
Compute winter0910cl56=winter0910*cl56.
Compute summer06cl56=summer06*cl56.
Compute summer07cl56=summer07*cl56.
Compute summer08cl56=summer08*cl56.
Compute summer09cl56=summer09*cl56.
Compute summer10cl56=summer10*cl56.
Compute octtdec10cl56=octtdec10*cl56.

Compute jantmar06cl778=jantmar06*cl778.
Compute winter0607cl778=winter0607*cl778.
Compute winter0708cl778=winter0708*cl778.
Compute winter0809cl778=winter0809*cl778.
Compute winter0910cl778=winter0910*cl778.
Compute summer06cl778=summer06*cl778.
Compute summer07cl778=summer07*cl778.
Compute summer08cl778=summer08*cl778.
Compute summer09cl778=summer09*cl778.
Compute summer10cl778=summer10*cl778.
Compute octtdec10cl778=octtdec10*cl778.

Compute jantmar06cl89=jantmar06*cl89.
Compute winter0607cl89=winter0607*cl89.
Compute winter0708cl89=winter0708*cl89.
Compute winter0809cl89=winter0809*cl89.
Compute winter0910cl89=winter0910*cl89.
Compute summer06cl89=summer06*cl89.
Compute summer07cl89=summer07*cl89.
Compute summer08cl89=summer08*cl89.
Compute summer09cl89=summer09*cl89.
Compute summer10cl89=summer10*cl89.
Compute octtdec10cl89=octtdec10*cl89.

Compute jantmar06cl1112=jantmar06*cl1112. 
Compute winter0607cl1112=winter0607*cl1112.
Compute winter0708cl1112=winter0708*cl1112.
Compute winter0809cl1112=winter0809*cl1112.
Compute winter0910cl1112=winter0910*cl1112.
Compute summer06cl1112=summer06*cl1112.
Compute summer07cl1112=summer07*cl1112.
Compute summer08cl1112=summer08*cl1112.
Compute summer09cl1112=summer09*cl1112.
Compute summer10cl1112=summer10*cl1112.
Compute octtdec10cl1112=octtdec10*cl1112.

Compute jantmar06cl1095=jantmar06*cl1095.
Compute winter0607cl1095=winter0607*cl1095.
Compute winter0708cl1095=winter0708*cl1095.
Compute winter0809cl1095=winter0809*cl1095.
Compute winter0910cl1095=winter0910*cl1095.
Compute summer06cl1095=summer06*cl1095.
Compute summer07cl1095=summer07*cl1095.
Compute summer08cl1095=summer08*cl1095.
Compute summer09cl1095=summer09*cl1095.
Compute summer10cl1095=summer10*cl1095.
Compute octtdec10cl1095=octtdec10*cl1095.

Compute jantmar06clsplt=jantmar06*clsplt.
Compute winter0607clsplt=winter0607*clsplt.
Compute winter0708clsplt=winter0708*clsplt.
Compute winter0809clsplt=winter0809*clsplt.
Compute winter0910clsplt=winter0910*clsplt.
Compute summer06clsplt=summer06*clsplt.
Compute summer07clsplt=summer07*clsplt.
Compute summer08clsplt=summer08*clsplt.
Compute summer09clsplt=summer09*clsplt.
Compute summer10clsplt=summer10*clsplt.
Compute octtdec10clsplt=octtdec10*clsplt.

if num=6 bnum=0.
if num=0 bnum=0.
if num=1 bnum=2.
if num=2 bnum=3.
if num=3 bnum=4.
if num=4 bnum=5.
if num=5 bnum=6.


compute res=bnum - comm.
compute bnumb=bnum*bsf.
compute nres=n*res.

Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute frabsf=frame*bsf.
Compute frmsbsf=framas*bsf.
Compute stubsf=stucco*bsf.
Compute masbsf=mason*bsf.
Compute nfrabsf=n*frabsf.
Compute nfrmsbsf=n*frmsbsf.
Compute nmasbsf=n*masbsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.
Compute nfrastbsf=n*frastbsf.
Compute bathsum=fullbath + 0.25*halfbath.
Compute nbathsum=n*bathsum.

Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.

If firepl>1 firepl=1.  
compute nfirepl=n*firepl.

Compute gar1car=0.
Compute gar1hcar=0.
Compute gar2car=0.
Compute gar2hcar=0.
Compute gar3car=0.
Compute gar3hcar=0.
Compute gar4car=0.	

Compute garnogar=0.
Compute garage=0.
Compute biggar=0.
If gar=1 gar1car=1.
If gar=2 gar1hcar=1.
If gar=3 gar2car=1.
If gar=4 gar2hcar=1.
If gar=5 gar3car=1.
If gar=6 gar3hcar=1.
If gar=7 garnogar=1.
If gar=8 gar4car=1. 
compute garage1=0.
compute garage2=0.
compute garage3=0.
compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1.
Compute biggar=0.
if garage3=1 or garage4=1 biggar=1.

Compute basefull=0.
Compute nobasful=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 or basment=3 or basment=4  nobasful=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
Compute basement=0.
If basment=1 or basment=2 basement=1.
Compute nbasfull=n*basefull.
Compute nbaspart=n*basepart.
Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.
Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
Compute nnobase=n*nobase.

Compute attcfull=0.
Compute attcpart=0.
Compute attcnone=0.
If attc=1 attcfull=1.
If attc=2 attcpart=1.
If attc=3 attcnone=1.
Compute atfnliv=0.
Compute atfnapt=0.
If atfn=1 atfnliv=1.
If atfn=2 atfnapt=1.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
Compute deluxbsf = qualdlux*bsf.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute nsitedet=n*sitedetr.
Compute repabove=0.
Compute repbelow=0.
Compute repave=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.
Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.
Compute npremrf=n*premrf.
compute prembsf=premrf*bsf.

compute b=1.
*select if year1 = 2010.
*select if puremarket=1.
*Table observation = b
                amount1
               /table = tnb by 
                           amount1 
                             + b      
	/title = 'Ave & Med Sales'
     	/statistics =  median (amount1 'MED SP')
              mean (amount1 'MEAN SP')
                         validn (b '# PROPS').
exe.
reg des=defaults cov
    	/var=amount1
	nsrlsf nsrbsf nbsf234 nbsf5 nbsf6 nbsf778 nbsf89 nbsf34 nbsf95 
	nsrage nbsfair garnogar garage1 garage2 biggar masbsf frmsbsf frabsf frastbsf bsfnrec nbasfull
	nfirepl nbaspart nnobase attcfull nbathsum nrepabsf sb27010 sb27020 sb27030 sb27040
	sb27041 sb27050 sb27060 sb27070 sb27080 sb27090 sb27100 sb33015 sb33020
	sb33030 sb33040 sb33050 sb34010 sb34020 sb34040 sb34050 sb34070 sb34080
	nluxbsf onharlem archlist srfx 
     jantmar06cl234 winter0607cl234 winter0708cl234 winter0809cl234 winter0910cl234
	summer06cl234 summer07cl234 summer08cl234 summer09cl234 summer10cl234
	octtdec10cl234 jantmar06cl56 winter0607cl56 winter0708cl56 winter0809cl56
	winter0910cl56 summer06cl56 summer07cl56 summer08cl56 summer09cl56
	summer10cl56 octtdec10cl56 jantmar06cl778 winter0607cl778 winter0708cl778
	winter0809cl778 winter0910cl778 summer06cl778 summer07cl778 summer08cl778
     summer09cl778 summer10cl778 octtdec10cl778 jantmar06cl89 winter0607cl89
	winter0708cl89 winter0809cl89 winter0910cl89 summer06cl89 summer07cl89		
	summer08cl89 summer09cl89 summer10cl89 octtdec10cl89 jantmar06cl1112
	winter0607cl1112 winter0708cl1112 winter0809cl1112 winter0910cl1112
	summer06cl1112 summer07cl1112 summer08cl1112 summer09cl1112
	summer10cl1112 octtdec10cl1112 jantmar06cl1095 winter0607cl1095
	winter0708cl1095 winter0809cl1095 winter0910cl1095 summer06cl1095
	summer07cl1095 summer08cl1095 summer09cl1095 summer10cl1095
	octtdec10cl1095 jantmar06clsplt winter0607clsplt winter0708clsplt
	winter0809clsplt winter0910clsplt summer06clsplt summer07clsplt
	summer08clsplt summer09clsplt summer10clsplt octtdec10clsplt
	srfxlowblockoakpark srfxlowblockrf   
	/dep=amount1
     /method=stepwise
     /method=enter jantmar06cl234 winter0607cl234 winter0708cl234 winter0809cl234 winter0910cl234
	summer06cl234 summer07cl234 summer08cl234 summer09cl234 summer10cl234
	octtdec10cl234 jantmar06cl56 winter0607cl56 winter0708cl56 winter0809cl56
	winter0910cl56 summer06cl56 summer07cl56 summer08cl56 summer09cl56
	summer10cl56 octtdec10cl56 jantmar06cl778 winter0607cl778 winter0708cl778
	winter0809cl778 winter0910cl778 summer06cl778 summer07cl778 summer08cl778
     summer09cl778 summer10cl778 octtdec10cl778 jantmar06cl89 winter0607cl89
	winter0708cl89 winter0809cl89 winter0910cl89 summer06cl89 summer07cl89		
	summer08cl89 summer09cl89 summer10cl89 octtdec10cl89 jantmar06cl1112
	winter0607cl1112 winter0708cl1112 winter0809cl1112 winter0910cl1112
	summer06cl1112 summer07cl1112 summer08cl1112 summer09cl1112
	summer10cl1112 octtdec10cl1112 jantmar06cl1095 winter0607cl1095
	winter0708cl1095 winter0809cl1095 winter0910cl1095 summer06cl1095
	summer07cl1095 summer08cl1095 summer09cl1095 summer10cl1095
	octtdec10cl1095 jantmar06clsplt winter0607clsplt winter0708clsplt
	winter0809clsplt winter0910clsplt summer06clsplt summer07clsplt
	summer08clsplt summer09clsplt summer10clsplt octtdec10clsplt
	/method=enter srfxlowblockoakpark srfxlowblockrf 
	/method=enter nsrbsf nbsf234 sb27020 sb27070 sb34080 onharlem nbasfull nbaspart nsrage nluxbsf sb33050 sb34020 sb27030 nnobase  garnogar garage1 frmsbsf frabsf
	/method=enter nbsf34 nbsf95 nbsf778
	/save pred (pred) resid (resid).
            sort cases by tnb pin.	
           value labels extcon 'Frame' 2 'Mason' 3 'Framas' 4 'Stucco'
           /basment 1 'Full' 2 'Slab' 3 'Part' 4 'Crwl'
           /bsfn 1 'Rec' 2 'Apt' 3 'Unf'
           /gar 1 '1' 2 '1.5' 3 '2' 4 '2.5' 5 '3' 6 '3.5' 7 'non'
           /class 2 '2-02' 3 '2-03' 4 '2-04' 5 '2-05' 6 '2-06' 7 '2-07' 8 '2-08' 9 '2-09' 10 '2-10' 11'2-11' 12 '2-12' 34 '2-34' 78 '2-78' 95 '2-95'.
compute perdif=(resid)/(amount1)*100.
formats pred(COMMA9.0)
          /resid (f6.0).

*plot
   /vertical='Sales Price' MIN(50000) MAX(1000000)
   /horizontal='RESID'
   /vsize=25
   /hsize=140
   /plot=amount1 with RESID.
*compute badsal=0.
*if perdif>50 badsal=1.
*if perdif<-50 badsal=1.
*select if badsal=1.
set wid=125.
set len=59.
*REPORT FORMAT=automatic list(1)
   /title='Office of the Assessor'
        'Residential Regression Report'
        'Town is OAK PARK, RIVERSIDE, RIVER FOREST'
    /ltitle 'Report Ran On)Date' 
    /rtitle='PAGE)PAGE'
    /string=prop(area(2) '-' subarea(2) '-' block(3) '-' parcel(3))
          date(mos(2) '-'yr(2))
   /var=prop 'Prop' 'Index Number'(13)
       class 'Class' (label) (5)
       nghcde 'nbhb'(5)
       amount1 'Price'(7)
       pred 'Predicted'(9)
       resid 'Resid'(7)
       perdif '% DIF' (5)
       aos 'AOS' (3)
       sqftb 'Sqftb'(5)
       age 'Age'(4)
       extcon 'Extcon' (label)(6)
       sqftl 'Land'(6).
	
 	
exe.
