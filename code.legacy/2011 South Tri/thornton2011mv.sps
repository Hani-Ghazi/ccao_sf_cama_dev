                        *SPSS Regression.
				*THORNTON REGRESSION 2011	

Get file='C:\Program Files\SPSS\spssa\regt37mergefcl2.sav'.

*select if (amount1>95000).
*select if (amount1<800000).
*select if (multi<1).

Compute year1=0.
If  (amount1>0) year1=1900 + yr.
If  (yr=10 and amount1>0)  year1=2010.
If  (yr=9 and amount1>0)  year1=2009.
If  (yr=8 and amount1>0)  year1=2008.  
If  (yr=7 and amount1>0)  year1=2007.   
If  (yr=6 and amount1>0)  year1=2006.
If  (yr=5 and amount1>0)  year1=2005.
If  (yr=4 and amount1>0)  year1=2004.
If  (yr=3 and amount1>0)  year1=2003.   
If  (yr=2 and amount1>0)  year1=2002.
If  (yr=1 and amount1>0)  year1=2001.
If  (yr=0 and amount1>0)  year1=2000.


*select if sqftb<6000.
*select if (year1>2005).

COMPUTE FX = cumfile7891011.
COMPUTE SRFX = sqrt(fx).
RECODE FX (SYSMIS=0).
*******************************************************************************************************************.

*select if (year1=2006 or puremarket=1). 

Compute bs=0.
if age<10 and (amount1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.
if (pin=	29082080600000
or pin=	29042140420000
or pin=	29092260190000
or pin=	29114250110000
or pin=	29151080390000
or pin=	29014180220000
or pin=	29121120220000
or pin=	29121170190000
or pin=	29122020110000
or pin=	29124060130000
or pin=	29124290510000
or pin=	30073190130000
or pin=	30171100240000
or pin=	30173000640000
or pin=	30081150050000
or pin=	29074150210000
or pin=	29083040190000
or pin=	29183030180000
or pin=	29143040550000
or pin=	29222130080000
or pin=	29271070100000
or pin=	30202040430000
or pin=	30291200060000
or pin=	29174060570000
or pin=	29201030200000
or pin=	29163070480000
or pin=	29213140180000
or pin=	29224000650000
or pin=	29232030140000
or pin=	29261070010000
or pin=	29272100070000
or pin=	29272120150000
or pin=	29314000150000
or pin=	29314070020000
or pin=	29314090010000
or pin=	29323100890000
or pin=	29324040500000
or pin=	29273000080000
or pin=	29352000030000
or pin=	29352110230000
or pin=	29361030030000
or pin=	30303140590000
or pin=	30304150320000
or pin=	30311200410000
or pin=	30312000460000
or pin=	30312210390000
or pin=	30322000440000
or pin=	29364000320000
or pin=	29364150060000
or pin=	30324030320000
or pin=	29251090010000
or pin=	29252120080000)  bs=1.
*select if bs=0.

Compute N=1.

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.

compute bsf=sqftb.

compute bsf02=0.
compute bsf03=0.
compute bsf04=0.
compute bsf05=0.
compute bsf06=0.
compute bsf07=0.
compute bsf08=0.
compute bsf09=0.
compute bsf10=0.
compute bsf11=0.
compute bsf12=0.
compute bsf34=0.
compute bsf78=0.
compute bsf95=0.

if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.

Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).

Compute winter0607=0.
if (mos > 9 and yr=6) or (mos <= 3 and yr=7) winter0607=1.
Compute winter0708=0.
if (mos > 9 and yr=7) or (mos <= 3 and yr=8) winter0708=1.
Compute winter0809=0.
if (mos > 9 and yr=8) or (mos <= 3 and yr=9) winter0809=1.
Compute winter0910=0.
if (mos > 9 and yr=9) or (mos <= 3 and yr=10) winter0910=1.
Compute summer06=0.
if (mos > 3 and yr=6) and (mos <= 9 and yr=6) summer06=1.
Compute summer07=0.
if (mos > 3 and yr=7) and (mos <= 9 and yr=7) summer07=1. 
Compute summer08=0.
if (mos > 3 and yr=8) and (mos <= 9 and yr=8) summer08=1.
Compute summer09=0.
if (mos > 3 and yr=9) and (mos <= 9 and yr=9) summer09=1.
Compute summer10=0.
if (mos > 3 and yr=10) and (mos <= 9 and yr=10) summer10=1.
Compute jantmar06=0.
if (year1=2006 and (mos>=1 and mos<=3)) jantmar06=1. 
Compute octtdec10=0.
if (year1=2010 and (mos>=10 and mos<=12)) octtdec10=1.


Compute jantmar06cl234=jantmar06*cl234.
Compute winter0607cl234=winter0607*cl234.
Compute winter0708cl234=winter0708*cl234.
Compute winter0809cl234=winter0809*cl234.
Compute winter0910cl234=winter0910*cl234.
Compute summer06cl234=summer06*cl234.
Compute summer07cl234=summer07*cl234.
Compute summer08cl234=summer08*cl234.
Compute summer09cl234=summer09*cl234.
Compute summer10cl234=summer10*cl234.
Compute octtdec10cl234=octtdec10*cl234.

Compute jantmar06cl56=jantmar06*cl56.
Compute winter0607cl56=winter0607*cl56.
Compute winter0708cl56=winter0708*cl56.
Compute winter0809cl56=winter0809*cl56.
Compute winter0910cl56=winter0910*cl56.
Compute summer06cl56=summer06*cl56.
Compute summer07cl56=summer07*cl56.
Compute summer08cl56=summer08*cl56.
Compute summer09cl56=summer09*cl56.
Compute summer10cl56=summer10*cl56.
Compute octtdec10cl56=octtdec10*cl56.

Compute jantmar06cl778=jantmar06*cl778.
Compute winter0607cl778=winter0607*cl778.
Compute winter0708cl778=winter0708*cl778.
Compute winter0809cl778=winter0809*cl778.
Compute winter0910cl778=winter0910*cl778.
Compute summer06cl778=summer06*cl778.
Compute summer07cl778=summer07*cl778.
Compute summer08cl778=summer08*cl778.
Compute summer09cl778=summer09*cl778.
Compute summer10cl778=summer10*cl778.
Compute octtdec10cl778=octtdec10*cl778.

Compute jantmar06cl89=jantmar06*cl89.
Compute winter0607cl89=winter0607*cl89.
Compute winter0708cl89=winter0708*cl89.
Compute winter0809cl89=winter0809*cl89.
Compute winter0910cl89=winter0910*cl89.
Compute summer06cl89=summer06*cl89.
Compute summer07cl89=summer07*cl89.
Compute summer08cl89=summer08*cl89.
Compute summer09cl89=summer09*cl89.
Compute summer10cl89=summer10*cl89.
Compute octtdec10cl89=octtdec10*cl89.

Compute jantmar06cl1112=jantmar06*cl1112. 
Compute winter0607cl1112=winter0607*cl1112.
Compute winter0708cl1112=winter0708*cl1112.
Compute winter0809cl1112=winter0809*cl1112.
Compute winter0910cl1112=winter0910*cl1112.
Compute summer06cl1112=summer06*cl1112.
Compute summer07cl1112=summer07*cl1112.
Compute summer08cl1112=summer08*cl1112.
Compute summer09cl1112=summer09*cl1112.
Compute summer10cl1112=summer10*cl1112.
Compute octtdec10cl1112=octtdec10*cl1112.

Compute jantmar06cl1095=jantmar06*cl1095.
Compute winter0607cl1095=winter0607*cl1095.
Compute winter0708cl1095=winter0708*cl1095.
Compute winter0809cl1095=winter0809*cl1095.
Compute winter0910cl1095=winter0910*cl1095.
Compute summer06cl1095=summer06*cl1095.
Compute summer07cl1095=summer07*cl1095.
Compute summer08cl1095=summer08*cl1095.
Compute summer09cl1095=summer09*cl1095.
Compute summer10cl1095=summer10*cl1095.
Compute octtdec10cl1095=octtdec10*cl1095.

Compute jantmar06clsplt=jantmar06*clsplt.
Compute winter0607clsplt=winter0607*clsplt.
Compute winter0708clsplt=winter0708*clsplt.
Compute winter0809clsplt=winter0809*clsplt.
Compute winter0910clsplt=winter0910*clsplt.
Compute summer06clsplt=summer06*clsplt.
Compute summer07clsplt=summer07*clsplt.
Compute summer08clsplt=summer08*clsplt.
Compute summer09clsplt=summer09*clsplt.
Compute summer10clsplt=summer10*clsplt.
Compute octtdec10clsplt=octtdec10*clsplt.


Compute lowzonethornton=0.
if town=37 and  (nghcde=75 or nghcde=161  or nghcde=162  or nghcde=164 
or nghcde=171 or nghcde=180 or nghcde=183 or nghcde=185)  lowzonethornton=1.                         	

Compute midzonethornton=0.
if town=37 and (nghcde=11 or nghcde=51 or nghcde=63 
or nghcde=102 or nghcde=103 or nghcde=150  or nghcde=163 
or nghcde=181 or nghcde=190 or nghcde=200)   midzonethornton=1. 

Compute highzonethornton=0.
if town=37 and (nghcde=10 or nghcde=21 or nghcde=22 or nghcde=23
or nghcde=24 or nghcde=33  or nghcde=52  or nghcde=53     
or nghcde=54  or nghcde=61 or nghcde=70 or nghcde=92 or nghcde=101
or nghcde=121 or nghcde=130 or nghcde=134)   highzonethornton=1.


Compute srfxlowblockthornton=0.
if lowzonethornton=1 srfxlowblockthornton=srfx*lowzonethornton.

Compute srfxmidblockthornton=0.
if midzonethornton=1 srfxmidblockthornton=srfx*midzonethornton.

Compute srfxhighblockthornton=0.
if highzonethornton=1 srfxhighblockthornton=srfx*highzonethornton.

Compute n10=0.
Compute n11=0.
Compute n21=0.
Compute n22=0.
Compute n23=0.
Compute n24=0.
Compute n33=0.
Compute n51=0.
Compute n52=0.
Compute n53=0.
Compute n54=0.
Compute n61=0.
Compute n63=0.
Compute n70=0.
Compute n75=0.
Compute n92=0.
Compute n101=0.
Compute n102=0.
Compute n103=0.
Compute n121=0.
Compute n130=0.
Compute n134=0.
Compute n150=0.
Compute n161=0.
Compute n162=0.
Compute n163=0.
Compute n164=0.
Compute n171=0.
Compute n180=0.
Compute n181=0.
Compute n183=0.
Compute n185=0.
Compute n190=0.
Compute n200=0.

Compute sb10=0.
Compute sb11=0.
Compute sb21=0.
Compute sb22=0.
Compute sb23=0.
Compute sb24=0.
Compute sb33=0.
Compute sb51=0.
Compute sb52=0.
Compute sb53=0.
Compute sb54=0.
Compute sb61=0.
Compute sb63=0.
Compute sb70=0.
Compute sb75=0.
Compute sb92=0.
Compute sb101=0.
Compute sb102=0.
Compute sb103=0.
Compute sb121=0.
Compute sb130=0.
Compute sb134=0.
Compute sb150=0.
Compute sb161=0.
Compute sb162=0.
Compute sb163=0.
Compute sb164=0.
Compute sb171=0.
Compute sb180=0.
Compute sb181=0.
Compute sb183=0.
Compute sb185=0.
Compute sb190=0.
Compute sb200=0.

If nghcde=10 n10=1.
if nghcde=11 n11=1.
If nghcde=21 n21=1.
if nghcde=22 n22=1.
if nghcde=23 n23=1.
If nghcde=24 n24=1.
if nghcde=33 n33=1.
If nghcde=51 n51=1.
if nghcde=52 n52=1.
If nghcde=53 n53=1.
if nghcde=54 n54=1.
if nghcde=61 n61=1.
If nghcde=63 n63=1.
if nghcde=70 n70=1.
If nghcde=75 n75=1.
if nghcde=92 n92=1.
If nghcde=101 n101=1.
if nghcde=102 n102=1.
If nghcde=103 n103=1.
if nghcde=121 n121=1.
If nghcde=130 n130=1.
If nghcde=134 n134=1.
if nghcde=150 n150=1.
if nghcde=161 n161=1.
If nghcde=162 n162=1.
if nghcde=163 n163=1.
If nghcde=164 n164=1.
if nghcde=171 n171=1.
If nghcde=180 n180=1.
if nghcde=181 n181=1.
If nghcde=183 n183=1.
if nghcde=185 n185=1.
If nghcde=190 n190=1.
if nghcde=200 n200=1.

If nghcde=10 sb10=sqrt(bsf).
if nghcde=11 sb11=sqrt(bsf).
If nghcde=21 sb21=sqrt(bsf).
if nghcde=22 sb22=sqrt(bsf).
if nghcde=23 sb23=sqrt(bsf).
If nghcde=24 sb24=sqrt(bsf).
if nghcde=33 sb33=sqrt(bsf).
If nghcde=51 sb51=sqrt(bsf).
if nghcde=52 sb52=sqrt(bsf).
If nghcde=53 sb53=sqrt(bsf).
if nghcde=54 sb54=sqrt(bsf).
if nghcde=61 sb61=sqrt(bsf).
If nghcde=63 sb63=sqrt(bsf).
if nghcde=70 sb70=sqrt(bsf).
If nghcde=75 sb75=sqrt(bsf).
if nghcde=92 sb92=sqrt(bsf).
If nghcde=101 sb101=sqrt(bsf).
if nghcde=102 sb102=sqrt(bsf).
If nghcde=103 sb103=sqrt(bsf).
if nghcde=121 sb121=sqrt(bsf).
If nghcde=130 sb130=sqrt(bsf).
If nghcde=134 sb134=sqrt(bsf).
if nghcde=150 sb150=sqrt(bsf).
if nghcde=161 sb161=sqrt(bsf).
If nghcde=162 sb162=sqrt(bsf).
if nghcde=163 sb163=sqrt(bsf).
If nghcde=164 sb164=sqrt(bsf).
if nghcde=171 sb171=sqrt(bsf).
If nghcde=180 sb180=sqrt(bsf).
if nghcde=181 sb181=sqrt(bsf).
If nghcde=183 sb183=sqrt(bsf).
if nghcde=185 sb185=sqrt(bsf).
If nghcde=190 sb190=sqrt(bsf).
if nghcde=200 sb200=sqrt(bsf).

Compute nbsf=n*bsf.
Compute nsrbsf=n*sqrt(bsf).

If firepl>1 firepl=1 .  
compute nfirepl=n*firepl.

Compute bathsum=fullbath + 0.25*halfbath.
Compute nbathsum=n*bathsum.

*Cutpoints for land square foot are 1.75 * median lsf.
Compute lsf=sqftl.
If lsf > 10718  lsf = 10718 + ((lsf - 10718)/3).
If (class=8 or class=9) lsf=sqftl.

Compute srlsf=sqrt(lsf).
Compute nsrlsf=n*sqrt(lsf).

If nghcde=10 N=1.38.
if nghcde=11 N=1.34.
If nghcde=21 N=1.15.
if nghcde=22 N=1.35.
if nghcde=23 N=1.30.
If nghcde=24 N=1.22.
if nghcde=33 N=1.34.
If nghcde=51 N=1.15.
if nghcde=52 N=1.40.
If nghcde=53 N=1.36.
if nghcde=54 N=1.36.
if nghcde=61 N=1.27.
If nghcde=63 N=1.38.
if nghcde=70 N=1.23.
If nghcde=75 N=1.21.
if nghcde=92 N=1.40.
If nghcde=101 N=1.20.
if nghcde=102 N=1.14.
If nghcde=103 N=1.35.
if nghcde=121 N=1.20.
If nghcde=130 N=1.28.
If nghcde=134 N=1.44.
if nghcde=150 N=1.57.
if nghcde=161 N=1.51.
If nghcde=162 N=1.60.
if nghcde=163 N=1.66.
If nghcde=164 N=1.27.
if nghcde=171 N=1.24.
If nghcde=180 N=2.37.
if nghcde=181 N=1.29.
If nghcde=183 N=1.37.
if nghcde=185 N=1.25.
If nghcde=190 N=1.90.
if nghcde=200 N=1.79.


Compute srage=sqrt(age).
Compute nsrage=n*srage.

Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.

Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute frabsf=frame*bsf.
Compute stubsf=stucco*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.
compute masonbsf=mason*bsf.


if num=6 bnum=0.
if num=0 bnum=0.
if num=1 bnum=2.
if num=2 bnum=3.
if num=3 bnum=4.
if num=4 bnum=5.
if num=5 bnum=6.

compute res=bnum - comm.
compute bnumb=bnum*bsf.
compute nres=n*res.


Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.


Compute gar1car=0.
Compute gar1hcar=0.
Compute gar2car=0.
Compute gar2hcar=0.
Compute gar3car=0.
Compute gar3hcar=0.
Compute gar4car=0.	

Compute garnogar=0.
Compute garage=0.
If gar=1 gar1car=1.
If gar=2 gar1hcar=1.
If gar=3 gar2car=1.
If gar=4 gar2hcar=1.
If gar=5 gar3car=1.
If gar=6 gar3hcar=1.
If gar=7 garnogar=1.
If gar=8 gar4car=1. 
If gar ne 7 garage=1.

compute garage1=0.
compute garage2=0.
compute garage3=0.
compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1.
Compute biggar=0.
If garage3=1 or garage4=1 biggar=1.  

Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
Compute basement=0.
If basment=1 or basment=2 basement=1.
Compute nbasfull=n*basefull.
Compute nbaspart=n*basepart.


Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.
Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
Compute nnobase=n*nobase.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
Compute deluxbsf = qualdlux*bsf.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute repabove=0.
Compute repbelow=0.
Compute repave=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.
Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.
Compute twosty=0.
If class07=1 or class08=1 or class78=1 twosty=+1.

compute b=1.
*select if year1 = 2009 or year1 = 2010.
*select if puremarket=1.
*Table observation = b
                amount1
               /table = nghcde by 
                           amount1 
                             + b      
	/title = 'Ave & Med Sales'
     	/statistics =  median (amount1 'MED SP')
              mean (amount1 'MEAN SP')
                         validn (b '# PROPS').


compute mv = (46081.91082	
+ 7864.721392*nbathsum
+ 482.8836379	*nsrlsf
+ 32349.69883*biggar
+ 5671.975677*nbsf778
+ 0.725198122*masonbsf
+ 1396.37137*nbsf234
+ 1811.488074*sb200
+ 2882.772764*sb162
- 12917.68298*nnobase
+ 3980.289246*sb180
- 4753.092429*nsrage
- 12564.5317*midzonethornton
+ 3639.710241*nbsf1112
+ 1477.444798*sb190
+ 1271.912507*sb150
+ 770.3869572*sb92
+ 656.2110549*sb53
+ 466.1422135*sb33
+ 2034.305624*sb183
+ 1468.806495*sb163
+ 257.9110636	*sb61
+ 372.9022216*sb52
+ 521.9661712*sb181
+ 2689.198776*nbsf56
+ 698.0167635*sb103
+ 18074.49879*npremrf
- 371.014118*sb121
- 371.3709584*sb70
+ 4006.304513*nfirepl
+ 1711.615018*sb185
+ 1703.874004*sb164
+ 531.7783426*sb75
+ 8660.546045	*garage2
+ 4804.172107*nbsf1095
+ 1420.833365	*sb171
- 1447.396748	*srfxlowblockthornton
+ 1390.065126*sb161
+ 2402.123243*nbsf34
- 2954.195987*nbaspart
- 1.741100496	*frabsf
- 139.9400057	*sb24
- 1135.310846	*srfxhighblockthornton
+ 343.4261249*sb63
- 19235.50704	*highzonethornton
- 1056.447467	*srfxmidblockthornton
+ 3336.617619	*garage1
+ 4.636621904*stubsf)*1.0.
	


	
save outfile='C:\Program Files\SPSS\spssa\mv37.sav'/keep town pin mv.
 
	

 
