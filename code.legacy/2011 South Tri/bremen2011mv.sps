                                        *SPSS Regression.
		                		       * Bremen 2011.


Get file='C:\Program Files\SPSS\spssa\regt13mergefcl2.sav'.
*select if (amount1>65000).
*select if (amount1<650000).
*select if (multi<1).
*select if sqftb<6000.

Compute year1=0.
If  (amount1>0) year1=1900 + yr.
if  (yr=10 and amount1>0) year1=2010.
if  (yr=9 and amount1>0)  year1=2009.
if  (yr=8 and amount1>0)  year1=2008.    
if  (yr=7 and amount1>0)  year1=2007. 
If  (yr=6 and amount1>0)  year1=2006.
If  (yr=5 and amount1>0)  year1=2005.
If  (yr=4 and amount1>0)  year1=2004.
If  (yr=3 and amount1>0)  year1=2003.   
If  (yr=2 and amount1>0)  year1=2002.
If  (yr=1 and amount1>0)  year1=2001.
If  (yr=0 and amount1>0)  year1=2000.


COMPUTE FX = cumfile7891011.
COMPUTE SRFX = sqrt(fx).
RECODE FX (SYSMIS=0).
*******************************************************************************************************************.


*select if (year1=2006 or puremarket=1). 


Compute bs=0.
*if age<10 and (amount1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.
if (pin=	28021110390000
or pin=	28022180610000
or pin=	28024280120000
or pin=	28032080290000
or pin=	28044150010000
or pin=	28083050090000
or pin=	28091001400000
or pin=	28092010740000
or pin=	28093100010000
or pin=	28094040940000
or pin=	28101130320000
or pin=	28101150060000
or pin=	28102080030000
or pin=	28104080170000
or pin=	28111170040000
or pin=	28112020190000
or pin=	28112090130000
or pin=	28112160080000
or pin=	28112210040000
or pin=	28113190140000
or pin=	28113210060000
or pin=	28122090530000
or pin=	28123240130000
or pin=	28124010060000
or pin=	28124070110000
or pin=	28124410310000
or pin=	28141020430000
or pin=	28142070160000
or pin=	28143090040000
or pin=	28143150270000
or pin=	28144020080000
or pin=	28144170280000
or pin=	28151050130000
or pin=	28151060290000
or pin=	28153030040000
or pin=	28164090380000
or pin=	28164120310000
or pin=	28171150300000
or pin=	28172070290000
or pin=	28174090040000
or pin=	28181040090000
or pin=	28194110040000
or pin=	28194150170000
or pin=	28211010070000
or pin=	28211090160000
or pin=	28212060260000
or pin=	28212130190000
or pin=	28223080470000
or pin=	28231190040000
or pin=	28232220270000
or pin=	28232250390000
or pin=	28232260370000
or pin=	28233050570000
or pin=	28234040370000
or pin=	28234050340000
or pin=	28234070200000
or pin=	28234080140000
or pin=	28234110520000
or pin=	28234130420000
or pin=	28234130430000
or pin=	28234220310000
or pin=	28234230220000
or pin=	28234250470000
or pin=	28234270350000
or pin=	28234300210000
or pin=	28242030050000
or pin=	28244210280000
or pin=	28253080100000
or pin=	28253230390000
or pin=	28262100070000
or pin=	28263090020000
or pin=	28263090130000
or pin=	28263110030000
or pin=	28263120150000
or pin=	28264110160000
or pin=	28274030140000
or pin=	28302030280000
or pin=	28302110800000
or pin=	28303200030000
or pin=	28304120400000
or pin=	28304130140000
or pin=	28312030090000
or pin=	28312120720000
or pin=	28332100060000
or pin=	28332100230000
or pin=	28334070090000
or pin=	28334070100000
or pin=	28341020180000
or pin=	28341130090000
or pin=	28342130120000
or pin=	28342180290000
or pin=	28343130190000
or pin=	28344150400000
or pin=	28351030920000
or pin=	28351080090000
or pin=	28351100050000
or pin=	28351110270000
or pin=	28351130240000
or pin=	28352060130000
or pin=	28353010350000
or pin=	28361040600000
or pin=	28361060100000
or pin=	28361070180000
or pin=	28363000060000
or pin=	28363070100000)   bs=1.
*select if bs=0.

Compute N=1.
compute bsf=sqftb.
compute lsf=sqftl.


compute srbsf=sqrt(sqftb).
compute srage=sqrt(age).
compute srlsf=sqrt(sqftl).
compute apt=0.
compute noapt=0.
if class=11 or class=12 apt=1.
if apt=0 noapt=1.
compute nontown=0.
compute c1095=0.
if class=10 or class=95 c1095=1.
if c1095=0 nontown=1.
compute townlsf=0.
compute twnsrlsf=0.
if class=95 townlsf=lsf.
if class=95 twnsrlsf=srlsf.
compute srages=noapt*srage.

if class=95 and nghcde=20   lsf=4000.
if class=95 and nghcde=170  lsf=2850.
if class=95 and nghcde=171  lsf=2000.
if class=95 and nghcde=341  lsf=2400.
if class=95 and nghcde=342  lsf=2500.

compute n10=0.
compute n20=0.
compute n32=0.
compute n50=0.
compute n60=0.
compute n70=0.
compute n80=0.
compute n90=0.
compute n100=0.
compute n110=0.
compute n120=0.
compute n130=0.
compute n140=0.
compute n151=0.
compute n152=0.
compute n160=0.
compute n170=0.
compute n171=0.
compute n180=0.
compute n182=0.
compute n210=0.
compute n240=0.
compute n250=0.
compute n260=0.
compute n270=0.
compute n280=0.
compute n310=0.
compute n312=0.
compute n314=0.
compute n315=0.
compute n316=0.
compute n320=0.
compute n330=0.
compute n340=0.
compute n341=0.
compute n342=0.
compute n344=0.
compute n345=0.
compute n400=0.

compute sb10=0.
compute sb20=0.
compute sb32=0.
compute sb50=0.
compute sb60=0.
compute sb70=0.
compute sb80=0.
compute sb90=0.
compute sb100=0.
compute sb110=0.
compute sb120=0.
compute sb130=0.
compute sb140=0.
compute sb151=0.
compute sb152=0.
compute sb160=0.
compute sb170=0.
compute sb171=0.
compute sb180=0.
compute sb182=0.
compute sb210=0.
compute sb240=0.
compute sb250=0.
compute sb260=0.
compute sb270=0.
compute sb280=0.
compute sb310=0.
compute sb312=0.
compute sb314=0.
compute sb315=0.
compute sb316=0.
compute sb320=0.
compute sb330=0.
compute sb340=0.
compute sb341=0.
compute sb342=0.
compute sb344=0.
compute sb345=0.
compute sb400=0.

if (nghcde=10)   n10=1.
if (nghcde=20)   n20=1.
if (nghcde=32)   n32=1.
if (nghcde=50)   n50=1.
if (nghcde=60)   n60=1.
if (nghcde=70)   n70=1.
if (nghcde=80)   n80=1.
if (nghcde=90)   n90=1.
if (nghcde=100)  n100=1.
if (nghcde=110)  n110=1.
if (nghcde=120)  n120=1.
if (nghcde=130)  n130=1.
if (nghcde=140)  n140=1.
if (nghcde=151)  n151=1.
if (nghcde=152)  n152=1.
if (nghcde=160)  n160=1.
if (nghcde=170)  n170=1.
if (nghcde=171)  n171=1.
if (nghcde=180)  n180=1.
if (nghcde=182)  n182=1.
if (nghcde=210)  n210=1.
if (nghcde=240)  n240=1.
if (nghcde=250)  n250=1.
if (nghcde=260)  n260=1.
if (nghcde=270)  n270=1.
if (nghcde=280)  n280=1.
if (nghcde=310)  n310=1.
if (nghcde=312)  n312=1.
if (nghcde=314)  n314=1.
if (nghcde=315)  n315=1.
if (nghcde=316)  n316=1.
if (nghcde=320)  n320=1.
if (nghcde=330)  n330=1.
if (nghcde=340)  n340=1.
if (nghcde=341)  n341=1.
if (nghcde=342)  n342=1.
if (nghcde=344)  n344=1.
if (nghcde=345)  n345=1.
if (nghcde=400)  n400=1.

if (nghcde=10)  sb10=sqrt(bsf).
if (nghcde=20)  sb20=sqrt(bsf).
if (nghcde=32)  sb32=sqrt(bsf).
if (nghcde=50)  sb50=sqrt(bsf).
if (nghcde=60)  sb60=sqrt(bsf).
if (nghcde=70)  sb70=sqrt(bsf).
if (nghcde=80)  sb80=sqrt(bsf).
if (nghcde=90)  sb90=sqrt(bsf).
if (nghcde=100) sb100=sqrt(bsf).
if (nghcde=110) sb110=sqrt(bsf).
if (nghcde=120) sb120=sqrt(bsf).
if (nghcde=121) sb121=sqrt(bsf).
if (nghcde=130) sb130=sqrt(bsf).
if (nghcde=140) sb140=sqrt(bsf).
if (nghcde=151) sb151=sqrt(bsf).
if (nghcde=152) sb152=sqrt(bsf).
if (nghcde=160) sb160=sqrt(bsf).
if (nghcde=170) sb170=sqrt(bsf).
if (nghcde=171) sb171=sqrt(bsf).
if (nghcde=180) sb180=sqrt(bsf).
if (nghcde=182) sb182=sqrt(bsf).
if (nghcde=210) sb210=sqrt(bsf).
if (nghcde=240) sb240=sqrt(bsf).
if (nghcde=250) sb250=sqrt(bsf).
if (nghcde=260) sb260=sqrt(bsf).
if (nghcde=270) sb270=sqrt(bsf).
if (nghcde=280) sb280=sqrt(bsf).
if (nghcde=310) sb310=sqrt(bsf).
if (nghcde=312) sb312=sqrt(bsf).
if (nghcde=314) sb314=sqrt(bsf).
if (nghcde=315) sb315=sqrt(bsf).
if (nghcde=316) sb316=sqrt(bsf).
if (nghcde=320) sb320=sqrt(bsf).
if (nghcde=330) sb330=sqrt(bsf).
if (nghcde=340) sb340=sqrt(bsf).
if (nghcde=341) sb341=sqrt(bsf).
if (nghcde=342) sb342=sqrt(bsf).
if (nghcde=344) sb344=sqrt(bsf).
if (nghcde=345) sb345=sqrt(bsf).
if (nghcde=400) sb400=sqrt(bsf).

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.

compute bsf02=0.
compute bsf03=0.
compute bsf04=0.
compute bsf05=0.
compute bsf06=0.
compute bsf07=0.
compute bsf08=0.
compute bsf09=0.
compute bsf10=0.
compute bsf11=0.
compute bsf12=0.
compute bsf34=0.
compute bsf78=0.
compute bsf95=0.

if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.

compute bsf=sqftb.

If firepl>1 firepl=1.  
compute nfirepl=n*firepl.

Compute bathsum=fullbath + 0.25*halfbath.
Compute nbathsum=n*bathsum.

compute cenair=0.
if aircond =1 cenair=1.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.
Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute frabsf=frame*bsf.
Compute frmsbsf=framas*bsf.
Compute stubsf=stucco*bsf.
Compute masbsf=mason*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.
Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.


Compute garage1=0.
Compute garage2=0.
Compute garage3=0.
Compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1.  
Compute biggar=0.
if garage3=1 or garage4=1 biggar=1.

compute n=1.0.
if (nghcde=10)  n= 1.22.
if (nghcde=20)  n= 1.18.
if (nghcde=32)  n= 1.67.
if (nghcde=50)  n= 1.07.
if (nghcde=60)  n= 1.64.
if (nghcde=70)  n= 1.30.
if (nghcde=80)  n= 1.32.
if (nghcde=90)  n= 1.97.
if (nghcde=100) n= 2.21.
if (nghcde=110) n= 1.29.
if (nghcde=120) n= 1.49.
if (nghcde=130) n= 1.90.
if (nghcde=140) n= 1.94.
if (nghcde=151) n= 1.31.
if (nghcde=152) n= 1.93.
if (nghcde=160) n= 1.90.
if (nghcde=170) n= 1.77.
if (nghcde=171) n= 1.67.
if (nghcde=180) n= 2.47.
if (nghcde=182) n= 3.09.
if (nghcde=210) n= 1.88.
if (nghcde=240) n= 1.21.
if (nghcde=250) n= 1.38.
if (nghcde=260) n= 1.25.
if (nghcde=270) n= 1.37.
if (nghcde=280) n= 2.08.
if (nghcde=310) n= 2.08.
if (nghcde=312) n= 1.70.
if (nghcde=314) n= 1.35.
if (nghcde=315) n= 2.60.
if (nghcde=316) n= 2.65.
if (nghcde=320) n= 1.65.
if (nghcde=330) n= 1.23.
if (nghcde=340) n= 1.37.
if (nghcde=341) n= 2.12.   
if (nghcde=342) n= 3.37.
if (nghcde=344) n= 2.20.
if (nghcde=345) n= 2.30. 
if (nghcde=400) n= 1.72.

compute nbsf=n*bsf.
compute nlsf=n*lsf.
compute nsrbsf=n*srbsf.
compute nsrlsf=n*srlsf.
compute nbsfs=noapt*nbsf.
compute nsrbsfs=n*srbsf*noapt.
compute nsrage=n*srage.
compute nsrages=n*srage*noapt.

Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).



Compute winter0607=0.
if (mos > 9 and yr=6) or (mos <= 3 and yr=7) winter0607=1.
Compute winter0708=0.
if (mos > 9 and yr=7) or (mos <= 3 and yr=8) winter0708=1.
Compute winter0809=0.
if (mos > 9 and yr=8) or (mos <= 3 and yr=9) winter0809=1.
Compute winter0910=0.
if (mos > 9 and yr=9) or (mos <= 3 and yr=10) winter0910=1.
Compute summer06=0.
if (mos > 3 and yr=6) and (mos <= 9 and yr=6) summer06=1.
Compute summer07=0.
if (mos > 3 and yr=7) and (mos <= 9 and yr=7) summer07=1. 
Compute summer08=0.
if (mos > 3 and yr=8) and (mos <= 9 and yr=8) summer08=1.
Compute summer09=0.
if (mos > 3 and yr=9) and (mos <= 9 and yr=9) summer09=1.
Compute summer10=0.
if (mos > 3 and yr=10) and (mos <= 9 and yr=10) summer10=1.
Compute jantmar06=0.
if (year1=2006 and (mos>=1 and mos<=3)) jantmar06=1. 
Compute octtdec10=0.
if (year1=2010 and (mos>=10 and mos<=12)) octtdec10=1.

Compute jantmar06cl234=jantmar06*cl234.
Compute winter0607cl234=winter0607*cl234.
Compute winter0708cl234=winter0708*cl234.
Compute winter0809cl234=winter0809*cl234.
Compute winter0910cl234=winter0910*cl234.
Compute summer06cl234=summer06*cl234.
Compute summer07cl234=summer07*cl234.
Compute summer08cl234=summer08*cl234.
Compute summer09cl234=summer09*cl234.
Compute summer10cl234=summer10*cl234.
Compute octtdec10cl234=octtdec10*cl234.

Compute jantmar06cl56=jantmar06*cl56.
Compute winter0607cl56=winter0607*cl56.
Compute winter0708cl56=winter0708*cl56.
Compute winter0809cl56=winter0809*cl56.
Compute winter0910cl56=winter0910*cl56.
Compute summer06cl56=summer06*cl56.
Compute summer07cl56=summer07*cl56.
Compute summer08cl56=summer08*cl56.
Compute summer09cl56=summer09*cl56.
Compute summer10cl56=summer10*cl56.
Compute octtdec10cl56=octtdec10*cl56.

Compute jantmar06cl778=jantmar06*cl778.
Compute winter0607cl778=winter0607*cl778.
Compute winter0708cl778=winter0708*cl778.
Compute winter0809cl778=winter0809*cl778.
Compute winter0910cl778=winter0910*cl778.
Compute summer06cl778=summer06*cl778.
Compute summer07cl778=summer07*cl778.
Compute summer08cl778=summer08*cl778.
Compute summer09cl778=summer09*cl778.
Compute summer10cl778=summer10*cl778.
Compute octtdec10cl778=octtdec10*cl778.

Compute jantmar06cl89=jantmar06*cl89.
Compute winter0607cl89=winter0607*cl89.
Compute winter0708cl89=winter0708*cl89.
Compute winter0809cl89=winter0809*cl89.
Compute winter0910cl89=winter0910*cl89.
Compute summer06cl89=summer06*cl89.
Compute summer07cl89=summer07*cl89.
Compute summer08cl89=summer08*cl89.
Compute summer09cl89=summer09*cl89.
Compute summer10cl89=summer10*cl89.
Compute octtdec10cl89=octtdec10*cl89.

Compute jantmar06cl1112=jantmar06*cl1112. 
Compute winter0607cl1112=winter0607*cl1112.
Compute winter0708cl1112=winter0708*cl1112.
Compute winter0809cl1112=winter0809*cl1112.
Compute winter0910cl1112=winter0910*cl1112.
Compute summer06cl1112=summer06*cl1112.
Compute summer07cl1112=summer07*cl1112.
Compute summer08cl1112=summer08*cl1112.
Compute summer09cl1112=summer09*cl1112.
Compute summer10cl1112=summer10*cl1112.
Compute octtdec10cl1112=octtdec10*cl1112.

Compute jantmar06cl1095=jantmar06*cl1095.
Compute winter0607cl1095=winter0607*cl1095.
Compute winter0708cl1095=winter0708*cl1095.
Compute winter0809cl1095=winter0809*cl1095.
Compute winter0910cl1095=winter0910*cl1095.
Compute summer06cl1095=summer06*cl1095.
Compute summer07cl1095=summer07*cl1095.
Compute summer08cl1095=summer08*cl1095.
Compute summer09cl1095=summer09*cl1095.
Compute summer10cl1095=summer10*cl1095.
Compute octtdec10cl1095=octtdec10*cl1095.

Compute jantmar06clsplt=jantmar06*clsplt.
Compute winter0607clsplt=winter0607*clsplt.
Compute winter0708clsplt=winter0708*clsplt.
Compute winter0809clsplt=winter0809*clsplt.
Compute winter0910clsplt=winter0910*clsplt.
Compute summer06clsplt=summer06*clsplt.
Compute summer07clsplt=summer07*clsplt.
Compute summer08clsplt=summer08*clsplt.
Compute summer09clsplt=summer09*clsplt.
Compute summer10clsplt=summer10*clsplt.
Compute octtdec10clsplt=octtdec10*clsplt.


Compute lowzonebremen=0.
if town=13 and  (nghcde=342 or nghcde=140  or nghcde=180  or nghcde=182 
or nghcde=170 or nghcde=210 or nghcde=310 or nghcde=100 or nghcde=341
or nghcde=315 or nghcde=130 or nghcde=316 )  lowzonebremen=1.                         	

Compute midzonebremen=0.
if town=13 and (nghcde=171 or nghcde=10 or nghcde=32 or nghcde=312
or nghcde=152 or nghcde=20 or nghcde=70 or nghcde=80 or nghcde=280
or nghcde=400 or nghcde=160)   midzonebremen=1. 

Compute highzonebremen=0.
if town=13 and (nghcde=90 or nghcde=314 or nghcde=60 
or nghcde=151 or nghcde=344 or nghcde=330  or nghcde=50  or nghcde=320
or nghcde=270 or nghcde=120  or nghcde=110 or nghcde=240 or nghcde=340 or nghcde=260 or nghcde=250 or nghcde=345)   highzonebremen=1.


Compute srfxlowblockbremen=0.
if lowzonebremen=1 srfxlowblockbremen=srfx*lowzonebremen.

Compute srfxmidblockbremen=0.
if midzonebremen=1 srfxmidblockbremen=srfx*midzonebremen.

Compute srfxhighblockbremen=0.
if highzonebremen=1 srfxhighblockbremen=srfx*highzonebremen.


if num=6 bnum=0.
if num=0 bnum=0.
if num=1 bnum=2.
if num=2 bnum=3.
if num=3 bnum=4.
if num=4 bnum=5.
if num=5 bnum=6.

compute res=bnum - comm.
compute bnumb=bnum*bsf.
compute nres=n*res.

Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
compute nbasfull=n*basefull.
compute nbaspart=n*basepart.

Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.
Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
Compute nnobase=n*nobase.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
compute nluxbsf=0.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.

Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute repabove=0.
Compute repbelow=0.
Compute repave=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabsf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.
Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.
Compute twosty=0.
If class07=1 or class08=1 or class78=1 twosty=1.

compute b=1.

*select if year1 = 2010.
*select if puremarket=1.
*Table observation = b
                amount1
               /table = nghcde by 
                           amount1 
                             + b      
	/title = 'Ave & Med Sales'
     /statistics =  median (amount1 'MED SP')
              mean (amount1 'MEAN SP')
              validn (b '# PROPS').
   
compute mv = (132314.551	
+ 1.882553473*nbsf
- 68580.06299*highzonebremen
+ 4592.742883*nnobase
- 7409.403277*nsrage
- 2538.474075*srfxmidblockbremen
+ 178.056158*nsrlsf
- 588.5820083*sb10
+ 6896.456098*nbathsum
- 1004.93692*sb20
+ 988.7738037*sb314
+ 1382.904721*sb151
+ 366.0221437	*sb170
+ 724.8347539*nbsf1095
+ 523.3866032*sb50
+ 39178.5833*biggar
+ 12599.46811*garage2
- 55991.37613*midzonebremen
+ 2216.316266*sb90
- 685.1842413*sb320
+ 708.069645*sb344
+ 5865.901217*nfirepl
+ 11.26370409*masbsf
- 750.826656*sb340
- 566.5826909*sb316
+ 2.338013268*nbsfair
- 537.0795857*sb110
+ 1387.00898*sb152
+ 1736.802585*sb171
- 82.7830608*sb182
+ 6731.379953*garage1
+ 506.63979*nbsf778
+ 1269.956634*sb312
+ 1135.87873*sb32
+ 1117.822297*sb80
+ 818.9414461*sb280
+ 839.5413139*sb70
+ 239.0495339*sb140
+ 16282.14106*nbasfull
+ 5.734652578*frabsf
+ 262.1845594*sb270
+ 13364.48063*nbaspart
+ 581.9415282*sb160
+ 346.3400642*sb330
- 1121.636589*srfxlowblockbremen
- 1117.89761*srfxhighblockbremen
+ 770.3311241*nbsf34
+ 2127.408239*nbsf56
+ 1174.490104*nbsf1112
+ 10.26581728*stubsf
+ 8.69158211*frmsbsf
+ 26.66556892*sb100
+ 25.5923271*sb210
- 20.32005876*sb250
+ 706.361606*nbsf234)*1.0.



save outfile='C:\Program Files\SPSS\spssa\mv13.sav'/keep town pin mv.
 

