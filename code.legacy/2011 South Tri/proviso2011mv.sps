                               		*SPSS Regression.
		                       		* Proviso 2011.

Get file='C:\Program Files\SPSS\spssa\provisomergefcl2.sav'.
*select if (amount1>85000).
*select if (amount1<600000).
*select if (multi<1).
*select if sqftb<6000.
Compute year1=0.
If  (amount1>0) year1=1900 + yr.  
If  (yr=10 and amount1>0) year1=2010.
If  (yr=9 and amount1>0)  year1=2009.
If  (yr=8 and amount1>0)  year1=2008.  
If  (yr=7 and amount1>0)  year1=2007.
If  (yr=6 and amount1>0)  year1=2006.
If  (yr=5 and amount1>0)  year1=2005.
If  (yr=4 and amount1>0)  year1=2004.
If  (yr=3 and amount1>0)  year1=2003.   
If  (yr=2 and amount1>0)  year1=2002.
If  (yr=1 and amount1>0)  year1=2001.
If  (yr=0 and amount1>0)  year1=2000.

COMPUTE FX = cumfile7891011.
COMPUTE SRFX = sqrt(fx).
RECODE FX (SYSMIS=0).
*******************************************************************************************************************.

*select if (year1=2006 or puremarket=1). 

set mxcells=2500000.
compute bs=0.
if age<10 and (amount1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.
if (pin=	15052180090000
or pin=	15052200050000
or pin=	15081180100000
or pin=	15082240150000
or pin=	15084050200000
or pin=	15092060040000
or pin=	15092060200000
or pin=	15092070320000
or pin=	15191010800000
or pin=	15222110270000
or pin=	15222160170000
or pin=	15141320140000
or pin=	15141320200000
or pin=	15141610130000
or pin=	15141630170000
or pin=	15151030370000
or pin=	15151240230000
or pin=	15152220250000
or pin=	15152320040000
or pin=	15093200230000
or pin=	15151080010000
or pin=	15151110070000
or pin=	15151200220000
or pin=	15161070530000
or pin=	15161250010000
or pin=	15162040500000
or pin=	15143030120000
or pin=	15153150210000
or pin=	15153310100000
or pin=	15154170470000
or pin=	15111070080000
or pin=	15111250130000
or pin=	15031110200000
or pin=	15031200130000
or pin=	15031210270000
or pin=	15032040320000
or pin=	15032070420000
or pin=	15032100140000
or pin=	15033450020000
or pin=	15034130020000
or pin=	15034500080000
or pin=	15034530020000
or pin=	15101060150000
or pin=	15101060280000
or pin=	15021070110000
or pin=	15021070260000
or pin=	15021070460000
or pin=	15021110530000
or pin=	15021110890000
or pin=	15102310440000
or pin=	15102320280000
or pin=	15102330320000
or pin=	15103070150000
or pin=	15103070160000
or pin=	15103110280000
or pin=	15103130260000
or pin=	15103150100000
or pin=	15103160180000
or pin=	15104290010000
or pin=	15104390070000
or pin=	15113020130000
or pin=	15113050100000
or pin=	15113240110000
or pin=	15141120150000
or pin=	15141230160000
or pin=	15152060260000
or pin=	15072100080000
or pin=	15072100120000
or pin=	15072150830000
or pin=	15073040210000
or pin=	15073100310000
or pin=	15174100210000
or pin=	15182290050000
or pin=	15124090050000
or pin=	15124150180000
or pin=	15124350010000
or pin=	15132060070000
or pin=	15132060250000
or pin=	15132090210000
or pin=	15132090270000
or pin=	15132140080000
or pin=	15133150200000
or pin=	15134010040000
or pin=	15134070080000
or pin=	15134090090000
or pin=	15134100090000
or pin=	15134160180000
or pin=	15134180220000
or pin=	15134210100000
or pin=	15134220080000
or pin=	15134250170000
or pin=	15242020230000
or pin=	15242020530000
or pin=	15242140040000
or pin=	15242150350000
or pin=	15293150190000
or pin=	15284120220000
or pin=	15203040130000
or pin=	15203130220000
or pin=	15204030150000
or pin=	15204140770000
or pin=	15291030200000
or pin=	15291130310000
or pin=	15292030400000
or pin=	15292290160000
or pin=	15201020370000
or pin=	15201120470000
or pin=	15201130440000
or pin=	15201170420000
or pin=	15201180480000
or pin=	15211060530000
or pin=	15211090600000
or pin=	15213070540000
or pin=	15213130110000
or pin=	15163140500000
or pin=	15164180310000
or pin=	15273110110000
or pin=	15273170020000
or pin=	15284300040000
or pin=	15331120200000
or pin=	15334050080000
or pin=	15334050190000
or pin=	15314080220000
or pin=	15323040200000
or pin=	15333010220000
or pin=	15333030060000
or pin=	15333310230000
or pin=	15331030090000
or pin=	15331170180000
or pin=	15331190010000
or pin=	15331190150000
or pin=	15103000080000
or pin=	15341220530000
or pin=	15341250190000
or pin=	15341300510000
or pin=	15341300550000
or pin=	15343060080000
or pin=	15343080290000
or pin=	15343250140000
or pin=	15272020400000
or pin=	15272100180000
or pin=	15272150420000
or pin=	15041000330000
or pin=	15041120270000
or pin=	15041130190000
or pin=	15043000550000
or pin=	15054040030000
or pin=	15041220290000) bs=1.
*select if bs=0.

Compute N=1.
compute b=1.
compute bsf=sqftb.
compute lsf=sqftl.
compute srbsf=sqrt(sqftb).
compute srage=sqrt(age).
compute srlsf=sqrt(sqftl).

compute tnb=(town*1000) + nghcde.

if (tnb= 31010 ) n=1.45.
if (tnb= 31020 ) n=1.44.
if (tnb= 31021 ) n=1.44.
if (tnb= 31030 ) n=1.46.
if (tnb= 31031 ) n=1.18.
if (tnb= 31032 ) n=1.25.
if (tnb= 31033 ) n=2.19.
if (tnb= 31034 ) n=1.35.
if (tnb= 31040 ) n=1.49.
if (tnb= 31041 ) n=1.34.
if (tnb= 31042 ) n=1.61.
if (tnb= 31050 ) n=2.39.
if (tnb= 31060 ) n=1.23.
if (tnb= 31080 ) n=1.65.
if (tnb= 31091 ) n=2.83.
if (tnb= 31092 ) n=2.45.
if (tnb= 31102 ) n=2.71.
if (tnb= 31103 ) n=2.93.
if (tnb= 31104 ) n=2.41.
if (tnb= 31110 ) n=2.02.
if (tnb= 31120 ) n=1.72.
if (tnb= 31141 ) n=2.62.
if (tnb= 31150 ) n=4.10.
if (tnb= 31151 ) n=3.88.
if (tnb= 31152 ) n=3.42.
if (tnb= 31160 ) n=2.15.
if (tnb= 31161 ) n=2.47.
if (tnb= 31170 ) n=2.25.
if (tnb= 31173 ) n=3.01.
if (tnb= 31174 ) n=4.45.
if (tnb= 31190 ) n=1.49.
if (tnb= 31200 ) n=1.52.
if (tnb= 31210 ) n=2.29.


Compute n31010=0.
Compute n31020=0.
Compute n31021=0.
Compute n31030=0.
Compute n31031=0.
Compute n31032=0.
Compute n31033=0.
Compute n31034=0.
Compute n31040=0.
Compute n31041=0.
compute n31042=0.
compute n31050=0.
compute n31060=0.
compute n31080=0.
compute n31091=0.
compute n31092=0.
compute n31102=0.
compute n31103=0.
compute n31104=0.
compute n31110=0.
compute n31120=0.
compute n31141=0.
compute n31150=0.
compute n31151=0.
compute n31152=0.
compute n31160=0.
compute n31161=0.
compute n31170=0.
compute n31173=0.
compute n31174=0.
compute n31190=0.
compute n31200=0.
compute n31210=0.


Compute b31010=0.
Compute b31020=0.
Compute b31021=0.
Compute b31030=0.
Compute b31031=0.
Compute b31032=0.
Compute b31033=0.
Compute b31034=0.
Compute b31040=0.
Compute b31041=0.
compute b31042=0.
compute b31050=0.
compute b31060=0.
compute b31080=0.
compute b31091=0.
compute b31092=0.
compute b31102=0.
compute b31103=0.
compute b31104=0.
compute b31110=0.
compute b31120=0.
compute b31141=0.
compute b31150=0.
compute b31151=0.
compute b31152=0.
compute b31160=0.
compute b31161=0.
compute b31170=0.
compute b31172=0.
compute b31173=0.
compute b31174=0.
compute b31190=0.
compute b31200=0.
compute b31210=0.

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.

compute bsf=sqftb.
compute bsf02=0.
compute bsf03=0.
compute bsf04=0.
compute bsf05=0.
compute bsf06=0.
compute bsf07=0.
compute bsf08=0.
compute bsf09=0.
compute bsf10=0.
compute bsf11=0.
compute bsf12=0.
compute bsf34=0.
compute bsf78=0.
compute bsf95=0.
if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.


if (tnb=31010)    n31010=1.
if (tnb=31020)    n31020=1.
if (tnb=31021)    n31021=1.
if (tnb=31030)    n31030=1.
if (tnb=31031)    n31031=1.
if (tnb=31032)    n31032=1.
if (tnb=31033)    n31033=1.
if (tnb=31034)    n31034=1.
if (tnb=31040)    n31040=1.
if (tnb=31041)    n31041=1.
if (tnb=31042)    n31042=1.
if (tnb=31050)    n31050=1.
if (tnb=31060)    n31060=1.
if (tnb=31080)    n31080=1.
if (tnb=31091)    n31091=1.
if (tnb=31092)    n31092=1.
if (tnb=31102)    n31102=1.
if (tnb=31103)    n31103=1.
if (tnb=31104)    n31104=1.
if (tnb=31110)    n31110=1.
if (tnb=31120)    n31120=1.
if (tnb=31141)    n31141=1.
if (tnb=31150)    n31150=1.
if (tnb=31151)    n31151=1.
if (tnb=31152)    n31152=1.
if (tnb=31160)    n31160=1.
if (tnb=31161)    n31161=1.
if (tnb=31170)    n31170=1.
if (tnb=31173)    n31173=1.
if (tnb=31174)    n31174=1.
if (tnb=31190)    n31190=1.
if (tnb=31200)    n31200=1.
if (tnb=31210)    n31210=1.

compute bathsum= fullbath + .25*halfbath.
compute nbathsum=n*bathsum.
compute nbsf=n*bsf. 
compute nsrbsf=n*srbsf.

compute sb31010=0.
compute sb31020=0.
compute sb31021=0.
compute sb31030=0.
compute sb31031=0.
compute sb31032=0.
compute sb31033=0.
compute sb31034=0.
compute sb31040=0.
compute sb31041=0.
compute sb31042=0.
compute sb31050=0.
compute sb31060=0.
compute sb31080=0.
compute sb31091=0.
compute sb31092=0.
compute sb31102=0.
compute sb31103=0.
compute sb31104=0.
compute sb31110=0.
compute sb31120=0.
compute sb31141=0.
compute sb31150=0.
compute sb31151=0.
compute sb31152=0.
compute sb31160=0.
compute sb31161=0.
compute sb31170=0.
compute sb31173=0.
compute sb31174=0.
compute sb31190=0.
compute sb31200=0.
compute sb31210=0.

if (tnb=31010)   sb31010=sqrt(bsf).
if (tnb=31020)   sb31020=sqrt(bsf).
if (tnb=31021)   sb31021=sqrt(bsf).
if (tnb=31030)   sb31030=sqrt(bsf).
if (tnb=31031)   sb31031=sqrt(bsf).
if (tnb=31032)   sb31032=sqrt(bsf).
if (tnb=31033)   sb31033=sqrt(bsf).
if (tnb=31034)   sb31034=sqrt(bsf).
if (tnb=31040)   sb31040=sqrt(bsf).
if (tnb=31041)   sb31041=sqrt(bsf).
if (tnb=31042)   sb31042=sqrt(bsf).
if (tnb=31050)   sb31050=sqrt(bsf).
if (tnb=31060)   sb31060=sqrt(bsf).
if (tnb=31080)   sb31080=sqrt(bsf).
if (tnb=31091)   sb31091=sqrt(bsf).
if (tnb=31092)   sb31092=sqrt(bsf).
if (tnb=31102)   sb31102=sqrt(bsf).
if (tnb=31103)   sb31103=sqrt(bsf).
if (tnb=31104)   sb31104=sqrt(bsf).
if (tnb=31110)   sb31110=sqrt(bsf).
if (tnb=31120)   sb31120=sqrt(bsf).
if (tnb=31141)   sb31141=sqrt(bsf).
if (tnb=31150)   sb31150=sqrt(bsf).
if (tnb=31151)   sb31151=sqrt(bsf).
if (tnb=31152)   sb31152=sqrt(bsf).
if (tnb=31160)   sb31160=sqrt(bsf).
if (tnb=31161)   sb31161=sqrt(bsf).
if (tnb=31170)   sb31170=sqrt(bsf).
if (tnb=31173)   sb31173=sqrt(bsf).
if (tnb=31174)   sb31174=sqrt(bsf).
if (tnb=31190)   sb31190=sqrt(bsf).
if (tnb=31200)   sb31200=sqrt(bsf).
if (tnb=31210)   sb31210=sqrt(bsf).


compute cenair=0.
if aircond=1 cenair=1.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.

Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute frabsf=frame*bsf.
Compute stubsf=stucco*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.
Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
compute frabsf=0.
compute masbsf=0.
compute frmsbsf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.
if extcon=1 frabsf=frame*bsf.
if extcon=2 masbsf=mason*bsf.
if extcon=3 frmsbsf=framas*bsf.

If firepl>0 firepl=1.
compute nfirepl=n*firepl.
compute nsrage=n*srage.
compute frast=frame + stucco.
compute frastbsf=frast*sqftb.

compute nbsf=n*bsf.
compute nlsf=n*lsf.
compute nsrbsf=n*srbsf.
compute nsrlsf=n*srlsf.

compute nbsf02=nbsf*class02.
compute nbsf03=nbsf*class03.
compute nbsf04=nbsf*class04.
compute nbsf05=nbsf*class05.
compute nbsf06=nbsf*class06.
compute nbsf07=nbsf*class07.
compute nbsf08=nbsf*class08.
compute nbsf09=nbsf*class09.
compute nbsf10=nbsf*class10.
compute nbsf11=nbsf*class11.
compute nbsf12=nbsf*class12.
compute nbsf34=nbsf*class34.
compute nbsf78=nbsf*class78.
compute nbsf95=nbsf*class95.

if class=95 and nghcde=32  lsf=1000.
if class=95 and nghcde=41  lsf=1750.
if class=95 and nghcde=91  lsf=1300.
if class=95 and nghcde=92  lsf=1300.
if class=95 and nghcde=102 lsf=3000.
if class=95 and nghcde=160 lsf=2800.
if class=95 and nghcde=173 lsf=4000.
if class=95 and nghcde=174 lsf=5000.  


compute nsrage=n*srage.

if num=6 bnum=0.
if num=0 bnum=0.
if num=1 bnum=2.
if num=2 bnum=3.
if num=3 bnum=4.
if num=4 bnum=5.
if num=5 bnum=6.

compute res=bnum - comm.
compute bnumb=bnum*bsf.
compute nbnum=n*bnum.
compute nres=n*res.
compute nbsfair=n*bsfair.

Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
compute nbasefull=n*basefull.
compute nbasepart=n*basepart.
Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
compute nnobase=n*nobase.

Compute lowzoneproviso=0.
if town=31 and  (nghcde=91 or nghcde=102  or nghcde=103  or nghcde=104 
or nghcde=110 or nghcde=141 or nghcde=150 or nghcde=151
or nghcde=152 or nghcde=160 or nghcde=161 or nghcde=170 or nghcde=173
or nghcde=174 or nghcde=210)  lowzoneproviso=1.                         	

Compute midzoneproviso=0.
if town=31 and (nghcde=21 or nghcde=30 or nghcde=33 
or nghcde=42 or nghcde=50 or nghcde=80    
or nghcde=92 or nghcde=120)   midzoneproviso=1. 

Compute highzoneproviso=0.
if town=31 and (nghcde=10 or nghcde=20 or nghcde=31 
or nghcde=32 or nghcde=34  or nghcde=40 or nghcde=41
or nghcde=60 or nghcde=190 or nghcde=200)   highzoneproviso=1.

Compute srfxlowblockproviso=0.
if lowzoneproviso=1 srfxlowblockproviso=srfx*lowzoneproviso.

Compute srfxmidblockproviso=0.
if midzoneproviso=1 srfxmidblockproviso=srfx*midzoneproviso.

Compute srfxhighblockproviso=0.
if highzoneproviso=1 srfxhighblockproviso=srfx*highzoneproviso.

Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).

Compute winter0607=0.
if (mos > 9 and yr=6) or (mos <= 3 and yr=7) winter0607=1.
Compute winter0708=0.
if (mos > 9 and yr=7) or (mos <= 3 and yr=8) winter0708=1.
Compute winter0809=0.
if (mos > 9 and yr=8) or (mos <= 3 and yr=9) winter0809=1.
Compute winter0910=0.
if (mos > 9 and yr=9) or (mos <= 3 and yr=10) winter0910=1.
Compute summer06=0.
if (mos > 3 and yr=6) and (mos <= 9 and yr=6) summer06=1.
Compute summer07=0.
if (mos > 3 and yr=7) and (mos <= 9 and yr=7) summer07=1. 
Compute summer08=0.
if (mos > 3 and yr=8) and (mos <= 9 and yr=8) summer08=1.
Compute summer09=0.
if (mos > 3 and yr=9) and (mos <= 9 and yr=9) summer09=1.
Compute summer10=0.
if (mos > 3 and yr=10) and (mos <= 9 and yr=10) summer10=1.
Compute jantmar06=0.
if (year1=2006 and (mos>=1 and mos<=3)) jantmar06=1. 
Compute octtdec10=0.
if (year1=2010 and (mos>=10 and mos<=12)) octtdec10=1.

Compute jantmar06cl234=jantmar06*cl234.
Compute winter0607cl234=winter0607*cl234.
Compute winter0708cl234=winter0708*cl234.
Compute winter0809cl234=winter0809*cl234.
Compute winter0910cl234=winter0910*cl234.
Compute summer06cl234=summer06*cl234.
Compute summer07cl234=summer07*cl234.
Compute summer08cl234=summer08*cl234.
Compute summer09cl234=summer09*cl234.
Compute summer10cl234=summer10*cl234.
Compute octtdec10cl234=octtdec10*cl234.

Compute jantmar06cl56=jantmar06*cl56.
Compute winter0607cl56=winter0607*cl56.
Compute winter0708cl56=winter0708*cl56.
Compute winter0809cl56=winter0809*cl56.
Compute winter0910cl56=winter0910*cl56.
Compute summer06cl56=summer06*cl56.
Compute summer07cl56=summer07*cl56.
Compute summer08cl56=summer08*cl56.
Compute summer09cl56=summer09*cl56.
Compute summer10cl56=summer10*cl56.
Compute octtdec10cl56=octtdec10*cl56.

Compute jantmar06cl778=jantmar06*cl778.
Compute winter0607cl778=winter0607*cl778.
Compute winter0708cl778=winter0708*cl778.
Compute winter0809cl778=winter0809*cl778.
Compute winter0910cl778=winter0910*cl778.
Compute summer06cl778=summer06*cl778.
Compute summer07cl778=summer07*cl778.
Compute summer08cl778=summer08*cl778.
Compute summer09cl778=summer09*cl778.
Compute summer10cl778=summer10*cl778.
Compute octtdec10cl778=octtdec10*cl778.

Compute jantmar06cl89=jantmar06*cl89.
Compute winter0607cl89=winter0607*cl89.
Compute winter0708cl89=winter0708*cl89.
Compute winter0809cl89=winter0809*cl89.
Compute winter0910cl89=winter0910*cl89.
Compute summer06cl89=summer06*cl89.
Compute summer07cl89=summer07*cl89.
Compute summer08cl89=summer08*cl89.
Compute summer09cl89=summer09*cl89.
Compute summer10cl89=summer10*cl89.
Compute octtdec10cl89=octtdec10*cl89.

Compute jantmar06cl1112=jantmar06*cl1112. 
Compute winter0607cl1112=winter0607*cl1112.
Compute winter0708cl1112=winter0708*cl1112.
Compute winter0809cl1112=winter0809*cl1112.
Compute winter0910cl1112=winter0910*cl1112.
Compute summer06cl1112=summer06*cl1112.
Compute summer07cl1112=summer07*cl1112.
Compute summer08cl1112=summer08*cl1112.
Compute summer09cl1112=summer09*cl1112.
Compute summer10cl1112=summer10*cl1112.
Compute octtdec10cl1112=octtdec10*cl1112.

Compute jantmar06cl1095=jantmar06*cl1095.
Compute winter0607cl1095=winter0607*cl1095.
Compute winter0708cl1095=winter0708*cl1095.
Compute winter0809cl1095=winter0809*cl1095.
Compute winter0910cl1095=winter0910*cl1095.
Compute summer06cl1095=summer06*cl1095.
Compute summer07cl1095=summer07*cl1095.
Compute summer08cl1095=summer08*cl1095.
Compute summer09cl1095=summer09*cl1095.
Compute summer10cl1095=summer10*cl1095.
Compute octtdec10cl1095=octtdec10*cl1095.

Compute jantmar06clsplt=jantmar06*clsplt.
Compute winter0607clsplt=winter0607*clsplt.
Compute winter0708clsplt=winter0708*clsplt.
Compute winter0809clsplt=winter0809*clsplt.
Compute winter0910clsplt=winter0910*clsplt.
Compute summer06clsplt=summer06*clsplt.
Compute summer07clsplt=summer07*clsplt.
Compute summer08clsplt=summer08*clsplt.
Compute summer09clsplt=summer09*clsplt.
Compute summer10clsplt=summer10*clsplt.
Compute octtdec10clsplt=octtdec10*clsplt.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
compute nluxbsf=0.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.

Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute repabove=0.
Compute repbelow=0.
Compute repave=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabsf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.

Compute garage1=0.
Compute garage2=0.
Compute garage3=0.
Compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1.  
Compute biggar=0.
if garage3=1 or garage4=1 biggar=1.

compute b=1.
*select if year1 = 2010.
*select if puremarket=1.
*Table observation = b
                amount1
               /table = nghcde by 
                           amount1 
                             + b      
	/title = 'Ave & Med Sales'
     	/statistics =  median (amount1 'MED SP')
              mean (amount1 'MEAN SP')
                         validn (b '# PROPS').


compute mv = (3939.329496	
+ 2648.141115*nsrbsf
- 13523.58483*nnobase
+ 1748.966768*sb31041
+ 7226.415556	*nbathsum
+ 152.9548016*nsrlsf
+ 52.7231067*masbsf
+ 752.0893913*sb31042
+ 8723.203264	*garage2
+ 52.66316659	*frmsbsf
+ 47.39187466	*frastbsf
- 34.66407332	*nbsf
- 318.7888482*sb31050
+ 2.234946409	*nbsfair
+ 5423.276751*nfirepl
+ 816.0202282*sb31091
+ 574.6749516*sb31160
+ 609.1366775*sb31141
+ 1093.767223*sb31010
+ 745.9689594*sb31092
+ 561.5403341*sb31210
+ 369.5596255*sb31110
+ 1028.669541*sb31200
- 6280.388705*nbasepart
+ 1351.592703*sb31021
+ 250.488019*sb31080
+ 508.2838476*sb31190
+ 402.7805896*sb31161
+ 298.411855*sb31032
- 621.8225136*sb31033
- 868.3460872*nsrage
+ 531.2595761*sb31150
- 15623.7991*	highzoneproviso
- 7952.408874	*srfxhighblockproviso
- 5578.511015	*midzoneproviso
- 3888.820336	*srfxlowblockproviso
- 1679.791275	*srfxmidblockproviso
- 1402.362735	*nbasefull
+ 5415.622951*garage1
+ 10691.05945*biggar
+ 196.9755679*sb31040)*1.0.


* RUN THIS PROCEDURE TO CALCULATE THE ADJ FACTOR.
*sel if town=31.
*sel if puremarket=1 and year1=2010.
*compute av=mv*0.10.
*compute ratio=av/amount1.
*fre format=notable/var=ratio/sta=med.

save outfile='C:\Program Files\SPSS\spssa\mv31.sav'/keep town pin mv.


