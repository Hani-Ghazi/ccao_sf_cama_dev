                             *SPSS Regression.
		 	*Lemont, Orland, and Palos Regression 2011.                                                            



get file='C:\Program Files\SPSS\spssa\lempalorlmergefcl2.sav'.
*select if (amount1>145000).
*select if (amount1<890000).
*select if (multi<1).
*select if sqftb<6000.
Compute year1=0.
If  (amount1>0) year1=1900 + yr. 
If  (yr=10 and amount1>0)  year1=2010.
If  (yr=9 and amount1>0)  year1=2009.
If  (yr=8 and amount1>0)  year1=2008. 
If  (yr=7 and amount1>0)  year1=2007. 
If  (yr=6 and amount1>0)  year1=2006.
If  (yr=5 and amount1>0)  year1=2005.
If  (yr=4 and amount1>0)  year1=2004.
If  (yr=3 and amount1>0)  year1=2003.   
If  (yr=2 and amount1>0)  year1=2002.
If  (yr=1 and amount1>0)  year1=2001.
If  (yr=0 and amount1>0)  year1=2000.

COMPUTE FX = cumfile7891011.
COMPUTE SRFX = sqrt(fx).
RECODE FX (SYSMIS=0).
*******************************************************************************************************************.

*select if (year1=2006 or puremarket=1). 

set mxcells=2000500.
Compute bs=0.
if age<10 and (amount1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.
if (pin=	22291110020000
or pin=	22144020030000
or pin=	22281040480000
or pin=	22294070030000
or pin=	22243010140000
or pin=	22273020070000
or pin=	22274010140000
or pin=	22312030080000
or pin=	22312030090000
or pin=	22312040040000
or pin=	22323060030000
or pin=	22351040240000
or pin=	22351060090000
or pin=	22341030410000
or pin=	22341060050000
or pin=	22341080140000
or pin=	22261030060000
or pin=	22303080120000
or pin=	22303100010000
or pin=	22303100140000
or pin=	22303120130000
or pin=	22303120250000
or pin=	27064090010000
or pin=	27071010190000
or pin=	27071030070000
or pin=	27071060180000
or pin=	27182090070000
or pin=	27091160020000
or pin=	27011050100000
or pin=	27031080030000
or pin=	27111070210000
or pin=	27144070020000
or pin=	27092170360000
or pin=	27251060060000
or pin=	27264070030000
or pin=	27264090070000
or pin=	27023190270000
or pin=	27053040270000
or pin=	27053040310000
or pin=	27171010220000
or pin=	27173140010000
or pin=	27174050050000
or pin=	27291000380000
or pin=	27291120010000
or pin=	27291120030000
or pin=	27291130010000
or pin=	27292010140000
or pin=	27292050070000
or pin=	27292210200000
or pin=	27293130070000
or pin=	27294050100000
or pin=	27294190070000
or pin=	27273070100000
or pin=	23141080290000
or pin=	23022050240000
or pin=	23024110140000
or pin=	23112080120000
or pin=	23114050370000
or pin=	23102040100000
or pin=	23013070220000
or pin=	23134030330000
or pin=	23023040010000
or pin=	23354140050000
or pin=	23104010120000
or pin=	23361090070000
or pin=	23362130230000
or pin=	23014180030000
or pin=	23014180160000
or pin=	23294070180000
or pin=	23322060100000
or pin=	23261060120000
or pin=	23262090080000
or pin=	23264120070000
or pin=	23351050340000
or pin=	23352000280000
or pin=	23271050030000
or pin=	23272030390000
or pin=	23254050030000
or pin=	23254100020000
or pin=	23254140010000)	bs=1.
*select if bs=0.

Compute N=1.

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.

compute bsf=sqftb.

compute bsf02=0.
compute bsf03=0.
compute bsf04=0.
compute bsf05=0.
compute bsf06=0.
compute bsf07=0.
compute bsf08=0.
compute bsf09=0.
compute bsf10=0.
compute bsf11=0.
compute bsf12=0.
compute bsf34=0.
compute bsf78=0.
compute bsf95=0.

if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.

compute tnb=(town*1000)+nghcde.

if (tnb= 19010 ) n=2.30.
if (tnb= 19011 ) n=3.55.
if (tnb= 19020 ) n=3.12.
if (tnb= 19025 ) n=3.90.
if (tnb= 19030 ) n=3.59.
if (tnb= 19032 ) n=2.38.
if (tnb= 19038 ) n=4.24.
if (tnb= 19040 ) n=5.08.
if (tnb= 19050 ) n=2.57.
if (tnb= 19060 ) n=4.72.
if (tnb= 19061 ) n=4.05.
if (tnb= 19100 ) n=5.43.

if (tnb= 28011 ) n=2.88.
if (tnb= 28012 ) n=5.88.
if (tnb= 28013 ) n=3.37.
if (tnb= 28014 ) n=3.35.
if (tnb= 28015 ) n=3.02.
if (tnb= 28022 ) n=2.17.
if (tnb= 28030 ) n=3.38.
if (tnb= 28031 ) n=2.72. 
if (tnb= 28032 ) n=2.30.
if (tnb= 28034 ) n=5.04.
if (tnb= 28036 ) n=2.75.
if (tnb= 28037 ) n=3.70.
if (tnb= 28039 ) n=2.31.
if (tnb= 28040 ) n=1.83.
if (tnb= 28043 ) n=2.52.
if (tnb= 28045 ) n=3.20.
if (tnb= 28046 ) n=2.40.
if (tnb= 28047 ) n=3.05.
if (tnb= 28050 ) n=2.13.
if (tnb= 28061 ) n=2.11.
if (tnb= 28088 ) n=2.90.
if (tnb= 28099 ) n=2.48.
if (tnb= 28100 ) n=4.70.
if (tnb= 28101 ) n=4.10.
if (tnb= 28102 ) n=3.45.
if (tnb= 28103 ) n=2.61.
if (tnb= 28104 ) n=3.05. 

if (tnb= 30010 ) n=2.30.
if (tnb= 30011 ) n=2.73.
if (tnb= 30012 ) n=2.39.
if (tnb= 30020 ) n=2.60.
if (tnb= 30022 ) n=2.10.
if (tnb= 30030 ) n=1.98.
if (tnb= 30031 ) n=2.63.
if (tnb= 30040 ) n=2.83.
if (tnb= 30041 ) n=3.46.
if (tnb= 30042 ) n=1.55.
if (tnb= 30050 ) n=3.35.
if (tnb= 30051 ) n=3.60.
if (tnb= 30052 ) n=4.38.
if (tnb= 30053 ) n=3.19.
if (tnb= 30054 ) n=3.41.
if (tnb= 30060 ) n=2.67.
if (tnb= 30061 ) n=2.57.
if (tnb= 30062 ) n=4.36.
if (tnb= 30063 ) n=4.85.
if (tnb= 30070 ) n=2.95.
if (tnb= 30071 ) n=4.10.
if (tnb= 30080 ) n=3.20.
if (tnb= 30082 ) n=7.83.
if (tnb= 30092 ) n=4.05.

compute n19010=0.
compute n19011=0.
compute n19020=0.
compute n19025=0.
compute n19030=0.
compute n19032=0.
compute n19038=0.
compute n19040=0.
compute n19050=0.
compute n19060=0.
compute n19061=0.
compute n19071=0.
compute n19100=0.

compute n28011=0.
compute n28012=0.
compute n28013=0.
compute n28014=0.
compute n28015=0.
compute n28022=0.
compute n28030=0.
compute n28031=0. 
compute n28032=0.
compute n28034=0.
compute n28036=0.
compute n28037=0.
compute n28039=0.
compute n28040=0.
compute n28043=0.
compute n28045=0.
compute n28046=0.
compute n28047=0.
compute n28050=0.
compute n28061=0.
compute n28088=0.
compute n28099=0.
compute n28100=0.
compute n28101=0.
compute n28102=0.
compute n28103=0.
compute n28104=0. 

compute n30010=0.
compute n30011=0.
compute n30012=0.
compute n30020=0.
compute n30022=0.
compute n30030=0.
compute n30031=0.
compute n30038=0.
compute n30040=0. 
compute n30041=0.
compute n30042=0.
compute n30050=0.
compute n30051=0.
compute n30052=0.
compute n30053=0.
compute n30054=0.
compute n30055=0.
compute n30060=0.
compute n30061=0.
compute n30062=0.
compute n30063=0.
compute n30070=0.
compute n30071=0.
compute n30080=0.
compute n30082=0.
compute n30092=0.

compute b19010=0.
compute b19011=0.
compute b19020=0.
compute b19025=0.
compute b19030=0.
compute b19032=0.
compute b19040=0.
compute b19050=0.
compute b19038=0.
compute b19060=0.
compute b19061=0.
compute b19071=0.
compute b19100=0.

compute b28011=0.
compute b28012=0.
compute b28013=0.
compute b28014=0.
compute b28015=0.
compute b28022=0.
compute b28030=0.
compute b28031=0. 
compute b28032=0.
compute b28034=0.
compute b28036=0.
compute b28037=0.
compute b28039=0.
compute b28040=0.
compute b28043=0.
compute b28045=0.
compute b28046=0.
compute b28047=0.
compute b28050=0.
compute b28061=0.
compute b28088=0.
compute b28099=0.
compute b28100=0.
compute b28101=0.
compute b28102=0.
compute b28103=0.
compute b28104=0. 

compute b30010=0.
compute b30011=0.
compute b30012=0.
compute b30020=0.
compute b30022=0.
compute b30030=0.
compute b30031=0.
compute b30040=0. 
compute b30041=0.
compute b30042=0.
compute b30050=0.
compute b30051=0.
compute b30052=0.
compute b30053=0.
compute b30054=0.
compute b30060=0.
compute b30061=0.
compute b30062=0.
compute b30063=0.
compute b30070=0.
compute b30071=0.
compute b30080=0.
compute b30082=0.
compute b30092=0.


compute sb19010=0.
compute sb19011=0.
compute sb19020=0.
compute sb19025=0.
compute sb19030=0.
compute sb19032=0.
compute sb19040=0.
compute sb19050=0.
compute sb19038=0.
compute sb19060=0.
compute sb19061=0.
compute sb19071=0.
compute sb19100=0.

compute sb28011=0.
compute sb28012=0.
compute sb28013=0.
compute sb28014=0.
compute sb28015=0.
compute sb28022=0.
compute sb28030=0.
compute sb28031=0. 
compute sb28032=0.
compute sb28034=0.
compute sb28036=0.
compute sb28037=0.
compute sb28039=0.
compute sb28040=0.
compute sb28043=0.
compute sb28045=0.
compute sb28046=0.
compute sb28047=0.
compute sb28050=0.
compute sb28061=0.
compute sb28088=0.
compute sb28099=0.
compute sb28100=0.
compute sb28101=0.
compute sb28102=0.
compute sb28103=0.
compute sb28104=0. 

compute sb30010=0.
compute sb30011=0.
compute sb30012=0.
compute sb30020=0.
compute sb30022=0.
compute sb30030=0.
compute sb30031=0.
compute sb30040=0. 
compute sb30041=0.
compute sb30042=0.
compute sb30050=0.
compute sb30051=0.
compute sb30052=0.
compute sb30053=0.
compute sb30054=0.
compute sb30060=0.
compute sb30061=0.
compute sb30062=0.
compute sb30063=0.
compute sb30070=0.
compute sb30071=0.
compute sb30080=0.
compute sb30082=0.
compute sb30092=0.



if (tnb= 19010 ) n19010=1.
if (tnb= 19011 ) n19011=1.
if (tnb= 19020 ) n19020=1.
if (tnb= 19025 ) n19025=1.
if (tnb= 19030 ) n19030=1.
if (tnb= 19032 ) n19032=1.
if (tnb= 19038 ) n19038=1.
if (tnb= 19040 ) n19040=1.
if (tnb= 19050 ) n19050=1.
if (tnb= 19060 ) n19060=1.
if (tnb= 19061 ) n19061=1.
if (tnb= 19071 ) n19071=1.
if (tnb= 19100 ) n19100=1.

if (tnb= 28011 ) n28011=1.
if (tnb= 28012 ) n28012=1.
if (tnb= 28013 ) n28013=1.
if (tnb= 28014 ) n28014=1.
if (tnb= 28015 ) n28015=1.
if (tnb= 28022 ) n28022=1.
if (tnb= 28030 ) n28030=1.
if (tnb= 28031 ) n28031=1. 
if (tnb= 28032 ) n28032=1.
if (tnb= 28034 ) n28034=1.
if (tnb= 28036 ) n28036=1.
if (tnb= 28037 ) n28037=1.
if (tnb= 28039 ) n28039=1.
if (tnb= 28040 ) n28040=1.
if (tnb= 28043 ) n28043=1.
if (tnb= 28045 ) n28045=1.
if (tnb= 28046 ) n28046=1.
if (tnb= 28047 ) n28047=1.
if (tnb= 28050 ) n28050=1.
if (tnb= 28061 ) n28061=1.
if (tnb= 28088 ) n28088=1.
if (tnb= 28099 ) n28099=1.
if (tnb= 28100 ) n28100=1.
if (tnb= 28101 ) n28101=1.
if (tnb= 28102 ) n28102=1.
if (tnb= 28103 ) n28103=1.
if (tnb= 28104 ) n28104=1. 

if (tnb= 30010 ) n30010=1.
if (tnb= 30011 ) n30011=1.
if (tnb= 30012 ) n30012=1.
if (tnb= 30020 ) n30020=1.
if (tnb= 30022 ) n30022=1.
if (tnb= 30030 ) n30030=1.
if (tnb= 30031 ) n30031=1.
if (tnb= 30040 ) n30040=1. 
if (tnb= 30041 ) n30041=1.
if (tnb= 30042 ) n30042=1.
if (tnb= 30050 ) n30050=1.
if (tnb= 30051 ) n30051=1.
if (tnb= 30052 ) n30052=1.
if (tnb= 30053 ) n30053=1.
if (tnb= 30054 ) n30054=1.
if (tnb= 30055 ) n30055=1.
if (tnb= 30060 ) n30060=1.
if (tnb= 30061 ) n30061=1.
if (tnb= 30062 ) n30062=1.
if (tnb= 30063 ) n30063=1.
if (tnb= 30070 ) n30070=1.
if (tnb= 30080 ) n30080=1.
if (tnb= 30082 ) n30082=1.
if (tnb= 30092 ) n30092=1.

if (tnb= 19010 ) sb19010=sqrt(bsf).
if (tnb= 19011 ) sb19011=sqrt(bsf).
if (tnb= 19020 ) sb19020=sqrt(bsf).
if (tnb= 19025 ) sb19025=sqrt(bsf).
if (tnb= 19030 ) sb19030=sqrt(bsf).
if (tnb= 19032 ) sb19032=sqrt(bsf).
if (tnb= 19040 ) sb19040=sqrt(bsf).
if (tnb= 19050 ) sb19050=sqrt(bsf).
if (tnb= 19038 ) sb19038=sqrt(bsf).
if (tnb= 19060 ) sb19060=sqrt(bsf).
if (tnb= 19061 ) sb19061=sqrt(bsf).
if (tnb= 19071 ) sb19071=sqrt(bsf).
if (tnb= 19100 ) sb19100=sqrt(bsf).

if (tnb= 28011 ) sb28011=sqrt(bsf).
if (tnb= 28012 ) sb28012=sqrt(bsf).
if (tnb= 28013 ) sb28013=sqrt(bsf).
if (tnb= 28014 ) sb28014=sqrt(bsf).
if (tnb= 28015 ) sb28015=sqrt(bsf).
if (tnb= 28022 ) sb28022=sqrt(bsf).
if (tnb= 28030 ) sb28030=sqrt(bsf).
if (tnb= 28031 ) sb28031=sqrt(bsf). 
if (tnb= 28032 ) sb28032=sqrt(bsf).
if (tnb= 28034 ) sb28034=sqrt(bsf).
if (tnb= 28036 ) sb28036=sqrt(bsf).
if (tnb= 28037 ) sb28037=sqrt(bsf).
if (tnb= 28039 ) sb28039=sqrt(bsf).
if (tnb= 28040 ) sb28040=sqrt(bsf).
if (tnb= 28043 ) sb28043=sqrt(bsf).
if (tnb= 28045 ) sb28045=sqrt(bsf).
if (tnb= 28046 ) sb28046=sqrt(bsf).
if (tnb= 28047 ) sb28047=sqrt(bsf).
if (tnb= 28050 ) sb28050=sqrt(bsf).
if (tnb= 28061 ) sb28061=sqrt(bsf).
if (tnb= 28088 ) sb28088=sqrt(bsf).
if (tnb= 28099 ) sb28099=sqrt(bsf).
if (tnb= 28100 ) sb28100=sqrt(bsf).
if (tnb= 28101 ) sb28101=sqrt(bsf).
if (tnb= 28102 ) sb28102=sqrt(bsf).
if (tnb= 28103 ) sb28103=sqrt(bsf).
if (tnb= 28104 ) sb28104=sqrt(bsf). 

if (tnb= 30010 ) sb30010=sqrt(bsf).
if (tnb= 30011 ) sb30011=sqrt(bsf).
if (tnb= 30012 ) sb30012=sqrt(bsf).
if (tnb= 30020 ) sb30020=sqrt(bsf).
if (tnb= 30022 ) sb30022=sqrt(bsf).
if (tnb= 30030 ) sb30030=sqrt(bsf).
if (tnb= 30031 ) sb30031=sqrt(bsf).
if (tnb= 30040 ) sb30040=sqrt(bsf). 
if (tnb= 30041 ) sb30041=sqrt(bsf).
if (tnb= 30042 ) sb30042=sqrt(bsf).
if (tnb= 30050 ) sb30050=sqrt(bsf).
if (tnb= 30051 ) sb30051=sqrt(bsf).
if (tnb= 30052 ) sb30052=sqrt(bsf).
if (tnb= 30053 ) sb30053=sqrt(bsf).
if (tnb= 30054 ) sb30054=sqrt(bsf).
if (tnb= 30060 ) sb30060=sqrt(bsf).
if (tnb= 30061 ) sb30061=sqrt(bsf).
if (tnb= 30062 ) sb30062=sqrt(bsf).
if (tnb= 30063 ) sb30063=sqrt(bsf).
if (tnb= 30070 ) sb30070=sqrt(bsf).
if (tnb= 30071 ) sb30071=sqrt(bsf).
if (tnb= 30080 ) sb30080=sqrt(bsf).
if (tnb= 30082 ) sb30082=sqrt(bsf).
if (tnb= 30092 ) sb30092=sqrt(bsf).

compute lem=0.
compute pal=0.
compute orl=0.
if town=19 lem=1.
if town=28 orl=1.
if town=30 pal=1.

compute bsf=sqftb.
compute srbsf=sqrt(sqftb).
compute srage=sqrt(age).
compute nsrbsf=n*sqrt(bsf).

if class=95 and nghcde=11  sqftl=4350.
if class=95 and nghcde=14  sqftl=3175.
if class=95 and nghcde=15  sqftl=2400.
if class=95 and nghcde=32  sqftl=3000.
if class=95 and nghcde=36  sqftl=3500.
if class=95 and nghcde=43  sqftl=1500.
if class=95 and nghcde=50  sqftl=2500.
if class=95 and nghcde=99  sqftl=2400.
if class=95 and nghcde=100 sqftl=2700.
if class=95 and nghcde=103 sqftl=3000. 


Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).

Compute winter0607=0.
if (mos > 9 and yr=6) or (mos <= 3 and yr=7) winter0607=1.
Compute winter0708=0.
if (mos > 9 and yr=7) or (mos <= 3 and yr=8) winter0708=1.
Compute winter0809=0.
if (mos > 9 and yr=8) or (mos <= 3 and yr=9) winter0809=1.
Compute winter0910=0.
if (mos > 9 and yr=9) or (mos <= 3 and yr=10) winter0910=1.
Compute summer06=0.
if (mos > 3 and yr=6) and (mos <= 9 and yr=6) summer06=1.
Compute summer07=0.
if (mos > 3 and yr=7) and (mos <= 9 and yr=7) summer07=1. 
Compute summer08=0.
if (mos > 3 and yr=8) and (mos <= 9 and yr=8) summer08=1.
Compute summer09=0.
if (mos > 3 and yr=9) and (mos <= 9 and yr=9) summer09=1.
Compute summer10=0.
if (mos > 3 and yr=10) and (mos <= 9 and yr=10) summer10=1.
Compute jantmar06=0.
if (year1=2006 and (mos>=1 and mos<=3)) jantmar06=1. 
Compute octtdec10=0.
if (year1=2010 and (mos>=10 and mos<=12)) octtdec10=1.

Compute jantmar06cl234=jantmar06*cl234.
Compute winter0607cl234=winter0607*cl234.
Compute winter0708cl234=winter0708*cl234.
Compute winter0809cl234=winter0809*cl234.
Compute winter0910cl234=winter0910*cl234.
Compute summer06cl234=summer06*cl234.
Compute summer07cl234=summer07*cl234.
Compute summer08cl234=summer08*cl234.
Compute summer09cl234=summer09*cl234.
Compute summer10cl234=summer10*cl234.
Compute octtdec10cl234=octtdec10*cl234.

Compute jantmar06cl56=jantmar06*cl56.
Compute winter0607cl56=winter0607*cl56.
Compute winter0708cl56=winter0708*cl56.
Compute winter0809cl56=winter0809*cl56.
Compute winter0910cl56=winter0910*cl56.
Compute summer06cl56=summer06*cl56.
Compute summer07cl56=summer07*cl56.
Compute summer08cl56=summer08*cl56.
Compute summer09cl56=summer09*cl56.
Compute summer10cl56=summer10*cl56.
Compute octtdec10cl56=octtdec10*cl56.

Compute jantmar06cl778=jantmar06*cl778.
Compute winter0607cl778=winter0607*cl778.
Compute winter0708cl778=winter0708*cl778.
Compute winter0809cl778=winter0809*cl778.
Compute winter0910cl778=winter0910*cl778.
Compute summer06cl778=summer06*cl778.
Compute summer07cl778=summer07*cl778.
Compute summer08cl778=summer08*cl778.
Compute summer09cl778=summer09*cl778.
Compute summer10cl778=summer10*cl778.
Compute octtdec10cl778=octtdec10*cl778.

Compute jantmar06cl89=jantmar06*cl89.
Compute winter0607cl89=winter0607*cl89.
Compute winter0708cl89=winter0708*cl89.
Compute winter0809cl89=winter0809*cl89.
Compute winter0910cl89=winter0910*cl89.
Compute summer06cl89=summer06*cl89.
Compute summer07cl89=summer07*cl89.
Compute summer08cl89=summer08*cl89.
Compute summer09cl89=summer09*cl89.
Compute summer10cl89=summer10*cl89.
Compute octtdec10cl89=octtdec10*cl89.

Compute jantmar06cl1112=jantmar06*cl1112. 
Compute winter0607cl1112=winter0607*cl1112.
Compute winter0708cl1112=winter0708*cl1112.
Compute winter0809cl1112=winter0809*cl1112.
Compute winter0910cl1112=winter0910*cl1112.
Compute summer06cl1112=summer06*cl1112.
Compute summer07cl1112=summer07*cl1112.
Compute summer08cl1112=summer08*cl1112.
Compute summer09cl1112=summer09*cl1112.
Compute summer10cl1112=summer10*cl1112.
Compute octtdec10cl1112=octtdec10*cl1112.

Compute jantmar06cl1095=jantmar06*cl1095.
Compute winter0607cl1095=winter0607*cl1095.
Compute winter0708cl1095=winter0708*cl1095.
Compute winter0809cl1095=winter0809*cl1095.
Compute winter0910cl1095=winter0910*cl1095.
Compute summer06cl1095=summer06*cl1095.
Compute summer07cl1095=summer07*cl1095.
Compute summer08cl1095=summer08*cl1095.
Compute summer09cl1095=summer09*cl1095.
Compute summer10cl1095=summer10*cl1095.
Compute octtdec10cl1095=octtdec10*cl1095.

Compute jantmar06clsplt=jantmar06*clsplt.
Compute winter0607clsplt=winter0607*clsplt.
Compute winter0708clsplt=winter0708*clsplt.
Compute winter0809clsplt=winter0809*clsplt.
Compute winter0910clsplt=winter0910*clsplt.
Compute summer06clsplt=summer06*clsplt.
Compute summer07clsplt=summer07*clsplt.
Compute summer08clsplt=summer08*clsplt.
Compute summer09clsplt=summer09*clsplt.
Compute summer10clsplt=summer10*clsplt.
Compute octtdec10clsplt=octtdec10*clsplt.

Compute lowzoneorland=0.
if town=28 and  (nghcde=11 or nghcde=12  or nghcde=14  or nghcde=15 
or nghcde=22 or nghcde=30 or nghcde=31 or nghcde=32 or nghcde=34
or nghcde=36 or nghcde=37 or nghcde=39 or nghcde=43 or nghcde=45
or nghcde=46 or nghcde=47 or nghcde=50 or nghcde=61 or nghcde=88
or nghcde=99 or nghcde=100 or nghcde=101 or nghcde=102 
or nghcde=103 or nghcde=104)  lowzoneorland=1.                 	
Compute midzoneorland=0.
if town=28 and (nghcde=13 or nghcde=40) midzoneorland=1. 

Compute srfxlowblockorland=0.
if lowzoneorland=1 srfxlowblockorland=srfx*lowzoneorland.
Compute srfxmidblockorland=0.
if midzoneorland=1 srfxmidblockorland=srfx*midzoneorland.

Compute lowzonepalos=0.
if town=30 and  (nghcde=10 or nghcde=11  or nghcde=12  or nghcde=20 
or nghcde=30 or nghcde=31 or nghcde=40 or nghcde=41 or nghcde=50
or nghcde=51 or nghcde=52 or nghcde=53 or nghcde=54 or nghcde=60
or nghcde=62 or nghcde=63 or nghcde=70 or nghcde=71 or nghcde=80
or nghcde=82 or nghcde=92 or nghcde=100) lowzonepalos=1.                              	
Compute midzonepalos=0.
if town=30 and (nghcde=22 or nghcde=30  
or nghcde=42 or nghcde=61 )  midzonepalos=1. 

Compute srfxlowblockpalos=0.
if lowzonepalos=1 srfxlowblockpalos=srfx*lowzonepalos.
Compute srfxmidblockpalos=0.
if midzonepalos=1 srfxmidblockpalos=srfx*midzonepalos.

Compute lowzonelemont=0.
if town=19 and  (nghcde=10 or nghcde=11  or nghcde=20  or nghcde=25 
or nghcde=30 or nghcde=32 or nghcde=38 or nghcde=40  or nghcde=50    
or nghcde=60 or nghcde=61 or nghcde=100) lowzonelemont=1.                         	

Compute srfxlowblocklemont=0.
if lowzonelemont=1 srfxlowblocklemont=srfx*lowzonelemont.


if num=6 bnum=0.
if num=0 bnum=0.
if num=1 bnum=2.
if num=2 bnum=3.
if num=3 bnum=4.
if num=4 bnum=5.
if num=5 bnum=6.

compute res=bnum - comm.
compute bnumb=bnum*bsf.
compute nres=n*res.

Compute nsrbsf=n*sqrt(bsf).
Compute nbsf=n*bsf.
Compute srage=sqrt(age).
Compute nsrage=n*srage.

Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.

Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute nframas=n*framas.
Compute nmason=n*mason.
Compute frabsf=frame*bsf.
Compute stubsf=stucco*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute nfrast=n*frast.
Compute frastbsf=frast*bsf.
Compute masonbsf=mason*bsf.
Compute frmsbsf=framas*bsf.

Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
Compute prembsf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.
Compute nprembsf=n*premrf*bsf.


compute attcfull=0.
compute attcpart=0.
compute attcnone=0.
if attc=1 attcfull=1.
if attc=2 attcpart=1.
if attc=3 attcnone=1.
compute atfnliv=0.
compute atfnapt=0.
if atfn=1 atfnliv=1.
if atfn=2 atfnapt=1.

Compute garnogar=0.
Compute garage=0.
Compute biggar=0.
compute garage1=0.
compute garage2=0.
compute garage3=0.
compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1.
Compute biggar=0.
if garage3=1 or garage4=1 biggar=1.


Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
Compute nbasfull=n*basefull.
Compute nbaspart=n*basepart.

Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.

Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
compute nnobase=n*nobase.

If firepl>1 firepl=1. 
compute nfirepl=n*firepl.
 
Compute bathsum=fullbath + 0.25*halfbath.
Compute nbathsum=n*bathsum.

compute lsf=sqftl.
compute nbsfair=n*bsfair.
compute nmasbsf=n*masonbsf.
compute nfrmsbsf=n*frmsbsf.
compute nfrstbsf=n*frastbsf.
compute lemlsf=lem*lsf.
compute orllsf=orl*lsf.
compute lemage=lem*age.
compute orlage=orl*age.
compute srlsf=sqrt(lsf).
compute nsrlsf=n*srlsf.

compute nsragpal=nsrage*pal.
compute nsra1930=nsrage*(lem+pal).
compute nsraglem=nsrage*lem.
compute nsragorl=nsrage*orl.
compute srlsforl=srlsf*orl.
compute nsrlorl=nsrlsf*orl.
compute nsrlem=nsrlsf*lem.
compute nsrlpal=nsrlsf*pal.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute repabove=0.
Compute repbelow=0.
Compute repave=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.

Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.
Compute twosty=0.
If class07=1 or class08=1 or class78=1 twosty=1.

compute cathdral=0.
if ceiling=1 cathdral=1.
if ceiling=2 cathdral=0.
  
compute b=1.
*select if year1 = 2010.
*select if puremarket=1.
*Table observation = b
                amount1
               /table = tnb by 
                           amount1 
                             + b      
	/title = 'Ave & Med Sales'
     	/statistics =  median (amount1 'MED SP')
              mean (amount1 'MEAN SP')
                         validn (b '# PROPS').
 
select if town=28.

compute mv = (170739.5049	
+ 9.584372958*nbsf
+ 383.5721999*nbsf1095
+ 37185.50185*biggar
+ 9555.27067*garage2
+ 1738.495777*sb28014
+ 15213.94599*cathdral
+ 1055.219886*sb30070
- 5621.919357*nsrage
+ 249.023447*nsrlpal
- 1926.614137*sb30062
- 10625.06096*sb30082
+ 9458.398453*nbasfull
+ 1834.920096*sb19061
+ 1496.1349*sb19011
+ 2244.175023*nbathsum
+ 6047.938397*nbaspart
+ 2468.860784*sb19032
+ 362.7690965*sb28030
+ 314.7074247*nbsf778
- 1448.730927*sb30092
- 1036.164331*sb28045
+ 319.5430475*nsrlorl
+ 181.5976595*nsrlem
+ 2255.571034*sb19010
+ 1017.627459*sb28032
+ 843.0383446*sb28022
+ 1012.957264*sb28039
- 1389.316751*sb30063
- 2044.684028*sb28034
+ 1824.511487*sb19050
+ 2469.397772*nfirepl
+ 1224.950817*sb28099
+ 197.0375508*sb28036
+ 351.7389607*sb30080
- 2098.679132*sb28012
- 1090.767032*sb28100
- 5934.092263*srfxlowblockorland
- 91941.45341*midzoneorland
- 58831.50522*lowzoneorland
+ 104.8735121*sb30030
- 1585.169123*sb28101
- 440.3422456*sb28047
- 820.074174*sb30031
+ 996.0828337*sb19030
- 1029.209096*sb28037
- 384.2336565*sb19100
- 403.3709284*sb30052
+ 1028.348747*sb28040
+ 492.5136821*nbsf234
+ 1584.640616*nbsf56
+ 431.8433644*sb28050
+ 0.938773992*nbsfair
+ 790.2566952*sb19020
- 1808.773226*srfxmidblockorland
- 24357.58366*lowzonepalos
- 23620.16585*midzonepalos
- 1022.567715*srfxlowblockpalos
- 2125.296409*srfxmidblockpalos
- 72616.1482*lowzonelemont
- 212.9029046*srfxlowblocklemont
+ 465.9628885*nbsf34
+ 2.293861561*bnumb
+ 3387.131756*garage1
+ 3.08248453*nprembsf
+ 223.5921385*sb28103
+ 406.4323334*sb28061)*1.0.
 


*save outfile='C:\Program Files\SPSS\spssa\mv28.sav'/keep town pin mv.
 
 
 
