                        *SPSS Regression.
			*BERWYN + CALUMET + CICERO + STICKNEY  2011.


Get file='C:\Program Files\SPSS\spssa\regt11141536mergefcl2.sav'.
set wid=125.
set len=59.
select if (amount1>130000).
select if (amount1<850000).
select if (multi<1).
select if sqftb<6000.

COMPUTE YEAR1=0.
If  (amount1>0) year1=1900 + yr. 
If  (yr=10 and amount1>0)  year1=2010.
If  (yr=9 and amount1>0)  year1=2009.
If  (yr=8 and amount1>0)  year1=2008.  
If  (yr=7 and amount1>0)  year1=2007.
If  (yr=6 and amount1>0)  year1=2006. 
If  (yr=5 and amount1>0)  year1=2005.
If  (yr=4 and amount1>0)  year1=2004.
If  (yr=3 and amount1>0)  year1=2003.   
If  (yr=2 and amount1>0)  year1=2002.
If  (yr=1 and amount1>0)  year1=2001.
If  (yr=0 and amount1>0)  year1=2000.



COMPUTE FX = cumfile7891011.
COMPUTE SRFX = sqrt(fx).
RECODE FX (SYSMIS=0).
*******************************************************************************************************************.

select if (year1=2006 or puremarket=1). 


compute bsf=sqftb.

Compute bs=0.
if age<10 and (amount1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.
if (pin=	16191000380000
or pin=	16191090110000
or pin=	16191140250000
or pin=	16191140530000
or pin=	16191230290000
or pin=	16192020450000
or pin=	16192190260000
or pin=	16192240360000
or pin=	16192260270000
or pin=	16193050100000
or pin=	16193050420000
or pin=	16193060090000
or pin=	16193100430000
or pin=	16193220040000
or pin=	16193230070000
or pin=	16194000120000
or pin=	16194170230000
or pin=	16194200190000
or pin=	16194210220000
or pin=	16201010300000
or pin=	16203210270000
or pin=	16203210290000
or pin=	16293160010000
or pin=	16301130090000
or pin=	16301150080000
or pin=	16302010160000
or pin=	16302060340000
or pin=	16302080300000
or pin=	16302250010000
or pin=	16302250060000
or pin=	16302310200000
or pin=	16303000120000
or pin=	16303090370000
or pin=	16303260200000
or pin=	16304050010000
or pin=	16304110460000
or pin=	16304110650000
or pin=	16304110700000
or pin=	16311230060000
or pin=	16311320280000
or pin=	16312110100000
or pin=	16312120030000
or pin=	16312180280000
or pin=	16312200040000
or pin=	16313070140000
or pin=	16313240250000
or pin=	16314191210000
or pin=	16314220320000
or pin=	16314230720000
or pin=	16321230140000
or pin=	16323110270000
or pin=	16323250280000
or pin=	25293260550000
or pin=	25301130740000
or pin=	25303030010000
or pin=	25304060430000
or pin=	25304190010000
or pin=	25311170350000
or pin=	25311250450000
or pin=	25312040210000
or pin=	25312060350000
or pin=	25312130180000
or pin=	16202030110000
or pin=	16203260190000
or pin=	16204020270000
or pin=	16204140140000
or pin=	16204160120000
or pin=	16204270190000
or pin=	16204280070000
or pin=	16212200130000
or pin=	16214000110000
or pin=	16214210260000
or pin=	16214290200000
or pin=	16223090070000
or pin=	16281020300000
or pin=	16281130010000
or pin=	16281210370000
or pin=	16281220370000
or pin=	16281240370000
or pin=	16282000230000
or pin=	16282000240000
or pin=	16282060100000
or pin=	16282170220000
or pin=	16283120090000
or pin=	16284150150000
or pin=	16284160200000
or pin=	16284240090000
or pin=	16284250270000
or pin=	16284260010000
or pin=	16284320110000
or pin=	16291040180000
or pin=	16292030220000
or pin=	16292150400000
or pin=	16292280430000
or pin=	16293120230000
or pin=	16293120270000
or pin=	16293150310000
or pin=	16294070180000
or pin=	16321350580000
or pin=	16322060350000
or pin=	16322200260000
or pin=	16324040270000
or pin=	16324090070000
or pin=	16332050280000
or pin=	16332070160000
or pin=	16332130010000
or pin=	16332180360000
or pin=	19061020350000
or pin=	19061180360000
or pin=	19062110270000
or pin=	19062210330000
or pin=	19063010390000
or pin=	19063060360000
or pin=	19091160280000
or pin=	19091270510000
or pin=	19283110060000
or pin=	19283190040000
or pin=	19283210120000
or pin=	19283230150000
or pin=	19294020390000
or pin=	19301070020000
or pin=	19311000360000
or pin=	19312010200000
or pin=	19312010290000
or pin=	19312040190000
or pin=	19312090170000
or pin=	19312090330000
or pin=	19312121480000
or pin=	19312160160000
or pin=	19314040150000
or pin=	19321110060000
or pin=	19321160230000
or pin=	19321240080000
or pin=	19322020080000
or pin=	19322140130000
or pin=	19322240350000
or pin=	19324090410000
or pin=	19332040050000
or pin=	19334011130000
or pin=	19334030420000) bs=1.

select if bs=0.


Compute N=1.
compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.

compute bsf02=0.
compute bsf03=0.
compute bsf04=0.
compute bsf05=0.
compute bsf06=0.
compute bsf07=0.
compute bsf08=0.
compute bsf09=0.
compute bsf10=0.
compute bsf11=0.
compute bsf12=0.
compute bsf34=0.
compute bsf78=0.
compute bsf95=0.
if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.

Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).

Compute winter0607=0.
if (mos > 9 and yr=6) or (mos <= 3 and yr=7) winter0607=1.
Compute winter0708=0.
if (mos > 9 and yr=7) or (mos <= 3 and yr=8) winter0708=1.
Compute winter0809=0.
if (mos > 9 and yr=8) or (mos <= 3 and yr=9) winter0809=1.
Compute winter0910=0.
if (mos > 9 and yr=9) or (mos <= 3 and yr=10) winter0910=1.
Compute summer06=0.
if (mos > 3 and yr=6) and (mos <= 9 and yr=6) summer06=1.
Compute summer07=0.
if (mos > 3 and yr=7) and (mos <= 9 and yr=7) summer07=1. 
Compute summer08=0.
if (mos > 3 and yr=8) and (mos <= 9 and yr=8) summer08=1.
Compute summer09=0.
if (mos > 3 and yr=9) and (mos <= 9 and yr=9) summer09=1.
Compute summer10=0.
if (mos > 3 and yr=10) and (mos <= 9 and yr=10) summer10=1.
Compute jantmar06=0.
if (year1=2006 and (mos>=1 and mos<=3)) jantmar06=1. 
Compute octtdec10=0.
if (year1=2010 and (mos>=10 and mos<=12)) octtdec10=1.

Compute jantmar06cl234=jantmar06*cl234.
Compute winter0607cl234=winter0607*cl234.
Compute winter0708cl234=winter0708*cl234.
Compute winter0809cl234=winter0809*cl234.
Compute winter0910cl234=winter0910*cl234.
Compute summer06cl234=summer06*cl234.
Compute summer07cl234=summer07*cl234.
Compute summer08cl234=summer08*cl234.
Compute summer09cl234=summer09*cl234.
Compute summer10cl234=summer10*cl234.
Compute octtdec10cl234=octtdec10*cl234.

Compute jantmar06cl56=jantmar06*cl56.
Compute winter0607cl56=winter0607*cl56.
Compute winter0708cl56=winter0708*cl56.
Compute winter0809cl56=winter0809*cl56.
Compute winter0910cl56=winter0910*cl56.
Compute summer06cl56=summer06*cl56.
Compute summer07cl56=summer07*cl56.
Compute summer08cl56=summer08*cl56.
Compute summer09cl56=summer09*cl56.
Compute summer10cl56=summer10*cl56.
Compute octtdec10cl56=octtdec10*cl56.

Compute jantmar06cl778=jantmar06*cl778.
Compute winter0607cl778=winter0607*cl778.
Compute winter0708cl778=winter0708*cl778.
Compute winter0809cl778=winter0809*cl778.
Compute winter0910cl778=winter0910*cl778.
Compute summer06cl778=summer06*cl778.
Compute summer07cl778=summer07*cl778.
Compute summer08cl778=summer08*cl778.
Compute summer09cl778=summer09*cl778.
Compute summer10cl778=summer10*cl778.
Compute octtdec10cl778=octtdec10*cl778.

Compute jantmar06cl89=jantmar06*cl89.
Compute winter0607cl89=winter0607*cl89.
Compute winter0708cl89=winter0708*cl89.
Compute winter0809cl89=winter0809*cl89.
Compute winter0910cl89=winter0910*cl89.
Compute summer06cl89=summer06*cl89.
Compute summer07cl89=summer07*cl89.
Compute summer08cl89=summer08*cl89.
Compute summer09cl89=summer09*cl89.
Compute summer10cl89=summer10*cl89.
Compute octtdec10cl89=octtdec10*cl89.

Compute jantmar06cl1112=jantmar06*cl1112. 
Compute winter0607cl1112=winter0607*cl1112.
Compute winter0708cl1112=winter0708*cl1112.
Compute winter0809cl1112=winter0809*cl1112.
Compute winter0910cl1112=winter0910*cl1112.
Compute summer06cl1112=summer06*cl1112.
Compute summer07cl1112=summer07*cl1112.
Compute summer08cl1112=summer08*cl1112.
Compute summer09cl1112=summer09*cl1112.
Compute summer10cl1112=summer10*cl1112.
Compute octtdec10cl1112=octtdec10*cl1112.

Compute jantmar06cl1095=jantmar06*cl1095.
Compute winter0607cl1095=winter0607*cl1095.
Compute winter0708cl1095=winter0708*cl1095.
Compute winter0809cl1095=winter0809*cl1095.
Compute winter0910cl1095=winter0910*cl1095.
Compute summer06cl1095=summer06*cl1095.
Compute summer07cl1095=summer07*cl1095.
Compute summer08cl1095=summer08*cl1095.
Compute summer09cl1095=summer09*cl1095.
Compute summer10cl1095=summer10*cl1095.
Compute octtdec10cl1095=octtdec10*cl1095.

Compute jantmar06clsplt=jantmar06*clsplt.
Compute winter0607clsplt=winter0607*clsplt.
Compute winter0708clsplt=winter0708*clsplt.
Compute winter0809clsplt=winter0809*clsplt.
Compute winter0910clsplt=winter0910*clsplt.
Compute summer06clsplt=summer06*clsplt.
Compute summer07clsplt=summer07*clsplt.
Compute summer08clsplt=summer08*clsplt.
Compute summer09clsplt=summer09*clsplt.
Compute summer10clsplt=summer10*clsplt.
Compute octtdec10clsplt=octtdec10*clsplt.

	
Compute midzoneberwyn=0.
if town=11 and (nghcde=40 or nghcde=60) midzoneberwyn=1. 
Compute highzoneberwyn=0.
if town=11 and (nghcde=10 or nghcde=20 or nghcde=30 
or nghcde=50 or nghcde=70)   highzoneberwyn=1.

Compute srfxmidblockberwyn=0.
if midzoneberwyn=1 srfxmidblockberwyn=srfx*midzoneberwyn.
Compute srfxhighblockberwyn=0.
if highzoneberwyn=1 srfxhighblockberwyn=srfx*highzoneberwyn.

Compute highzonecalumet=0.
if town=14 and (nghcde=10 or nghcde=20 or nghcde=30 
or nghcde=40)   highzonecalumet=1.

Compute srfxhighblockcalumet=0.
if highzonecalumet=1 srfxhighblockcalumet=srfx*highzonecalumet.

Compute highzonecicero=0.
if town=15 and (nghcde=15 or nghcde=20 or nghcde=25  
or nghcde=30 or nghcde=40 or nghcde=65 or nghcde=71 or nghcde=72
or nghcde=80 or nghcde=85 or nghcde=90 )   highzonecicero=1.

Compute srfxhighblockcicero=0.
if highzonecicero=1 srfxhighblockcicero=srfx*highzonecicero.

	
Compute midzonestickney=0.
if town=36 and (nghcde=36 or nghcde=41 or nghcde=50 
or nghcde=60)  midzonestickney=1. 
Compute highzonestickney=0.
if town=36 and (nghcde=12 or nghcde=21 or nghcde=22)  highzonestickney=1.

Compute srfxmidblockstickney=0.
if midzonestickney=1 srfxmidblockstickney=srfx*midzonestickney.
Compute srfxhighblockstickney=0.
if highzonestickney=1 srfxhighblockstickney=srfx*highzonestickney.

*Neighborhood change in Berwyn - 
 lower part of nbhd 40 (south of 35th st
 to Ogden Ave on the south and Harlem on the west)
 becomes part of nbhd 60 .

Compute bern10=0.
If town=11 and nghcde=10 bern10=1.
Compute bern20=0.
If town=11 and nghcde=20 bern20=1.
Compute bern30=0.
If town=11 and nghcde=30 bern30=1.
Compute bern40=0.
If town=11 and nghcde=40 bern40=1.
Compute bern50=0.
If town=11 and nghcde=50 bern50=1.
Compute bern60=0.
If town=11 and nghcde=60 bern60=1.
Compute bern70=0.
If town=11 and nghcde=70 bern70=1.
Compute caln10=0.
If town=14 and nghcde=10 caln10=1.
Compute caln20=0.
If town=14 and nghcde=20 caln20=1.
Compute caln30=0.
If town=14 and nghcde=30 caln30=1.
Compute caln40=0.
If town=14 and nghcde=40 caln40=1.
Compute cicn10=0.
If town=15 and nghcde=10 cicn10=1.
Compute cicn15=0.
If town=15 and nghcde=15 cicn15=1.
Compute cicn20=0.
If town=15 and nghcde=20 cicn20=1.
Compute cicn25=0.
If town=15 and nghcde=25 cicn25=1.
Compute cicn30=0.
If town=15 and nghcde=30 cicn30=1.
Compute cicn40=0.
If town=15 and nghcde=40 cicn40=1.
Compute cicn50=0.
If town=15 and nghcde=50 cicn50=1.
Compute cicn60=0.
If town=15 and nghcde=60 cicn60=1.
Compute cicn65=0.
If town=15 and nghcde=65 cicn65=1.
Compute cicn71=0.
If town=15 and nghcde=71 cicn71=1.
Compute cicn72=0.
If town=15 and nghcde=72 cicn72=1.
Compute cicn80=0.
If town=15 and nghcde=80 cicn80=1.
Compute cicn85=0.
If town=15 and nghcde=85 cicn85=1.
Compute cicn90=0.
If town=15 and nghcde=90 cicn90=1.
Compute stin12=0.
If town=36 and nghcde=12 stin12=1.
Compute stin21=0.
If town=36 and nghcde=21 stin21=1.
Compute stin22=0.
If town=36 and nghcde=22 stin22=1.
Compute stin36=0.
If town=36 and nghcde=36 stin36=1.
Compute stin41=0.
If town=36 and nghcde=41 stin41=1.
Compute stin50=0.
If town=36 and nghcde=50 stin50=1.
Compute stin60=0.
If town=36 and nghcde=60 stin60=1.

If town=11 and nghcde=10 N=1.95.
If town=11 and nghcde=20 N=1.75.
If town=11 and nghcde=30 N=1.84.
If town=11 and nghcde=40 N=2.10.
If town=11 and nghcde=50 N=1.80.
If town=11 and nghcde=60 N=2.80.
If town=11 and nghcde=70 N=1.80.
If town=14 and nghcde=10 N=1.45.
If town=14 and nghcde=20 N=1.44.
If town=14 and nghcde=30 N=1.34.
If town=14 and nghcde=40 N=1.55.
If town=15 and nghcde=15 N=1.46.
If town=15 and nghcde=20 N=1.39.		
If town=15 and nghcde=25 N=1.76.
If town=15 and nghcde=30 N=1.65.
If town=15 and nghcde=40 N=1.85.
If town=15 and nghcde=65 N=1.65.
If town=15 and nghcde=71 N=1.76.
If town=15 and nghcde=72 N=1.49.
If town=15 and nghcde=80 N=1.48.
If town=15 and nghcde=85 N=1.56.
If town=15 and nghcde=90 N=1.46.
If town=36 and nghcde=12 N=1.63.
If town=36 and nghcde=21 N=2.03.
If town=36 and nghcde=22 N=1.85.
If town=36 and nghcde=36 N=1.95.
If town=36 and nghcde=41 N=1.88.
If town=36 and nghcde=50 N=1.90.
If town=36 and nghcde=60 N=1.87.

Compute T11=0.
If town=11 T11=1.
Compute T14=0.
If town=14 T14=1.
Compute T15=0.
If town=15 T15=1.
Compute T36=0.
If town=36 T36=1.

*Cutpoints for land square foot are 1.75 * median lsf.
Compute lsf=sqftl.
If town=11 and lsf > 7140 lsf= 7140 + ((lsf - 7140)/3).
If town=14 and lsf > 8202 lsf= 8202 + ((lsf - 8202)/3).
If town=15 and lsf > 6615 lsf= 6615 + ((lsf - 6615)/3).
If town=36 and lsf > 11637 lsf= 11637 + ((lsf - 11637)/3).
If (class=8 or class=9) lsf=sqftl.


Compute sbebsf=0.
If T11=1 sbebsf=T11*sqrt(bsf).
Compute scabsf=0.
If T14=1 scabsf=T14*sqrt(bsf).
Compute scibsf=0.
If T15=1 scibsf=T15*sqrt(bsf).
Compute sstbsf=0.
If T36=1 sstbsf=T36*sqrt(bsf).

Compute srbelsf=0.
If T11=1 srbelsf=T11*sqrt(lsf).
Compute srcalsf=0.
If T14=1 srcalsf=T14*sqrt(lsf).
Compute srcilsf=0.
If T15=1 srcilsf=T15*sqrt(lsf).
Compute srstlsf=0.
If T36=1 srstlsf=T36*sqrt(lsf).

Compute nsrbelsf=n*srbelsf.
Compute nsrcalsf=n*srcalsf.
Compute nsrcilsf=n*srcilsf.
Compute nsrstlsf=n*srstlsf.

Compute sbsfbe10=0.
Compute sbsfbe20=0.
Compute sbsfbe30=0.
Compute sbsfbe40=0.
Compute sbsfbe50=0.
Compute sbsfbe60=0.
Compute sbsfbe70=0.
Compute sbsfbe71=0.
If town=11 and nghcde=10 sbsfbe10=sqrt(bsf).
If town=11 and nghcde=20 sbsfbe20=sqrt(bsf).
If town=11 and nghcde=30 sbsfbe30=sqrt(bsf).
If town=11 and nghcde=40 sbsfbe40=sqrt(bsf).
If town=11 and nghcde=50 sbsfbe50=sqrt(bsf).
If town=11 and nghcde=60 sbsfbe60=sqrt(bsf).
If town=11 and nghcde=70 sbsfbe70=sqrt(bsf).
If town=11 and nghcde=71 sbsfbe71=sqrt(bsf).
Compute sbsfca10=0.
Compute sbsfca20=0.
Compute sbsfca30=0.
Compute sbsfca40=0.
If town=14 and nghcde=10 sbsfca10=sqrt(bsf).
If town=14 and nghcde=20 sbsfca20=sqrt(bsf).
If town=14 and nghcde=30 sbsfca30=sqrt(bsf).
If town=14 and nghcde=40 sbsfca40=sqrt(bsf).
Compute sbsfci10=0.
Compute sbsfci15=0.
Compute sbsfci20=0.
Compute sbsfci25=0.
Compute sbsfci30=0.
Compute sbsfci40=0.
Compute sbsfci50=0.
Compute sbsfci60=0.
Compute sbsfci65=0.
Compute sbsfci71=0.
Compute sbsfci72=0.
Compute sbsfci80=0.
Compute sbsfci85=0.
Compute sbsfci90=0.
If town=15 and nghcde=10 sbsfci10=sqrt(bsf).
If town=15 and nghcde=20 sbsfci20=sqrt(bsf).
If town=15 and nghcde=30 sbsfci30=sqrt(bsf).
If town=15 and nghcde=40 sbsfci40=sqrt(bsf).
If town=15 and nghcde=50 sbsfci50=sqrt(bsf).
If town=15 and nghcde=60 sbsfci60=sqrt(bsf).
If town=15 and nghcde=65 sbsfci65=sqrt(bsf).
If town=15 and nghcde=71 sbsfci71=sqrt(bsf).
If town=15 and nghcde=72 sbsfci72=sqrt(bsf).
If town=15 and nghcde=80 sbsfci80=sqrt(bsf).
If town=15 and nghcde=85 sbsfci85=sqrt(bsf).
If town=15 and nghcde=90 sbsfci90=sqrt(bsf).
Compute sbsfst12=0.
Compute sbsfst21=0.
Compute sbsfst22=0.
Compute sbsfst36=0.
Compute sbsfst41=0.
Compute sbsfst50=0.
Compute sbsfst60=0.
If town=36 and nghcde=12 sbsfst12=sqrt(bsf).
If town=36 and nghcde=21 sbsfst21=sqrt(bsf).
If town=36 and nghcde=22 sbsfst22=sqrt(bsf).
If town=36 and nghcde=36 sbsfst36=sqrt(bsf).
If town=36 and nghcde=41 sbsfst41=sqrt(bsf).
If town=36 and nghcde=50 sbsfst50=sqrt(bsf).
If town=36 and nghcde=60 sbsfst60=sqrt(bsf).

if num=6 bnum=0.
if num=0 bnum=0.
if num=1 bnum=2.
if num=2 bnum=3.
if num=3 bnum=4.
if num=4 bnum=5.
if num=5 bnum=6.

compute res=bnum - comm.
compute bnumb=bnum*bsf.
compute nres=n*res.


Compute nsrbsf=n*sqrt(bsf).
Compute nsrlsf=n*sqrt(lsf).
Compute nbsf=n*bsf.
Compute srage=sqrt(age).
Compute nsrage=n*srage.

Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.

Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute frabsf=frame*bsf.
Compute stubsf=stucco*bsf.
Compute masbsf=mason*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.

Compute bathsum=fullbath + 0.25*halfbath.
Compute nbathsum=n*bathsum.

Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.

If firepl>1 firepl = 1.
Compute nfirepl=n*firepl.

Compute garage1=0.
Compute garage2=0.
Compute garage3=0.
Compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1.  
Compute biggar=0.
if garage3=1 or garage4=1 biggar=1.

Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.

Compute nbasfull=n*basefull.
Compute nbaspart=n*basepart.
Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.

Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
Compute nnobase=n*nobase.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute nluxbsf=n*qualdlux*bsf.
Compute deluxbsf = qualdlux*bsf.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute repabove=0.
Compute repbelow=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.
Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.
Compute twosty=0.
If class07=1 or class08=1 or class78=1 twosty=1.


*compute b=1.
*select if year1 >= 2009.
*select if puremarket=1.
*Table observation = b
                amount1
               /table = town by nghcde by 
                           amount1 
                             + b      
	/title = 'Ave & Med Sales'
     	/statistics =  median (amount1 'MED SP')
              mean (amount1 'MEAN SP')
                         validn (b '# PROPS').

reg des=defaults cov
    /var=amount1 nsrbelsf nsrcalsf nsrcilsf nsrstlsf  
	sbsfbe10 sbsfbe20 sbsfbe30 sbsfbe40 sbsfbe50 sbsfbe60 sbsfbe70
     sbsfbe71 	sbsfca10 sbsfca20 sbsfca30 sbsfca40 sbsfci10
     sbsfci15 sbsfci20 sbsfci25 sbsfci30 sbsfci40 sbsfci50 sbsfci60
     sbsfci65 sbsfci71 sbsfci72 sbsfci80 sbsfci85 sbsfci90
	sbsfst12 sbsfst21 sbsfst22 sbsfst36 sbsfst41 sbsfst50 sbsfst60
     nbsf234 nbsf56 nbsf778 nbsf89 nbsf1095 nbsf34 nbsf1112 nsrlsf nsrage
	nbsfair masbsf frastbsf frabsf stubsf nbathsum npremrf nfirepl garage1 garage2
	nbasfull nbaspart biggar nluxbsf nrepbesf bnumb nnobase
	jantmar06cl234 winter0607cl234 winter0708cl234 winter0809cl234 winter0910cl234
	summer06cl234 summer07cl234 summer08cl234 summer09cl234 summer10cl234
	octtdec10cl234 jantmar06cl56 winter0607cl56 winter0708cl56 winter0809cl56
	winter0910cl56 summer06cl56 summer07cl56 summer08cl56 summer09cl56
	summer10cl56 octtdec10cl56 jantmar06cl778 winter0607cl778 winter0708cl778
	winter0809cl778 winter0910cl778 summer06cl778 summer07cl778 summer08cl778
     summer09cl778 summer10cl778 octtdec10cl778 jantmar06cl89 winter0607cl89
	winter0708cl89 winter0809cl89 winter0910cl89 summer06cl89 summer07cl89		
	summer08cl89 summer09cl89 summer10cl89 octtdec10cl89 jantmar06cl1112
	winter0607cl1112 winter0708cl1112 winter0809cl1112 winter0910cl1112
	summer06cl1112 summer07cl1112 summer08cl1112 summer09cl1112
	summer10cl1112 octtdec10cl1112 jantmar06cl1095 winter0607cl1095
	winter0708cl1095 winter0809cl1095 winter0910cl1095 summer06cl1095
	summer07cl1095 summer08cl1095 summer09cl1095 summer10cl1095
	octtdec10cl1095 jantmar06clsplt winter0607clsplt winter0708clsplt
	winter0809clsplt winter0910clsplt summer06clsplt summer07clsplt
	summer08clsplt summer09clsplt summer10clsplt octtdec10clsplt
	midzoneberwyn highzoneberwyn midzonestickney highzonestickney highzonecalumet 
	highzonecicero srfxmidblockberwyn srfxhighblockberwyn srfxhighblockcalumet 
	srfxhighblockcicero srfxmidblockstickney srfxhighblockstickney
    /dep=amount1	
    /method=stepwise
    /method=enter jantmar06cl234 winter0607cl234 winter0708cl234 winter0809cl234 winter0910cl234
	summer06cl234 summer07cl234 summer08cl234 summer09cl234 summer10cl234
	octtdec10cl234 jantmar06cl56 winter0607cl56 winter0708cl56 winter0809cl56
	winter0910cl56 summer06cl56 summer07cl56 summer08cl56 summer09cl56
	summer10cl56 octtdec10cl56 jantmar06cl778 winter0607cl778 winter0708cl778
	winter0809cl778 winter0910cl778 summer06cl778 summer07cl778 summer08cl778
     summer09cl778 summer10cl778 octtdec10cl778 jantmar06cl89 winter0607cl89
	winter0708cl89 winter0809cl89 winter0910cl89 summer06cl89 summer07cl89		
	summer08cl89 summer09cl89 summer10cl89 octtdec10cl89 jantmar06cl1112
	winter0607cl1112 winter0708cl1112 winter0809cl1112 winter0910cl1112
	summer06cl1112 summer07cl1112 summer08cl1112 summer09cl1112
	summer10cl1112 octtdec10cl1112 jantmar06cl1095 winter0607cl1095
	winter0708cl1095 winter0809cl1095 winter0910cl1095 summer06cl1095
	summer07cl1095 summer08cl1095 summer09cl1095 summer10cl1095
	octtdec10cl1095 jantmar06clsplt winter0607clsplt winter0708clsplt
	winter0809clsplt winter0910clsplt summer06clsplt summer07clsplt
	summer08clsplt summer09clsplt summer10clsplt octtdec10clsplt
    /method=enter midzoneberwyn highzoneberwyn midzonestickney highzonestickney
	highzonecalumet highzonecicero srfxmidblockberwyn srfxhighblockberwyn srfxhighblockcalumet 
	srfxhighblockcicero srfxmidblockstickney srfxhighblockstickney
	/method=enter npremrf
    /save pred (pred) resid (resid).
     sort cases by town pin.	
     value labels extcon 1 'Frame' 2 'Mason' 3 'Framas' 4 'Stucco'
    /basment 1 'Full' 2 'Slab' 3 'Part' 4 'Crwl'
    /bsfn 1 'Rec' 2 'Apt' 3 'Unf'
    /gar 1 '1' 2 '1.5' 3 '2' 4 '2.5' 5 '3' 6 '3.5' 7 'non'
    /class 2 '2-02' 3 '2-03' 4 '2-04' 5 '2-05' 6 '2-06' 7 '2-07' 8 '2-08' 9 '2-09' 10 '2-10' 11'2-11' 12 '2-12' 34 '2-34' 78 '2-78' 95 '2-95'.
*compute perdif=(resid)/(amount1)*100.
*formats pred(COMMA9.0)
          /resid (f6.0).

*plot
/vertical='Sales Price' MIN(50000) MAX(1000000)
/horizontal='RESID'
/vsize=25
/hsize=140
/plot=amount1 with RESID.
*compute badsal=0.
*if perdif>40 badsal=1.
*if perdif<-40 badsal=1.
*select if badsal=1.
set wid=125.
set len=59.

*REPORT FORMAT=automatic list(1)
/title='Office of the Assessor'
        'Residential Regression Report'
        'Town=11,14,15,36'
    /ltitle 'Report Ran On)Date' 
    /rtitle='PAGE)PAGE'
    /string=prop(area(2) '-' subarea(2) '-' block(3) '-' parcel(3))
          date(mos(2) '-'yr(2))
   /var=prop 'Prop' 'Index Number'(13)
       class 'Class' (label) (5)
       nghcde 'nbhb'(5)
       amount1 'Price'(7)
       pred 'Predicted'(9)
       resid 'Resid'(7)
       perdif '% DIF' (5)
       aos 'AOS' (3)
       sqftb 'Sqftb'(5)
       age 'Age'(4)
       extcon 'Extcon' (label)(6)
       sqftl 'Land'(6).
	
 	

