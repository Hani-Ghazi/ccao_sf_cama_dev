﻿*                            HEADER
*  THIS PROGRAM DEFINES THE HEADER FILE.
*  CHANGE ASCII FILE NAME ON DATA LIST COMMAND.
*  CHANGE FILE NAME ON SAVE COMMAND FOR APPROPRIATE TOWNSHIP.



data list fixed file ='S:\2016 Condos\Norwood Park\Norwood ParkA' 
/pin 2-15 area 2-3 subarea 4-5 block 6-8 parcel 9-11 CONDO 12-15
vol 17-19
town 21-22
taxcode 23-25
class 27-29
majorcls 27
minorcls 28-29
nbhd 31-33
landval 35-43
bldgval 45-53
priorlan 55-63
priorbld 65-73
name 74-95 (A)
deed 96-97
salemo 100-101
saleday 102-103
saleyr 104-105
salepric 107-115
LANDsf 117-123
stcode 126-129
address 136-157 (A)
city 158-170 (A)
state 171-172 (A)
zipcode 174-178
document 181-188.
save outfile ='S:\2016 Condos\Norwood Park\Norwood ParkA.sav'.


*                             DETAIL
* THIS PROGRAM DEFINES THE DETAIL FILE.
* CHANGE ASCII FILE NAME ON DATA LIST COMMAND.
* CHANGE FILE NAME ON SAVE COMMAND FOR APPROPRIATE TOWNSHIP

data list fixed file ='S:\2016 Condos\Norwood Park\Norwood ParkB' 
/pin 2-15 area 2-3 subarea 4-5 block 6-8 parcel 9-11 condo 12-15
multi 17-19
type 21
code 23
decimal 25
unmeas 26-27(A)
cls 29-31
conditi 32-33(A)
ftftar 34-42
depth 43-47(2)
unitpric 50-56(2)
depthfac 58-66(3)
cornerfa 68-71(3)
extcorn 73-76(3)
reprocos 82-89
year 90-93(1)
age 94-97
economic 99-101(3)
percent 102-110(9)
related 112-114
totalav 116-124
keypin 127-140
sc 141(A)
valdet 143-151
market 153-161.

If decimal=1 ftftar=ftftar/10.
If decimal=2 ftftar=ftftar/100.
If decimal=3 ftftar=ftftar/1000.
If decimal=4 ftftar=ftftar/10000.

If cls=288 year=year*10.


save outfile ='S:\2016 Condos\Norwood Park\Norwood ParkB.sav'.  

****THE FOLLOWING WILL SELECT OUT FROM HEADER AND DETAIL  ALL 2-99 AN 3-99 AND 5-99.

Get file ='S:\2016 Condos\Norwood Park\Norwood ParkB.sav'.
SEL IF (CLS=299) OR (CLS=399) OR (CLS=599) OR (CLS=589) .
RENAME VAR(CONDITI=GARAGE).
Save outfile='S:\2016 Condos\Norwood Park\Norwood ParkBa.sav'.

Get file ='S:\2016 Condos\Norwood Park\Norwood ParkA.sav'.

Select if condo>0 AND condo<9999.

Save outfile='S:\2016 Condos\Norwood Park\Norwood ParkAa.sav'.

GET FILE = 'S:\2016 Condos\Norwood Park\Norwood ParkBa.sav'.
SORT CASES BY PIN.
SAVE OUTFILE= 'S:\2016 Condos\Norwood Park\Norwood ParkBa.sav'.


GET FILE = 'S:\2016 Condos\Norwood Park\Norwood ParkAa.sav'.
SORT CASES BY PIN.
SAVE OUTFILE = 'S:\2016 Condos\Norwood Park\Norwood ParkAa.sav'.


MATCH FILES FILE= 'S:\2016 Condos\Norwood Park\Norwood ParkAa.sav'
           /FILE= 'S:\2016 Condos\Norwood Park\Norwood ParkBa.sav'
            /BY=PIN.


Compute pin10 = trunc(pin/10000).
Formats pin10 (F10.0).


SAVE OUTFILE = 'S:\2016 Condos\Norwood Park\Norwood ParkC.sav'.

GET File = 'S:\2015 City_condo_valuations\Mort Data.sav'.
Sort Cases By Pin.
Save Outfile = 'S:\2016 Condos\Norwood Park\Norwood ParkMort.sav'.

MATCH FILES FILE= 'S:\2016 Condos\Norwood Park\Norwood ParkMort.sav'
           /FILE= 'S:\2016 Condos\Norwood Park\Norwood ParkC.sav'
            /BY=PIN.

select if class ge 0.
execute.
Save Outfile = 'S:\2016 Condos\Norwood Park\Norwood ParkE.sav'.

Get File= 'S:\2015 City_condo_valuations\Foreclosures_and_Sales_RES_2014copy2.sav'.
select if (Class=299) OR (Class=399) OR (Class=599) OR (Class=589).
execute.
Select if town=26.
execute.
SELECT IF RANGE (filing_date,date.dmy(1,1,2013),date.dmy(31,12,2015))
or RANGE (auction_date,date.dmy(1,1,2013),date.dmy(31,12,2015)).
execute.

Save outfile = 'S:\2016 Condos\Norwood Park\Norwood ParkFC.sav'
/keep pin filing_date auction_date.
execute.



MATCH FILES FILE= 'S:\2016 Condos\Norwood Park\Norwood ParkE.sav'
           /FILE= 'S:\2016 Condos\Norwood Park\Norwood ParkFC.sav'
            /BY=PIN.

Save Outfile = 'S:\2016 Condos\Norwood Park\Norwood ParkD.sav'.





