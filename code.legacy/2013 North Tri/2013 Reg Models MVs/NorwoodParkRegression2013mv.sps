			*NORWOOD PARK AND LEYDEN REGRESSION 2013.

GET FILE='C:\Program Files\IBM\SPSS\Statistics\19\1\regt20andregt26mergefcl2.sav'.
*select if (amount1>85000).
*select if (amount1<550000).
set mxcells=100000.
*select if (multi<1).
*select if sqftb<6000.
Compute year1=0.
If  (amount1>0) year1=1900 + yr.
If (yr=12 and amount1>0) year1=2012.
If (yr=11 and amount1>0) year1=2011.
If (yr=10 and amount1>0) year1=2010.
If  (yr=9 and amount1>0)  year1=2009.
If  (yr=8 and amount1>0)  year1=2008.
If  (yr=7 and amount1>0)  year1=2007.
If  (yr=6 and amount1>0)  year1=2006.   
If  (yr=5 and amount1>0)  year1=2005.
If  (yr=4 and amount1>0)  year1=2004.  

*select if (year1>2007).
Compute bs=0.
if age<10 and (AMOUNT1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.
if (pin=	12123070060000
or pin=	12124010030000
or pin=	12124070370000
or pin=	12124100210000
or pin=	12124180050000
or pin=	12124200330000
or pin=	12134010110000
or pin=	12134060300000
or pin=	12134070120000
or pin=	12134260020000
or pin=	12142130180000
or pin=	13181020630000
or pin=	13181080330000
or pin=	13181160190000
or pin=	13183050150000
or pin=	13184000410000
or pin=	12113210080000
or pin=	12094240170000
or pin=	12094240330000
or pin=	12161130260000
or pin=	12164020190000
or pin=	12164120410000
or pin=	12123050210000
or pin=	12213100670000
or pin=	12213190440000
or pin=	12213250520000
or pin=	12271100100000
or pin=	12013020050000
or pin=	12013040300000
or pin=	12021080200000
or pin=	12021230260000
or pin=	12022010330000
or pin=	12022080080000
or pin=	12022310200000
or pin=	12024030530000
or pin=	12024050150000
or pin=	12121000400000
or pin=	12291030120000
or pin=	12291050070000
or pin=	12293100230000
or pin=	12293230210000
or pin=	12294001100000
or pin=	12294170290000
or pin=	12322030180000
or pin=	12322090340000
or pin=	12322110300000
or pin=	12323090090000
or pin=	12324040280000
or pin=	12324120010000
or pin=	12332030030000
or pin=	12332240120000
or pin=	12332240220000
or pin=	12284240090000
or pin=	12284250130000
or pin=	12361080120000
or pin=	12361100460000
or pin=	12251150150000
or pin=	12251230190000
or pin=	12251290090000
or pin=	12253200170000
or pin=	12253210140000
or pin=	12253210270000
or pin=	12254090130000
or pin=	12361010360000
or pin=	12361040050000
or pin=	12361040330000
or pin=	12362000050000
or pin=	12362010210000
or pin=	12362090340000
or pin=	12362180030000
or pin=	12362230120000
or pin=	12362230320000
or pin=	12362250350000
or pin=	12364000210000
or pin=	12263020030000
or pin=	12263180580000
or pin=	12263290600000
or pin=	12264150210000
or pin=	12264150420000
or pin=	12274060270000
or pin=	12351010520000
or pin=	12351050390000
or pin=	12343020270000)   bs=1.
*select if bs=0.

compute t26=0.
if town=26 t26=1.
compute t20=0.                                                                             
if town=20 t20=1.

compute bsf=sqftb.
Compute N=1.


COMPUTE FX = cumfile78910111213.
COMPUTE SRFX = sqrt(fx).
RECODE FX (SYSMIS=0).
*******************************************************************************************************************.

*select if puremarket=1 .

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.

compute bsf=sqftb.

if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.

compute tnb=(town*1000)+nghcde.

compute N20010=0.
compute N20011=0.
compute N20020=0.
compute N20030=0.
compute N20040=0.
compute N20050=0.
compute N20060=0.
compute N20070=0.
compute N20080=0.
compute N20081=0.
compute N20090=0.
compute N20091=0.

IF TNB=20010 N20010=1.
IF TNB=20011 N20011=1.
IF TNB=20020 N20020=1.
IF TNB=20030 N20030=1.
IF TNB=20040 N20040=1.
IF TNB=20050 N20050=1.
IF TNB=20060 N20060=1.
IF TNB=20070 N20070=1.
IF TNB=20080 N20080=1.
IF TNB=20081 N20081=1.
IF TNB=20090 N20090=1.
IF TNB=20091 N20091=1.

compute SB20010=0.
compute SB20011=0.
compute SB20020=0.
compute SB20030=0.
compute SB20040=0.
compute SB20050=0.
compute SB20060=0.
compute SB20070=0.
compute SB20080=0.
compute SB20081=0.
compute SB20090=0.
compute SB20091=0.

IF TNB=20010 SB20010=SQRT(BSF).
IF TNB=20011 SB20011=SQRT(BSF).
IF TNB=20020 SB20020=SQRT(BSF).
IF TNB=20030 SB20030=SQRT(BSF).
IF TNB=20040 SB20040=SQRT(BSF).
IF TNB=20050 SB20050=SQRT(BSF).
IF TNB=20060 SB20060=SQRT(BSF).
IF TNB=20070 SB20070=SQRT(BSF).
IF TNB=20080 SB20080=SQRT(BSF).
IF TNB=20081 SB20081=SQRT(BSF).
IF TNB=20090 SB20090=SQRT(BSF).
IF TNB=20091 SB20091=SQRT(BSF).

compute n26010=0.
compute n26030=0.
compute n26040=0.
compute n26050=0.

IF TNB=26010 N26010=1.
IF TNB=26030 N26030=1.
IF TNB=26040 N26040=1.
IF TNB=26050 N26050=1.

compute SB26010=0.
compute SB26030=0.
compute SB26040=0.
compute SB26050=0.

IF TNB=26010 SB26010=SQRT(BSF).
IF TNB=26030 SB26030=SQRT(BSF).
IF TNB=26040 SB26040=SQRT(BSF).
IF TNB=26050 SB26050=SQRT(BSF).
  
compute n=1.
if tnb=20010 n=3.45.
if tnb=20011 n=2.32.
If tnb=20020 n=1.74.
if tnb=20030 n=1.41.
If tnb=20040 n=2.94.
if tnb=20050 n=1.35.
If tnb=20060 n=1.37.
If tnb=20070 n=2.48.
If tnb=20080 n=1.88.
if tnb=20081 n=1.57.
If tnb=20090 n=1.80.
If tnb=20091 n=2.55.
If tnb=26010 n=2.19.
If tnb=26030 n=2.86.
if tnb=26040 n=3.05.
If tnb=26050 n=3.16.

Compute lowzonenorwood=0.
if town=26 and  (nghcde=40 or nghcde=50)  lowzonenorwood=1.                         	

Compute midzonenorwood=0.
if town=26 and (nghcde=10 or nghcde=30 )  midzonenorwood=1. 

Compute srfxlowblocknorwood=0.
if lowzonenorwood=1 srfxlowblocknorwood=srfx*lowzonenorwood.

Compute srfxmidblocknorwood=0.
if midzonenorwood=1 srfxmidblocknorwood=srfx*midzonenorwood.


Compute lowzoneleyden=0.
if town=20 and  (nghcde=10 or nghcde=11 or nghcde=40)   lowzoneleyden=1.                         	

Compute midzoneleyden=0.
if town=20 and (nghcde=20 or nghcde=30 or nghcde=70 or nghcde=80 or nghcde=81) midzoneleyden=1. 

Compute highzoneleyden=0.
if town=20 and (nghcde=50 or nghcde=60 or nghcde=90 or nghcde=91) highzoneleyden=1.

Compute srfxlowblockleyden=0.
if lowzoneleyden=1 srfxlowblockleyden=srfx*lowzoneleyden.

Compute srfxmidblockleyden=0.
if midzoneleyden=1 srfxmidblockleyden=srfx*midzoneleyden.

Compute srfxhighblockleyden=0.
if highzoneleyden=1 srfxhighblockleyden=srfx*highzoneleyden.


Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).

Compute winter0809=0.
if (mos > 9 and yr=8) or (mos <= 3 and yr=9) winter0809=1.
Compute winter0910=0.
if (mos > 9 and yr=9) or (mos <= 3 and yr=10) winter0910=1.
Compute winter1011=0.
if (mos > 9 and yr=10) or (mos <= 3 and yr=11) winter1011=1.
Compute winter1112=0.
if (mos > 9 and yr=11) or (mos <= 3 and yr=12) winter1112=1.
Compute summer08=0.
if (mos > 3 and yr=8) and (mos <= 9 and yr=8) summer08=1. 
Compute summer09=0.
if (mos > 3 and yr=9) and (mos <= 9 and yr=9) summer09=1.
Compute summer10=0.
if (mos > 3 and yr=10) and (mos <= 9 and yr=10) summer10=1.
Compute summer11=0.
if (mos > 3 and yr=11) and (mos <= 9 and yr=11) summer11=1.
Compute summer12=0.
if (mos > 3 and yr=12) and (mos <= 9 and yr=12) summer12=1.
Compute jantmar08=0.
if (year1=2008 and (mos>=1 and mos<=3)) jantmar08=1. 
Compute octtdec12=0.
if (year1=2012 and (mos>=10 and mos<=12)) octtdec12=1.

Compute jantmar08cl234=jantmar08*cl234.
Compute winter0809cl234=winter0809*cl234.
Compute winter0910cl234=winter0910*cl234.
Compute winter1011cl234=winter1011*cl234.
Compute winter1112cl234=winter1112*cl234.
Compute summer08cl234=summer08*cl234.
Compute summer09cl234=summer09*cl234.
Compute summer10cl234=summer10*cl234.
Compute summer11cl234=summer11*cl234.
Compute summer12cl234=summer12*cl234.
Compute octtdec12cl234=octtdec12*cl234.

Compute jantmar08cl56=jantmar08*cl56.
Compute winter0809cl56=winter0809*cl56.
Compute winter0910cl56=winter0910*cl56.
Compute winter1011cl56=winter1011*cl56.
Compute winter1112cl56=winter1112*cl56.
Compute summer08cl56=summer08*cl56.
Compute summer09cl56=summer09*cl56.
Compute summer10cl56=summer10*cl56.
Compute summer11cl56=summer11*cl56.
Compute summer12cl56=summer12*cl56.
Compute octtdec12cl56=octtdec12*cl56.

Compute jantmar08cl778=jantmar08*cl778.
Compute winter0809cl778=winter0809*cl778.
Compute winter0910cl778=winter0910*cl778.
Compute winter1011cl778=winter1011*cl778.
Compute winter1112cl778=winter1112*cl778.
Compute summer08cl778=summer08*cl778.
Compute summer09cl778=summer09*cl778.
Compute summer10cl778=summer10*cl778.
Compute summer11cl778=summer11*cl778.
Compute summer12cl778=summer12*cl778.
Compute octtdec12cl778=octtdec12*cl778.

Compute jantmar08cl89=jantmar08*cl89.
Compute winter0809cl89=winter0809*cl89.
Compute winter0910cl89=winter0910*cl89.
Compute winter1011cl89=winter1011*cl89.
Compute winter1112cl89=winter1112*cl89.
Compute summer08cl89=summer08*cl89.
Compute summer09cl89=summer09*cl89.
Compute summer10cl89=summer10*cl89.
Compute summer11cl89=summer11*cl89.
Compute summer12cl89=summer12*cl89.
Compute octtdec12cl89=octtdec12*cl89.

Compute jantmar08cl1112=jantmar08*cl1112.
Compute winter0809cl1112=winter0809*cl1112.
Compute winter0910cl1112=winter0910*cl1112.
Compute winter1011cl1112=winter1011*cl1112.
Compute winter1112cl1112=winter1112*cl1112.
Compute summer08cl1112=summer08*cl1112.
Compute summer09cl1112=summer09*cl1112.
Compute summer10cl1112=summer10*cl1112.
Compute summer11cl1112=summer11*cl1112.
Compute summer12cl1112=summer12*cl1112.
Compute octtdec12cl1112=octtdec12*cl1112.

Compute jantmar08cl1095=jantmar08*cl1095.
Compute winter0809cl1095=winter0809*cl1095.
Compute winter0910cl1095=winter0910*cl1095.
Compute winter1011cl1095=winter1011*cl1095.
Compute winter1112cl1095=winter1112*cl1095.
Compute summer08cl1095=summer08*cl1095.
Compute summer09cl1095=summer09*cl1095.
Compute summer10cl1095=summer10*cl1095.
Compute summer11cl1095=summer11*cl1095.
Compute summer12cl1095=summer12*cl1095.
Compute octtdec12cl1095=octtdec12*cl1095.

Compute jantmar08clsplt=jantmar08*clsplt.
Compute winter0809clsplt=winter0809*clsplt.
Compute winter0910clsplt=winter0910*clsplt.
Compute winter1011clsplt=winter1011*clsplt.
Compute winter1112clsplt=winter1112*clsplt.
Compute summer08clsplt=summer08*clsplt.
Compute summer09clsplt=summer09*clsplt.
Compute summer10clsplt=summer10*clsplt.
Compute summer11clsplt=summer11*clsplt.
Compute summer12clsplt=summer12*clsplt.
Compute octtdec12clsplt=octtdec12*clsplt.

Compute lsf=sqftl.
if lsf > 6750 lsf = 6750  + ((lsf - 6750)/4).

if class=95 and tnb=26010  lsf=1063.
if class=95 and tnb=20020 	lsf=1875.
if class=95 and tnb=20040 	lsf=2712.

Compute nsrlsf=n*sqrt(lsf).
compute srbsf=sqrt(bsf).
Compute nsrbsf=n*sqrt(bsf).
Compute nbsf=n*bsf.
Compute srage=sqrt(age).
Compute nsrage=n*srage.
Compute bsfage=bsf*age.


Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute frabsf=frame*bsf.
compute masbsf=mason*bsf.
Compute stubsf=stucco*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.
Compute frms=0.
If extcon=3  frms=1.
Compute frmsbsf=frms*bsf.
Compute bathsum=fullbath + 0.25*halfbath.
compute nbathsum=n*bathsum.

Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.
compute prembsf=premrf*bsf.
compute nprembsf=n*prembsf.

Compute garnogar=0.
Compute garage=0.
If gar=1 gar1car=1.
If gar=2 gar1hcar=1.
If gar=3 gar2car=1.
If gar=4 gar2hcar=1.
If gar=5 gar3car=1.
If gar=6 gar3hcar=1.
If gar=7 garnogar=1.
If gar=8 gar4car=1. 
If gar ne 7 garage=1.

Compute garage1=0.
Compute garage2=0.
Compute garage3=0.
Compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1.  
Compute biggar=0.
if garage3=1 or garage4=1 biggar=1.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.


Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
Compute basement=0.
If basment=1 or basment=3 basement=1.
Compute nbasfull=n*basefull.
Compute nbaspart=n*basepart.
Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.
Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
compute nnobase=n*nobase.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
compute deluxbsf=qualdlux*bsf.

if num=6 bnum=0.
if num=0 bnum=0.
if num=1 bnum=2.
if num=2 bnum=3.
if num=3 bnum=4.
if num=4 bnum=5.
if num=5 bnum=6.

Compute nbnum=n*bnum.
Compute ncomm=n*comm.
Compute totunit=bnum + comm.
Compute totunitb=totunit*sqrt(bsf).


If firepl>0 firepl=1.
compute nfirepl=n*firepl.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute repabove=0.
Compute repbelow=0.
Compute repave=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.
Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.

Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.

compute b=1.
*select if (town=26 and year1>=2012) or (town=20 and year1>=2012).
*select if puremarket=1. 
*Table observation = b
                amount1
               /table = tnb by 
                           amount1 
                             + b      
	/title = 'Ave & Med Sales'
     	/statistics =  median (amount1 'MED SP')
            mean (amount1 'MEAN SP')
            validn (b '# PROPS').

select if town=26.

compute mv = (54019.8878896	
+ 8314.3631984*nbathsum
+ 403.9825840*nsrlsf
+ 795.1161733*nbsf778
+ 993.5702064*nbsf56
- 7928.9486513*highzoneleyden
- 3051.9135472*nsrage
- 5794.2814272*midzoneleyden
+ 1.7709629*nbsfair
+ 37.1040026*masbsf
+ 34.2624642*frmsbsf
+ 818.4680462*nbsf34
+ 25.5993693*frabsf
+ 40.2890616*stubsf
+ 638.6660603*nbsf234
+ 9558.8208709*nbasfull
+ 12699.2704191*garage2
+ 7553.4872655*nbaspart
+ 526.9333393*SB26050
- 1139.4133949*SB20010
- 2334.6604704*SB20091
+ 4.0619914*nprembsf
+ 31612.0392742*lowzoneleyden
- 5947.8653374*srfxlowblockleyden
- 1172.4338026*srfxmidblockleyden
- 2841.0648901*srfxhighblockleyden
- 3861.4041283*lowzonenorwood
- 9862.3434343*midzonenorwood
- 11578.6520728*srfxlowblocknorwood
- 612.9541535*srfxmidblocknorwood
+ 593.1342720*nfirepl
+ 5295.2055122*nsiteben
+ 6850.4591557*garage1
+ 19617.2958837*biggar
- 116.1802695*SB20060
+ 1387.3019524*nbsf89
+ 163.5141309*nbsf1112
+ 55.2754822*nbsf1095
+ 5.4117673*nluxbsf)*1.0.

* RUN THIS PROCEDURE TO CALCULATE THE ADJ FACTOR.
*sel if town=26.
*sel if puremarket=1 and yr1=12.
*compute av=mv*0.10.
*compute ratio=av/amount1.
*fre format=notable/var=ratio/sta=med.

save outfile='C:\Program Files\IBM\SPSS\Statistics\19\1\mv26.sav'/keep town pin mv.















