                     *SPSS Regression.
                     *Niles Regression 2013.

get file = 'C:\Program Files\IBM\SPSS\Statistics\19\1\regt24mergefcl2.sav'.
*select if (amount1>100000).
*select if (amount1<1500000).
*select if (multi<1).
*select if sqftb<7000.

Compute year1=0.
If  (amount1>0) year1=1900 + yr.
If (yr=12 and amount1>0) year1=2012.
If (yr=11 and amount1>0) year1=2011.
If (yr=10 and amount1>0) year1=2010.
If  (yr=9 and amount1>0)  year1=2009.
If  (yr=8 and amount1>0)  year1=2008.
If  (yr=7 and amount1>0)  year1=2007. 
If  (yr=6 and amount1>0)  year1=2006. 
If  (yr=5 and amount1>0)  year1=2005.
If  (yr=4 and amount1>0)  year1=2004.  

*select if (year1>2007).
set mxcells=2500000.
Compute bs=0.
if age<10 and (AMOUNT1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.
if (pin=	10073150030000
or pin=	10074070490000
or pin=	10074110470000
or pin=	10071060100000
or pin=	10153230190000
or pin=	10161150450000
or pin=	10163020430000
or pin=	10163120200000
or pin=	10163320430000
or pin=	10172030330000
or pin=	10172140440000
or pin=	10172160030000
or pin=	10174170460000
or pin=	10181020380000
or pin=	10181110320000
or pin=	10181120060000
or pin=	10184120280000
or pin=	10201100020000
or pin=	10204290230000
or pin=	10211240100000
or pin=	10214110180000
or pin=	10223130030000
or pin=	10281110250000
or pin=	10201020070000
or pin=	10201130470000
or pin=	10104060210000
or pin=	10104230440000
or pin=	10104250580000
or pin=	10104260100000
or pin=	10144080070000
or pin=	10144130450000
or pin=	10151020370000
or pin=	10151200130000
or pin=	10152090270000
or pin=	10152210190000
or pin=	10153140340000
or pin=	10154020530000
or pin=	10154120530000
or pin=	10154280050000
or pin=	10154280500000
or pin=	10141090200000
or pin=	10141240210000
or pin=	10142200390000
or pin=	10183080180000
or pin=	10222160470000
or pin=	10224010270000
or pin=	10224040270000
or pin=	10224270460000
or pin=	10231100420000
or pin=	10232050080000
or pin=	10232080550000
or pin=	10232200640000
or pin=	10233040240000
or pin=	10233140470000
or pin=	10233170180000
or pin=	10271100370000
or pin=	10272230200000
or pin=	10272260310000
or pin=	10273060560000
or pin=	10284000290000
or pin=	10301110310000
or pin=	10301250410000
or pin=	10331070300000
or pin=	10332300380000
or pin=	10341230230000
or pin=	10342030090000
or pin=	10342130450000
or pin=	10342190230000
or pin=	10342270030000
or pin=	10351310160000
or pin=	10353060330000
or pin=	10354030090000
or pin=	10354230130000
or pin=	10333070310000
or pin=	10334100410000
or pin=	10334260380000
or pin=	10334330500000
or pin=	10334370070000
or pin=	10343050530000
or pin=	10343230150000
or pin=	10343290060000
or pin=	10143180270000
or pin=	10143230290000) bs=1.
*select if bs=0.

compute tnb=(town*1000) + nghcde.
compute bsf=sqftb.
compute lsf=sqftl.
compute srbsf=sqrt(sqftb).
compute srage=sqrt(age).
compute srlsf=sqrt(sqftl).

COMPUTE FX = cumfile78910111213.
If FX >= 8   FX = 8.
COMPUTE SRFX = sqrt(fx).
RECODE FX (SYSMIS=0).
*******************************************************************************************************************.

*select if puremarket=1.

compute n24013=0.
compute n24021=0.
compute n24022=0.
compute n24023=0.
compute n24031=0.
compute n24032=0.
compute n24051=0.
compute n24052=0.
compute n24060=0.
compute n24081=0.
compute n24101=0.
compute n24102=0.
compute n24103=0.
compute n24140=0.
compute n24150=0.
compute n24160=0.


compute sb24013=0.
compute sb24021=0.
compute sb24022=0.
compute sb24023=0.
compute sb24031=0.
compute sb24032=0.
compute sb24051=0.
compute sb24052=0.
compute sb24060=0.
compute sb24081=0.
compute sb24101=0.
compute sb24102=0.
compute sb24103=0.
compute sb24140=0.
compute sb24150=0.
compute sb24160=0.


if (tnb=24013) n=2.35.
if (tnb=24021) n=9.95.
if (tnb=24022) n=4.15.
if (tnb=24023) n=6.10.
if (tnb=24031) n=2.33.
if (tnb=24032) n=2.45.
if (tnb=24051) n=3.27.
if (tnb=24052) n=3.01.
if (tnb=24060) n=4.26.
if (tnb=24081) n=2.35.
if (tnb=24101) n=3.12.
if (tnb=24102) n=6.10.
if (tnb=24103) n=3.69.
if (tnb=24140) n=2.95.
if (tnb=24150) n=3.37.
if (tnb=24160) n=5.99.


if (tnb=24013) n24013=1.
if (tnb=24021) n24021=1.
if (tnb=24022) n24022=1.
if (tnb=24023) n24023=1.
if (tnb=24031) n24031=1.
if (tnb=24032) n24032=1.
if (tnb=24051) n24051=1.
if (tnb=24052) n24052=1.
if (tnb=24060) n24060=1.
if (tnb=24081) n24081=1.
if (tnb=24101) n24101=1.
if (tnb=24102) n24102=1.
if (tnb=24103) n24103=1.
if (tnb=24140) n24140=1.
if (tnb=24150) n24150=1.
if (tnb=24160) n24160=1.

if tnb=24013  sb24013=sqrt(bsf).
if tnb=24021  sb24021=sqrt(bsf).
if tnb=24022  sb24022=sqrt(bsf).
if tnb=24023  sb24023=sqrt(bsf).
if tnb=24031  sb24031=sqrt(bsf).
if tnb=24032  sb24032=sqrt(bsf).
if tnb=24051  sb24051=sqrt(bsf).
if tnb=24052  sb24052=sqrt(bsf).
if tnb=24060  sb24060=sqrt(bsf).
if tnb=24081  sb24081=sqrt(bsf).
if tnb=24101  sb24101=sqrt(bsf).
if tnb=24102  sb24102=sqrt(bsf).
if tnb=24103  sb24103=sqrt(bsf).
if tnb=24140  sb24140=sqrt(bsf).
if tnb=24150  sb24150=sqrt(bsf).
if tnb=24160  sb24160=sqrt(bsf).

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.

if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.

Compute midzoneniles=0.
if town=24 and (nghcde=31 or nghcde=81 or nghcde=103)  midzoneniles=1.

Compute highzoneniles=0.
if town=24 and (nghcde=52 or nghcde=101 or nghcde=140)    highzoneniles=1.

Compute lowzoneniles=0.
If town=24 and (midzoneniles=0 and highzoneniles=0)  lowzoneniles=1.

Compute srfxlowblockniles=0.
if lowzoneniles=1 srfxlowblockniles=srfx*lowzoneniles.

Compute srfxmidblockniles=0.
if midzoneniles=1 srfxmidblockniles=srfx*midzoneniles.

Compute srfxhighblockniles=0.
if highzoneniles=1 srfxhighblockniles=srfx*highzoneniles.

Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).

Compute winter0809=0.
if (mos > 9 and yr=8) or (mos <= 3 and yr=9) winter0809=1.
Compute winter0910=0.
if (mos > 9 and yr=9) or (mos <= 3 and yr=10) winter0910=1.
Compute winter1011=0.
if (mos > 9 and yr=10) or (mos <= 3 and yr=11) winter1011=1.
Compute winter1112=0.
if (mos > 9 and yr=11) or (mos <= 3 and yr=12) winter1112=1.
Compute summer08=0.
if (mos > 3 and yr=8) and (mos <= 9 and yr=8) summer08=1. 
Compute summer09=0.
if (mos > 3 and yr=9) and (mos <= 9 and yr=9) summer09=1.
Compute summer10=0.
if (mos > 3 and yr=10) and (mos <= 9 and yr=10) summer10=1.
Compute summer11=0.
if (mos > 3 and yr=11) and (mos <= 9 and yr=11) summer11=1.
Compute summer12=0.
if (mos > 3 and yr=12) and (mos <= 9 and yr=12) summer12=1.
Compute jantmar08=0.
if (year1=2008 and (mos>=1 and mos<=3)) jantmar08=1. 
Compute octtdec12=0.
if (year1=2012 and (mos>=10 and mos<=12)) octtdec12=1.

Compute jantmar08cl234=jantmar08*cl234.
Compute winter0809cl234=winter0809*cl234.
Compute winter0910cl234=winter0910*cl234.
Compute winter1011cl234=winter1011*cl234.
Compute winter1112cl234=winter1112*cl234.
Compute summer08cl234=summer08*cl234.
Compute summer09cl234=summer09*cl234.
Compute summer10cl234=summer10*cl234.
Compute summer11cl234=summer11*cl234.
Compute summer12cl234=summer12*cl234.
Compute octtdec12cl234=octtdec12*cl234.

Compute jantmar08cl56=jantmar08*cl56.
Compute winter0809cl56=winter0809*cl56.
Compute winter0910cl56=winter0910*cl56.
Compute winter1011cl56=winter1011*cl56.
Compute winter1112cl56=winter1112*cl56.
Compute summer08cl56=summer08*cl56.
Compute summer09cl56=summer09*cl56.
Compute summer10cl56=summer10*cl56.
Compute summer11cl56=summer11*cl56.
Compute summer12cl56=summer12*cl56.
Compute octtdec12cl56=octtdec12*cl56.

Compute jantmar08cl778=jantmar08*cl778.
Compute winter0809cl778=winter0809*cl778.
Compute winter0910cl778=winter0910*cl778.
Compute winter1011cl778=winter1011*cl778.
Compute winter1112cl778=winter1112*cl778.
Compute summer08cl778=summer08*cl778.
Compute summer09cl778=summer09*cl778.
Compute summer10cl778=summer10*cl778.
Compute summer11cl778=summer11*cl778.
Compute summer12cl778=summer12*cl778.
Compute octtdec12cl778=octtdec12*cl778.

Compute jantmar08cl89=jantmar08*cl89.
Compute winter0809cl89=winter0809*cl89.
Compute winter0910cl89=winter0910*cl89.
Compute winter1011cl89=winter1011*cl89.
Compute winter1112cl89=winter1112*cl89.
Compute summer08cl89=summer08*cl89.
Compute summer09cl89=summer09*cl89.
Compute summer10cl89=summer10*cl89.
Compute summer11cl89=summer11*cl89.
Compute summer12cl89=summer12*cl89.
Compute octtdec12cl89=octtdec12*cl89.

Compute jantmar08cl1112=jantmar08*cl1112.
Compute winter0809cl1112=winter0809*cl1112.
Compute winter0910cl1112=winter0910*cl1112.
Compute winter1011cl1112=winter1011*cl1112.
Compute winter1112cl1112=winter1112*cl1112.
Compute summer08cl1112=summer08*cl1112.
Compute summer09cl1112=summer09*cl1112.
Compute summer10cl1112=summer10*cl1112.
Compute summer11cl1112=summer11*cl1112.
Compute summer12cl1112=summer12*cl1112.
Compute octtdec12cl1112=octtdec12*cl1112.

Compute jantmar08cl1095=jantmar08*cl1095.
Compute winter0809cl1095=winter0809*cl1095.
Compute winter0910cl1095=winter0910*cl1095.
Compute winter1011cl1095=winter1011*cl1095.
Compute winter1112cl1095=winter1112*cl1095.
Compute summer08cl1095=summer08*cl1095.
Compute summer09cl1095=summer09*cl1095.
Compute summer10cl1095=summer10*cl1095.
Compute summer11cl1095=summer11*cl1095.
Compute summer12cl1095=summer12*cl1095.
Compute octtdec12cl1095=octtdec12*cl1095.

Compute jantmar08clsplt=jantmar08*clsplt.
Compute winter0809clsplt=winter0809*clsplt.
Compute winter0910clsplt=winter0910*clsplt.
Compute winter1011clsplt=winter1011*clsplt.
Compute winter1112clsplt=winter1112*clsplt.
Compute summer08clsplt=summer08*clsplt.
Compute summer09clsplt=summer09*clsplt.
Compute summer10clsplt=summer10*clsplt.
Compute summer11clsplt=summer11*clsplt.
Compute summer12clsplt=summer12*clsplt.
Compute octtdec12clsplt=octtdec12*clsplt.

Compute lsf=sqftl.

if class=95 and tnb=24031   lsf=1260.
if class=95 and tnb=24032   lsf=3100.
if class=95 and tnb=24051   lsf=1852.
if class=95 and tnb=24052   lsf=1018.
if class=95 and tnb=24081   lsf=2540.
if class=95 and tnb=24101   lsf=2156.
if class=95 and tnb=24103   lsf=3024.	
if class=95 and tnb=24140   lsf=2056.	

Compute nsrlsf=n*sqrt(lsf).
compute srbsf=sqrt(bsf).
Compute nsrbsf=n*sqrt(bsf).
Compute nbsf=n*bsf.
Compute srage=sqrt(age).
Compute bsfage=bsf*age.


Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute frabsf=frame*bsf.
compute masbsf=mason*bsf.
Compute stubsf=stucco*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.
Compute bathsum=fullbath + 0.25*halfbath.
compute nbathsum=n*bathsum.

Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.
compute prembsf=premrf*bsf.
compute nprembsf=n*prembsf.

Compute garnogar=0.
Compute garage=0.
If gar=1 gar1car=1.
If gar=2 gar1hcar=1.
If gar=3 gar2car=1.
If gar=4 gar2hcar=1.
If gar=5 gar3car=1.
If gar=6 gar3hcar=1.
If gar=7 garnogar=1.
If gar=8 gar4car=1. 
If gar ne 7 garage=1.

Compute garage1=0.
Compute garage2=0.
Compute garage3=0.
Compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1.  
Compute biggar=0.
if garage3=1 or garage4=1 biggar=1.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
compute nsrage=n*srage.

Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
Compute basement=0.
If basment=1 or basment=3 basement=1.
Compute nbasfull=n*basefull.
Compute nbaspart=n*basepart.
Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.
Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
compute nnobase=n*nobase.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
compute deluxbsf=qualdlux*bsf.

if num=6 bnum=0.
if num=0 bnum=0.
if num=1 bnum=2.
if num=2 bnum=3.
if num=3 bnum=4.
if num=4 bnum=5.
if num=5 bnum=6.

Compute nbnum=n*bnum.
Compute ncomm = n*comm.
Compute totunit=bnum + comm.
Compute totunitb=totunit*sqrt(bsf).


If firepl>0 firepl=1.
compute nfirepl=n*firepl.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute repabove=0.
Compute repbelow=0.
Compute repave=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.
Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.

Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.


Compute b=1.

compute mv = (4593.788881	
+ 4951.060652*nbathsum
+ 6.184449219*masbsf
+ 424.2258889*nbsf1095
- 57.11773211*stubsf
- 8347.52347*nsrage
+ 2054.744556*nbsf56
+ 382.754437*nsrlsf
+ 0.8782828*nbsfair
+ 3303.581774*sb24060
- 6456.225629*sb24021
+ 1682.289314*nbsf778
+ 1353.7432925*nbsf89
+ 4248.877959*nfirepl
+ 27.51272082*frabsf
+ 18827.16543*garage2
+ 3217.863916*sb24051
+ 4207.549177*sb24101
+ 3711.403784*sb24031
+ 3937.489978*sb24150
+ 1542.809682*nbsf234
+ 1679.377312*nbsf34
+ 1327.38588938*nbsf1112
+ 3577.72318*sb24081
+ 3962.169378*sb24052
+ 2396.491711*sb24103
+ 3654.703198*sb24013
+ 3598.83354*sb24032
+ 14061.14509*nbasfull
+ 5.74615751*nprembsf
- 11463.30949*srfxlowblockniles
+ 12026.18151*nbaspart
+ 3.821151783*nrepabsf
+ 2614.42528*sb24140
+ 13714.22947*garage1
- 33600.37565*highzoneniles
- 6208.948664*srfxmidblockniles
- 2135.068389*srfxhighblockniles
+ 30496.25255*biggar
+ 9161.528525*nnobase)*1.0.


save outfile='C:\Program Files\IBM\SPSS\Statistics\19\1\mv24.sav'/keep town pin mv.


