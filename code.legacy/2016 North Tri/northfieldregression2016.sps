					*SPSS Regression.
					*NORTHFIELD 2016.


Get file='C:\Users\daaaron\Documents\regt25may25.sav'.
set mxcells=300000.
set wid=125.
set len=59.
select if (amount1>220000).
select if (amount1<5800000).
select if (multi<1).
select if sqftb<6000.

Compute year1=0.
If  (amount1>0) year1=1900 + yr1.
If (yr1=15 and amount1>0)  year1=2015.
If (yr1=14 and amount1>0) year1=2014.
If (yr1=13 and amount1>0) year1=2013. 
If (yr1=12 and amount1>0)  year1=2012.
If (yr1=11 and amount1>0)  year1=2011.
If (yr1=10 and amount1>0)  year1=2010.
If  (yr1=9 and amount1>0)  year1=2009.
If  (yr1=8 and amount1>0)  year1=2008.
If  (yr1=7 and amount1>0)  year1=2007. 	 
If  (yr1=6 and amount1>0)  year1=2006. 
If  (yr1=5 and amount1>0)  year1=2005. 
If  (yr1=4 and amount1>0)  year1=2004.

select if (year1>2010).
Compute bs=0.
if age<10 and (AMOUNT1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.
if (pin=	4063030280000
or pin=	4324010700000
or pin=	4324010980000
or pin=	4324011000000
or pin=	4324011030000
or pin=	4324011040000
or pin=	4324011050000
or pin=	4324011620000
or pin=	4333020080000
or pin=	4333020400000
or pin=	4333020780000
or pin=	4334060060000
or pin=	4334060070000
or pin=	4334070290000
or pin=	4061020150000
or pin=	4064000470000
or pin=	4071020280000
or pin=	4071020310000
or pin=	4071100310000
or pin=	4072000250000
or pin=	4073050030000
or pin=	4074130010000
or pin=	4102001520000
or pin=	4102020260000
or pin=	4102060310000
or pin=	4104070050000
or pin=	4182020050000
or pin=	4184040240000
or pin=	4021110080000
or pin=	4082140550000
or pin=	4084080010000
or pin=	4091050440000
or pin=	4091110100000
or pin=	4093170180000
or pin=	4161050100000
or pin=	4161160190000
or pin=	4163070030000
or pin=	4171010890000
or pin=	4172110070000
or pin=	4031060280000
or pin=	4302070010000
or pin=	4304070120000
or pin=	4304071030000
or pin=	4031110200000
or pin=	4033020090000
or pin=	4033020130000
or pin=	4033020150000
or pin=	4033020300000
or pin=	4033020330000
or pin=	4092150080000
or pin=	4093070130000
or pin=	4093090080000
or pin=	4101020090000
or pin=	4101030210000
or pin=	4101080330000
or pin=	4101120310000
or pin=	4112060250000
or pin=	4112060420000
or pin=	4112060490000
or pin=	4112060530000
or pin=	4112210420000
or pin=	4112220420000
or pin=	4123000270000
or pin=	4162020310000
or pin=	4162080160000
or pin=	4162150010000
or pin=	4114030240000
or pin=	4142010100000
or pin=	4242190170000
or pin=	4243070280000
or pin=	4244070350000
or pin=	4252021140000
or pin=	4263020110000
or pin=	4263080280000
or pin=	4264070150000
or pin=	4204000530000
or pin=	4204000540000
or pin=	4204050450000
or pin=	4212060090000
or pin=	4213010910000
or pin=	4213020230000
or pin=	4251070150000
or pin=	4251080250000
or pin=	4251160460000
or pin=	4253060690000
or pin=	4253060750000
or pin=	4253070250000
or pin=	4253090310000
or pin=	4253110330000
or pin=	4253150190000
or pin=	4341020080000
or pin=	4341020380000
or pin=	4341040660000
or pin=	4341041340000
or pin=	4341050020000
or pin=	4341050160000
or pin=	4341090200000
or pin=	4342140040000
or pin=	4351030110000
or pin=	4351150130000
or pin=	4351220020000
or pin=	4351220080000
or pin=	4092050020000
or pin=	4092080250000
or pin=	4092090020000
or pin=	4092130030000
or pin=	4092130040000
or pin=	4094020030000
or pin=	4094050080000
or pin=	4094050290000
or pin=	4094090160000
or pin=	4094100230000
or pin=	4094200080000
or pin=	4103190290000
or pin=	4343000190000
or pin=	4343040050000
or pin=	4343060090000
or pin=	4343070180000
or pin=	4344040400000
or pin=	4344060030000
or pin=	4344060160000
or pin=	4344080210000
or pin=	4344130050000
or pin=	4344130730000
or pin=	4344130920000
or pin=	4344130950000
or pin=	4344140300000
or pin=	4344150090000
or pin=	4344150220000
or pin=	4353000100000
or pin=	4353150040000
or pin=	4353190100000
or pin=	4352010280000
or pin=	4352070160000
or pin=	4151000630000
or pin=	4211080110000
or pin=	4211080120000
or pin=	4211080300000
or pin=	4211080520000
or pin=	4211080530000
or pin=	4212000550000
or pin=	4212000610000
or pin=	4212001130000
or pin=	4212100040000
or pin=	4231010410000
or pin=	4233030020000
or pin=	4354110060000
or pin=	4363000030000
or pin=	4363060620000
or pin=	4363110220000
or pin=	4363140380000
or pin=	4363150440000
or pin=	4131030360000
or pin=	4131120410000
or pin=	4131180230000
or pin=	4133030490000
or pin=	4133050640000
or pin=	4143010780000
or pin=	4143011470000
or pin=	4234010810000
or pin=	4241030230000
or pin=	4241030290000
or pin=	4243000350000
or pin=	4251110220000
or pin=	4251120230000
or pin=	4251170050000
or pin=	4251170150000
or pin=	4253000460000
or pin=	4262001310000
or pin=	4364000250000
or pin=	4043041670000
or pin=	4304120100000
or pin=	4304130020000
or pin=	4304130140000
or pin=	4053090120000
or pin=	4283010130000
or pin=	4283010310000
or pin=	4322040270000
or pin=	4334100080000
or pin=	4043020150000
or pin=	4043060110000
or pin=	4054010050000
or pin=	4171080120000
or pin=	4183010090000
or pin=	4113000170000
or pin=	4113000300000
or pin=	4113040240000
or pin=	4081000110000
or pin=	4081030010000
or pin=	4061010140000
or pin=	4261020520000
or pin=	4324010750000
or pin=	4324010850000
or pin=	4333000710000
or pin=	4333000920000
or pin=	4333020200000
or pin=	4333020520000
or pin=	4333040160000
or pin=	4333090170000
or pin=	4334040170000
or pin=	4053150130000
or pin=	4064090040000
or pin=	4071100230000
or pin=	4072050660000
or pin=	4074100180000
or pin=	4164040070000
or pin=	4102011110000
or pin=	4102040100000
or pin=	4104010180000
or pin=	4104020400000
or pin=	4173001120000
or pin=	4182020080000
or pin=	4291002420000
or pin=	4291002500000
or pin=	4291050080000
or pin=	4032040030000
or pin=	4083030250000
or pin=	4084110150000
or pin=	4093060210000
or pin=	4161120130000
or pin=	4163030120000
or pin=	4163030630000
or pin=	4174110010000
or pin=	4174190020000
or pin=	4031010220000
or pin=	4031020090000
or pin=	4304050170000
or pin=	4093100230000
or pin=	4101030160000
or pin=	4101030170000
or pin=	4112020570000
or pin=	4112060220000
or pin=	4112060430000
or pin=	4112060450000
or pin=	4112060460000
or pin=	4112060550000
or pin=	4112160560000
or pin=	4162000140000
or pin=	4162050290000
or pin=	4162100030000
or pin=	4162120080000
or pin=	4162130140000
or pin=	4242010390000
or pin=	4242180070000
or pin=	4242190220000
or pin=	4244010420000
or pin=	4244070330000
or pin=	4244150240000
or pin=	4252000020000
or pin=	4252000500000
or pin=	4254000280000
or pin=	4264150160000
or pin=	4204040070000
or pin=	4251020290000
or pin=	4251030150000
or pin=	4251090120000
or pin=	4251160180000
or pin=	4251160330000
or pin=	4253060730000
or pin=	4253130330000
or pin=	4253140010000
or pin=	4253140040000
or pin=	4253140160000
or pin=	4253140270000
or pin=	4341010220000
or pin=	4341030200000
or pin=	4341030250000
or pin=	4341030530000
or pin=	4341050050000
or pin=	4341050080000
or pin=	4341060100000
or pin=	4342020580000
or pin=	4351100050000
or pin=	4352030170000
or pin=	4352040120000
or pin=	4092080070000
or pin=	4094080090000
or pin=	4094080100000
or pin=	4094100360000
or pin=	4094120240000
or pin=	4094120250000
or pin=	4094160280000
or pin=	4103100070000
or pin=	4103190460000
or pin=	4343030080000
or pin=	4343040370000
or pin=	4353090170000
or pin=	4353110210000
or pin=	4353120550000
or pin=	4353170050000
or pin=	4361050220000
or pin=	4151000910000
or pin=	4151001190000
or pin=	4211000360000
or pin=	4211000480000
or pin=	4211080210000
or pin=	4211080260000
or pin=	4211090150000
or pin=	4212000320000
or pin=	4212000530000
or pin=	4212001150000
or pin=	4212090130000
or pin=	4212090210000
or pin=	4212100060000
or pin=	4212100070000
or pin=	4212100100000
or pin=	4212100110000
or pin=	4223030290000
or pin=	4231010290000
or pin=	4354100110000
or pin=	4354100230000
or pin=	4354110030000
or pin=	4363040010000
or pin=	4364020010000
or pin=	4364030080000
or pin=	4364030370000
or pin=	4364030580000
or pin=	4131120280000
or pin=	4133030620000
or pin=	4141000010000
or pin=	4234000250000
or pin=	4234000410000
or pin=	4241010380000
or pin=	4251170040000
or pin=	4253000200000
or pin=	4261030230000
or pin=	4262000270000
or pin=	4201010280000
or pin=	4152010110000
or pin=	4043020650000
or pin=	4043070010000
or pin=	4043070020000
or pin=	4113010490000
or pin=	4113050010000
or pin=	4081000120000
or pin=	4083000670000
or pin=	4083110130000
or pin=	4281040220000
or pin=	4201040270000)    bs=1.                           
select if bs=0.
compute bsf=sqftb.
Compute N=1.



*******************************************************************************************************************.



compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.

compute bsf=sqftb.

compute bsf02=0.
compute bsf03=0.
compute bsf04=0.
compute bsf05=0.
compute bsf06=0.
compute bsf07=0.
compute bsf08=0.
compute bsf09=0.
compute bsf10=0.
compute bsf11=0.
compute bsf12=0.
compute bsf34=0.
compute bsf78=0.
compute bsf95=0.

if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.

compute n=1.

Compute n10=0.
If nghcde=10 n10=1.
Compute n11=0.
If nghcde=11 n11=1.
Compute n21=0.
If nghcde=21 n21=1.
Compute n23=0.
If nghcde=23 n23=1.
Compute n30=0.
If nghcde=30 n30=1.
Compute n51=0.
If nghcde=51 n51=1.
Compute n52=0.
If nghcde=52 n52=1.
Compute n61=0.
If nghcde=61 n61=1.
Compute n72=0.
If nghcde=72 n72=1.
Compute n82=0.
If nghcde=82 n82=1.
Compute n90=0.
If nghcde=90 n90=1.
Compute n100=0.
If nghcde=100 n100=1.
Compute n102=0.
If nghcde=102 n102=1.
Compute n110=0.
If nghcde=110 n110=1.
Compute n131=0.
If nghcde=131 n131=1.
Compute n132=0.
If nghcde=132 n132=1.
Compute n140=0.
If nghcde=140 n140=1.
Compute n160=0.
If nghcde=160 n160=1.
Compute n180=0.
If nghcde=180 n180=1.
Compute n190=0.
If nghcde=190 n190=1.
Compute n200=0.
If nghcde=200 n200=1.
Compute n210=0.
If nghcde=210 n210=1.
Compute n220=0.
If nghcde=220 n220=1.
Compute n230=0.
If nghcde=230 n230=1.
Compute n250=0.
If nghcde=250 n250=1.
Compute n260=0.
If nghcde=260 n260=1.
Compute n270=0.
If nghcde=270 n270=1.
Compute n280=0.
If nghcde=280 n280=1.
Compute n300=0.
If nghcde=300 n300=1.
Compute n310=0.
If nghcde=310 n310=1.
Compute n320=0.
If nghcde=320 n320=1.
Compute n350=0.
If nghcde=350 n350=1.
Compute n400=0.
If nghcde=400 n400=1.
Compute n420=0.
If nghcde=420 n420=1.

If nghcde=10 N=4.77.
If nghcde=11 N=4.25.
If nghcde=21 N=5.75.
If nghcde=23 N=5.99.
If nghcde=51 N=6.39.
If nghcde=52 N=5.59.
If nghcde=61 N=4.07.
If nghcde=72 N=3.62.
If nghcde=82 N=13.94.
If nghcde=90 N=6.05.
If nghcde=100 N=6.08.
If nghcde=102 N=5.51.
If nghcde=110 N=8.86.
If nghcde=131 N=5.88.
If nghcde=132 N=4.80.
If nghcde=140 N=5.38.
If nghcde=160 N=6.30.
If nghcde=180 N=7.45.
If nghcde=190 N=6.70.
If nghcde=200 N=9.08.
If nghcde=210 N=7.58.
If nghcde=220 N=9.31.
If nghcde=230 N=6.59.
If nghcde=240 N=7.75.
If nghcde=250 N=5.43.
If nghcde=260 N=4.51.
If nghcde=270 N=4.28.
If nghcde=280 N=4.49.
If nghcde=300 N=9.30.
if nghcde=310 N=6.50.
If nghcde=320 N=9.70.
If nghcde=350 N=4.38.
If nghcde=400 N=5.85.
If nghcde=420 N=6.36.

Compute sb10=0.
Compute sb11=0.
Compute sb21=0.
Compute sb23=0.
Compute sb30=0.
Compute sb51=0.
Compute sb52=0.
Compute sb61=0.
Compute sb72=0.
Compute sb82=0.
Compute sb90=0.
Compute sb100=0.
Compute sb102=0.
Compute sb110=0.
Compute sb131=0.
Compute sb132=0.
Compute sb140=0.
Compute sb160=0.
Compute sb180=0.
Compute sb190=0.
Compute sb200=0.
Compute sb210=0.
Compute sb220=0.
Compute sb230=0.
Compute sb250=0.
Compute sb260=0.
Compute sb270=0.
Compute sb280=0.
Compute sb300=0.
Compute sb310=0.
Compute sb320=0.
Compute sb350=0.
Compute sb400=0.
Compute sb420=0.
If nghcde=10 sb10=sqrt(bsf).
If nghcde=11 sb11=sqrt(bsf).
if nghcde=21 sb21=sqrt(bsf).
If nghcde=23 sb23=sqrt(bsf).
If nghcde=30 sb30=sqrt(bsf).
If nghcde=51 sb51=sqrt(bsf).
if nghcde=52 sb52=sqrt(bsf).
If nghcde=61 sb61=sqrt(bsf).
If nghcde=72 sb72=sqrt(bsf).
If nghcde=82 sb82=sqrt(bsf).
if nghcde=90 sb90=sqrt(bsf).
If nghcde=100 sb100=sqrt(bsf).
If nghcde=102 sb102=sqrt(bsf).
If nghcde=110 sb110=sqrt(bsf).
If nghcde=131 sb131=sqrt(bsf).
If nghcde=132 sb132=sqrt(bsf).
If nghcde=140 sb140=sqrt(bsf).
If nghcde=160 sb160=sqrt(bsf).
If nghcde=180 sb180=sqrt(bsf).
If nghcde=190 sb190=sqrt(bsf).
If nghcde=200 sb200=sqrt(bsf).
If nghcde=210 sb210=sqrt(bsf).
If nghcde=220 sb220=sqrt(bsf).
If nghcde=230 sb230=sqrt(bsf).
If nghcde=250 sb250=sqrt(bsf).
If nghcde=260 sb260=sqrt(bsf).
If nghcde=270 sb270=sqrt(bsf).
If nghcde=280 sb280=sqrt(bsf).
If nghcde=300 sb300=sqrt(bsf).
If nghcde=310 sb310=sqrt(bsf).
If nghcde=320 sb320=sqrt(bsf).
If nghcde=350 sb350=sqrt(bsf).
If nghcde=400 sb400=sqrt(bsf).
If nghcde=420 sb420=sqrt(bsf).
       	

*Compute midzonenorthfield=0.
*if town=25 and (nghcde=11 or nghcde=61 or nghcde=250 or nghcde=270)   midzonenorthfield=1. 

*Compute lowzonenorthfield=0.
*if midzonenorthfield=0  lowzonenorthfield=1.                    

*Compute srfxlowblocknorthfield=0.
*if lowzonenorthfield=1 srfxlowblocknorthfield=srfx*lowzonenorthfield.

*Compute srfxmidblocknorthfield=0.
*if midzonenorthfield=1 srfxmidblocknorthfield=srfx*midzonenorthfield.


Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).

Compute winter1112=0.
if (mos > 9 and yr1=11 or (mos <= 3 and yr1=12)) winter1112=1.
Compute winter1213=0.
if (mos > 9 and yr1=12) or (mos <= 3 and yr1=13) winter1213=1.
Compute winter1314=0.
if (mos > 9 and yr1=13) or (mos <= 3 and yr1=14) winter1314=1.
Compute winter1415=0.
if (mos > 9 and yr1=14) or (mos <= 3 and yr1=15) winter1415=1.
Compute summer11=0.
if (mos > 3 and yr1=11) and (mos <= 9 and yr1=11) summer11=1. 
Compute summer12=0.
if (mos > 3 and yr1=12) and (mos <= 9 and yr1=12) summer12=1.
Compute summer13=0.
if (mos > 3 and yr1=13) and (mos <= 9 and yr1=13) summer13=1.
Compute summer14=0.
if (mos > 3 and yr1=14) and (mos <= 9 and yr1=14) summer14=1.
Compute summer15=0.
if (mos > 3 and yr1=15) and (mos <= 9 and yr1=15) summer15=1.
Compute jantmar11=0.
if (year1=2011 and (mos>=1 and mos<=3)) jantmar11=1. 
Compute octtdec15=0.
if (year1=2015 and (mos>=10 and mos<=12)) octtdec15=1.

Compute jantmar11cl234=jantmar11*cl234.
Compute winter1112cl234=winter1112*cl234.
Compute winter1213cl234=winter1213*cl234.
Compute winter1314cl234=winter1314*cl234.
Compute winter1415cl234=winter1415*cl234.
Compute summer11cl234=summer11*cl234.
Compute summer12cl234=summer12*cl234.
Compute summer13cl234=summer13*cl234.
Compute summer14cl234=summer14*cl234.
Compute summer15cl234=summer15*cl234.
Compute octtdec15cl234=octtdec15*cl234.

Compute jantmar11cl56=jantmar11*cl56.
Compute winter1112cl56=winter1112*cl56.
Compute winter1213cl56=winter1213*cl56.
Compute winter1314cl56=winter1314*cl56.
Compute winter1415cl56=winter1415*cl56.
Compute summer11cl56=summer11*cl56.
Compute summer12cl56=summer12*cl56.
Compute summer13cl56=summer13*cl56.
Compute summer14cl56=summer14*cl56.
Compute summer15cl56=summer15*cl56.
Compute octtdec15cl56=octtdec15*cl56.

Compute jantmar11cl778=jantmar11*cl778.
Compute winter1112cl778=winter1112*cl778.
Compute winter1213cl778=winter1213*cl778.
Compute winter1314cl778=winter1314*cl778.
Compute winter1415cl778=winter1415*cl778.
Compute summer11cl778=summer11*cl778.
Compute summer12cl778=summer12*cl778.
Compute summer13cl778=summer13*cl778.
Compute summer14cl778=summer14*cl778.
Compute summer15cl778=summer15*cl778.
Compute octtdec15cl778=octtdec15*cl778.

Compute jantmar11cl89=jantmar11*cl89.
Compute winter1112cl89=winter1112*cl89.
Compute winter1213cl89=winter1213*cl89.
Compute winter1314cl89=winter1314*cl89.
Compute winter1415cl89=winter1415*cl89.
Compute summer11cl89=summer11*cl89.
Compute summer12cl89=summer12*cl89.
Compute summer13cl89=summer13*cl89.
Compute summer14cl89=summer14*cl89.
Compute summer15cl89=summer15*cl89.
Compute octtdec15cl89=octtdec15*cl89.


Compute jantmar11cl1112=jantmar11*cl1112.
Compute winter1112cl1112=winter1112*cl1112.
Compute winter1213cl1112=winter1213*cl1112.
Compute winter1314cl1112=winter1314*cl1112.
Compute winter1415cl1112=winter1415*cl1112.
Compute summer11cl1112=summer11*cl1112.
Compute summer12cl1112=summer12*cl1112.
Compute summer13cl1112=summer13*cl1112.
Compute summer14cl1112=summer14*cl1112.
Compute summer15cl1112=summer15*cl1112.
Compute octtdec15cl1112=octtdec15*cl1112.

Compute jantmar11cl1095=jantmar11*cl1095.
Compute winter1112cl1095=winter1112*cl1095.
Compute winter1213cl1095=winter1213*cl1095.
Compute winter1314cl1095=winter1314*cl1095.
Compute winter1415cl1095=winter1415*cl1095.
Compute summer11cl1095=summer11*cl1095.
Compute summer12cl1095=summer12*cl1095.
Compute summer13cl1095=summer13*cl1095.
Compute summer14cl1095=summer14*cl1095.
Compute summer15cl1095=summer15*cl1095.
Compute octtdec15cl1095=octtdec15*cl1095.

Compute jantmar11clsplt=jantmar11*clsplt.
Compute winter1112clsplt=winter1112*clsplt.
Compute winter1213clsplt=winter1213*clsplt.
Compute winter1314clsplt=winter1314*clsplt.
Compute winter1415clsplt=winter1415*clsplt.
Compute summer11clsplt=summer11*clsplt.
Compute summer12clsplt=summer12*clsplt.
Compute summer13clsplt=summer13*clsplt.
Compute summer14clsplt=summer14*clsplt.
Compute summer15clsplt=summer15*clsplt.
Compute octtdec15clsplt=octtdec15*clsplt.



Compute lsf=sqftl.

if class=95 and nghcde=21   lsf=3375.
if class=95 and nghcde=51   lsf=2570.
if class=95 and nghcde=54   lsf=2570.
if class=95 and nghcde=61   lsf=3100.
if class=95 and nghcde=72   lsf=2914.
if class=95 and nghcde=131  lsf=1792.
if class=95 and nghcde=132  lsf=3100.
if class=95 and nghcde=160  lsf=2621.
if class=95 and nghcde=180  lsf=1255.
if class=95 and nghcde=200  lsf=2509.
if class=95 and nghcde=230  lsf=3918.
if class=95 and nghcde=260  lsf=1688.
if class=95 and nghcde=270  lsf=3843.
if class=95 and nghcde=350  lsf=3202.
if class=95 and nghcde=400  lsf=3965.
if class=95 and nghcde=420  lsf=4228.
	

Compute nsrlsf=n*sqrt(lsf).


compute srbsf=sqrt(bsf).
Compute nsrbsf=n*sqrt(bsf).
Compute nbsf=n*bsf.
Compute srage=sqrt(age).
Compute bsfage=bsf*age.


Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute frabsf=frame*bsf.
compute masbsf=mason*bsf.
Compute stubsf=stucco*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.
Compute bathsum=fullbath + 0.25*halfbath.
compute nbathsum=n*bathsum.

Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.
compute prembsf=premrf*bsf.
compute nprembsf=n*prembsf.

Compute garnogar=0.
Compute garage=0.
If gar=1 gar1car=1.
If gar=2 gar1hcar=1.
If gar=3 gar2car=1.
If gar=4 gar2hcar=1.
If gar=5 gar3car=1.
If gar=6 gar3hcar=1.
If gar=7 garnogar=1.
If gar=8 gar4car=1. 
If gar ne 7 garage=1.

Compute garage1=0.
Compute garage2=0.
Compute garage3=0.
Compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1.  
Compute biggar=0.
if garage3=1 or garage4=1 biggar=1.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
compute nsrage=n*srage.

Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
Compute basement=0.
If basment=1 or basment=3 basement=1.
Compute nbasfull=n*basefull.
Compute nbaspart=n*basepart.
Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.
Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
compute nnobase=n*nobase.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
compute deluxbsf=qualdlux*bsf.

if num=6 bnum=0.
if num=0 bnum=0.
if num=1 bnum=2.
if num=2 bnum=3.
if num=3 bnum=4.
if num=4 bnum=5.
if num=5 bnum=6.

Compute nbnum=n*bnum.
Compute ncomm=n*comm.
Compute totunit=bnum + comm.
Compute totunitb=totunit*sqrt(bsf).


if firepl > 2 firepl = firepl - 1.
compute nfirepl=n*firepl.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute repabove=0.
Compute repbelow=0.
Compute repave=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.
Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.

Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.
exe.
compute b=1.
*select if (year1 = 2015).
*Table observation = b
                amount1
             /table = nghcde by 
                           amount1 
                             + b      
	/title = 'Ave & Med Sales'
     	/statistics =  median (amount1 'MED SP')
              mean (amount1 'MEAN SP')
                         validn (b '# PROPS').
    
reg des=defaults cov
      /var=amount1 nsrlsf nsrbsf nbasfull nbaspart nnobase nfirepl nbsfair
     nsiteben garage1 garage2 biggar nbathsum nprembsf nsrage
     masbsf frabsf stubsf frastbsf 
	   sb10 sb11 sb21 sb23 sb30 sb51 sb52 sb61 sb72 sb82 sb90 sb100 sb102
	   sb110 sb131 sb132 sb140 sb160 sb180 sb190 sb200 sb210 sb220
	   sb230 sb250 sb260 sb270 sb280 sb300 sb310 sb320 sb350 sb400 sb420
	   jantmar11cl234 winter1112cl234 winter1213cl234 winter1314cl234 winter1415cl234 summer11cl234
   summer12cl234 summer13cl234 summer14cl234 summer15cl234 octtdec15cl234 jantmar11cl56
   winter1112cl56 winter1213cl56 winter1314cl56 winter1415cl56 summer11cl56 summer12cl56
   summer13cl56 summer14cl56 summer15cl56 octtdec15cl56  jantmar11cl778 winter1112cl778
   winter1213cl778 winter1314cl778 winter1415cl778 summer11cl778 summer12cl778 summer13cl778
   summer14cl778 summer15cl778 octtdec15cl778 jantmar11cl89 winter1112cl89 winter1213cl89
   winter1314cl89 winter1415cl89 summer11cl89 summer12cl89 summer13cl89 summer14cl89
   summer15cl89 octtdec15cl89 jantmar11cl1112 winter1112cl1112 winter1213cl1112 winter1314cl1112 
   winter1415cl1112 summer11cl1112 summer12cl1112 summer13cl1112 summer14cl1112 summer15cl1112
   octtdec15cl1112 jantmar11cl1095 winter1112cl1095 winter1213cl1095 winter1314cl1095 winter1415cl1095
   summer11cl1095 summer12cl1095 summer13cl1095 summer14cl1095 summer15cl1095 octtdec15cl1095
   jantmar11clsplt winter1112clsplt winter1213clsplt winter1314clsplt winter1415clsplt summer11clsplt summer12clsplt
   summer13clsplt summer14clsplt summer15clsplt octtdec15clsplt 
  /dep=amount1
  /method=stepwise
	 /method=enter  jantmar11cl234 winter1112cl234 winter1213cl234 winter1314cl234 winter1415cl234 summer11cl234
   summer12cl234 summer13cl234 summer14cl234 summer15cl234 octtdec15cl234 jantmar11cl56
   winter1112cl56 winter1213cl56 winter1314cl56 winter1415cl56 summer11cl56 summer12cl56
   summer13cl56 summer14cl56 summer15cl56 octtdec15cl56  jantmar11cl778 winter1112cl778
   winter1213cl778 winter1314cl778 winter1415cl778 summer11cl778 summer12cl778 summer13cl778
   summer14cl778 summer15cl778 octtdec15cl778 jantmar11cl89 winter1112cl89 winter1213cl89
   winter1314cl89 winter1415cl89 summer11cl89 summer12cl89 summer13cl89 summer14cl89
   summer15cl89 octtdec15cl89 jantmar11cl1112 winter1112cl1112 winter1213cl1112 winter1314cl1112 
   winter1415cl1112 summer11cl1112 summer12cl1112 summer13cl1112 summer14cl1112 summer15cl1112
   octtdec15cl1112 jantmar11cl1095 winter1112cl1095 winter1213cl1095 winter1314cl1095 winter1415cl1095
   summer11cl1095 summer12cl1095 summer13cl1095 summer14cl1095 summer15cl1095 octtdec15cl1095
   jantmar11clsplt winter1112clsplt winter1213clsplt winter1314clsplt winter1415clsplt summer11clsplt summer12clsplt
   summer13clsplt summer14clsplt summer15clsplt octtdec15clsplt 
  /method=enter frastbsf nbaspart nbsfair nfirepl masbsf nbasfull sb102 sb400 sb21 garage1 nnobase
	 /save pred (pred) resid (resid).
  	sort cases by nghcde pin.	
  	value labels extcon  1 'Frame' 2 'Mason' 3 'Framas' 4 'Stucco'
    /basment 1 'Full' 2 'Slab' 3 'Part' 4 'Crwl'
    /bsfn 1 'Rec' 2 'Apt' 3 'Unf'
    /gar 1 '1' 2 '1.5' 3 '2' 4 '2.5' 5 '3' 6 '3.5' 7 'non'
    /class 2 '2-02' 3 '2-03' 4 '2-04' 5 '2-05' 6 '2-06' 7 '2-07' 8 '2-08' 9 '2-09' 10 '2-10' 
        11'2-11' 12 '2-12' 34 '2-34' 78 '2-78' 95 '2-95'.
compute perdif=(resid)/(amount1)*100.
formats pred(COMMA9.0)
          /resid (f6.0).
exe.
*plot
/vertical='Sales Price' MIN(50000) MAX(1000000)
/horizontal='RESID'
/vsize=25
/hsize=140
/plot=amount with RESID.
compute badsal=0.
if perdif>50 badsal=1.
if perdif<-50 badsal=1.
select if badsal=1.
set wid=125.
set len=59.
*REPORT FORMAT=automatic list(1)
	/title='Office of the Assessor'
        	'Residential Regression Report'
        	'Town is Northfield'
    	/ltitle 'Report Ran On)Date' 
    	/rtitle='PAGE)PAGE'
    	/string=prop(area(2) '-' subarea(2) '-' block(3) '-' parcel(3))
          	date(mos(2) '-'yr(2))
   /var=prop 'Prop' 'Index Number'(13)
       class 'Class' (label) (5)
       nghcde 'nbhd'(5)
       amount1 'Price'(7)
       pred 'Predicted'(9)
       resid 'Resid'(7)
       perdif '% DIF' (5)
       sqftb 'Sqftb'(5)
       age 'Age'(4)
       extcon 'Extcon' (label)(6)
       sqftl 'Land'(6).

