

townships <- "26"
# Pull records from server
modeldata <- dbGetQuery(CCAODATA, paste0("
SELECT HD_PIN as PIN, HD_CLASS as CLASS, H.TAX_YEAR, centroid_x, centroid_y, CONVERT(BIGINT, SALE_PRICE) as sale_price
, CONVERT(INT, SUBSTRING(REC_DATE,1,4)) as sale_year, REC_DATE
FROM
HEAD AS H
LEFT JOIN
PINGEO
ON CONVERT(BIGINT, NAME)=H.HD_PIN
LEFT JOIN 
IDORSALES AS S
ON (CONVERT(BIGINT, S.PIN)=H.HD_PIN AND CONVERT(INT, SUBSTRING(REC_DATE,1,4))=H.TAX_YEAR
WHERE primary_polygon IN (1) AND PIN999 NOT IN (1)
AND H.TAX_YEAR=2018 AND HD_CLASS>=300 AND HD_CLASS<400 
AND (MULTI_IND!=1)
AND CONVERT(INT, SUBSTRING(CONVERT(CHARACTER, HD_TOWN),1,2)) IN (",townships,")
"))

# Map of individual outlier PINs
# Define an outlier
bins <- c(-Inf, 0.8, 1.2, Inf)
# Map aesthetics
pal <- colorBin(c("red","green", "purple")
                , domain = modeldata$ratio
                , bins = bins
                , na.color = "#808080")
# Define popup for leaflet map
popup <- paste0(
  "<b> PIN: </b>", modeldata$PIN, "<br/>"
  , "<b> Address: </b>", modeldata$HD_ADDR, "<br/>"
  , "<b> Last sale date: </b>", modeldata$sale_date, "<br/>"
  , "<b> Sale price: </b>", paste("$", prettyNum(modeldata$sale_price,big.mark=",",scientific=FALSE), sep=""), "<br/>"
  , "<b> Model estimate: </b>", paste("$", prettyNum(modeldata$fitted_value,big.mark=",",scientific=FALSE), sep=""), "<br/>"
  , "<b> Ratio: </b>", round(modeldata$ratio,3), "<br/>"
)
# Create map of outliers
leaflet(modeldata) %>%
  addProviderTiles(providers$Esri.NatGeoWorldMap) %>%
  addCircleMarkers( lat = modeldata$centroid_y, lng =  modeldata$centroid_x
                    , color = pal(modeldata$ratio), popup =  popup) %>%
  addLegend(pal = pal
            , values = modeldata$ratio
            , opacity = 0.7
            , title = "Modeling errors"
            , position = "bottomleft") 