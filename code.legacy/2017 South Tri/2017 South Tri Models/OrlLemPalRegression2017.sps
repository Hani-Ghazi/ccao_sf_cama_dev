                             *SPSS Regression.
		 	        *Lemont, Orland, and Palos Regression 2017.                                                            


Get file='C:\Users\daaaron\documents\regts192830mergefcl2b.sav'.
select if (amount1>145000).
select if (amount1<990000).
select if (multi<1).
select if sqftb<6000.
Compute year1=0.
If  (amount1>0) year1=1900 + yr.
If  (yr=16 and amount1>0)  year1=2016.
If  (yr=15 and amount1>0)  year1=2015.
If  (yr=14 and amount1>0)  year1=2014.  
If  (yr=13 and amount1>0)  year1=2013.
If  (yr=12 and amount1>0)  year1=2012.
if  (yr=11 and amount1>0)  year1=2011. 


COMPUTE FX = cumfile14151617.
If FX > 6 FX = 6.
COMPUTE SRFX = sqrt(fx).
RECODE FX (SYSMIS=0).
*******************************************************************************************************************.

select if (year1>2011). 
select if puremarket=1.

set mxcells=2000500.
Compute bs=0.
if age<10 and (amount1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.
if (pin=	22203180010000
or pin=	22204000180000
or pin=	22291060170000
or pin=	22291100250000
or pin=	22291110140000
or pin=	22291160010000
or pin=	22291160200000
or pin=	22292200110000
or pin=	22281000110000
or pin=	22281041070000
or pin=	22281120320000
or pin=	22281120570000
or pin=	22281130300000
or pin=	22294160390000
or pin=	22294230170000
or pin=	22283020060000
or pin=	22271000320000
or pin=	22282040070000
or pin=	22293040140000
or pin=	22353020130000
or pin=	22241010380000
or pin=	22243080050000
or pin=	22251040170000
or pin=	22213100170000
or pin=	22273020350000
or pin=	22273020390000
or pin=	22282130020000
or pin=	22304040020000
or pin=	22311090060000
or pin=	22311120020000
or pin=	22321060040000
or pin=	22321080050000
or pin=	22322010220000
or pin=	22322030180000
or pin=	22332020210000
or pin=	22343040110000
or pin=	22343060010000
or pin=	22344100090000
or pin=	22344100100000
or pin=	22354040150000
or pin=	22274090110000
or pin=	22274090160000
or pin=	22283100410000
or pin=	22303050010000
or pin=	22303050350000
or pin=	27061030030000
or pin=	27063000180000
or pin=	27073080150000
or pin=	27182050050000
or pin=	27182070150000
or pin=	27183060130000
or pin=	27183070070000
or pin=	27183070100000
or pin=	27184060020000
or pin=	27184240010000
or pin=	27082000160000
or pin=	27082000180000
or pin=	27082020020000
or pin=	27082050270000
or pin=	27082080090000
or pin=	27082090240000
or pin=	27082100350000
or pin=	27082100360000
or pin=	27082110140000
or pin=	27082130090000
or pin=	27082140110000
or pin=	27084060190000
or pin=	27084060440000
or pin=	27084070080000
or pin=	27303020140000
or pin=	27304140590000
or pin=	27311120170000
or pin=	27312030150000
or pin=	27313020080000
or pin=	27314090480000
or pin=	27324040110000
or pin=	27092000140000
or pin=	27093070070000
or pin=	27093090070000
or pin=	27101040060000
or pin=	27101090070000
or pin=	27102070010000
or pin=	27102090010000
or pin=	27102200030000
or pin=	27161050300000
or pin=	27161060030000
or pin=	27161070150000
or pin=	27161080500000
or pin=	27162030060000
or pin=	27013000180000
or pin=	27021100020000
or pin=	27031020070000
or pin=	27033010070000
or pin=	27033010230000
or pin=	27034020240000
or pin=	27034030270000
or pin=	27034120040000
or pin=	27023010220000
or pin=	27023080160000
or pin=	27024050080000
or pin=	27024070550000
or pin=	27024080130000
or pin=	27131020120000
or pin=	27131040080000
or pin=	27131080050000
or pin=	27131110460000
or pin=	27134030740000
or pin=	27141020180000
or pin=	27144090150000
or pin=	27123030020000
or pin=	27091110240000
or pin=	27091230370000
or pin=	27091240170000
or pin=	27092160590000
or pin=	27092170390000
or pin=	27092170440000
or pin=	27222060270000
or pin=	27222070130000
or pin=	27232090360000
or pin=	27262180280000
or pin=	27274080020000
or pin=	27353040090000
or pin=	27223190050000
or pin=	27271080110000
or pin=	27271090070000
or pin=	27352210010000
or pin=	27242090100000
or pin=	27242090110000
or pin=	27252200120000
or pin=	27121000230000
or pin=	27164040110000
or pin=	27164040260000
or pin=	27164040360000
or pin=	27164070320000
or pin=	27212030600000
or pin=	27212050350000
or pin=	27053080260000
or pin=	27171070070000
or pin=	27173070010000
or pin=	27291000320000
or pin=	27291100010000
or pin=	27291110110000
or pin=	27291130060000
or pin=	27292010170000
or pin=	27292030130000
or pin=	27294180050000
or pin=	27294240280000
or pin=	27302010050000
or pin=	27302020040000
or pin=	27302030420000
or pin=	27322100080000
or pin=	27322140010000
or pin=	27133150070000
or pin=	27022050300000
or pin=	27253170080000
or pin=	27253170390000
or pin=	27273100170000
or pin=	27273160360000
or pin=	27344070140000
or pin=	27344100010000
or pin=	27344130440000
or pin=	23021030100000
or pin=	23021140230000
or pin=	23032070240000
or pin=	23034110030000
or pin=	23141040290000
or pin=	23141080080000
or pin=	23144001390000
or pin=	23022040250000
or pin=	23024030120000
or pin=	23024060240000
or pin=	23024080050000
or pin=	23034121100000
or pin=	23114120420000
or pin=	23114120590000
or pin=	23123060310000
or pin=	23142100330000
or pin=	23142130100000
or pin=	23102010220000
or pin=	23132030050000
or pin=	23132040020000
or pin=	23132050170000
or pin=	23132060170000
or pin=	23013100070000
or pin=	23134020080000
or pin=	23134080060000
or pin=	23134150040000
or pin=	23242030060000
or pin=	23242140100000
or pin=	23352060120000
or pin=	23353030030000
or pin=	23021080030000
or pin=	23021130030000
or pin=	23023030850000
or pin=	23023080190000
or pin=	23023100050000
or pin=	23344070090000
or pin=	23354150200000
or pin=	23354150270000
or pin=	23332000270000
or pin=	23293020070000
or pin=	23304010140000
or pin=	23104070040000
or pin=	23104120040000
or pin=	23113030080000
or pin=	23152030030000
or pin=	23152050090000
or pin=	23361020080000
or pin=	23361120120000
or pin=	23362120090000
or pin=	23052040180000
or pin=	23014030120000
or pin=	23322110010000
or pin=	23233150030000
or pin=	23234130230000
or pin=	23243002470000
or pin=	23243002490000
or pin=	23251020050000
or pin=	23261050310000
or pin=	23261060020000
or pin=	23261120050000
or pin=	23262011370000
or pin=	23264040150000
or pin=	23264100070000
or pin=	23264150180000
or pin=	23351030220000
or pin=	23351050070000
or pin=	23351060190000
or pin=	23352020100000
or pin=	23263040030000
or pin=	23263130060000
or pin=	23263170050000
or pin=	23271040080000
or pin=	23272000120000
or pin=	23272020040000
or pin=	23272020160000
or pin=	23272030440000
or pin=	23274160050000
or pin=	23274170030000
or pin=	23251070010000
or pin=	23251120140000
or pin=	23252000050000
or pin=	23252000330000
or pin=	23252020300000
or pin=	23252040050000
or pin=	23252080100000
or pin=	23252100080000
or pin=	23252160030000
or pin=	23252200080000
or pin=	23252250280000
or pin=	23253000120000
or pin=	23253000740000
or pin=	23254030090000
or pin=	23254060080000
or pin=	23254120080000
or pin=	23254130060000
or pin=	23254230070000
or pin=	23362020120000)	bs=1.
select if bs=0.

Compute N=1.

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.

compute bsf=sqftb.

compute bsf02=0.
compute bsf03=0.
compute bsf04=0.
compute bsf05=0.
compute bsf06=0.
compute bsf07=0.
compute bsf08=0.
compute bsf09=0.
compute bsf10=0.
compute bsf11=0.
compute bsf12=0.
compute bsf34=0.
compute bsf78=0.
compute bsf95=0.

if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.

compute tnb=(town*1000)+nghcde.

if (tnb= 19010 ) n=2.62.
if (tnb= 19011 ) n=4.75.
if (tnb= 19020 ) n=3.12.
if (tnb= 19025 ) n=4.05.
if (tnb= 19030 ) n=3.75.
if (tnb= 19032 ) n=2.53.
if (tnb= 19038 ) n=4.34.
if (tnb= 19040 ) n=6.08.
if (tnb= 19050 ) n=3.21.
if (tnb= 19060 ) n=4.82.
if (tnb= 19061 ) n=4.33.
if (tnb= 19100 ) n=5.73.

if (tnb= 28011 ) n=3.23.
if (tnb= 28012 ) n=5.99.
if (tnb= 28013 ) n=3.64.
if (tnb= 28014 ) n=3.85.
if (tnb= 28015 ) n=3.25.
if (tnb= 28022 ) n=2.43.
if (tnb= 28030 ) n=4.09.
if (tnb= 28031 ) n=2.81. 
if (tnb= 28032 ) n=2.85.
if (tnb= 28034 ) n=4.44.
if (tnb= 28036 ) n=2.97.
if (tnb= 28037 ) n=3.45.
if (tnb= 28039 ) n=2.77.
if (tnb= 28040 ) n=2.03.
if (tnb= 28043 ) n=2.65.
if (tnb= 28045 ) n=3.17.
if (tnb= 28046 ) n=2.58.
if (tnb= 28047 ) n=3.15.
if (tnb= 28050 ) n=2.23.
if (tnb= 28061 ) n=2.05.
if (tnb= 28088 ) n=2.69.
if (tnb= 28099 ) n=2.46.
if (tnb= 28100 ) n=4.50.
if (tnb= 28101 ) n=3.88.
if (tnb= 28102 ) n=3.38.
if (tnb= 28103 ) n=2.84.
if (tnb= 28104 ) n=3.43. 

if (tnb= 30010 ) n=2.50.
if (tnb= 30011 ) n=2.73.
if (tnb= 30012 ) n=2.59.
if (tnb= 30020 ) n=3.00.
if (tnb= 30022 ) n=2.32.
if (tnb= 30030 ) n=2.08.
if (tnb= 30031 ) n=2.83.
if (tnb= 30040 ) n=3.23.
if (tnb= 30041 ) n=3.46.
if (tnb= 30042 ) n=1.69.
if (tnb= 30050 ) n=3.25.
if (tnb= 30051 ) n=3.69.
if (tnb= 30052 ) n=4.45.
if (tnb= 30053 ) n=3.19.
if (tnb= 30054 ) n=3.48.
if (tnb= 30060 ) n=2.97.
if (tnb= 30061 ) n=2.99.
if (tnb= 30062 ) n=4.73.
if (tnb= 30063 ) n=4.95.
if (tnb= 30070 ) n=3.11.
if (tnb= 30071 ) n=4.67.
if (tnb= 30080 ) n=3.20.
if (tnb= 30082 ) n=8.39.
if (tnb= 30092 ) n=3.98.

compute n19010=0.
compute n19011=0.
compute n19020=0.
compute n19025=0.
compute n19030=0.
compute n19032=0.
compute n19038=0.
compute n19040=0.
compute n19050=0.
compute n19060=0.
compute n19061=0.
compute n19071=0.
compute n19100=0.

compute n28011=0.
compute n28012=0.
compute n28013=0.
compute n28014=0.
compute n28015=0.
compute n28022=0.
compute n28030=0.
compute n28031=0. 
compute n28032=0.
compute n28034=0.
compute n28036=0.
compute n28037=0.
compute n28039=0.
compute n28040=0.
compute n28043=0.
compute n28045=0.
compute n28046=0.
compute n28047=0.
compute n28050=0.
compute n28061=0.
compute n28088=0.
compute n28099=0.
compute n28100=0.
compute n28101=0.
compute n28102=0.
compute n28103=0.
compute n28104=0. 

compute n30010=0.
compute n30011=0.
compute n30012=0.
compute n30020=0.
compute n30022=0.
compute n30030=0.
compute n30031=0.
compute n30038=0.
compute n30040=0. 
compute n30041=0.
compute n30042=0.
compute n30050=0.
compute n30051=0.
compute n30052=0.
compute n30053=0.
compute n30054=0.
compute n30055=0.
compute n30060=0.
compute n30061=0.
compute n30062=0.
compute n30063=0.
compute n30070=0.
compute n30071=0.
compute n30080=0.
compute n30082=0.
compute n30092=0.

compute b19010=0.
compute b19011=0.
compute b19020=0.
compute b19025=0.
compute b19030=0.
compute b19032=0.
compute b19040=0.
compute b19050=0.
compute b19038=0.
compute b19060=0.
compute b19061=0.
compute b19071=0.
compute b19100=0.

compute b28011=0.
compute b28012=0.
compute b28013=0.
compute b28014=0.
compute b28015=0.
compute b28022=0.
compute b28030=0.
compute b28031=0. 
compute b28032=0.
compute b28034=0.
compute b28036=0.
compute b28037=0.
compute b28039=0.
compute b28040=0.
compute b28043=0.
compute b28045=0.
compute b28046=0.
compute b28047=0.
compute b28050=0.
compute b28061=0.
compute b28088=0.
compute b28099=0.
compute b28100=0.
compute b28101=0.
compute b28102=0.
compute b28103=0.
compute b28104=0. 

compute b30010=0.
compute b30011=0.
compute b30012=0.
compute b30020=0.
compute b30022=0.
compute b30030=0.
compute b30031=0.
compute b30040=0. 
compute b30041=0.
compute b30042=0.
compute b30050=0.
compute b30051=0.
compute b30052=0.
compute b30053=0.
compute b30054=0.
compute b30060=0.
compute b30061=0.
compute b30062=0.
compute b30063=0.
compute b30070=0.
compute b30071=0.
compute b30080=0.
compute b30082=0.
compute b30092=0.


compute sb19010=0.
compute sb19011=0.
compute sb19020=0.
compute sb19025=0.
compute sb19030=0.
compute sb19032=0.
compute sb19040=0.
compute sb19050=0.
compute sb19038=0.
compute sb19060=0.
compute sb19061=0.
compute sb19071=0.
compute sb19100=0.

compute sb28011=0.
compute sb28012=0.
compute sb28013=0.
compute sb28014=0.
compute sb28015=0.
compute sb28022=0.
compute sb28030=0.
compute sb28031=0. 
compute sb28032=0.
compute sb28034=0.
compute sb28036=0.
compute sb28037=0.
compute sb28039=0.
compute sb28040=0.
compute sb28043=0.
compute sb28045=0.
compute sb28046=0.
compute sb28047=0.
compute sb28050=0.
compute sb28061=0.
compute sb28088=0.
compute sb28099=0.
compute sb28100=0.
compute sb28101=0.
compute sb28102=0.
compute sb28103=0.
compute sb28104=0. 

compute sb30010=0.
compute sb30011=0.
compute sb30012=0.
compute sb30020=0.
compute sb30022=0.
compute sb30030=0.
compute sb30031=0.
compute sb30040=0. 
compute sb30041=0.
compute sb30042=0.
compute sb30050=0.
compute sb30051=0.
compute sb30052=0.
compute sb30053=0.
compute sb30054=0.
compute sb30060=0.
compute sb30061=0.
compute sb30062=0.
compute sb30063=0.
compute sb30070=0.
compute sb30071=0.
compute sb30080=0.
compute sb30082=0.
compute sb30092=0.



if (tnb= 19010 ) n19010=1.
if (tnb= 19011 ) n19011=1.
if (tnb= 19020 ) n19020=1.
if (tnb= 19025 ) n19025=1.
if (tnb= 19030 ) n19030=1.
if (tnb= 19032 ) n19032=1.
if (tnb= 19038 ) n19038=1.
if (tnb= 19040 ) n19040=1.
if (tnb= 19050 ) n19050=1.
if (tnb= 19060 ) n19060=1.
if (tnb= 19061 ) n19061=1.
if (tnb= 19071 ) n19071=1.
if (tnb= 19100 ) n19100=1.

if (tnb= 28011 ) n28011=1.
if (tnb= 28012 ) n28012=1.
if (tnb= 28013 ) n28013=1.
if (tnb= 28014 ) n28014=1.
if (tnb= 28015 ) n28015=1.
if (tnb= 28022 ) n28022=1.
if (tnb= 28030 ) n28030=1.
if (tnb= 28031 ) n28031=1. 
if (tnb= 28032 ) n28032=1.
if (tnb= 28034 ) n28034=1.
if (tnb= 28036 ) n28036=1.
if (tnb= 28037 ) n28037=1.
if (tnb= 28039 ) n28039=1.
if (tnb= 28040 ) n28040=1.
if (tnb= 28043 ) n28043=1.
if (tnb= 28045 ) n28045=1.
if (tnb= 28046 ) n28046=1.
if (tnb= 28047 ) n28047=1.
if (tnb= 28050 ) n28050=1.
if (tnb= 28061 ) n28061=1.
if (tnb= 28088 ) n28088=1.
if (tnb= 28099 ) n28099=1.
if (tnb= 28100 ) n28100=1.
if (tnb= 28101 ) n28101=1.
if (tnb= 28102 ) n28102=1.
if (tnb= 28103 ) n28103=1.
if (tnb= 28104 ) n28104=1. 

if (tnb= 30010 ) n30010=1.
if (tnb= 30011 ) n30011=1.
if (tnb= 30012 ) n30012=1.
if (tnb= 30020 ) n30020=1.
if (tnb= 30022 ) n30022=1.
if (tnb= 30030 ) n30030=1.
if (tnb= 30031 ) n30031=1.
if (tnb= 30040 ) n30040=1. 
if (tnb= 30041 ) n30041=1.
if (tnb= 30042 ) n30042=1.
if (tnb= 30050 ) n30050=1.
if (tnb= 30051 ) n30051=1.
if (tnb= 30052 ) n30052=1.
if (tnb= 30053 ) n30053=1.
if (tnb= 30054 ) n30054=1.
if (tnb= 30055 ) n30055=1.
if (tnb= 30060 ) n30060=1.
if (tnb= 30061 ) n30061=1.
if (tnb= 30062 ) n30062=1.
if (tnb= 30063 ) n30063=1.
if (tnb= 30070 ) n30070=1.
if (tnb= 30080 ) n30080=1.
if (tnb= 30082 ) n30082=1.
if (tnb= 30092 ) n30092=1.

Compute lsf=sqftl.

if (tnb= 19010 ) sb19010=sqrt(bsf).
if (tnb= 19011 ) sb19011=sqrt(bsf).
if (tnb= 19020 ) sb19020=sqrt(bsf).
if (tnb= 19025 ) sb19025=sqrt(bsf).
if (tnb= 19030 ) sb19030=sqrt(bsf).
if (tnb= 19032 ) sb19032=sqrt(bsf).
if (tnb= 19040 ) sb19040=sqrt(bsf).
if (tnb= 19050 ) sb19050=sqrt(bsf).
if (tnb= 19038 ) sb19038=sqrt(bsf).
if (tnb= 19060 ) sb19060=sqrt(bsf).
if (tnb= 19061 ) sb19061=sqrt(bsf).
if (tnb= 19071 ) sb19071=sqrt(bsf).
if (tnb= 19100 ) sb19100=sqrt(bsf).

if (tnb= 28011 ) sb28011=sqrt(bsf).
if (tnb= 28012 ) sb28012=sqrt(bsf).
if (tnb= 28013 ) sb28013=sqrt(bsf).
if (tnb= 28014 ) sb28014=sqrt(bsf).
if (tnb= 28015 ) sb28015=sqrt(bsf).
if (tnb= 28022 ) sb28022=sqrt(bsf).
if (tnb= 28030 ) sb28030=sqrt(bsf).
if (tnb= 28031 ) sb28031=sqrt(bsf). 
if (tnb= 28032 ) sb28032=sqrt(bsf).
if (tnb= 28034 ) sb28034=sqrt(bsf).
if (tnb= 28036 ) sb28036=sqrt(bsf).
if (tnb= 28037 ) sb28037=sqrt(bsf).
if (tnb= 28039 ) sb28039=sqrt(bsf).
if (tnb= 28040 ) sb28040=sqrt(bsf).
if (tnb= 28043 ) sb28043=sqrt(bsf).
if (tnb= 28045 ) sb28045=sqrt(bsf).
if (tnb= 28046 ) sb28046=sqrt(bsf).
if (tnb= 28047 ) sb28047=sqrt(bsf).
if (tnb= 28050 ) sb28050=sqrt(bsf).
if (tnb= 28061 ) sb28061=sqrt(bsf).
if (tnb= 28088 ) sb28088=sqrt(bsf).
if (tnb= 28099 ) sb28099=sqrt(bsf).
if (tnb= 28100 ) sb28100=sqrt(bsf).
if (tnb= 28101 ) sb28101=sqrt(bsf).
if (tnb= 28102 ) sb28102=sqrt(bsf).
if (tnb= 28103 ) sb28103=sqrt(bsf).
if (tnb= 28104 ) sb28104=sqrt(bsf). 

if (tnb= 30010 ) sb30010=sqrt(bsf).
if (tnb= 30011 ) sb30011=sqrt(bsf).
if (tnb= 30012 ) sb30012=sqrt(bsf).
if (tnb= 30020 ) sb30020=sqrt(bsf).
if (tnb= 30022 ) sb30022=sqrt(bsf).
if (tnb= 30030 ) sb30030=sqrt(bsf).
if (tnb= 30031 ) sb30031=sqrt(bsf).
if (tnb= 30040 ) sb30040=sqrt(bsf). 
if (tnb= 30041 ) sb30041=sqrt(bsf).
if (tnb= 30042 ) sb30042=sqrt(bsf).
if (tnb= 30050 ) sb30050=sqrt(bsf).
if (tnb= 30051 ) sb30051=sqrt(bsf).
if (tnb= 30052 ) sb30052=sqrt(bsf).
if (tnb= 30053 ) sb30053=sqrt(bsf).
if (tnb= 30054 ) sb30054=sqrt(bsf).
if (tnb= 30060 ) sb30060=sqrt(bsf).
if (tnb= 30061 ) sb30061=sqrt(bsf).
if (tnb= 30062 ) sb30062=sqrt(bsf).
if (tnb= 30063 ) sb30063=sqrt(bsf).
if (tnb= 30070 ) sb30070=sqrt(bsf).
if (tnb= 30071 ) sb30071=sqrt(bsf).
if (tnb= 30080 ) sb30080=sqrt(bsf).
if (tnb= 30082 ) sb30082=sqrt(bsf).
if (tnb= 30092 ) sb30092=sqrt(bsf).

if tnb=19020 and class=95  lsf=7603.
if tnb=19025 and class=95  lsf=3810.
if tnb=19060 and class=95  lsf=3461.
if tnb=19061 and class=95  lsf=5422.
if tnb=19100 and class=95  lsf=2633.
if tnb=28011 and class=95  lsf=4346.
if tnb=28014 and class=95  lsf=6622.
if tnb=28015 and class=95  lsf=2431.
if tnb=28022 and class=95  lsf=2730.
if tnb=28032 and class=95  lsf=1915.
if tnb=28036 and class=95  lsf=3463.
if tnb=28040 and class=95  lsf=2624.
if tnb=28043 and class=95  lsf=1511.
if tnb=28045 and class=95  lsf=1979.
if tnb=28046 and class=95  lsf=2720.
if tnb=28050 and class=95  lsf=2464.
if tnb=28088 and class=95  lsf=3070.
if tnb=28099 and class=95  lsf=2401.
if tnb=28100 and class=95  lsf=2145.
if tnb=28103 and class=95  lsf=3081.
if tnb=30011 and class=95  lsf=2976.
if tnb=30012 and class=95  lsf=1683.
if tnb=30042 and class=95  lsf=1428.
if tnb=30060 and class=95  lsf=1854.
if tnb=30061 and class=95  lsf=1600.
if tnb=30070 and class=95  lsf=3056.
if tnb=30092 and class=95  lsf=5755.


compute lem=0.
compute pal=0.
compute orl=0.
if town=19 lem=1.
if town=28 orl=1.
if town=30 pal=1.

compute bsf=sqftb.
compute srbsf=sqrt(sqftb).
compute srage=sqrt(age).
compute nsrbsf=n*sqrt(bsf).

Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).

Compute winter1213=0.
if (mos > 9 and yr=12) or (mos <= 3 and yr=13) winter1213=1.
Compute winter1314=0.
if (mos > 9 and yr=13) or (mos <= 3 and yr=14) winter1314=1.
Compute winter1415=0.
if (mos > 9 and yr=14) or (mos <= 3 and yr=15) winter1415=1.
Compute winter1516=0.
if (mos > 9 and yr=15) or (mos <= 3 and yr=16) winter1516=1.
Compute summer12=0.
if (mos > 3 and yr=12) and (mos <= 9 and yr=12) summer12=1.
Compute summer13=0.
if (mos > 3 and yr=13) and (mos <= 9 and yr=13) summer13=1.
Compute summer14=0.
if (mos > 3 and yr=14) and (mos <= 9 and yr=14) summer14=1.
Compute summer15=0.
if (mos > 3 and yr=15) and (mos <= 9 and yr=15) summer15=1.
Compute summer16=0.
if (mos > 3 and yr=16) and (mos <= 9 and yr=16) summer16=1.
Compute jantmar12=0.
if (year1=2012 and (mos>=1 and mos<=3)) jantmar12=1. 
Compute octtdec16=0.
if (year1=2016 and (mos>=10 and mos<=12)) octtdec16=1.

Compute jantmar12cl234=jantmar12*cl234.
Compute winter1213cl234=winter1213*cl234.
Compute winter1314cl234=winter1314*cl234.
Compute winter1415cl234=winter1415*cl234.
Compute winter1516cl234=winter1516*cl234.
Compute summer12cl234=summer12*cl234.
Compute summer13cl234=summer13*cl234.
Compute summer14cl234=summer14*cl234.
Compute summer15cl234=summer15*cl234.
Compute summer16cl234=summer16*cl234.
Compute octtdec16cl234=octtdec16*cl234.

Compute jantmar12cl56=jantmar12*cl56.
Compute winter1213cl56=winter1213*cl56.
Compute winter1314cl56=winter1314*cl56.
Compute winter1415cl56=winter1415*cl56.
Compute winter1516cl56=winter1516*cl56.
Compute summer12cl56=summer12*cl56.
Compute summer13cl56=summer13*cl56.
Compute summer14cl56=summer14*cl56.
Compute summer15cl56=summer15*cl56.
Compute summer16cl56=summer16*cl56.
Compute octtdec16cl56=octtdec16*cl56.

Compute jantmar12cl778=jantmar12*cl778.
Compute winter1213cl778=winter1213*cl778.
Compute winter1314cl778=winter1314*cl778.
Compute winter1415cl778=winter1415*cl778.
Compute winter1516cl778=winter1516*cl778.
Compute summer12cl778=summer12*cl778.
Compute summer13cl778=summer13*cl778.
Compute summer14cl778=summer14*cl778.
Compute summer15cl778=summer15*cl778.
Compute summer16cl778=summer16*cl778.
Compute octtdec16cl778=octtdec16*cl778.


Compute jantmar12cl89=jantmar12*cl89.
Compute winter1213cl89=winter1213*cl89.
Compute winter1314cl89=winter1314*cl89.
Compute winter1415cl89=winter1415*cl89.
Compute winter1516cl89=winter1516*cl89.
Compute summer12cl89=summer12*cl89.
Compute summer13cl89=summer13*cl89.
Compute summer14cl89=summer14*cl89.
Compute summer15cl89=summer15*cl89.
Compute summer16cl89=summer16*cl89.
Compute octtdec16cl89=octtdec16*cl89.

Compute jantmar12cl1112=jantmar12*cl1112.
Compute winter1213cl1112=winter1213*cl1112.
Compute winter1314cl1112=winter1314*cl1112.
Compute winter1415cl1112=winter1415*cl1112.
Compute winter1516cl1112=winter1516*cl1112.
Compute summer12cl1112=summer12*cl1112.
Compute summer13cl1112=summer13*cl1112.
Compute summer14cl1112=summer14*cl1112.
Compute summer15cl1112=summer15*cl1112.
Compute summer16cl1112=summer16*cl1112.
Compute octtdec16cl1112=octtdec16*cl1112.

Compute jantmar12cl1095=jantmar12*cl1095.
Compute winter1213cl1095=winter1213*cl1095.
Compute winter1314cl1095=winter1314*cl1095.
Compute winter1415cl1095=winter1415*cl1095.
Compute winter1516cl1095=winter1516*cl1095.
Compute summer12cl1095=summer12*cl1095.
Compute summer13cl1095=summer13*cl1095.
Compute summer14cl1095=summer14*cl1095.
Compute summer15cl1095=summer15*cl1095.
Compute summer16cl1095=summer16*cl1095.
Compute octtdec16cl1095=octtdec16*cl1095.

Compute jantmar12clsplt=jantmar12*clsplt.
Compute winter1213clsplt=winter1213*clsplt.
Compute winter1314clsplt=winter1314*clsplt.
Compute winter1415clsplt=winter1415*clsplt.
Compute winter1516clsplt=winter1516*clsplt.
Compute summer12clsplt=summer12*clsplt.
Compute summer13clsplt=summer13*clsplt.
Compute summer14clsplt=summer14*clsplt.
Compute summer15clsplt=summer15*clsplt.
Compute summer16clsplt=summer16*clsplt.
Compute octtdec16clsplt=octtdec16*clsplt.

Compute lowzoneorland=0.
if town=28 and  (nghcde=11 or nghcde=12  or nghcde=14  or nghcde=15 
or nghcde=22 or nghcde=30 or nghcde=31 or nghcde=32 or nghcde=34
or nghcde=36 or nghcde=37 or nghcde=39 or nghcde=43 or nghcde=45
or nghcde=46 or nghcde=47 or nghcde=61 or nghcde=88
or nghcde=99 or nghcde=100 or nghcde=101 or nghcde=102 
or nghcde=103 or nghcde=104)  lowzoneorland=1.                 	

Compute midzoneorland=0.
if town=28 and (nghcde=13 or nghcde=40 or nghcde=50) midzoneorland=1. 

Compute srfxlowblockorland=0.
if lowzoneorland=1 srfxlowblockorland=srfx*lowzoneorland.

Compute srfxmidblockorland=0.
if midzoneorland=1 srfxmidblockorland=srfx*midzoneorland.

Compute lowzonepalos=0.
if town=30 and  (nghcde=10 or nghcde=11  or nghcde=12  or nghcde=20 
or nghcde=31 or nghcde=40 or nghcde=41 or nghcde=50
or nghcde=51 or nghcde=52 or nghcde=53 or nghcde=54 or nghcde=62 
or nghcde=63 or nghcde=70 or nghcde=71 or nghcde=80
or nghcde=82 or nghcde=92 or nghcde=100) lowzonepalos=1.
                              	
Compute midzonepalos=0.
if town=30 and (nghcde=22 or nghcde=30 or nghcde=42 or nghcde=60 or nghcde=61 )  midzonepalos=1. 

Compute srfxlowblockpalos=0.
if lowzonepalos=1 srfxlowblockpalos=srfx*lowzonepalos.

Compute srfxmidblockpalos=0.
if midzonepalos=1 srfxmidblockpalos=srfx*midzonepalos.

Compute lowzonelemont=0.
if town=19 and  (nghcde=10 or nghcde=11  or nghcde=20  or nghcde=25 
or nghcde=30 or nghcde=32 or nghcde=38 or nghcde=40  or nghcde=50    
or nghcde=60 or nghcde=61 or nghcde=100) lowzonelemont=1.                         	

Compute srfxlowblocklemont=0.
if lowzonelemont=1 srfxlowblocklemont=srfx*lowzonelemont.


if num=6 bnum=0.
if num=0 bnum=0.
if num=1 bnum=2.
if num=2 bnum=3.
if num=3 bnum=4.
if num=4 bnum=5.
if num=5 bnum=6.

compute res=bnum - comm.
compute bnumb=bnum*bsf.
compute nres=n*res.

Compute nsrbsf=n*sqrt(bsf).
Compute nbsf=n*bsf.
Compute srage=sqrt(age).
Compute nsrage=n*srage.

Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.

Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute nframas=n*framas.
Compute nmason=n*mason.
Compute frabsf=frame*bsf.
Compute stubsf=stucco*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute nfrast=n*frast.
Compute frastbsf=frast*bsf.
Compute masonbsf=mason*bsf.
Compute frmsbsf=framas*bsf.

Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
Compute prembsf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.
Compute nprembsf=n*premrf*bsf.


compute attcfull=0.
compute attcpart=0.
compute attcnone=0.
if attc=1 attcfull=1.
if attc=2 attcpart=1.
if attc=3 attcnone=1.
compute atfnliv=0.
compute atfnapt=0.
if atfn=1 atfnliv=1.
if atfn=2 atfnapt=1.

Compute garnogar=0.
Compute garage=0.
Compute biggar=0.
compute garage1=0.
compute garage2=0.
compute garage3=0.
compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1.
Compute biggar=0.
if garage3=1 or garage4=1 biggar=1.


Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
Compute nbasfull=n*basefull.
Compute nbaspart=n*basepart.

Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.

Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
compute nnobase=n*nobase.


If firepl>1 firepl=firepl - 1. 
compute nfirepl=n*firepl.
 
Compute bathsum=fullbath + 0.25*halfbath.
Compute nbathsum=n*bathsum.


*Cutpoints for land square foot are 1.75 * median lsf.
*If town=28 and lsf > 17500  lsf = 17500 + ((lsf - 17500)/3).
*If town=19 and lsf > 19785  lsf = 19785 + ((lsf - 19785)/3).
*If town=30 and lsf > 18037  lsf = 18037 + ((lsf - 18037)/3).
*If (class=8 or class=9) lsf=sqftl.

compute nbsfair=n*bsfair.
compute nmasbsf=n*masonbsf.
compute nfrmsbsf=n*frmsbsf.
compute nfrstbsf=n*frastbsf.
compute lemlsf=lem*lsf.
compute orllsf=orl*lsf.
compute lemage=lem*age.
compute orlage=orl*age.
compute srlsf=sqrt(lsf).
compute nsrlsf=n*srlsf.

compute nsragpal=nsrage*pal.
compute nsra1930=nsrage*(lem+pal).
compute nsraglem=nsrage*lem.
compute nsragorl=nsrage*orl.
compute srlsforl=srlsf*orl.
compute nsrlorl=nsrlsf*orl.
compute nsrlem=nsrlsf*lem.
compute nsrlpal=nsrlsf*pal.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute repabove=0.
Compute repbelow=0.
Compute repave=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.

Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.
Compute twosty=0.
If class07=1 or class08=1 or class78=1 twosty=1.

compute cathdral=0.
if ceiling=1 cathdral=1.
if ceiling=2 cathdral=0.
  
compute b=1.
*select if year1 = 2016.
*select if puremarket=1.
*Table observation = b
                amount1
               /table = tnb by 
                           amount1 
                             + b      
	/title = 'Ave & Med Sales'
     	/statistics =  median (amount1 'MED SP')
              mean (amount1 'MEAN SP')
                         validn (b '# PROPS').
 

reg des=defaults cov
     /var=amount1 nbsf234 nbsf56 nbsf778 nbsf34 nbsf1095 
 nbsf1112 nsrage nsrlorl nsrlpal nsrlem nbathsum nbsfair nfirepl 
 garnogar 	garage1 garage2 biggar nbasfull nbaspart nnobase nprembsf srfx
	cathdral sb30010 sb30011 sb30012 sb30020 sb30022
	sb30030 sb30031 sb30040 sb30041 sb30042 sb30050 sb30051
	sb30052 sb30053 sb30054 sb30060 sb30061 sb30062 sb30063
	sb30070 sb30071 sb30080 sb30082 sb30092 sb28011 sb28012
	sb28013 sb28014 sb28015 sb28022 sb28030 sb28031 sb28032
	sb28034 sb28036 sb28037 sb28039 sb28040 sb28043 sb28045
	sb28046 sb28047 sb28050 sb28061 sb28088 sb28099 sb28100
	sb28101 sb28102 sb28103 sb28104 sb19010 sb19011 sb19020
	sb19025 sb19030 sb19032 sb19038 sb19040 sb19050 sb19060
	sb19061 sb19071 sb19100  jantmar12cl234  winter1213cl234 winter1314cl234 winter1415cl234 winter1516cl234 
 summer12cl234 summer13cl234 summer14cl234 summer15cl234 summer16cl234 octtdec16cl234
 winter1213cl56 winter1314cl56 winter1415cl56 winter1516cl56 summer12cl56
 summer13cl56 summer14cl56 summer15cl56 summer16cl56 octtdec16cl56 winter1213cl778
 winter1314cl778 winter1415cl778 winter1516cl778 summer12cl778 summer13cl778
 summer14cl778 summer15cl778 summer16cl778 octtdec16cl778  winter1213cl89
 winter1314cl89 winter1415cl89 winter1516cl89 summer12cl89 summer13cl89 summer14cl89
 summer15cl89 summer16cl89 octtdec16cl89  winter1213cl1112 winter1314cl1112 winter1415cl1112 winter1516cl1112 
 summer12cl1112 summer13cl1112 summer14cl1112 summer15cl1112 summer16cl1112
 octtdec16cl1112  winter1213cl1095 winter1314cl1095 winter1415cl1095 winter1516cl1095
 summer12cl1095 summer13cl1095 summer14cl1095 summer15cl1095 summer16cl1095 octtdec16cl1095
 winter1213clsplt winter1314clsplt winter1415clsplt winter1516clsplt  summer12clsplt
 summer13clsplt summer14clsplt summer15clsplt summer16clsplt octtdec16clsplt 
	midzoneorland srfxlowblockorland srfxmidblockorland 	midzonepalos srfxlowblockpalos
 srfxmidblockpalos srfxlowblocklemont   
	/dep=amount1	
 /method=stepwise 
	/method=enter    jantmar12cl234  winter1213cl234 winter1314cl234 winter1415cl234 winter1516cl234 
 summer12cl234 summer13cl234 summer14cl234 summer15cl234 summer16cl234 octtdec16cl234
 winter1213cl56 winter1314cl56 winter1415cl56 winter1516cl56 summer12cl56
 summer13cl56 summer14cl56 summer15cl56 summer16cl56 octtdec16cl56 winter1213cl778
 winter1314cl778 winter1415cl778 winter1516cl778 summer12cl778 summer13cl778
 summer14cl778 summer15cl778 summer16cl778 octtdec16cl778  winter1213cl89
 winter1314cl89 winter1415cl89 winter1516cl89 summer12cl89 summer13cl89 summer14cl89
 summer15cl89 summer16cl89 octtdec16cl89  winter1213cl1112 winter1314cl1112 winter1415cl1112 winter1516cl1112 
 summer12cl1112 summer13cl1112 summer14cl1112 summer15cl1112 summer16cl1112
 octtdec16cl1112  winter1213cl1095 winter1314cl1095 winter1415cl1095 winter1516cl1095
 summer12cl1095 summer13cl1095 summer14cl1095 summer15cl1095 summer16cl1095 octtdec16cl1095
 winter1213clsplt winter1314clsplt winter1415clsplt winter1516clsplt  summer12clsplt
 summer13clsplt summer14clsplt summer15clsplt summer16clsplt octtdec16clsplt  
 /method=enter midzoneorland srfxlowblockorland srfxmidblockorland 	midzonepalos 
 srfxlowblockpalos srfxmidblockpalos  srfxlowblocklemont
 /method=enter garnogar nbsf34 nfirepl  garage1 srfx
		/save pred (pred) resid (resid).
      sort cases by tnb pin.	
      value labels extcon 1 'Frame' 2 'Mason' 3 'Framas' 4 'Stucco'
     /basment 1 'Full' 2 'Slab' 3 'Part' 4 'Crwl'
     /bsfn 1 'Rec' 2 'Apt' 3 'Unf'
     /gar 1 '1' 2 '1.5' 3 '2' 4 '2.5' 5 '3' 6 '3.5' 7 'non'
     /class 2 '2-02' 3 '2-03' 4 '2-04' 5 '2-05' 6 '2-06' 7 '2-07' 8 '2-08' 9 '2-09' 10 '2-10' 11 '2-11' 12 '2-12' 34 '2-34' 78 '2-78' 95 '2-95'.
*compute perdif=(resid)/(amount1)*100.
*formats pred(COMMA9.0)
          /resid (f6.0).


exe.
*plot
/vertical='Sales Price' MIN(50000) MAX(1000000)
/horizontal='RESID'
/vsize=25
/hsize=140
/plot=amount1 with RESID.
*compute badsal=0.
*if perdif > 30 badsal=1.
*if perdif < - 30 badsal=1.
*select if badsal=1.
set wid=125.
set len=59.
*exe.
*REPORT FORMAT=automatic list(1)
/title='Office of the Assessor'
        'Residential Regression Report'
        'Town is Orland Lemont Palos'
    /ltitle 'Report Ran On)Date' 
    /rtitle='PAGE)PAGE'
    /string=prop(area(2) '-' subarea(2) '-' block(3) '-' parcel(3))
          date(mos(2) '-'yr(2))
   /var=prop 'Prop' 'Index Number'(13)
       class 'Class' (label) (5)
       nghcde 'nbhb'(5)
       amount1 'Price'(7)
       pred 'Predicted'(9)
       resid 'Resid'(7)
       perdif '% DIF' (5)
       aos 'AOS' (3)
       sqftb 'Sqftb'(5)
       age 'Age'(4)
       extcon 'Extcon' (label)(6)
       sqftl 'Land'(6).

	
 
 
