                        *SPSS Regression.
			*BERWYN + CALUMET + CICERO + STICKNEY  2017.


Get file='C:\Users\daaaron\documents\regts11141536mergefcl2b.sav'.
set wid=125.
set len=59.
*select if (amount1>110000).
*select if (amount1<450000).
*select if (multi<1).
*select if sqftb<6000.

COMPUTE YEAR1=0.
If  (amount1>0) year1=1900 + yr. 
If  (yr=16 and amount1>0)  year1=2016.
If  (yr=15 and amount1>0)  year1=2015.
If  (yr=14 and amount1>0)  year1=2014.
If  (yr=13 and amount1>0)  year1=2013.
If  (yr=12 and amount1>0)  year1=2012.
If  (yr=11 and amount1>0)  year1=2011. 
If  (yr=10 and amount1>0)  year1=2010.



COMPUTE FX = cumfile14151617.
If FX > 8 FX=8.
COMPUTE SRFX = sqrt(fx).
RECODE FX (SYSMIS=0).
*******************************************************************************************************************.

*select if (year1>2011).
*select if puremarket=1.


compute bsf=sqftb.

Compute bs=0.
if age<10 and (amount1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.
*if (pin=	16191010200000
or pin=	16191010270000
or pin=	16191020280000
or pin=	16191040290000
or pin=	16191060400000
or pin=	16191190110000
or pin=	16191190240000
or pin=	16192160140000
or pin=	16192160260000
or pin=	16192180220000
or pin=	16192230050000
or pin=	16193010080000
or pin=	16193020050000
or pin=	16193100160000
or pin=	16193110230000
or pin=	16193190180000
or pin=	16193250170000
or pin=	16193280180000
or pin=	16194000280000
or pin=	16194030430000
or pin=	16194160320000
or pin=	16194220070000
or pin=	16201010050000
or pin=	16201100150000
or pin=	16201170010000
or pin=	16203000210000
or pin=	16203080280000
or pin=	16203290040000
or pin=	16291160170000
or pin=	16291160330000
or pin=	16291170210000
or pin=	16291260050000
or pin=	16293000140000
or pin=	16293100060000
or pin=	16293100220000
or pin=	16301050190000
or pin=	16301060140000
or pin=	16301090250000
or pin=	16301100010000
or pin=	16301110370000
or pin=	16301150180000
or pin=	16302000230000
or pin=	16302010180000
or pin=	16302030110000
or pin=	16302130110000
or pin=	16302140110000
or pin=	16302170090000
or pin=	16302230260000
or pin=	16302270040000
or pin=	16302290260000
or pin=	16303000270000
or pin=	16303080310000
or pin=	16303090410000
or pin=	16303090420000
or pin=	16303100250000
or pin=	16303150260000
or pin=	16303210240000
or pin=	16303250070000
or pin=	16303250130000
or pin=	16304040200000
or pin=	16304040470000
or pin=	16304040530000
or pin=	16304170130000
or pin=	16311070060000
or pin=	16311110250000
or pin=	16311120090000
or pin=	16311120300000
or pin=	16311130090000
or pin=	16311260040000
or pin=	16311280330000
or pin=	16311290060000
or pin=	16311320160000
or pin=	16312040050000
or pin=	16312160230000
or pin=	16312180370000
or pin=	16312190250000
or pin=	16312220170000
or pin=	16312220330000
or pin=	16312270040000
or pin=	16313010230000
or pin=	16313020340000
or pin=	16313030250000
or pin=	16313050460000
or pin=	16313070330000
or pin=	16313080320000
or pin=	16313210140000
or pin=	16313250150000
or pin=	16313250160000
or pin=	16314160380000
or pin=	16314170120000
or pin=	16314220510000
or pin=	16314240480000
or pin=	16321130020000
or pin=	16321140260000
or pin=	16321190130000
or pin=	16321210020000
or pin=	16321320260000
or pin=	16321320440000
or pin=	16323010080000
or pin=	16323180240000
or pin=	16323250270000
or pin=	25311050350000
or pin=	25311100140000
or pin=	25311180260000
or pin=	25311250370000
or pin=	25311250670000
or pin=	25312010290000
or pin=	25312030410000
or pin=	25313550130000
or pin=	25314000130000
or pin=	16202080210000
or pin=	16202190070000
or pin=	16204210300000
or pin=	16212290180000
or pin=	16214100190000
or pin=	16214140220000
or pin=	16214180200000
or pin=	16282230460000
or pin=	16283070370000
or pin=	16284190050000
or pin=	16291060120000
or pin=	16291150310000
or pin=	16291310240000
or pin=	16293230160000
or pin=	16294030320000
or pin=	16322100180000
or pin=	16323120490000
or pin=	16324130630000
or pin=	16324290240000
or pin=	16332000010000
or pin=	16332180090000
or pin=	16333110110000
or pin=	16333210140000
or pin=	19061080410000
or pin=	19061230070000
or pin=	19061250350000
or pin=	19063190240000
or pin=	19063200170000
or pin=	19063210270000
or pin=	19091310150000
or pin=	19284190100000
or pin=	19293000280000
or pin=	19293050260000
or pin=	19293130470000
or pin=	19294120040000
or pin=	19294120400000
or pin=	19303030350000
or pin=	19303040240000
or pin=	19304070420000
or pin=	19311150280000
or pin=	19312090270000
or pin=	19312120380000
or pin=	19312121290000
or pin=	19313020220000
or pin=	19313090160000
or pin=	19314060590000
or pin=	19314060750000
or pin=	19321280060000
or pin=	19322000050000
or pin=	19322000350000
or pin=	19322110180000
or pin=	19323000350000
or pin=	19323090060000
or pin=	19323140050000
or pin=	19324200540000
or pin=	19324210670000
or pin=	19324230070000
or pin=	19324230420000
or pin=	19331070300000
or pin=	19331090290000
or pin=	19331120120000
or pin=	19331140100000
or pin=	19332030080000
or pin=	19332100250000
or pin=	19333000270000
or pin=	19334010730000
or pin=	19334020130000
or pin=	16191020200000
or pin=	16191030130000
or pin=	16191050300000
or pin=	16191130030000
or pin=	16191200130000
or pin=	16191250220000
or pin=	16191260240000
or pin=	16191310180000
or pin=	16192000530000
or pin=	16192140140000
or pin=	16192210340000
or pin=	16192250100000
or pin=	16192250480000
or pin=	16192260320000
or pin=	16192300370000
or pin=	16193030200000
or pin=	16193070190000
or pin=	16193100250000
or pin=	16193190130000
or pin=	16193200350000
or pin=	16193210010000
or pin=	16193220070000
or pin=	16193290080000
or pin=	16194010190000
or pin=	16194030400000
or pin=	16194070050000
or pin=	16194090310000
or pin=	16194110370000
or pin=	16194180100000
or pin=	16194200070000
or pin=	16194270020000
or pin=	16194280090000
or pin=	16201170080000
or pin=	16203220050000
or pin=	16203220230000
or pin=	16291010200000
or pin=	16291010230000
or pin=	16291030260000
or pin=	16291240010000
or pin=	16291250130000
or pin=	16293080260000
or pin=	16293110150000
or pin=	16293110340000
or pin=	16301050040000
or pin=	16301140280000
or pin=	16303090390000
or pin=	16303120660000
or pin=	16303131140000
or pin=	16303140450000
or pin=	16304060230000
or pin=	16304070340000
or pin=	16311000250000
or pin=	16311100050000
or pin=	16311220260000
or pin=	16311320200000
or pin=	16311340140000
or pin=	16312020480000
or pin=	16312110210000
or pin=	16312220150000
or pin=	16312220250000
or pin=	16312220270000
or pin=	16312220370000
or pin=	16312240030000
or pin=	16312250140000
or pin=	16312260280000
or pin=	16313220130000
or pin=	16313220400000
or pin=	16313250230000
or pin=	16313300020000
or pin=	16314010220000
or pin=	16314120150000
or pin=	16314160250000
or pin=	16314220470000
or pin=	16314220680000
or pin=	16321130150000
or pin=	16321310620000
or pin=	16321320560000
or pin=	16323180440000
or pin=	16323250180000
or pin=	25303100220000
or pin=	25313590070000
or pin=	16201200320000
or pin=	16201300150000
or pin=	16202110110000
or pin=	16202190040000
or pin=	16203060320000
or pin=	16204150410000
or pin=	16211010010000
or pin=	16214000380000
or pin=	16214230340000
or pin=	16281160080000
or pin=	16282070160000
or pin=	16282190150000
or pin=	16283070030000
or pin=	16284320170000
or pin=	16292130040000
or pin=	16294030180000
or pin=	16321290100000
or pin=	16322070190000
or pin=	16324280600000
or pin=	16332020300000
or pin=	16332140090000
or pin=	16333070070000
or pin=	16333290170000
or pin=	19061130060000
or pin=	19062130040000
or pin=	19063040210000
or pin=	19063070260000
or pin=	19091070400000
or pin=	19091110370000
or pin=	19091180130000
or pin=	19091310210000
or pin=	19283000170000
or pin=	19283040160000
or pin=	19283180020000
or pin=	19293120250000
or pin=	19294020010000
or pin=	19294100030000
or pin=	19294100110000
or pin=	19301020120000
or pin=	19304060010000
or pin=	19304060470000
or pin=	19304130020000
or pin=	19311000240000
or pin=	19311000640000
or pin=	19311010620000
or pin=	19311080110000
or pin=	19311130450000
or pin=	19312170070000
or pin=	19313010340000
or pin=	19313170110000
or pin=	19321120370000
or pin=	19321180060000
or pin=	19322060140000
or pin=	19322240840000
or pin=	19323160040000
or pin=	19323180130000
or pin=	19323210180000
or pin=	19324060090000
or pin=	19331100080000
or pin=	19331140080000
or pin=	19332160110000
or pin=	19334000470000
or pin=	19334010460000)  bs=1.
*select if bs=0.


Compute N=1.
compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.

compute bsf02=0.
compute bsf03=0.
compute bsf04=0.
compute bsf05=0.
compute bsf06=0.
compute bsf07=0.
compute bsf08=0.
compute bsf09=0.
compute bsf10=0.
compute bsf11=0.
compute bsf12=0.
compute bsf34=0.
compute bsf78=0.
compute bsf95=0.
if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.

Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).

Compute winter1213=0.
if (mos > 9 and yr=12) or (mos <= 3 and yr=13) winter1213=1.
Compute winter1314=0.
if (mos > 9 and yr=13) or (mos <= 3 and yr=14) winter1314=1.
Compute winter1415=0.
if (mos > 9 and yr=14) or (mos <= 3 and yr=15) winter1415=1.
Compute winter1516=0.
if (mos > 9 and yr=15) or (mos <= 3 and yr=16) winter1516=1.
Compute summer12=0.
if (mos > 3 and yr=12) and (mos <= 9 and yr=12) summer12=1.
Compute summer13=0.
if (mos > 3 and yr=13) and (mos <= 9 and yr=13) summer13=1.
Compute summer14=0.
if (mos > 3 and yr=14) and (mos <= 9 and yr=14) summer14=1.
Compute summer15=0.
if (mos > 3 and yr=15) and (mos <= 9 and yr=15) summer15=1.
Compute summer16=0.
if (mos > 3 and yr=16) and (mos <= 9 and yr=16) summer16=1.
Compute jantmar12=0.
if (year1=2012 and (mos>=1 and mos<=3)) jantmar12=1. 
Compute octtdec16=0.
if (year1=2016 and (mos>=10 and mos<=12)) octtdec16=1.

Compute jantmar12cl234=jantmar12*cl234.
Compute winter1213cl234=winter1213*cl234.
Compute winter1314cl234=winter1314*cl234.
Compute winter1415cl234=winter1415*cl234.
Compute winter1516cl234=winter1516*cl234.
Compute summer12cl234=summer12*cl234.
Compute summer13cl234=summer13*cl234.
Compute summer14cl234=summer14*cl234.
Compute summer15cl234=summer15*cl234.
Compute summer16cl234=summer16*cl234.
Compute octtdec16cl234=octtdec16*cl234.

Compute jantmar12cl56=jantmar12*cl56.
Compute winter1213cl56=winter1213*cl56.
Compute winter1314cl56=winter1314*cl56.
Compute winter1415cl56=winter1415*cl56.
Compute winter1516cl56=winter1516*cl56.
Compute summer12cl56=summer12*cl56.
Compute summer13cl56=summer13*cl56.
Compute summer14cl56=summer14*cl56.
Compute summer15cl56=summer15*cl56.
Compute summer16cl56=summer16*cl56.
Compute octtdec16cl56=octtdec16*cl56.

Compute jantmar12cl778=jantmar12*cl778.
Compute winter1213cl778=winter1213*cl778.
Compute winter1314cl778=winter1314*cl778.
Compute winter1415cl778=winter1415*cl778.
Compute winter1516cl778=winter1516*cl778.
Compute summer12cl778=summer12*cl778.
Compute summer13cl778=summer13*cl778.
Compute summer14cl778=summer14*cl778.
Compute summer15cl778=summer15*cl778.
Compute summer16cl778=summer16*cl778.
Compute octtdec16cl778=octtdec16*cl778.


Compute jantmar12cl89=jantmar12*cl89.
Compute winter1213cl89=winter1213*cl89.
Compute winter1314cl89=winter1314*cl89.
Compute winter1415cl89=winter1415*cl89.
Compute winter1516cl89=winter1516*cl89.
Compute summer12cl89=summer12*cl89.
Compute summer13cl89=summer13*cl89.
Compute summer14cl89=summer14*cl89.
Compute summer15cl89=summer15*cl89.
Compute summer16cl89=summer16*cl89.
Compute octtdec16cl89=octtdec16*cl89.

Compute jantmar12cl1112=jantmar12*cl1112.
Compute winter1213cl1112=winter1213*cl1112.
Compute winter1314cl1112=winter1314*cl1112.
Compute winter1415cl1112=winter1415*cl1112.
Compute winter1516cl1112=winter1516*cl1112.
Compute summer12cl1112=summer12*cl1112.
Compute summer13cl1112=summer13*cl1112.
Compute summer14cl1112=summer14*cl1112.
Compute summer15cl1112=summer15*cl1112.
Compute summer16cl1112=summer16*cl1112.
Compute octtdec16cl1112=octtdec16*cl1112.

Compute jantmar12cl1095=jantmar12*cl1095.
Compute winter1213cl1095=winter1213*cl1095.
Compute winter1314cl1095=winter1314*cl1095.
Compute winter1415cl1095=winter1415*cl1095.
Compute winter1516cl1095=winter1516*cl1095.
Compute summer12cl1095=summer12*cl1095.
Compute summer13cl1095=summer13*cl1095.
Compute summer14cl1095=summer14*cl1095.
Compute summer15cl1095=summer15*cl1095.
Compute summer16cl1095=summer16*cl1095.
Compute octtdec16cl1095=octtdec16*cl1095.

Compute jantmar12clsplt=jantmar12*clsplt.
Compute winter1213clsplt=winter1213*clsplt.
Compute winter1314clsplt=winter1314*clsplt.
Compute winter1415clsplt=winter1415*clsplt.
Compute winter1516clsplt=winter1516*clsplt.
Compute summer12clsplt=summer12*clsplt.
Compute summer13clsplt=summer13*clsplt.
Compute summer14clsplt=summer14*clsplt.
Compute summer15clsplt=summer15*clsplt.
Compute summer16clsplt=summer16*clsplt.
Compute octtdec16clsplt=octtdec16*clsplt.

	
Compute midzoneberwyn=0.
if town=11 and (nghcde=40 or nghcde=60) midzoneberwyn=1. 
Compute highzoneberwyn=0.
if town=11 and (nghcde=10 or nghcde=20 or nghcde=30 
or nghcde=50 or nghcde=70)   highzoneberwyn=1.

Compute srfxmidblockberwyn=0.
if midzoneberwyn=1 srfxmidblockberwyn=srfx*midzoneberwyn.
Compute srfxhighblockberwyn=0.
if highzoneberwyn=1 srfxhighblockberwyn=srfx*highzoneberwyn.

Compute highzonecalumet=0.
if town=14 and (nghcde=10 or nghcde=20 or nghcde=30 
or nghcde=40)   highzonecalumet=1.

Compute srfxhighblockcalumet=0.
if highzonecalumet=1 srfxhighblockcalumet=srfx*highzonecalumet.

Compute highzonecicero=0.
if town=15 and (nghcde=15 or nghcde=20 or nghcde=25  
or nghcde=30 or nghcde=40 or nghcde=65 or nghcde=71 or nghcde=72
or nghcde=80 or nghcde=85 or nghcde=90 )   highzonecicero=1.

Compute srfxhighblockcicero=0.
if highzonecicero=1 srfxhighblockcicero=srfx*highzonecicero.

	
Compute midzonestickney=0.
if town=36 and (nghcde=36 or nghcde=41 or nghcde=50 
or nghcde=60)  midzonestickney=1. 
Compute highzonestickney=0.
if town=36 and (nghcde=12 or nghcde=21 or nghcde=22)  highzonestickney=1.

Compute srfxmidblockstickney=0.
if midzonestickney=1 srfxmidblockstickney=srfx*midzonestickney.
Compute srfxhighblockstickney=0.
if highzonestickney=1 srfxhighblockstickney=srfx*highzonestickney.

*Neighborhood change in Berwyn - 
 lower part of nbhd 40 (south of 35th st
 to Ogden Ave on the south and Harlem on the west)
 becomes part of nbhd 60 .

Compute bern10=0.
If town=11 and nghcde=10 bern10=1.
Compute bern20=0.
If town=11 and nghcde=20 bern20=1.
Compute bern30=0.
If town=11 and nghcde=30 bern30=1.
Compute bern40=0.
If town=11 and nghcde=40 bern40=1.
Compute bern50=0.
If town=11 and nghcde=50 bern50=1.
Compute bern60=0.
If town=11 and nghcde=60 bern60=1.
Compute bern70=0.
If town=11 and nghcde=70 bern70=1.
Compute caln10=0.
If town=14 and nghcde=10 caln10=1.
Compute caln20=0.
If town=14 and nghcde=20 caln20=1.
Compute caln30=0.
If town=14 and nghcde=30 caln30=1.
Compute caln40=0.
If town=14 and nghcde=40 caln40=1.
Compute cicn10=0.
If town=15 and nghcde=10 cicn10=1.
Compute cicn15=0.
If town=15 and nghcde=15 cicn15=1.
Compute cicn20=0.
If town=15 and nghcde=20 cicn20=1.
Compute cicn25=0.
If town=15 and nghcde=25 cicn25=1.
Compute cicn30=0.
If town=15 and nghcde=30 cicn30=1.
Compute cicn40=0.
If town=15 and nghcde=40 cicn40=1.
Compute cicn50=0.
If town=15 and nghcde=50 cicn50=1.
Compute cicn60=0.
If town=15 and nghcde=60 cicn60=1.
Compute cicn65=0.
If town=15 and nghcde=65 cicn65=1.
Compute cicn71=0.
If town=15 and nghcde=71 cicn71=1.
Compute cicn72=0.
If town=15 and nghcde=72 cicn72=1.
Compute cicn80=0.
If town=15 and nghcde=80 cicn80=1.
Compute cicn85=0.
If town=15 and nghcde=85 cicn85=1.
Compute cicn90=0.
If town=15 and nghcde=90 cicn90=1.
Compute stin12=0.
If town=36 and nghcde=12 stin12=1.
Compute stin21=0.
If town=36 and nghcde=21 stin21=1.
Compute stin22=0.
If town=36 and nghcde=22 stin22=1.
Compute stin36=0.
If town=36 and nghcde=36 stin36=1.
Compute stin41=0.
If town=36 and nghcde=41 stin41=1.
Compute stin50=0.
If town=36 and nghcde=50 stin50=1.
Compute stin60=0.
If town=36 and nghcde=60 stin60=1.

If town=11 and nghcde=10 N=2.21.
If town=11 and nghcde=20 N=1.89.
If town=11 and nghcde=30 N=1.85.
If town=11 and nghcde=40 N=2.09.
If town=11 and nghcde=50 N=1.86.
If town=11 and nghcde=60 N=2.68.
If town=11 and nghcde=70 N=1.77.
If town=14 and nghcde=10 N=1.45.
If town=14 and nghcde=20 N=1.64.
If town=14 and nghcde=30 N=1.58.
If town=14 and nghcde=40 N=1.54.
If town=15 and nghcde=15 N=1.65.
If town=15 and nghcde=20 N=1.56.		
If town=15 and nghcde=25 N=1.70.
If town=15 and nghcde=30 N=1.63.
If town=15 and nghcde=40 N=1.85.
If town=15 and nghcde=65 N=1.67.
If town=15 and nghcde=71 N=1.70.
If town=15 and nghcde=72 N=1.57.
If town=15 and nghcde=80 N=1.64.
If town=15 and nghcde=85 N=1.51.
If town=15 and nghcde=90 N=1.69.
If town=36 and nghcde=12 N=1.61.
If town=36 and nghcde=21 N=1.98.
If town=36 and nghcde=22 N=1.85.
If town=36 and nghcde=36 N=2.35.
If town=36 and nghcde=41 N=1.99.
If town=36 and nghcde=50 N=1.87.
If town=36 and nghcde=60 N=1.69.

Compute T11=0.
If town=11 T11=1.
Compute T14=0.
If town=14 T14=1.
Compute T15=0.
If town=15 T15=1.
Compute T36=0.
If town=36 T36=1.

*Cutpoints for land square foot are 1.75 * median lsf.
Compute lsf=sqftl.
If town=11 and lsf > 7140 lsf= 7140 + ((lsf - 7140)/3).
If town=14 and lsf > 8202 lsf= 8202 + ((lsf - 8202)/3).
If town=15 and lsf > 6615 lsf= 6615 + ((lsf - 6615)/3).
If town=36 and lsf > 11637 lsf= 11637 + ((lsf - 11637)/3).
If (class=8 or class=9) lsf=sqftl.


Compute sbebsf=0.
If T11=1 sbebsf=T11*sqrt(bsf).
Compute scabsf=0.
If T14=1 scabsf=T14*sqrt(bsf).
Compute scibsf=0.
If T15=1 scibsf=T15*sqrt(bsf).
Compute sstbsf=0.
If T36=1 sstbsf=T36*sqrt(bsf).

Compute srbelsf=0.
If T11=1 srbelsf=T11*sqrt(lsf).
Compute srcalsf=0.
If T14=1 srcalsf=T14*sqrt(lsf).
Compute srcilsf=0.
If T15=1 srcilsf=T15*sqrt(lsf).
Compute srstlsf=0.
If T36=1 srstlsf=T36*sqrt(lsf).

Compute nsrbelsf=n*srbelsf.
Compute nsrcalsf=n*srcalsf.
Compute nsrcilsf=n*srcilsf.
Compute nsrstlsf=n*srstlsf.

Compute sbsfbe10=0.
Compute sbsfbe20=0.
Compute sbsfbe30=0.
Compute sbsfbe40=0.
Compute sbsfbe50=0.
Compute sbsfbe60=0.
Compute sbsfbe70=0.
Compute sbsfbe71=0.
If town=11 and nghcde=10 sbsfbe10=sqrt(bsf).
If town=11 and nghcde=20 sbsfbe20=sqrt(bsf).
If town=11 and nghcde=30 sbsfbe30=sqrt(bsf).
If town=11 and nghcde=40 sbsfbe40=sqrt(bsf).
If town=11 and nghcde=50 sbsfbe50=sqrt(bsf).
If town=11 and nghcde=60 sbsfbe60=sqrt(bsf).
If town=11 and nghcde=70 sbsfbe70=sqrt(bsf).
If town=11 and nghcde=71 sbsfbe71=sqrt(bsf).
Compute sbsfca10=0.
Compute sbsfca20=0.
Compute sbsfca30=0.
Compute sbsfca40=0.
If town=14 and nghcde=10 sbsfca10=sqrt(bsf).
If town=14 and nghcde=20 sbsfca20=sqrt(bsf).
If town=14 and nghcde=30 sbsfca30=sqrt(bsf).
If town=14 and nghcde=40 sbsfca40=sqrt(bsf).
Compute sbsfci10=0.
Compute sbsfci15=0.
Compute sbsfci20=0.
Compute sbsfci25=0.
Compute sbsfci30=0.
Compute sbsfci40=0.
Compute sbsfci50=0.
Compute sbsfci60=0.
Compute sbsfci65=0.
Compute sbsfci71=0.
Compute sbsfci72=0.
Compute sbsfci80=0.
Compute sbsfci85=0.
Compute sbsfci90=0.
If town=15 and nghcde=10 sbsfci10=sqrt(bsf).
If town=15 and nghcde=20 sbsfci20=sqrt(bsf).
If town=15 and nghcde=30 sbsfci30=sqrt(bsf).
If town=15 and nghcde=40 sbsfci40=sqrt(bsf).
If town=15 and nghcde=50 sbsfci50=sqrt(bsf).
If town=15 and nghcde=60 sbsfci60=sqrt(bsf).
If town=15 and nghcde=65 sbsfci65=sqrt(bsf).
If town=15 and nghcde=71 sbsfci71=sqrt(bsf).
If town=15 and nghcde=72 sbsfci72=sqrt(bsf).
If town=15 and nghcde=80 sbsfci80=sqrt(bsf).
If town=15 and nghcde=85 sbsfci85=sqrt(bsf).
If town=15 and nghcde=90 sbsfci90=sqrt(bsf).
Compute sbsfst12=0.
Compute sbsfst21=0.
Compute sbsfst22=0.
Compute sbsfst36=0.
Compute sbsfst41=0.
Compute sbsfst50=0.
Compute sbsfst60=0.
If town=36 and nghcde=12 sbsfst12=sqrt(bsf).
If town=36 and nghcde=21 sbsfst21=sqrt(bsf).
If town=36 and nghcde=22 sbsfst22=sqrt(bsf).
If town=36 and nghcde=36 sbsfst36=sqrt(bsf).
If town=36 and nghcde=41 sbsfst41=sqrt(bsf).
If town=36 and nghcde=50 sbsfst50=sqrt(bsf).
If town=36 and nghcde=60 sbsfst60=sqrt(bsf).

if num=6 bnum=0.
if num=0 bnum=0.
if num=1 bnum=2.
if num=2 bnum=3.
if num=3 bnum=4.
if num=4 bnum=5.
if num=5 bnum=6.

compute res=bnum - comm.
compute bnumb=bnum*bsf.
compute nres=n*res.


Compute nsrbsf=n*sqrt(bsf).
Compute nsrlsf=n*sqrt(lsf).
Compute nbsf=n*bsf.
Compute srage=sqrt(age).
Compute nsrage=n*srage.

Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.

Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute frabsf=frame*bsf.
Compute frmsbsf=framas*bsf.
Compute stubsf=stucco*bsf.
Compute masbsf=mason*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.

Compute bathsum=fullbath + 0.25*halfbath.
Compute nbathsum=n*bathsum.

Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.

If firepl>1 firepl = 1.
Compute nfirepl=n*firepl.

Compute garage1=0.
Compute garage2=0.
Compute garage3=0.
Compute garage4=0.
Compute garnogar=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1.
if gar=7 garnogar=1.  
Compute biggar=0.
if garage3=1 or garage4=1 biggar=1.

Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.

Compute nbasfull=n*basefull.
Compute nbaspart=n*basepart.
Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.

Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
Compute nnobase=n*nobase.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute nluxbsf=n*qualdlux*bsf.
Compute deluxbsf = qualdlux*bsf.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute repabove=0.
Compute repbelow=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.
Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.
Compute twosty=0.
If class07=1 or class08=1 or class78=1 twosty=1.

select if town=36.

compute mv = 8254.032	
+ 5475.907*nfirepl
- 43979.776*highzonecicero
+ 20930.691*nnobase
- 4009.026*nsrage
- 69834.043*highzonecalumet
- 338.940*sbsfst12
+ 700.197*sbsfst41
+ 3520.970*nbsf56
+ 4113.476*nbathsum
+ 1.623*nbsfair
+ 3401.445*nbsf778
+ 7.403*masbsf
+ 12731.640*garage2
+ 20010.977*biggar
+ 5.476*frmsbsf
- 868.283*sbsfca30
- 36402.817*midzonestickney
+ 91.773*nsrlsf
+ 543.632*sbsfst50
+ 975.392*sbsfst36
+ 3919.215*nbsf34
+ 3270.846*nbsf234
+ 2813.936*nbsf1095
+ 3.424*frastbsf
+ 3215.056*nbsf1112
- 28.624*sbsfbe40
- 54.238*sbsfbe10
+ 6011.868*garage1
- 3887.051*srfxhighblockberwyn
- 8409.526*highzonestickney
- 53.1105*srfxmidblockstickney
- 2189.585*srfxhighblockstickney
+ 1.8161*stubsf
+ 32688.103*nbaspart
+ 34006.618*nbasfull.


save outfile='C:\Users\daaaron\documents\mv36.sav'/keep town pin mv. 


