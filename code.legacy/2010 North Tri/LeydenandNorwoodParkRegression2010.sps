			*NORWOOD PARK AND LEYDEN REGRESSION 2010.

GET FILE='C:\PROGRAM FILES\SPSS\SPSSA\REGTS20&26REPSALES.SAV'.
select if (amount1>85000).
select if (amount1<550000).
set mxcells=100000.
select if (multi<1).
select if sqftb<6000.
Compute year1=0.
If  (amount1>0) year1=1900 + yr.
If  (yr=9 and amount1>0)  year1=2009.
If  (yr=8 and amount1>0)  year1=2008.
If  (yr=7 and amount1>0)  year1=2007.
If  (yr=6 and amount1>0)  year1=2006.   
If  (yr=5 and amount1>0)  year1=2005.
If  (yr=4 and amount1>0)  year1=2004.  
If  (yr=3 and amount1>0)  year1=2003. 
If  (yr=2 and amount1>0)  year1=2002.  
If  (yr=1 and amount1>0)  year1=2001.   
If  (yr=0 and amount1>0)  year1=2000.

select if (year1>2004).
Compute bs=0.
if age<10 and (AMOUNT1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.
if (pin= 	12042060490000
or pin=	12042070280000
or pin=	12042080510000
or pin=	12114090180000
or pin=	12123070550000
or pin=	12123071340000
or pin=	12131100070000
or pin=	12132050210000
or pin=	12133060150000
or pin=	12133110340000
or pin=	12134120210000
or pin=	12142130140000
or pin=	13181010300000
or pin=	13181020630000
or pin=	13181160140000
or pin=	13181160190000
or pin=	13183040120000
or pin=	13183050150000
or pin=	13184000410000
or pin=	13184040050000
or pin=	13184040060000
or pin=	12113020520000
or pin=	12113210080000
or pin=	12094220250000
or pin=	12094230540000
or pin=	12094230540000
or pin=	12151050160000
or pin=	12151090190000
or pin=	12151100180000
or pin=	12151150220000
or pin=	12151190250000
or pin=	12153160400000
or pin=	12153260020000
or pin=	12161130260000
or pin=	12163070290000
or pin=	12163070300000
or pin=	12163100260000
or pin=	12163140150000
or pin=	12164020010000
or pin=	12164020160000
or pin=	12164090220000
or pin=	12164150150000
or pin=	12213020270000
or pin=	12213170160000
or pin=	12213190440000
or pin=	12213241250000
or pin=	12213250520000
or pin=	12214310060000
or pin=	12214310360000
or pin=	12221030010000
or pin=	12223040260000
or pin=	12271160510000
or pin=	12271240490000
or pin=	12272100450000
or pin=	12272190310000
or pin=	12281030300000
or pin=	12281230470000
or pin=	12281300260000
or pin=	12282200520000
or pin=	12282210190000
or pin=	12011350210000
or pin=	12013010050000
or pin=	12013040300000
or pin=	12021030160000
or pin=	12021270240000
or pin=	12022290070000
or pin=	12022320040000
or pin=	12023000680000
or pin=	12024000570000
or pin=	12024050150000
or pin=	12024100350000
or pin=	12291130170000
or pin=	12292000210000
or pin=	12292000260000
or pin=	12293090100000
or pin=	12293100230000
or pin=	12293190250000
or pin=	12294002180000
or pin=	12294060090000
or pin=	12294070220000
or pin=	12294170290000
or pin=	12294240250000
or pin=	12302020070000
or pin=	12302200110000
or pin=	12314070240000
or pin=	12321010140000
or pin=	12321180340000
or pin=	12322010100000
or pin=	12322010520000
or pin=	12322020280000
or pin=	12322130240000
or pin=	12323220090000
or pin=	12331230020000
or pin=	12332120150000
or pin=	12332190050000
or pin=	12283050300000
or pin=	12284040050000
or pin=	12284250130000
or pin=	12361070330000
or pin=	12361070420000
or pin=	12363270320000
or pin=	12251150090000
or pin=	12251150150000
or pin=	12251170240000
or pin=	12251210300000
or pin=	12251290090000
or pin=	12252250350000
or pin=	12252260100000
or pin=	12252270130000
or pin=	12253040320000
or pin=	12253200200000
or pin=	12253210140000
or pin=	12253210270000
or pin=	12253280050000
or pin=	12254020420000
or pin=	12254090300000
or pin=	12254130360000
or pin=	12254150410000
or pin=	12254200140000
or pin=	12254250090000
or pin=	12254320090000
or pin=	12361010360000
or pin=	12361020420000
or pin=	12361040040000
or pin=	12361040050000
or pin=	12361040330000
or pin=	12362020010000
or pin=	12362050030000
or pin=	12362050260000
or pin=	12362080080000
or pin=	12362090340000
or pin=	12362100380000
or pin=	12362120190000
or pin=	12362130280000
or pin=	12362180030000
or pin=	12362200130000
or pin=	12362270230000
or pin=	12362270860000
or pin=	12362290380000
or pin=	12364000210000
or pin=	12364050140000
or pin=	12364060440000
or pin=	12364290120000
or pin=	12263180580000
or pin=	12263280210000
or pin=	12274040060000
or pin=	12274210450000
or pin=	12274280270000
or pin=	12342050290000) bs=1.
select if bs=0.

compute t26=0.
if town=26 t26=1.
compute t20=0.                                                                             
if town=20 t20=1.

compute bsf=sqftb.
Compute N=1.


*******************************************************************************************************************.
***** Compute Block-Level Filings.
* FX includes the own property when counting "block-level filings".
*********** We have combined the filings so that we are now using 2006, 2007 , 2008 and 2009 block filings.

COMPUTE FX = filings06 + filings07 + filings08 + filings09.
COMPUTE SRFX = sqrt(fx).
RECODE FX (SYSMIS=0).
*******************************************************************************************************************.

select if puremarket=1 .

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.

compute bsf=sqftb.

if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.

compute tnb=(town*1000)+nghcde.

compute N20010=0.
compute N20011=0.
compute N20020=0.
compute N20030=0.
compute N20040=0.
compute N20050=0.
compute N20060=0.
compute N20070=0.
compute N20080=0.
compute N20081=0.
compute N20090=0.
compute N20091=0.

IF TNB=20010 N20010=1.
IF TNB=20011 N20011=1.
IF TNB=20020 N20020=1.
IF TNB=20030 N20030=1.
IF TNB=20040 N20040=1.
IF TNB=20050 N20050=1.
IF TNB=20060 N20060=1.
IF TNB=20070 N20070=1.
IF TNB=20080 N20080=1.
IF TNB=20081 N20081=1.
IF TNB=20090 N20090=1.
IF TNB=20091 N20091=1.

compute SB20010=0.
compute SB20011=0.
compute SB20020=0.
compute SB20030=0.
compute SB20040=0.
compute SB20050=0.
compute SB20060=0.
compute SB20070=0.
compute SB20080=0.
compute SB20081=0.
compute SB20090=0.
compute SB20091=0.

IF TNB=20010 SB20010=SQRT(BSF).
IF TNB=20011 SB20011=SQRT(BSF).
IF TNB=20020 SB20020=SQRT(BSF).
IF TNB=20030 SB20030=SQRT(BSF).
IF TNB=20040 SB20040=SQRT(BSF).
IF TNB=20050 SB20050=SQRT(BSF).
IF TNB=20060 SB20060=SQRT(BSF).
IF TNB=20070 SB20070=SQRT(BSF).
IF TNB=20080 SB20080=SQRT(BSF).
IF TNB=20081 SB20081=SQRT(BSF).
IF TNB=20090 SB20090=SQRT(BSF).
IF TNB=20091 SB20091=SQRT(BSF).

compute n26010=0.
compute n26030=0.
compute n26040=0.
compute n26050=0.

IF TNB=26010 N26010=1.
IF TNB=26030 N26030=1.
IF TNB=26040 N26040=1.
IF TNB=26050 N26050=1.

compute SB26010=0.
compute SB26030=0.
compute SB26040=0.
compute SB26050=0.

IF TNB=26010 SB26010=SQRT(BSF).
IF TNB=26030 SB26030=SQRT(BSF).
IF TNB=26040 SB26040=SQRT(BSF).
IF TNB=26050 SB26050=SQRT(BSF).
  
compute n=1.
if tnb=20010 n=4.15.
if tnb=20011 n=2.81.
If tnb=20020 n=1.94.
if tnb=20030 n=1.79.
If tnb=20040 n=3.49.
if tnb=20050 n=1.57.
If tnb=20060 n=1.80.
If tnb=20070 n=2.80.
If tnb=20080 n=2.24.
if tnb=20081 n=1.92.
If tnb=20090 n=2.05.
If tnb=20091 n=2.85.
If tnb=26010 n=2.59.
If tnb=26030 n=3.16.
if tnb=26040 n=3.25.
If tnb=26050 n=3.30.


*Compute lowzonenorwood=0.
*if town=26 and  (nghcde= or nghcde=  or nghcde=   or nghcde= or nghcde=
or nghcde= or nghcde= or nghcde= or nghcde= or nghcde= 
or nghcde= or nghcde= or nghcde= or nghcde= or nghcde= 
or nghcde= or nghcde=)  lowzonenorwood=1.                         	

*Compute midzonenorwood=0.
*if town=26 and (nghcde= or nghcde= or nghcde= or nghcde= or nghcde= or nghcde=
or nghcde= or nghcde= or nghcde= or nghcde= or nghcde= or nghcde= or nghcde=
or nghcde= or nghcde= or nghcde= or nghcde= or nghcde= or nghcde= or nghcde=
or nghcde=)   midzonenorwood=1. 

*Compute highzonenorwood=0.
*if town=26 and (nghcde= or nghcde= or nghcde= or nghcde= or nghcde= 
or nghcde= or nghcde= or nghcde= or nghcde= or nghcde=
or nghcde= or nghcde=)   highzonenorwood=1.


*Compute srfxlowblocknorwood=0.
*if lowzonenorwood=1 srfxlowblocknorwood=srfx*lowzonenorwood.

*Compute srfxmidblocknorwood=0.
*if midzonenorwood=1 srfxmidblocknorwood=srfx*midzonenorwood.

*Compute srfxhighblocknorwood=0.
*if highzonenorwood=1 srfxhighblocknorwood=srfx*highzonenorwood.

*Compute lowzoneleyden=0.
*if town=20 and  (nghcde=10 or nghcde=11 or nghcde=40  or nghcde=70)  lowzoneleyden=1.                         	

*Compute midzoneleyden=0.
*if town=20 and (nghcde=20 or nghcde=30 or nghcde=80 or nghcde=81 or nghcde=91) midzoneleyden=1. 

*Compute highzoneleyden=0.
*if town=20 and (nghcde=50 or nghcde=60 or nghcde=90) highzoneleyden=1.

*Compute srfxlowblockleyden=0.
*if lowzoneleyden=1 srfxlowblockleyden=srfx*lowzoneleyden.

*Compute srfxmidblockleyden=0.
*if midzoneleyden=1 srfxmidblockleyden=srfx*midzoneleyden.

*Compute srfxhighblockleyden=0.
*if highzoneleyden=1 srfxhighblockleyden=srfx*highzoneleyden.


Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).

Compute winter0506=0.
if (mos > 9 and yr=5) or (mos <= 3 and yr=6) winter0506=1.
Compute winter0607=0.
if (mos > 9 and yr=6) or (mos <= 3 and yr=7) winter0607=1.
Compute winter0708=0.
if (mos > 9 and yr=7) or (mos <= 3 and yr=8) winter0708=1.
Compute winter0809=0.
if (mos > 9 and yr=8) or (mos <= 3 and yr=9) winter0809=1.
Compute summer05=0.
if (mos > 3 and yr=5) and (mos <= 9 and yr=5) summer05=1.
Compute summer06=0.
if (mos > 3 and yr=6) and (mos <= 9 and yr=6) summer06=1. 
Compute summer07=0.
if (mos > 3 and yr=7) and (mos <= 9 and yr=7) summer07=1.
Compute summer08=0.
if (mos > 3 and yr=8) and (mos <= 9 and yr=8) summer08=1.
Compute summer09=0.
if (mos > 3 and yr=9) and (mos <= 9 and yr=9) summer09=1.
Compute jantmar05=0.
if (year1=2005 and (mos>=1 and mos<=3)) jantmar05=1. 
Compute octtdec09=0.
if (year1=2009 and (mos>=10 and mos<=12)) octtdec09=1.

Compute jantmar05cl234=jantmar05*cl234.
Compute winter0506cl234=winter0506*cl234.
Compute winter0607cl234=winter0607*cl234.
Compute winter0708cl234=winter0708*cl234.
Compute winter0809cl234=winter0809*cl234.
Compute summer05cl234=summer05*cl234.
Compute summer06cl234=summer06*cl234.
Compute summer07cl234=summer07*cl234.
Compute summer08cl234=summer08*cl234.
Compute summer09cl234=summer09*cl234.
Compute octtdec09cl234=octtdec09*cl234.

Compute jantmar05cl56=jantmar05*cl56.
Compute winter0506cl56=winter0506*cl56.
Compute winter0607cl56=winter0607*cl56.
Compute winter0708cl56=winter0708*cl56.
Compute winter0809cl56=winter0809*cl56.
Compute summer05cl56=summer05*cl56.
Compute summer06cl56=summer06*cl56.
Compute summer07cl56=summer07*cl56.
Compute summer08cl56=summer08*cl56.
Compute summer09cl56=summer09*cl56.
Compute octtdec09cl56=octtdec09*cl56.

Compute jantmar05cl778=jantmar05*cl778.
Compute winter0506cl778=winter0506*cl778.
Compute winter0607cl778=winter0607*cl778.
Compute winter0708cl778=winter0708*cl778.
Compute winter0809cl778=winter0809*cl778.
Compute summer05cl778=summer05*cl778.
Compute summer06cl778=summer06*cl778.
Compute summer07cl778=summer07*cl778.
Compute summer08cl778=summer08*cl778.
Compute summer09cl778=summer09*cl778.
Compute octtdec09cl778=octtdec09*cl778.

Compute jantmar05cl89=jantmar05*cl89.
Compute winter0506cl89=winter0506*cl89.
Compute winter0607cl89=winter0607*cl89.
Compute winter0708cl89=winter0708*cl89.
Compute winter0809cl89=winter0809*cl89.
Compute summer05cl89=summer05*cl89.
Compute summer06cl89=summer06*cl89.
Compute summer07cl89=summer07*cl89.
Compute summer08cl89=summer08*cl89.
Compute summer09cl89=summer09*cl89.
Compute octtdec09cl89=octtdec09*cl89.

Compute jantmar05cl1112=jantmar05*cl1112. 
Compute winter0506cl1112=winter0506*cl1112.
Compute winter0607cl1112=winter0607*cl1112.
Compute winter0708cl1112=winter0708*cl1112.
Compute winter0809cl1112=winter0809*cl1112.
Compute summer05cl1112=summer05*cl1112.
Compute summer06cl1112=summer06*cl1112.
Compute summer07cl1112=summer07*cl1112.
Compute summer08cl1112=summer08*cl1112.
Compute summer09cl1112=summer09*cl1112.
Compute octtdec09cl1112=octtdec09*cl1112.

Compute jantmar05cl1095=jantmar05*cl1095.
Compute winter0506cl1095=winter0506*cl1095.
Compute winter0607cl1095=winter0607*cl1095.
Compute winter0708cl1095=winter0708*cl1095.
Compute winter0809cl1095=winter0809*cl1095.
Compute summer05cl1095=summer05*cl1095.
Compute summer06cl1095=summer06*cl1095.
Compute summer07cl1095=summer07*cl1095.
Compute summer08cl1095=summer08*cl1095.
Compute summer09cl1095=summer09*cl1095.
Compute octtdec09cl1095=octtdec09*cl1095.

Compute jantmar05clsplt=jantmar05*clsplt.
Compute winter0506clsplt=winter0506*clsplt.
Compute winter0607clsplt=winter0607*clsplt.
Compute winter0708clsplt=winter0708*clsplt.
Compute winter0809clsplt=winter0809*clsplt.
Compute summer05clsplt=summer05*clsplt.
Compute summer06clsplt=summer06*clsplt.
Compute summer07clsplt=summer07*clsplt.
Compute summer08clsplt=summer08*clsplt.
Compute summer09clsplt=summer09*clsplt.
Compute octtdec09clsplt=octtdec09*clsplt.

Compute lsf=sqftl.
if lsf > 6750 lsf = 6750  + ((lsf - 6750)/4).

if class=95 and tnb=26010  lsf=1063.
if class=95 and tnb=20020 	lsf=1875.
if class=95 and tnb=20040 	lsf=2712.

Compute nsrlsf=n*sqrt(lsf).
compute srbsf=sqrt(bsf).
Compute nsrbsf=n*sqrt(bsf).
Compute nbsf=n*bsf.
Compute srage=sqrt(age).
Compute nsrage=n*srage.
Compute bsfage=bsf*age.


Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute frabsf=frame*bsf.
compute masbsf=mason*bsf.
Compute stubsf=stucco*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.
Compute frms=0.
If extcon=1 or extcon=2 frms=1.
Compute frmsbsf=frms*bsf.
Compute bathsum=fullbath + 0.25*halfbath.
compute nbathsum=n*bathsum.

Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.
compute prembsf=premrf*bsf.
compute nprembsf=n*prembsf.

Compute garnogar=0.
Compute garage=0.
If gar=1 gar1car=1.
If gar=2 gar1hcar=1.
If gar=3 gar2car=1.
If gar=4 gar2hcar=1.
If gar=5 gar3car=1.
If gar=6 gar3hcar=1.
If gar=7 garnogar=1.
If gar=8 gar4car=1. 
If gar ne 7 garage=1.

Compute garage1=0.
Compute garage2=0.
Compute garage3=0.
Compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1.  
Compute biggar=0.
if garage3=1 or garage4=1 biggar=1.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.


Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
Compute basement=0.
If basment=1 or basment=3 basement=1.
Compute nbasfull=n*basefull.
Compute nbaspart=n*basepart.
Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.
Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
compute nnobase=n*nobase.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
compute deluxbsf=qualdlux*bsf.

if num=6 bnum=0.
if num=0 bnum=0.
if num=1 bnum=2.
if num=2 bnum=3.
if num=3 bnum=4.
if num=4 bnum=5.
if num=5 bnum=6.

Compute nbnum=n*bnum.
Compute ncomm=n*comm.
Compute totunit=bnum + comm.
Compute totunitb=totunit*sqrt(bsf).


If firepl>0 firepl=1.
compute nfirepl=n*firepl.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute repabove=0.
Compute repbelow=0.
Compute repave=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.
Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.

Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.

compute b=1.
*select if (town=26 and year1>=2009) or (town=20 and year1>=2009).
*select if puremarket=1. 
*Table observation = b
                amount1
               /table = tnb by 
                           amount1 
                             + b      
	/title = 'Ave & Med Sales'
     	/statistics =  median (amount1 'MED SP')
            mean (amount1 'MEAN SP')
            validn (b '# PROPS').

reg des=defaults cov
	             /var=amount1 nsrlsf nbsf nbsf234 nbsf56 nbsf778 nbsf89 nbsf1112 
			nbsf1095 nbsf34 nbasfull nbaspart nfirepl nbsfair 
			nsiteben nluxbsf nrepabsf garage1 garage2 biggar nbathsum nprembsf
		       nsrage masbsf frabsf stubsf frmsbsf fx sb20010 sb20011 sb20020 
			sb20030 sb20040 sb20050 sb20060 sb20070 sb20080 sb20081 sb20090 sb20091 
	    		sb26010 sb26030 sb26040 sb26050 jantmar05cl234 winter0506cl234 winter0607cl234
			winter0708cl234 winter0809cl234 summer05cl234 summer06cl234 summer07cl234 
			summer08cl234 summer09cl234 octtdec09cl234 jantmar05cl56 winter0506cl56 
			winter0607cl56 winter0708cl56 winter0809cl56 summer05cl56 summer06cl56 
			summer07cl56 summer08cl56 	summer09cl56 octtdec09cl56 jantmar05cl778 
			winter0506cl778 winter0607cl778 winter0708cl778 winter0809cl778 summer05cl778
			summer06cl778 summer07cl778 summer08cl778 summer09cl778 octtdec09cl778 
			jantmar05cl89 winter0506cl89 winter0607cl89 winter0708cl89 winter0809cl89 
			summer05cl89 summer06cl89 	summer07cl89 summer08cl89 summer09cl89 octtdec09cl89
			jantmar05cl1112 winter0506cl1112 winter0607cl1112 winter0708cl1112 winter0809cl1112
			summer05cl1112 summer06cl1112 summer07cl1112 summer08cl1112
			summer09cl1112 octtdec09cl1112 jantmar05cl1095 winter0506cl1095
			winter0607cl1095 winter0708cl1095 winter0809cl1095 summer05cl1095
			summer06cl1095 summer07cl1095 summer08cl1095 summer09cl1095
			octtdec09cl1095 jantmar05clsplt winter0506clsplt winter0607clsplt
			winter0708clsplt winter0809clsplt summer05clsplt summer06clsplt
			summer07clsplt summer08clsplt summer09clsplt octtdec09clsplt
			/dep=amount1	
                     /method=stepwise
			/method=enter jantmar05cl234 winter0506cl234 winter0607cl234 winter0708cl234 
			winter0809cl234 summer05cl234 summer06cl234 summer07cl234 summer08cl234 summer09cl234
			octtdec09cl234 jantmar05cl56 winter0506cl56 winter0607cl56 winter0708cl56
			winter0809cl56 summer05cl56 summer06cl56 summer07cl56 summer08cl56
			summer09cl56 octtdec09cl56 jantmar05cl778 winter0506cl778 winter0607cl778
			winter0708cl778 winter0809cl778 summer05cl778 summer06cl778 summer07cl778
    			summer08cl778 summer09cl778 octtdec09cl778 jantmar05cl89 winter0506cl89
			winter0607cl89 winter0708cl89 winter0809cl89 summer05cl89 summer06cl89		
			summer07cl89 summer08cl89 summer09cl89 octtdec09cl89 jantmar05cl1112
			winter0506cl1112 winter0607cl1112 winter0708cl1112 winter0809cl1112
			summer05cl1112 summer06cl1112 summer07cl1112 summer08cl1112
			summer09cl1112 octtdec09cl1112 jantmar05cl1095 winter0506cl1095
			winter0607cl1095 winter0708cl1095 winter0809cl1095 summer05cl1095
			summer06cl1095 summer07cl1095 summer08cl1095 summer09cl1095
			octtdec09cl1095 jantmar05clsplt winter0506clsplt winter0607clsplt
			winter0708clsplt winter0809clsplt summer05clsplt summer06clsplt
			summer07clsplt summer08clsplt summer09clsplt octtdec09clsplt
	             /method=enter fx garage1 biggar nsiteben nfirepl stubsf frmsbsf nbsfair
			nbsf56 nbsf778 sb20060
		      /save pred (pred) resid (resid).
                    sort cases by nghcde pin.	
                    value labels extcon 1 'Frame' 2 'Mason' 3 'Framas' 4 'Stucco'
                    /basment 1 'Full' 2 'Slab' 3 'Part' 4 'Crwl'
                   /bsfn 1 'Rec' 2 'Apt' 3 'Unf'
                   /gar 1 '1' 2 '1.5' 3 '2' 4 '2.5' 5 '3' 6 '3.5' 7 'non'
                   /class 2 '2-02' 3 '2-03' 4 '2-04' 5 '2-05' 6 '2-06' 7 '2-07' 8 '2-08' 9 '2-09' 10 '2-10' 11'2-11' 12 '2-12' 34 '2-34' 78 '2-78' 95 '2-95'.
*compute perdif=(resid)/(amount1)*100.
*formats pred(COMMA9.0)
          /resid (f6.0).

*plot
/vertical='Sales Price' MIN(50000) MAX(1000000)
/horizontal='RESID'
/vsize=25
/hsize=140
/plot=amount with RESID.
*compute badsal=0.
*if perdif>50 badsal=1.
*if perdif<-50 badsal=1.
*select if badsal=1.
set wid=125.
set len=59.
*REPORT FORMAT=automatic list(1)
/title='Office of the Assessor'
        'Residential Regression Report'
        'Town is Norwood Park and Leyden'
        /ltitle 'Report Ran On)Date' 
       /rtitle='PAGE)PAGE'
       /string=prop(area(2) '-' subarea(2) '-' block(3) '-' parcel(3))
       date(mos(2) '-'year1(4))
       /var=prop 'Prop' 'Index Number'(13)
       class 'Class' (label) (5)
       tnb 'tnb'(5)
       amount1 'Price'(7)
       pred 'Predicted'(9)
       resid 'Resid'(7)
       perdif '% DIF' (5)
       sqftb 'Sqftb'(5)
       age 'Age'(4)
       extcon 'Extcon' (label)(6)
       sqftl 'Land'(6).
	
















