                           	*SPSS Regression.
	   		     *EVANSTON & NEW TRIER 2010.

Get file='C:\Program Files\SPSS\SPSSA\regts17&23repsales.sav'.
select if (amount1>140000).
select if (amount1<9700000).
select if (multi<1).
select if sqftb<12000.

compute bsf=sqftb.
compute lsf=sqftl.

Compute year1=0.
If  (amount1>0) year1=1900 + yr. 
If  (yr=9 and amount1>0)  year1=2009.
If  (yr=8 and amount1>0)  year1=2008.
If  (yr=7 and amount1>0)  year1=2007.
If  (yr=6 and amount1>0)  year1=2006. 
If  (yr=5 and amount1>0)  year1=2005. 
If  (yr=4 and amount1>0)  year1=2004. 
If  (yr=3 and amount1>0)  year1=2003. 
If  (yr=2 and amount1>0)  year1=2002.  
If  (yr=1 and amount1>0)  year1=2001.   
If  (yr=0 and amount1>0)  year1=2000.

select if (year1>2004).
set mxcells=2000500.

Compute bs=0.
if age<10 and (AMOUNT1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.
if (pin=	5333100290000
or pin=	5333100450000
or pin=	10102000320000
or pin=	10111000610000
or pin=	10113030140000
or pin=	10114110350000
or pin=	10114120330000
or pin=	10114140240000
or pin=	10122050050000
or pin=	10123020150000
or pin=	10124090210000
or pin=	10142040570000
or pin=	10113130100000
or pin=	10113230210000
or pin=	10114010100000
or pin=	11192170210000
or pin=	11192060090000
or pin=	11201020420000
or pin=	10124150110000
or pin=	10124150110000
or pin=	10124200040000
or pin=	10131080100000
or pin=	10131080380000
or pin=	10131140540000
or pin=	10131160260000
or pin=	10131160300000
or pin=	10131210080000
or pin=	10132040150000
or pin=	10132070050000
or pin=	10132090110000
or pin=	10132140180000
or pin=	10132140400000
or pin=	11194050130000
or pin=	11194130920000
or pin=	11191170090000
or pin=	11193000110000
or pin=	11193080020000
or pin=	11193090110000
or pin=	11194060050000
or pin=	11194060140000
or pin=	10132180070000
or pin=	10134040020000
or pin=	10134170010000
or pin=	10242070050000
or pin=	11181220070000
or pin=	11183220160000
or pin=	10244030220000
or pin=	10244080260000
or pin=	10244170140000
or pin=	10244200140000
or pin=	10252130190000
or pin=	10252160010000
or pin=	10252190020000
or pin=	10252220090000
or pin=	11301060140000
or pin=	11301140120000
or pin=	11301140130000
or pin=	11301200220000
or pin=	11301210130000
or pin=	11302060110000
or pin=	11302060140000
or pin=	5334280030000
or pin=	5353130160000
or pin=	5353180040000
or pin=	10133170170000
or pin=	10133190230000
or pin=	10134140320000
or pin=	10134270090000
or pin=	10241060250000
or pin=	10243080170000
or pin=	10243130450000
or pin=	10251070260000
or pin=	10111050070000
or pin=	10111050080000
or pin=	10112000320000
or pin=	10242150230000
or pin=	10242180140000
or pin=	10242220080000
or pin=	4014010330000
or pin=	4014010390000
or pin=	4014010390000
or pin=	4014100060000
or pin=	4014100220000
or pin=	4122040080000
or pin=	5063010100000
or pin=	5072170140000
or pin=	5083060100000
or pin=	5083080030000
or pin=	5083190020000
or pin=	5171020160000
or pin=	5171030200000
or pin=	5172020060000
or pin=	5172020190000
or pin=	5161010250000
or pin=	5171070180000
or pin=	5171070210000
or pin=	5171070550000
or pin=	5171070630000
or pin=	5171070630000
or pin=	5171130060000
or pin=	5171190190000
or pin=	5173100040000
or pin=	5173150080000
or pin=	5174060220000
or pin=	5211010260000
or pin=	5211010310000
or pin=	5211060020000
or pin=	5212010120000
or pin=	5213020020000
or pin=	5214100070000
or pin=	5282000060000
or pin=	5282020140000
or pin=	5074070140000
or pin=	5171000080000
or pin=	5182020220000
or pin=	5182050210000
or pin=	5182070070000
or pin=	5182070080000
or pin=	5182070110000
or pin=	5182140040000
or pin=	5182190040000
or pin=	5182200250000
or pin=	5071090260000
or pin=	5071100210000
or pin=	5071140080000
or pin=	5073000100000
or pin=	5073010110000
or pin=	5073040250000
or pin=	5201070010000
or pin=	5201170090000
or pin=	5201180100000
or pin=	5201180150000
or pin=	5201220110000
or pin=	5201220130000
or pin=	5203080600000
or pin=	5184020340000
or pin=	5184030330000
or pin=	5184030760000
or pin=	5184030760000
or pin=	5173120650000
or pin=	5173120680000
or pin=	5173130050000
or pin=	5203180430000
or pin=	5204000210000
or pin=	5204000610000
or pin=	5213210150000
or pin=	5213210280000
or pin=	5213210390000
or pin=	5281060260000
or pin=	5281060470000
or pin=	5291020960000
or pin=	5292090020000
or pin=	5202260080000
or pin=	5204010090000
or pin=	5204050140000
or pin=	5213220370000
or pin=	5281010020000
or pin=	5281030890000
or pin=	5282170180000
or pin=	5283070070000
or pin=	5283070710000
or pin=	5283140010000
or pin=	5283150200000
or pin=	5283160040000
or pin=	5284250300000
or pin=	5294000130000
or pin=	5294030340000
or pin=	5331000660000
or pin=	5331060140000
or pin=	5332020290000
or pin=	5332150070000
or pin=	5341140100000
or pin=	5343050050000
or pin=	5342070060000
or pin=	5342090020000
or pin=	5342140090000
or pin=	5342180130000
or pin=	5351170310000
or pin=	5351200100000
or pin=	5351200290000
or pin=	5312090230000
or pin=	5321090110000
or pin=	5323000020000
or pin=	5323051360000
or pin=	5271130220000
or pin=	5271130230000
or pin=	5271130330000
or pin=	5273000430000
or pin=	5282060240000
or pin=	5193110240000
or pin=	5303030100000
or pin=	5312220270000
or pin=	5313030320000
or pin=	5313090170000
or pin=	5294240140000
or pin=	5302010160000
or pin=	5302010510000
or pin=	5302020820000
or pin=	5273090140000
or pin=	5274030070000
or pin=	5274080050000
or pin=	5274090110000
or pin=	5274130170000
or pin=	5321010050000
or pin=	5321060160000
or pin=	5324020160000
or pin=	5324080290000
or pin=	5331140110000
or pin=	5062010210000
or pin=	5062010210000
or pin=	5064040130000
or pin=	5081040070000
or pin=	5083030110000
or pin=	5083030240000
or pin=	5083210200000
or pin=	5214120180000
or pin=	5351050080000
or pin=	5352010100000
or pin=	5274040010000) bs=1.
select if bs=0.


Compute N=1.

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.

if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.




compute tnb=(town*1000) + nghcde.

if (tnb= 17011 ) n=4.00.
if (tnb= 17013 ) n=5.02.
if (tnb= 17020 ) n=8.50.
if (tnb= 17041 ) n=4.65.
if (tnb= 17042 ) n=7.10.
if (tnb= 17043 ) n=7.50.
if (tnb= 17050 ) n=10.47.
if (tnb= 17060 ) n=3.00.
if (tnb= 17071 ) n=4.70.
if (tnb= 17080 ) n=4.30.
if (tnb= 17090 ) n=5.06.
if (tnb= 17110 ) n=3.20.
if (tnb= 17112 ) n=3.53.
if (tnb= 17120 ) n=5.15.
if (tnb= 17130 ) n=2.49. 
if (tnb= 17140 ) n=2.42. 
if (tnb= 17200 ) n=5.63.
if (tnb= 17210 ) n=4.12.
if (tnb= 23010 ) n=12.14.
if (tnb= 23011 ) n=5.98.
if (tnb= 23021 ) n=12.87.
if (tnb= 23022 ) n=10.26.
if (tnb= 23041 ) n=7.50.
if (tnb= 23042 ) n=8.05.
if (tnb= 23043 ) n=10.95.
if (tnb= 23044 ) n=8.42.
if (tnb= 23045 ) n=6.23.
if (tnb= 23050 ) n=11.00.
if (tnb= 23080 ) n=17.91.
if (tnb= 23091 ) n=8.68.
if (tnb= 23092 ) n=6.90.
if (tnb= 23093 ) n=6.01.
if (tnb= 23094 ) n=8.05.
if (tnb= 23100 ) n=3.82.
if (tnb= 23110 ) n=21.00.
if (tnb= 23120 ) n=3.73.
if (tnb= 23132 ) n=10.00.
if (tnb= 23141 ) n=9.72.
if (tnb= 23150 ) n=5.01.
if (tnb= 23170 ) n=16.83.
if (tnb= 23171 ) n=34.25.

*******************************************************************************************************************.
***** Compute Block-Level Filings.
* FX includes the own property when counting "block-level filings".
*********** We have combined the filings so that we are now using 2006, 2007, 2008 and 2009 block filings.

COMPUTE FX = filings06 + filings07 + filings08 + filings09.
COMPUTE SRFX = sqrt(fx).
RECODE FX (SYSMIS=0).
*******************************************************************************************************************.

select if puremarket=1.

compute sb17011=0.
compute sb17013=0.
compute sb17020=0.
compute sb17041=0.
compute sb17042=0.
compute sb17043=0.
compute sb17050=0.
compute sb17060=0.
compute sb17071=0.
compute sb17080=0.
compute sb17090=0.
compute sb17110=0.
compute sb17112=0.
compute sb17120=0.
compute sb17130=0.
compute sb17140=0. 
compute sb17200=0.
compute sb17210=0.
compute sb23010=0.
compute sb23011=0.
compute sb23021=0.
compute sb23022=0.
compute sb23041=0.
compute sb23042=0.
compute sb23043=0.
compute sb23044=0.
compute sb23045=0.
compute sb23050=0.
compute sb23080=0.
compute sb23091=0.
compute sb23092=0.
compute sb23093=0.
compute sb23094=0.
compute sb23100=0.
compute sb23110=0.
compute sb23120=0.
compute sb23132=0.
compute sb23141=0.
compute sb23150=0.
compute sb23170=0.
compute sb23171=0.

compute n17011=0.
compute n17013=0.
compute n17020=0.
compute n17041=0.
compute n17042=0.
compute n17043=0.
compute n17050=0.
compute n17060=0.
compute n17071=0.
compute n17080=0.
compute n17090=0.
compute n17110=0.
compute n17112=0.
compute n17120=0.
compute n17130=0.
compute n17140=0. 
compute n17200=0.
compute n17210=0.
compute n23010=0.
compute n23011=0.
compute n23021=0.
compute n23022=0.
compute n23041=0.
compute n23042=0.
compute n23043=0.
compute n23044=0.
compute n23045=0.
compute n23050=0.
compute n23080=0.
compute n23091=0.
compute n23092=0.
compute n23093=0.
compute n23094=0.
compute n23100=0.
compute n23110=0.
compute n23120=0.
compute n23132=0.
compute n23141=0.
compute n23150=0.
compute n23170=0.
compute n23171=0.


if (tnb= 17011 ) n17011=1.
if (tnb= 17013 ) n17013=1.
if (tnb= 17020 ) n17020=1.
if (tnb= 17041 ) n17041=1.
if (tnb= 17042 ) n17042=1.
if (tnb= 17043 ) n17043=1.
if (tnb= 17050 ) n17050=1.
if (tnb= 17060 ) n17060=1.
if (tnb= 17071 ) n17071=1.
if (tnb= 17080 ) n17080=1.
if (tnb= 17090 ) n17090=1.
if (tnb= 17110 ) n17110=1.
if (tnb= 17112 ) n17112=1.
if (tnb= 17120 ) n17120=1.
if (tnb= 17130 ) n17130=1.
if (tnb= 17140 ) n17140=1.
if (tnb= 17200 ) n17200=1.
if (tnb= 17210 ) n17210=1.
if (tnb= 23010 ) n23010=1.
if (tnb= 23011 ) n23011=1.
if (tnb= 23021 ) n23021=1.
if (tnb= 23022 ) n23022=1.
if (tnb= 23041 ) n23041=1.
if (tnb= 23042 ) n23042=1.
if (tnb= 23043 ) n23043=1.
if (tnb= 23044 ) n23044=1.
if (tnb= 23045 ) n23045=1.
if (tnb= 23050 ) n23050=1.
if (tnb= 23080 ) n23080=1.
if (tnb= 23091 ) n23091=1.
if (tnb= 23092 ) n23092=1.
if (tnb= 23093 ) n23093=1.
if (tnb= 23094 ) n23094=1.
if (tnb= 23100 ) n23100=1.
if (tnb= 23110 ) n23110=1.
if (tnb= 23120 ) n23120=1.
if (tnb= 23132 ) n23132=1.
if (tnb= 23141 ) n23141=1.
if (tnb= 23150 ) n23150=1.
if (tnb= 23170 ) n23170=1.
if (tnb= 23171 ) n23171=1.

if (tnb=17011) sb17011=sqrt(bsf).
if (tnb=17013) sb17013=sqrt(bsf).
if (tnb=17020) sb17020=sqrt(bsf).
if (tnb=17041) sb17041=sqrt(bsf).
if (tnb=17042) sb17042=sqrt(bsf).
if (tnb=17043) sb17043=sqrt(bsf).
if (tnb=17050) sb17050=sqrt(bsf).
if (tnb=17060) sb17060=sqrt(bsf).
if (tnb=17071) sb17071=sqrt(bsf).
if (tnb=17080) sb17080=sqrt(bsf).
if (tnb=17090) sb17090=sqrt(bsf).
if (tnb=17110) sb17110=sqrt(bsf).
if (tnb=17112) sb17112=sqrt(bsf).
if (tnb=17120) sb17120=sqrt(bsf).
if (tnb=17130) sb17130=sqrt(bsf).
if (tnb=17140) sb17140=sqrt(bsf).
if (tnb=17200) sb17200=sqrt(bsf).
if (tnb=17210) sb17210=sqrt(bsf).
if (tnb=23010) sb23010=sqrt(bsf).
if (tnb=23011) sb23011=sqrt(bsf).
if (tnb=23021) sb23021=sqrt(bsf).
if (tnb=23022) sb23022=sqrt(bsf).
if (tnb=23041) sb23041=sqrt(bsf).
if (tnb=23042) sb23042=sqrt(bsf).
if (tnb=23043) sb23043=sqrt(bsf).
if (tnb=23044) sb23044=sqrt(bsf).
if (tnb=23045) sb23045=sqrt(bsf).
if (tnb=23050) sb23050=sqrt(bsf).
if (tnb=23080) sb23080=sqrt(bsf).
if (tnb=23091) sb23091=sqrt(bsf).
if (tnb=23092) sb23092=sqrt(bsf).
if (tnb=23093) sb23093=sqrt(bsf).
if (tnb=23094) sb23094=sqrt(bsf).
if (tnb=23100) sb23100=sqrt(bsf).
if (tnb=23110) sb23110=sqrt(bsf).
if (tnb=23120) sb23120=sqrt(bsf).
if (tnb=23132) sb23132=sqrt(bsf).
if (tnb=23141) sb23141=sqrt(bsf).
if (tnb=23150) sb23150=sqrt(bsf).
if (tnb=23170) sb23170=sqrt(bsf).
if (tnb=23171) sb23171=sqrt(bsf).


compute evn=0.
compute newt=0.
if town=17 evn=1.
if town=23 newt=1.

Compute lowzoneevn=0.
if town=17 and  (nghcde=11 or nghcde=13  or nghcde=20  or nghcde=41 or nghcde=42
or nghcde=43 or nghcde=50 or nghcde=71 or nghcde=80 or nghcde=90 
or nghcde=120 or nghcde=200)  lowzoneevn=1.                         	

Compute midzoneevn=0.
if town=17 and (nghcde=110 or nghcde=112 or nghcde=140 or nghcde=210)  midzoneevn=1. 

Compute highzoneevn=0.
if town=17 and (nghcde=60 or nghcde=130)  highzoneevn=1.


Compute srfxlowblockevn=0.
if lowzoneevn=1 srfxlowblockevn=srfx*lowzoneevn.

Compute srfxmidblockevn=0.
if midzoneevn=1 srfxmidblockevn=srfx*midzoneevn.

Compute srfxhighblockevn=0.
if highzoneevn=1 srfxhighblockevn=srfx*highzoneevn.


Compute midzonenewt=0.
if town=23 and (nghcde=10 or nghcde=11 or nghcde=42 or nghcde=43 
    or nghcde=100 or nghcde=120)  midzonenewt=1. 

Compute lowzonenewt=0.
if (town=23 and  midzonenewt=0)  lowzonenewt=1.                         	


Compute srfxlowblocknewt=0.
if lowzonenewt=1 srfxlowblocknewt=srfx*lowzonenewt.

Compute srfxmidblocknewt=0.
if midzonenewt=1 srfxmidblocknewt=srfx*midzonenewt.


Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).



Compute winter0506=0.
if (mos > 9 and yr=5) or (mos <= 3 and yr=6) winter0506=1.
Compute winter0607=0.
if (mos > 9 and yr=6) or (mos <= 3 and yr=7) winter0607=1.
Compute winter0708=0.
if (mos > 9 and yr=7) or (mos <= 3 and yr=8) winter0708=1.
Compute winter0809=0.
if (mos > 9 and yr=8) or (mos <= 3 and yr=9) winter0809=1.
Compute summer05=0.
if (mos > 3 and yr=5) and (mos <= 9 and yr=5) summer05=1.
Compute summer06=0.
if (mos > 3 and yr=6) and (mos <= 9 and yr=6) summer06=1. 
Compute summer07=0.
if (mos > 3 and yr=7) and (mos <= 9 and yr=7) summer07=1.
Compute summer08=0.
if (mos > 3 and yr=8) and (mos <= 9 and yr=8) summer08=1.
Compute summer09=0.
if (mos > 3 and yr=9) and (mos <= 9 and yr=9) summer09=1.
Compute jantmar05=0.
if (year1=2005 and (mos>=1 and mos<=3)) jantmar05=1. 
Compute octtdec09=0.
if (year1=2009 and (mos>=10 and mos<=12)) octtdec09=1.

Compute jantmar05cl234=jantmar05*cl234.
Compute winter0506cl234=winter0506*cl234.
Compute winter0607cl234=winter0607*cl234.
Compute winter0708cl234=winter0708*cl234.
Compute winter0809cl234=winter0809*cl234.
Compute summer05cl234=summer05*cl234.
Compute summer06cl234=summer06*cl234.
Compute summer07cl234=summer07*cl234.
Compute summer08cl234=summer08*cl234.
Compute summer09cl234=summer09*cl234.
Compute octtdec09cl234=octtdec09*cl234.

Compute jantmar05cl56=jantmar05*cl56.
Compute winter0506cl56=winter0506*cl56.
Compute winter0607cl56=winter0607*cl56.
Compute winter0708cl56=winter0708*cl56.
Compute winter0809cl56=winter0809*cl56.
Compute summer05cl56=summer05*cl56.
Compute summer06cl56=summer06*cl56.
Compute summer07cl56=summer07*cl56.
Compute summer08cl56=summer08*cl56.
Compute summer09cl56=summer09*cl56.
Compute octtdec09cl56=octtdec09*cl56.

Compute jantmar05cl778=jantmar05*cl778.
Compute winter0506cl778=winter0506*cl778.
Compute winter0607cl778=winter0607*cl778.
Compute winter0708cl778=winter0708*cl778.
Compute winter0809cl778=winter0809*cl778.
Compute summer05cl778=summer05*cl778.
Compute summer06cl778=summer06*cl778.
Compute summer07cl778=summer07*cl778.
Compute summer08cl778=summer08*cl778.
Compute summer09cl778=summer09*cl778.
Compute octtdec09cl778=octtdec09*cl778.

Compute jantmar05cl89=jantmar05*cl89.
Compute winter0506cl89=winter0506*cl89.
Compute winter0607cl89=winter0607*cl89.
Compute winter0708cl89=winter0708*cl89.
Compute winter0809cl89=winter0809*cl89.
Compute summer05cl89=summer05*cl89.
Compute summer06cl89=summer06*cl89.
Compute summer07cl89=summer07*cl89.
Compute summer08cl89=summer08*cl89.
Compute summer09cl89=summer09*cl89.
Compute octtdec09cl89=octtdec09*cl89.

Compute jantmar05cl1112=jantmar05*cl1112. 
Compute winter0506cl1112=winter0506*cl1112.
Compute winter0607cl1112=winter0607*cl1112.
Compute winter0708cl1112=winter0708*cl1112.
Compute winter0809cl1112=winter0809*cl1112.
Compute summer05cl1112=summer05*cl1112.
Compute summer06cl1112=summer06*cl1112.
Compute summer07cl1112=summer07*cl1112.
Compute summer08cl1112=summer08*cl1112.
Compute summer09cl1112=summer09*cl1112.
Compute octtdec09cl1112=octtdec09*cl1112.
 
Compute jantmar05cl1095=jantmar05*cl1095.
Compute winter0506cl1095=winter0506*cl1095.
Compute winter0607cl1095=winter0607*cl1095.
Compute winter0708cl1095=winter0708*cl1095.
Compute winter0809cl1095=winter0809*cl1095.
Compute summer05cl1095=summer05*cl1095.
Compute summer06cl1095=summer06*cl1095.
Compute summer07cl1095=summer07*cl1095.
Compute summer08cl1095=summer08*cl1095.
Compute summer09cl1095=summer09*cl1095.
Compute octtdec09cl1095=octtdec09*cl1095.

Compute jantmar05clsplt=jantmar05*clsplt.
Compute winter0506clsplt=winter0506*clsplt.
Compute winter0607clsplt=winter0607*clsplt.
Compute winter0708clsplt=winter0708*clsplt.
Compute winter0809clsplt=winter0809*clsplt.
Compute summer05clsplt=summer05*clsplt.
Compute summer06clsplt=summer06*clsplt.
Compute summer07clsplt=summer07*clsplt.
Compute summer08clsplt=summer08*clsplt.
Compute summer09clsplt=summer09*clsplt.
Compute octtdec09clsplt=octtdec09*clsplt.


if class=95 and tnb=17011  	lsf=3980.
if class=95 and tnb=17013  	lsf=2814.
if class=95 and tnb=17060  	lsf=2315.
if class=95 and tnb=17071  	lsf=926.
if class=95 and tnb=17080  	lsf=1783.
if class=95 and tnb=17110  	lsf=2789.
if class=95 and tnb=17112 	lsf=2789.
if class=95 and tnb=17120  	lsf=2916.
if class=95 and tnb=17130  	lsf=3006.
if class=95 and tnb=17140  	lsf=2164.
if class=95 and tnb=23093  	lsf=2621.
if class=95 and tnb=23094 	lsf=1313.
if class=95 and tnb=23100  	lsf=2462.
if class=95 and tnb=23120  	lsf=1855.
if class=95 and tnb=23150  	lsf=2160.


compute nsrevnl=0.
compute nsrnewtl=0.
if evn=1 nsrevnl=n*evn*sqrt(lsf).
if newt=1 nsrnewtl=n*newt*sqrt(lsf).
compute srbsf=sqrt(bsf).
compute nsrbsf=n*sqrt(bsf).
Compute nbsf=n*bsf.
Compute srage=sqrt(age).
Compute bsfage=bsf*age.


Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute frabsf=frame*bsf.
compute masbsf=mason*bsf.
Compute stubsf=stucco*bsf.
Compute frmsbsf=framas*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.
Compute bathsum=fullbath + 0.50*halfbath.
compute nbathsum=n*bathsum.

Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.
compute prembsf=premrf*bsf.
compute nprembsf=n*prembsf.


Compute garnogar=0.
Compute garage=0.
If gar=1 gar1car=1.
If gar=2 gar1hcar=1.
If gar=3 gar2car=1.
If gar=4 gar2hcar=1.
If gar=5 gar3car=1.
If gar=6 gar3hcar=1.
If gar=7 garnogar=1.
If gar=8 gar4car=1. 
If gar ne 7 garage=1.

Compute garage1=0.
Compute garage2=0.
Compute garage3=0.
Compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1.  
Compute biggar=0.
if garage3=1 or garage4=1 biggar=1.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
compute nsrage=n*srage.

Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
Compute basement=0.
If basment=1 or basment=3 basement=1.
Compute nbasfull=n*basefull.
Compute nbaspart=n*basepart.
Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.
Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
compute nnobase=n*nobase.

if num=6 bnum=0.
if num=0 bnum=0.
if num=1 bnum=2.
if num=2 bnum=3.
if num=3 bnum=4.
if num=4 bnum=5.
if num=5 bnum=6.

Compute nbnum=n*bnum.
Compute ncomm = n*comm.
Compute totunit=bnum + comm.
Compute totunitb=totunit*sqrt(bsf).


compute nfirepl=n*firepl.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute nsitebsf=nsiteben*bsf.
Compute repabove=0.
Compute repbelow=0.
Compute repave=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.
Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.

Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.

compute b=1.
*select if (town=17 and year1>=2009) or (town=23 and year1>=2009).
*select if puremarket=1. 
*Table observation = b
                amount1
               /table = tnb by 
                           amount1 
                             + b      
	/title = 'Ave & Med Sales'
     	/statistics =  median (amount1 'MED SP')
            mean (amount1 'MEAN SP')
            validn (b '# PROPS').


reg des=defaults cov
    /var=amount1 nsrevnl nsrnewtl nbsf nbsf56 nbsf778 nbsf89
	 nbsf1095 nbsf34 nbasfull nbaspart nfirepl nbsfair totunitb
	 nsitebsf garage1 garage2 biggar nbathsum nprembsf nsrage masbsf 
	 frabsf frmsbsf stubsf frastbsf srfx sb17011 sb17013 sb17020 sb17041
	 sb17042 sb17043 sb17050 sb17060 sb17071 sb17080 sb17090 sb17110 sb17112 sb17120 sb17130 
	 sb17200 sb17210 sb23010 sb23011 sb23021 sb23022 sb23041 sb23042 sb23043 sb23044 
	 sb23045 sb23050 sb23080 sb23091 sb23092 sb23093 sb23094 sb23100 sb23110 sb23120 sb23132
	 sb23141 sb23150 sb23170 jantmar05cl234 winter0506cl234 winter0607cl234
	 winter0708cl234 winter0809cl234 summer05cl234 summer06cl234 summer07cl234 summer08cl234
	 summer09cl234 octtdec09cl234 jantmar05cl56 winter0506cl56 winter0607cl56 winter0708cl56
	 winter0809cl56 summer05cl56 summer06cl56 summer07cl56 summer08cl56 summer09cl56 
	 octtdec09cl56 jantmar05cl778 winter0506cl778 winter0607cl778 winter0708cl778 winter0809cl778
	 summer05cl778 summer06cl778 summer07cl778 summer08cl778 summer09cl778 octtdec09cl778
	 jantmar05cl1112 winter0506cl1112 winter0607cl1112 winter0708cl1112 winter0809cl1112 summer05cl1112 
	 summer06cl1112 summer07cl1112 summer08cl1112 summer09cl1112 octtdec09cl1112 jantmar05cl1095
	 winter0506cl1095 winter0607cl1095 winter0708cl1095 winter0809cl1095 summer05cl1095
	 summer06cl1095 summer07cl1095 summer08cl1095 summer09cl1095 octtdec09cl1095 jantmar05clsplt
	 winter0506clsplt winter0607clsplt winter0708clsplt winter0809clsplt summer05clsplt 
	 summer06clsplt summer07clsplt summer08clsplt summer09clsplt octtdec09clsplt
	 lowzoneevn midzoneevn highzoneevn srfxlowblockevn srfxmidblockevn srfxhighblockevn
        srfxlowblocknewt srfxmidblocknewt 
	 /dep=amount1
        /method=stepwise 
	 /method=enter nbaspart garage1 garage2 biggar nbsf34 nbsfair nbathsum stubsf
	 /method=enter lowzoneevn midzoneevn highzoneevn srfxlowblockevn srfxmidblockevn srfxhighblockevn
        srfxlowblocknewt srfxmidblocknewt 
	 /method=enter jantmar05cl234 winter0506cl234 winter0607cl234 winter0708cl234 
	 winter0809cl234 summer05cl234 summer06cl234 summer07cl234 summer08cl234 
	 summer09cl234 octtdec09cl234 jantmar05cl56 winter0506cl56 winter0607cl56 
	 winter0708cl56 winter0809cl56 summer05cl56 summer06cl56 summer07cl56 
	 summer08cl56 summer09cl56 octtdec09cl56 jantmar05cl778 winter0506cl778 
	 winter0607cl778 winter0708cl778 winter0809cl778 summer05cl778 summer06cl778
	 summer07cl778 summer08cl778 summer09cl778 octtdec09cl778 
	 jantmar05cl1112 winter0506cl1112 winter0607cl1112 winter0708cl1112 
	 winter0809cl1112 summer05cl1112 summer06cl1112 summer07cl1112 
	 summer08cl1112 summer09cl1112 octtdec09cl1112 jantmar05cl1095 
	 winter0506cl1095 winter0607cl1095 winter0708cl1095 winter0809cl1095 
	 summer05cl1095 summer06cl1095 summer07cl1095 summer08cl1095 summer09cl1095
	 octtdec09cl1095 jantmar05clsplt winter0506clsplt winter0607clsplt
	 winter0708clsplt winter0809clsplt summer05clsplt summer06clsplt
	 summer07clsplt summer08clsplt summer09clsplt octtdec09clsplt 
	 /save pred (pred) resid (resid).
    sort cases by tnb pin.	
    value labels extcon 1 'Frame' 2 'Mason' 3 'Framas' 4 'Stucco'
    /basment 1 'Full' 2 'Slab' 3 'Part' 4 'Crwl'
    /bsfn 1 'Rec' 2 'Apt' 3 'Unf'
    /gar 1 '1' 2 '1.5' 3 '2' 4 '2.5' 5 '3' 6 '3.5' 7 'non'
    /class 2 '2-02' 3 '2-03' 4 '2-04' 5 '2-05' 6 '2-06' 7 '2-07' 8 '2-08' 9 '2-09' 10 '2-10' 11'2-11' 12 '2-12' 34 '2-34' 78 '2-78' 95 '2-95'.
compute perdif=(resid)/(amount1)*100.
formats pred(COMMA9.0)
          /resid (f6.0).
exe.
*plot
/vertical='Sales Price' MIN(50000) MAX(1000000)
/horizontal='RESID'
/vsize=25
/hsize=140
/plot=amount with RESID.
compute badsal=0.
if perdif>50 badsal=1.
if perdif<-50 badsal=1.
select if badsal=1.
set wid=125.
set len=65.
*REPORT FORMAT=automatic list(1)
/title='Office of the Assessor'
        'Residential Regression Report'
        'Town is Evanston'
    /ltitle 'Report Ran On)Date' 
    /rtitle='PAGE)PAGE'
    /string=prop(area(2) '-' subarea(2) '-' block(3) '-' parcel(3))
          date(mos(2) '-'year1(4))
   /var=prop 'Prop' 'Index Number'(13)
       class 'Class' (label) (5)
       nghcde 'nbhb'(5)
       amount1 'Price'(7)
       YEAR1 'YR'(4)
       pred 'Predicted'(9)
       resid 'Resid'(7)
       perdif '% DIF' (5)
       sqftb 'Sqftb'(5)
       age 'Age'(4)
       extcon 'Extcon' (label)(6)
       sqftl 'Land'(6).
	
 	

