# License notice ----

# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/.

# Top comments ----

# This script estimates market values for 200-class properties. The script evaluates a battery  of
# models using IAAO standards and identifies the top-performing functional forms and
# estimation methods.
# This script creates a number of objects:.
#
# modeldata is the data used to run regression models, comparison algorithms, and other valuation proceedures
# fitted_values contains all of the estimated values from all of the models run
# models is a list of all of the valuation algorithms used
# modeling_results stores performance information for all of the models run
#
# In terms of workflow, we will run this script twice. In the first iteration, we are looking for
# outliers in terms of modeling error. These will, hopefully, illuminate potential data issues.
# These outliers are assigned to analysts to check for inaccurate characteristics, or other
# reasons why perhaps they should not be included in our modeling data.
#
# We have two groups of properties that are modeled differently. 'Single-family' properties
# have a large number of descriptive characteristics which may be used in modeling.
# Condominiums, and other properties, do not. These can be modeled using only
# a fixed effect, time, and location.
#
# Version 0.1
# Written 12/22/2018
# Updated 1/17/2019
# Included previously omitted classes in NCHARS modeling group
# Updated 1/26/2019
# Included error-handling to account for issues caused by trimming

load(destfile)

start_time <- Sys.time()
# Template outcome tables ----
modeling_results <- data.frame(
  modeling_townships="", form_id="", form="", type="" , estimation_method="",model_id="", trim=NA, pct_trimmed=NA, modeling_group=NA, N=NA, adj_r=NA
  , p25_price_msample=NA, p50_price_msample=NA, p75_price_msample=NA, IQR="", outliers=NA, mean_ratio=NA, median_ratio=NA
  , p50_ratio=NA, p25_ratio=NA
  , p75_ratio=NA, COD=NA, COD_notbooted=NA, COD_SE=NA, COD_CI=NA, hold_out_COD=NA
  , median_hold_out_ratio=NA, model_COD=NA, model_COD_SE=NA, model_COD_CI=NA, P10_COD=NA, P10COD_SE=NA, P10COD_CI=NA, P90_COD=NA
  , P90_COD_SE=NA, P90_COD_CI=NA, PRD=NA, PRD_SE=NA, PRD_CI=NA, model_PRD=NA, model_PRD_SE=NA, model_PRD_CI=NA, PRB=NA, PRB_SE=NA, PRB_CI=NA, model_PRB=NA, model_PRB_SE=NA, model_PRB_CI=NA
  , MAD=NA , model_MAD=NA, RMSE=NA, model_RMSE=NA, ideal_trees=NA, morans_i=NA, sphere_errors=NA, proc_time=NA
)

modeldata_trim_levels <- list()

# The purpose of this loop is to select a high-performing functional form to use to identify properties
# to send to desktop review
models <- subset(
  read.xlsx(paste0(dirs$parent, "ccao_dictionary_draft.xlsx"), sheetName = "models", stringsAsFactors=FALSE)
  , estimation_method %in% c("OLS", "Quantile", "GBM", "COMPS") & status == "active"
  #, estimation_method %in% c("GBM") & status == "active" & modeling_group=="NCHARS"
)

# limit models to speed up testing
if(test_run == TRUE){

  models <- subset(models, modeling_group == 'SF' & estimation_method != "COMPS")

}

# remove ohare models if no towns under ohare flight path are in modeldata
if(!any(c(16, 20, 22, 71, 26) %in% modeling_townships)){
  models <- models[grep("ohare_noise", models$form, invert = TRUE),]
}

# allow user to re-model best models if so desired
if (exists("best_models")) {
  if(ask.user.yn.question("Would you like to limit models to 'best_models'?") == TRUE){
    models <- best_models[, c("modeling_group", "estimation_method", "type", "form_id", "model_id", "form")]
    models$form <- as.character(models$form)
  }else{
    print(noquote("All (or test) models will be considered."))
  }
}

for(m in 1:nrow(models)){ for(i in 1:filter_iters){
  ideal_trees <- NA
  filter_setting_1 <- i

  # Refresh COD table
  COD <- data.frame(group="", iter=NA , trim=NA, n=NA , COD=NA, p10_COD=NA, p90_COD=NA, model_COD=NA, PRD=NA, model_PRD=NA, PRB=NA, model_PRB=NA
                    , COD_SE=NA, p10_COD_SE=NA, p90_COD_SE=NA, model_COD_SE=NA, PRD_SE=NA, model_PRD_SE=NA, PRB_SE=NA, model_PRB_SE=NA
                    , COD_CI=NA, p10_COD_CI=NA, p90_COD_CI=NA, model_COD_CI=NA, PRD_CI=NA, model_PRD_CI=NA, PRB_CI=NA, model_PRB_CI=NA
                    , MAD=NA, model_MAD=NA, RMSE=NA, model_RMSE=NA)

  # Stop filtering once you have whittled your sample down to nothing
  if(nrow(subset(modeldata, filter_1>=filter_setting_1 & modeling_group==models$modeling_group[m] ))<=50){
    print(paste0("There are only ", nrow(subset(modeldata, filter_1>=filter_setting_1 & modeling_group==models$modeling_group[m])),
                 " observations in the next filter setting for ", models$modeling_group[m], models$model_id[m], ". That's not enought to run a reliable model."))
    next
  }else{print(paste0("Modeling ",
                     nrow(subset(modeldata, filter_1>=filter_setting_1 & modeling_group==models$modeling_group[m]))
                     , " properties in modeling group ", models$modeling_group[m]))
  }

  #skip filtering comps
  if(models$estimation_method[m]=="COMPS" & filter_setting_1 > 1){next}

  # Make sure you have enough RAM available
  if(memory.size()>=.93*memory.limit()){
    print(paste0("Memory = ", memory.size(),". Quitting current job..."))
    next}

  start_time <- Sys.time()
  print(paste0("Filter = ", filter_setting_1, "/", filter_iters))

  modeldata$fitted_value<- NA
  modeldata$log2 <- log(2)

  # Parameter estimation ----

  if (exists("model")){
    rm(model)
  }
  if(models$estimation_method[m]=="OLS"
     & (models$type[m]=="lin-lin" | models$type[m]=="log-log")){
    model <- try(lm(as.formula(models$form[m])
                    , data=subset(modeldata, filter_1>=filter_setting_1 & modeling_group==models$modeling_group[m]))
    )
  }
  if(models$estimation_method[m]=="Quantile"
     & (models$type[m]=="lin-lin" | models$type[m]=="log-log")){
    model <- try(rq(as.formula(models$form[m])
                    ,data=subset(modeldata, filter_1>=filter_setting_1 & modeling_group==models$modeling_group[m])
                    , tau=.5))
  }
  if(models$estimation_method[m]=="Random Forest"){
    model <- try(randomForest(as.formula(models$form[m])
                              , importance=TRUE
                              , data=subset(modeldata, filter_1>=filter_setting_1 & modeling_group==models$modeling_group[m])))
  }
  if(models$estimation_method[m]=="GBM"
     & (models$type[m]=="lin-lin" | models$type[m]=="log-log")){
    model <- try(gbm(as.formula(models$form[m])
                     ,distribution="gaussian"
                     ,interaction.depth=2
                     ,n.trees=ntree
                     ,cv.folds = 3
                     ,data=subset(modeldata, filter_1>=filter_setting_1 & modeling_group==models$modeling_group[m]
                                  , select = unique(unlist(strsplit(gsub(" [~] 1", "", models$form[m]), split = "[+]"))))))
    ideal_trees <- try(gbm.perf(model))
  }
  if(models$estimation_method[m]=="COMPS"){
    model <- "pass"
  }
  if (class(model)=="try-error"){
    if(filter_setting_1==1){

      check<- paste0("Able to estimate all models with no trimming?")
      msg<- paste0("Bad: failed to estimate parameters of ",models$model_id[m], " at trim setting 1")
      stage<- paste0("2")
      integrity_checks <- rbind.fill(integrity_checks, data.frame(check=check, outcome=msg, stage=stage))
      print(check); print(msg); rm( msg)
      next
    }else{
      print(paste0("Error estimating parameters, skipping loop"))
      next
    }
  }
  if(class(model)=="rq"){
    summary(model, se = "iid")
  }else{
  summary(model)
  }
  # Value estimation  ----
  if(models$estimation_method[m]=="Random Forest"  & models$type[m]=="lin-lin"){
    predictions <- NA
    TF <- modeldata$modeling_group==models$modeling_group[m] & modeldata$filter_1 >= 1
    predictions[TF] <- try(exp(predict(model, newdata=modeldata[TF,])))
  }
  if((models$estimation_method[m]=="OLS" | models$estimation_method[m]=="Quantile") & models$type[m]=="log-log"){
    predictions <- NA
    TF <- modeldata$modeling_group==models$modeling_group[m] & modeldata$filter_1 >= 1
    error <-  try(exp(predict(model, newdata=modeldata[TF,])))
    if(class(error)!="try-error"){
      predictions[TF] <-  try(exp(predict(model, newdata=modeldata[TF,])))
      modeldata$fitted_value <- predictions
      print("Values predicted")
    }else{
      if(filter_setting_1==1){
        print(error)
        check<- paste0("Able to estimate all models with no trimming?")
        msg<- paste0("Bad: failed to predict values for ",models$model_id[m], " at trim setting 1")
        stage<- paste0("2")
        integrity_checks<- rbind.fill(integrity_checks, data.frame(check=check, outcome=msg, stage=stage))
        print(check); print(msg); rm( msg)
        next
      }else{
        print(paste0("Error predicting values, skipping loop."))
        next
      }
    }
  }

  if((models$estimation_method[m]=="OLS" | models$estimation_method[m]=="Quantile") & models$type[m]=="lin-lin"){
    predictions <- NA
    TF <- modeldata$modeling_group==models$modeling_group[m] & modeldata$filter_1 >= 1

    error <- tryCatch({
      predict(model, newdata=modeldata[TF,])
    }, error=function(e){return(e)}
    )

    if(class(error)[1]!="simpleError"){
      predictions[TF] <- try(predict(model, newdata=modeldata[TF,]))
      modeldata$fitted_value <- predictions
      print("Values predicted")
    }else{
      if(filter_setting_1==1){
        print(error)
        check<- paste0("Able to estimate all models with no trimming?")
        msg<- paste0("Bad: failed to predict values for ",models$model_id[m], " at trim setting 1")
        stage<- paste0("2")
        integrity_checks <- rbind.fill(integrity_checks, data.frame(check=check, outcome=msg, stage=stage))
        print(check); print(msg); rm( msg)
        next
      }else{
        print(paste0("Error predicting values, skipping loop."))
        next
      }
    }
  }

  if (models$estimation_method[m]=='GBM' & models$type[m] == 'lin-lin'){
    predictions <- NA
    TF <- modeldata$modeling_group==models$modeling_group[m] & modeldata$filter_1 >= 1
    error <- try(predict(model, newdata=modeldata[TF,], n.trees=ntree))
    if (class(error) != "try-error"){
      predictions[TF] <- try(predict(model, newdata=modeldata[TF,], n.trees=ntree))
      modeldata$fitted_value <- predictions
      print("Values predicted")
    }
    else{
      if(filter_setting_1==1){
        print(error)
        check<- paste0("Able to estimate all models with no trimming?")
        msg<- paste0("Bad: failed to predict values for ",models$model_id[m], " at trim setting 1")
        integrity_checks <- rbind(integrity_checks, data.frame(check=check, outcome=msg))
        print(check); print(msg); rm( msg)
        next
      }else{
        print(paste0("Error predicting values, skipping loop."))
        next
      }
    }
  }
  if (models$estimation_method[m]=='GBM' & models$type[m] == "log-log"){
    predictions <- NA
    TF <- modeldata$modeling_group==models$modeling_group[m] & modeldata$filter_1 >= 1
    error <- try(exp(predict(model, newdata=modeldata[TF,], n.trees=ntree)))
    if (class(error) != "try-error"){
      predictions[TF] <- try(exp(predict(model, newdata=modeldata[TF,], n.trees=ntree)))
      modeldata$fitted_value <- predictions
      print("Values predicted")
    }
    else{
      if(filter_setting_1==1){
        print(error)
        check<- paste0("Able to estimate all models with no trimming?")
        msg<- paste0("Bad: failed to predict values for ",models$model_id[m], " at trim setting 1")
        integrity_checks <- rbind(integrity_checks, data.frame(check=check, outcome=msg))
        print(check); print(msg); rm( msg)
        next
      }else{
        print(paste0("Error predicting values, skipping loop."))
        next
      }
    }
  }
  if(models$estimation_method[m]=="COMPS"){
    predictions <- NA
    comp_method <- models$form_id[m]
    tmp <- subset(modeldata, filter_1>=filter_setting_1 & modeling_group==models$modeling_group[m])
    TF <- modeldata$modeling_group==models$modeling_group[m] & modeldata$filter_1 >= 1 & !is.na(modeldata$filter_1)
    newdata <- modeldata[TF,]
    if(comp_method == "f1"){
      result <- COMPS1
    }
    if(comp_method == "f2"){
      result <- COMPS2
    }
    if(comp_method == "f3"){
      result <- COMPS3
    }
    preds <- newdata %>% left_join(result, by = c("PIN" = "TARGET_PIN"))
    predictions[TF] <- preds$VALUE
    modeldata$fitted_value <- predictions
    print("Values predicted")
  }
  rm(predictions)

  # Can't have negative predictions
  modeldata$fitted_value <- ifelse(modeldata$fitted_value<=10000, 10000, modeldata$fitted_value)

  # Ratios ----
  modeldata$ratio <- NA
  modeldata$ratio <-modeldata$fitted_value/modeldata$sale_price

  mean <- mean(subset(modeldata, ratio<10)$ratio, na.rm=TRUE)
  std <- sd(subset(modeldata, ratio<10)$ratio, na.rm=TRUE)
  modeldata$Nratio <- NA
  modeldata$Nratio <-(modeldata$ratio-mean)/std
  rm(mean, std)


  # Re-filter ----
  # Find next filter using IQR range on normalized ratio
  p25 <- quantile(subset(modeldata, filter_1>=filter_setting_1 & modeling_group==models$modeling_group[m])$Nratio, c(.25), names = FALSE, na.rm=TRUE)
  p75 <- quantile(subset(modeldata, filter_1>=filter_setting_1 & modeling_group==models$modeling_group[m])$Nratio, c(.75), names = FALSE, na.rm=TRUE)
  IQR <- abs(p75-p25)
  print(paste0("IQR is ", round(p25,2), " to ", round(p75,2)))

  if(eval(filter_setting_1)>=1){
    for(j in seq(4, .4, by=-.2)){
      # Want to identify the LEAST aggressive trim that STILL trims something
      if(nrow(subset(modeldata, Nratio>(p25-j*IQR) & Nratio<(p75+j*IQR)
                     & filter_1==filter_setting_1
                     & modeldata$modeling_group==models$modeling_group[m]))<
         nrow(subset(modeldata, filter_1==filter_setting_1 & modeling_group==models$modeling_group[m]))
      ){
        # Use that to construct next filter setting, where n(filter_setting_1+1)<n(filter_setting_1)
        modeldata$filter_1<- ifelse(
          modeldata$Nratio>(p25-j*IQR) & modeldata$Nratio<(p75+j*IQR) & modeldata$filter_1>=filter_setting_1
          & modeldata$modeling_group==models$modeling_group[m]
          , filter_setting_1+1,modeldata$filter_1)
      }else{next}
    }
  }

  print("Bootstrapping")
  # COD & PRD ----
  # COD of bottom 10th percentile of sales price within target township
  df <-subset(modeldata[,c("ratio", "sale_price","filter_1","modeling_group", "TOWN_CODE")]
              , !is.na(sale_price) & !is.na(ratio) & modeling_group==models$modeling_group[m] & filter_1 >= 1 & TOWN_CODE %in% target_township)
  cod_10 <- cod_func(ratios = df$ratio, trim = c(0.05, 0.14))

  #COD of top 10th percentile of sales price within target township
  df <-subset(modeldata[,c("ratio", "sale_price","filter_1","modeling_group", "TOWN_CODE")]
              , !is.na(sale_price) & !is.na(ratio) & modeling_group==models$modeling_group[m] & filter_1 >= 1 & TOWN_CODE %in% target_township)
  cod_90 <- cod_func(ratios = df$ratio, trim = c(0.84, 0.95))

  # COD within target township
  df <-subset(modeldata[,c("ratio", "sale_price","filter_1","modeling_group","TOWN_CODE")], !is.na(ratio) & filter_1>=1 & modeling_group==models$modeling_group[m] & TOWN_CODE %in% target_township)
  cod <- cod_func(ratios = df$ratio)

  # PRD within target township
  df <-subset(modeldata[,c("ratio", "sale_price", "filter_1","modeling_group","TOWN_CODE")], !is.na(ratio) & filter_1>=1 & modeling_group==models$modeling_group[m] & TOWN_CODE %in% target_township)
  prd <- prd_func(ratios = df$ratio, sales = df$sale_price)

  # PRB within target township
  df <-subset(modeldata[,c("ratio", "sale_price", "filter_1","modeling_group","TOWN_CODE","fitted_value","log2")], !is.na(ratio) & filter_1>=1 & modeling_group==models$modeling_group[m] & TOWN_CODE %in% target_township)
  prb <- prb_func(ratios = df$ratio, assessed_values = df$fitted_value, sales = df$sale_price)

  # MAD within target township
  df <-subset(modeldata[,c("ratio", "filter_1","modeling_group","TOWN_CODE")], !is.na(ratio) & filter_1>=1 & modeling_group==models$modeling_group[m] & TOWN_CODE %in% target_township)
  mad <- MeanAD(df$ratio)

  # RMSE within target township
  df <-subset(modeldata[,c("ratio", "fitted_value", "sale_price", "filter_1","modeling_group","TOWN_CODE")], !is.na(ratio) & filter_1>=1 & modeling_group==models$modeling_group[m] & TOWN_CODE %in% target_township)
  rmse <- RMSE(df$fitted_value, df$sale_price)

  # COD within model sample
  df <-subset(modeldata[,c("ratio", "sale_price","filter_1","modeling_group")], !is.na(ratio) & filter_1>=eval(filter_setting_1) & modeling_group==models$modeling_group[m])
  cod_mod <- cod_func(ratios = df$ratio)

  # PRD within model sample
  df <-subset(modeldata[,c("ratio", "sale_price", "filter_1","modeling_group")], !is.na(ratio) & filter_1>=filter_setting_1 & modeling_group==models$modeling_group[m])
  prd_mod <- prd_func(ratios = df$ratio, sales = df$sale_price)

  # PRB within model sample
  df <-subset(modeldata[,c("ratio", "sale_price", "filter_1","modeling_group","fitted_value","log2")], !is.na(ratio) & filter_1>=filter_setting_1 & modeling_group==models$modeling_group[m])
  prb_mod <- prb_func(ratios = df$ratio, assessed_values = df$fitted_value, sales = df$sale_price)

  # MAD within model sample
  df <-subset(modeldata[,c("ratio", "filter_1","modeling_group")], !is.na(ratio) & filter_1>=filter_setting_1 & modeling_group==models$modeling_group[m])
  mad_mod <- MeanAD(df$ratio)

  # RMSE within target township
  df <-subset(modeldata[,c("ratio", "fitted_value", "sale_price", "filter_1","modeling_group")], !is.na(ratio) & filter_1>=filter_setting_1 & modeling_group==models$modeling_group[m])
  rmse_mod <- RMSE(df$fitted_value, df$sale_price)

  COD <- rbind(COD,  data.frame(
    group=models$modeling_group[m]
    , iter=i
    , trim=filter_setting_1
    , n=cod[4]
    , p10_COD=cod_10[1]
    , p10_COD_SE=cod_10[2]
    , p10_COD_CI=cod_10[3]
    , p90_COD=cod_90[1]
    , p90_COD_SE=cod_90[2]
    , p90_COD_CI=cod_90[3]
    , COD=cod[1]
    , COD_SE=cod[2]
    , COD_CI=cod[3]
    , PRD=prd[1]
    , PRD_SE=prd[2]
    , PRD_CI=prd[3]
    , PRB=prb[1]
    , PRB_SE=prb[2]
    , PRB_CI=prb[3]
    , MAD=mad
    , RMSE=rmse
    , model_COD=cod_mod[1]
    , model_COD_SE=cod_mod[2]
    , model_COD_CI=cod_mod[3]
    , model_PRD=prd_mod[1]
    , model_PRD_SE=prd_mod[2]
    , model_PRD_CI=prd_mod[3]
    , model_PRB=prb_mod[1]
    , model_PRB_SE=prb_mod[2]
    , model_PRB_CI=prb_mod[3]
    , model_MAD=mad_mod
    , model_RMSE=rmse_mod
  ))

  COD <- subset(COD, !is.na(COD))

  rm(df, cod, cod_mod, cod_10,cod_90, prd, prd_mod, prb, prb_mod, mad, mad_mod, rmse, rmse_mod)

  # Model R-Squared ----
  if(models$estimation_method[m]=="OLS") {
    r <- try(summary(model)$adj.r.squared)
    if(class(r)=="try-error"){r<-NA}
  }else{r<-NA}

  if(models$estimation_method[m]=="OLS"){
    n <- nobs(model)
  }else if(models$estimation_method[m]=="Quantile"){
    n <-length(model[["residuals"]])
  }else if(models$estimation_method[m]=="GBM"){
    n <- length(model[["fit"]])
  }else{
    n<-NA
  }

  print(paste0(models$model_id[m]," --> "
               , "Check n= ", n, " in ", models$modeling_group[m]
               , " Adjusted r = ", r
               , " COD = ", COD$COD
               , " COD NOT BOOTED = ",round(100*sum(abs(subset(modeldata, !is.na(ratio) & filter_1>=1 )$ratio-
                                                          median(subset(modeldata, !is.na(ratio) & filter_1>=1)$ratio)))  /
                                              (nrow(subset(modeldata, !is.na(ratio) & filter_1>=1))*
                                                 median(subset(modeldata, !is.na(ratio) & filter_1>=1)$ratio)),2)
               ," PRD = ",COD$PRD
               ," PRB = ",COD$PRB
               ," MAD = ",COD$MAD
               ," RMSE = ",COD$RMSE))
  rm(n)

  if (holdout_switch == TRUE){
    print("Holdout")
    # Holdout ----
    # To check for over-fitting
    holdout_failure_n1 <- holdout_failure_n2 <- holdout_failure_n3 <- holdout_failure_n4 <- NULL
    hold_out_COD <- NA
    median_hold_out_ratio <- NA
    if(filter_setting_1 %in% seq(1,filter_iters,10)){
      ho_predictions <- subset(modeldata, filter_1>=filter_setting_1 & modeling_group==models$modeling_group[m], select=c("DOC_NO", "sale_price"))
      ho_predictions$out_of_sample_fv <- NA

      for(deed in ho_predictions$DOC_NO){
        if (exists("model")){
          rm(model)
        }
        if(models$estimation_method[m]=="OLS"
           & (models$type[m]=="lin-lin" | models$type[m]=="log-log")){
          model <- try(
            lm(as.formula(models$form[m])
               , data=subset(modeldata, filter_1>=filter_setting_1 & modeling_group==models$modeling_group[m] & DOC_NO!=deed))
          )
        }
        if(models$estimation_method[m]=="Quantile"
           & (models$type[m]=="lin-lin" | models$type[m]=="log-log")){
          model <- try(rq(as.formula(models$form[m])
                          ,data=subset(modeldata, filter_1>=filter_setting_1 & modeling_group==models$modeling_group[m] & DOC_NO!=deed)
                          , tau=.5))
        }
        if(models$estimation_method[m]=="Random Forest"){
          model <- try(randomForest(as.formula(models$form[m])
                                    , importance=TRUE
                                    , data=subset(modeldata, filter_1>=filter_setting_1 & modeling_group==models$modeling_group[m] & DOC_NO!=deed)))
        }

        if(models$estimation_method[m]=="GBM"){
          model <- try(gbm(as.formula(models$form[m])
                           ,distribution="gaussian"
                           ,interaction.depth=2
                           ,n.trees=ntree
                           ,cv.folds = 3
                           ,data=subset(modeldata, filter_1>=filter_setting_1 & modeling_group==models$modeling_group[m])))
          holdout_ideal_trees <- gbm.perf(model)
        }

        if (class(model)=="try-error"){
          if(filter_setting_1==1){

            check<- paste0("Able to estimate all models with no trimming?")
            msg<- paste0("Bad: failed to estimate parameters of holdout ",models$model_id[m], " at trim setting 1")
            integrity_checks <- rbind(integrity_checks, data.frame(check=check, outcome=msg))
            print(check); print(msg); rm( msg)
            holdout_failure_n1 <- holdout_failure_n1 + 1
            next
          }else{
            print(paste0("Error estimating parameters in holdout, skipping loop"))
            next
          }
        }
        # Value estimation
        if(models$estimation_method[m]=="Random Forest"  & models$type[m]=="lin-lin"){
          predictions <- NA
          TF <- modeldata$modeling_group==models$modeling_group[m] & modeldata$filter_1 >= 1 & modeldata$DOC_NO==deed
          predictions[TF] <- exp(predict(model, newdata=modeldata[TF,]))
        }
        if((models$estimation_method[m]=="OLS" | models$estimation_method[m]=="Quantile") & models$type[m]=="log-log"){
          predictions <- NA
          TF <- modeldata$modeling_group==models$modeling_group[m] & modeldata$filter_1 >= 1 &  modeldata$DOC_NO==deed

          error <-  try(exp(predict(model, newdata=modeldata[TF,])))
          if(class(error)!="try-error"){
            predictions[TF] <-  try(exp(predict(model, newdata=modeldata[TF,])))
            modeldata$fitted_value <- predictions
          }else{
            if(filter_setting_1==1){
              print(error)
              check<- paste0("Able to estimate all holdout observations with no trimming?")
              msg<- paste0("Bad: failed to predict values for ",models$model_id[m], " at trim setting 1")
              stage<- paste0("2")
              integrity_checks <- rbind.fill(integrity_checks, data.frame(check=check, outcome=msg, stage=stage))
              print(check); print(msg); rm( msg)
              holdout_failure_n2 <- holdout_failure_n2 + 1
              next
            }else{
              print(paste0("Error predicting values, skipping loop."))
              next
            }
          }
        }
        if((models$estimation_method[m]=="OLS" | models$estimation_method[m]=="Quantile") & models$type[m]=="lin-lin"){
          predictions <- NA
          TF <- modeldata$modeling_group==models$modeling_group[m] & modeldata$filter_1 >= 1 &  modeldata$DOC_NO==deed
          error <- tryCatch({
            predict(model, newdata=modeldata[TF,])
          }, error=function(e){return(e)}
          )

          if(class(error)[1]!="simpleError"){
            predictions[TF] <- try(predict(model, newdata=modeldata[TF,]))
            modeldata$fitted_value <- predictions
          }else{
            if(filter_setting_1==1){
              print(error)
              check<- paste0("Able to estimate all holdout observations with no trimming?")
              msg<- paste0("Bad: failed to predict values for ",models$model_id[m], " at trim setting 1")
              stage<- paste0("2")
              integrity_checks <- rbind.fill(integrity_checks, data.frame(check=check, outcome=msg, stage=stage))
              print(check); print(msg); rm( msg)
              holdout_failure_n3 <- holdout_failure_n3 + 1
              next
            }else{
              print(paste0("Error predicting values, skipping loop."))
              next
            }
          }
        }

        if (models$estimation_method[m]=='GBM' & models$type[m] == 'lin-lin'){
          predictions <- NA
          TF <- modeldata$modeling_group==models$modeling_group[m] & modeldata$filter_1 >= 1
          error <- try(predict(model, newdata=modeldata[TF,], n.trees=ntree))
          if (class(error) != "try-error"){
            predictions[TF] <- try(predict(model, newdata=modeldata[TF,], n.trees=ntree))
            modeldata$fitted_value <- predictions
            print("Values predicted")
          }
          else{
            if(filter_setting_1==1){
              print(error)
              check<- paste0("Able to estimate all models with no trimming?")
              msg<- paste0("Bad: failed to predict values for ",models$model_id[m], " at trim setting 1")
              integrity_checks <- rbind(integrity_checks, data.frame(check=check, outcome=msg))
              print(check); print(msg); rm( msg)
              next
            }else{
              print(paste0("Error predicting values, skipping loop."))
              next
            }
          }
        }

        if (models$estimation_method[m]=='GBM' & models$type[m] == "log-log"){
          predictions <- NA
          TF <- modeldata$modeling_group==models$modeling_group[m] & modeldata$filter_1 >= 1
          error <- try(exp(predict(model, newdata=modeldata[TF,], n.trees=ntree)))
          if (class(error) != "try-error"){
            predictions[TF] <- try(exp(predict(model, newdata=modeldata[TF,], n.trees=ntree)))
            modeldata$fitted_value <- predictions
            print("Values predicted")
          }
          else{
            if(filter_setting_1==1){
              print(error)
              check<- paste0("Able to estimate all models with no trimming?")
              msg<- paste0("Bad: failed to predict values for ",models$model_id[m], " at trim setting 1")
              integrity_checks <- rbind(integrity_checks, data.frame(check=check, outcome=msg))
              print(check); print(msg); rm( msg)
              next
            }else{
              print(paste0("Error predicting values, skipping loop."))
              next
            }
          }
        }

        if(is.numeric(predictions)){
          try(ho_predictions$out_of_sample_fv[ho_predictions$DOC_NO==deed] <- predictions[!is.na(predictions)])
        }else{
          if(filter_setting_1==1){

            check<- paste0("Able to estimate all models with no trimming?")
            msg<- paste0("Bad: failed to predict values for ",models$model_id[m], " at trim setting 1")
            stage<- paste0("2")
            integrity_checks <- rbind.fill(integrity_checks, data.frame(check=check, outcome=msg, stage=stage))
            print(check); print(msg); rm( msg)
            holdout_failure_n4 <- holdout_failure_n4 + 1
            next
          }else{
            print(paste0("Error predicting values, skipping loop."))
            next
          }
        }
        rm(predictions)

      }
      # Can't have negative predictions
      ho_predictions$out_of_sample_fv <- ifelse(ho_predictions$out_of_sample_fv<=10000, 10000, ho_predictions$out_of_sample_fv)
      ho_predictions$ratio <- ho_predictions$out_of_sample_fv/ho_predictions$sale_price

      hold_out_COD <- round(100*sum(abs(subset(ho_predictions, !is.na(ratio))$ratio-
                                          median(subset(ho_predictions, !is.na(ratio))$ratio)))  /
                              (nrow(subset(ho_predictions, !is.na(ratio)))*
                                 median(subset(ho_predictions, !is.na(ratio))$ratio)),2)
      median_hold_out_ratio <- median(ho_predictions$ratio)
      rm(ho_predictions)

    }
    r <- try(summary(model)$adj.r.squared)
    if(is.null(r)|class(r)=="try-error"){r<-NA}
  }else if(holdout_switch == FALSE){
    hold_out_COD <- "NA"
    median_hold_out_ratio <- "NA"
    print(paste0(user, " chose to skip holdout"))
  }

  # Save Modeling Results ----
  # IQR
  p25 <- quantile(subset(modeldata, filter_1>=filter_setting_1)$ratio, c(.25), names = FALSE, na.rm=TRUE)
  p75 <- quantile(subset(modeldata, filter_1>=filter_setting_1)$ratio, c(.75), names = FALSE, na.rm=TRUE)
  IQR <- abs(p75-p25)
  IQR2 <- paste0("[ ", round(p25,2), ", ", round(p75,2), " ]")

  # weights <- cbind(modeldata$centroid_x, modeldata$centroid_y)
  # weights <- remove.na.rows(weights)
  # weights <- dnearneigh(weights, 0, 30, longlat = TRUE)

  # resid_inv <- as.matrix(dist(cbind(subset(modeldata, trim=filter_setting_1)$centroid_x, subset(modeldata, trim=filter_setting_1)$centroid_y)))
  # resid_inv <- 1/resid_inv
  # diag(resid_inv) <- 0
  try(modeling_results <-
        subset(
          rbind(modeling_results,
                data.frame(
                  modeling_townships=paste(modeling_townships, collapse = ', ')
                  , form_id=models$form_id[m]
                  , form=models$form[m]
                  , type=models$type[m]
                  , estimation_method=models$estimation_method[m]
                  , model_id=models$model_id[m]
                  , trim=filter_setting_1     #max(filter_setting_1, na.rm = TRUE) #modeldata$filter_1
                  , pct_trimmed=(nrow(subset(modeldata, modeling_group==models$modeling_group[m]))-nrow(subset(modeldata, filter_1>=filter_setting_1 & modeling_group==models$modeling_group[m])))/nrow(subset(modeldata, modeling_group==models$modeling_group[m]))
                  , modeling_group=models$modeling_group[m]
                  , N=nrow(subset(modeldata, filter_1>=filter_setting_1 & modeling_group==models$modeling_group[m]))
                  , adj_r=r
                  , p25_price_msample=quantile(subset(modeldata, filter_1>=filter_setting_1 & modeling_group==models$modeling_group[m])$sale_price, c(.25), names = FALSE, na.rm = TRUE)
                  , p50_price_msample=quantile(subset(modeldata, filter_1>=filter_setting_1 & modeling_group==models$modeling_group[m])$sale_price, c(.50), names = FALSE, na.rm = TRUE)
                  , p75_price_msample=quantile(subset(modeldata, filter_1>=filter_setting_1 & modeling_group==models$modeling_group[m])$sale_price, c(.75), names = FALSE, na.rm = TRUE)
                  , IQR=IQR2
                  , outliers=nrow(subset(modeldata, (ratio<.8 | ratio>1.2) & filter_1>=1 & modeling_group==models$modeling_group[m]))
                  , mean_ratio=mean(subset(modeldata, ratio<5 & filter_1>=filter_setting_1 & modeling_group==models$modeling_group[m])$ratio, na.rm = TRUE)
                  , median_ratio=median(subset(modeldata, ratio<5 & filter_1>=filter_setting_1 & modeling_group==models$modeling_group[m])$ratio, na.rm = TRUE)
                  , p50_ratio=subset(modeldata, modeling_group==models$modeling_group[24]  & filter_1>=1 & sale_price %in%
                                       quantile(subset(modeldata, filter_1>=1 & modeling_group==models$modeling_group[m])$sale_price, c(.45), na.rm = TRUE):
                                       quantile(subset(modeldata, filter_1>=1 & modeling_group==models$modeling_group[m])$sale_price, c(.55), na.rm = TRUE))$ratio[1]
                  , p25_ratio=subset(modeldata, filter_1>=1 & modeling_group==models$modeling_group[m]
                                     & sale_price<=quantile(subset(modeldata, filter_1>=1 & modeling_group==models$modeling_group[m])$sale_price, c(.25), names = FALSE, na.rm = TRUE))$ratio[1]
                  , p75_ratio=subset(modeldata, filter_1>=1 & modeling_group==models$modeling_group[m]
                                     & sale_price>=quantile(subset(modeldata, filter_1>=1 & modeling_group==models$modeling_group[m])$sale_price, c(.75), names = FALSE, na.rm = TRUE))$ratio[1]
                  , COD=COD$COD
                  , COD_notbooted=round(100*sum(abs(subset(modeldata, !is.na(ratio) & filter_1>=1 )$ratio-
                                                      median(subset(modeldata, !is.na(ratio) & filter_1>=1)$ratio)))  /
                                          (nrow(subset(modeldata, !is.na(ratio) & filter_1>=1))*
                                             median(subset(modeldata, !is.na(ratio) & filter_1>=1)$ratio)),2)
                  , COD_SE=COD$COD_SE
                  , COD_CI=COD$COD_CI
                  , hold_out_COD=hold_out_COD
                  , median_hold_out_ratio=median_hold_out_ratio
                  , model_COD=COD$model_COD
                  , model_COD_SE=COD$model_COD_SE
                  , model_COD_CI=COD$model_COD_CI
                  , P10_COD=COD$p10_COD
                  , P10COD_SE=COD$p10_COD_SE
                  , P10COD_CI=COD$p10_COD_CI
                  , P90_COD=COD$p90_COD
                  , P90_COD_SE=COD$p90_COD_SE
                  , P90_COD_CI=COD$p90_COD_CI
                  , PRD=COD$PRD
                  , PRD_SE=COD$PRD_SE
                  , PRD_CI=COD$PRD_CI
                  , model_PRD=COD$model_PRD
                  , model_PRD_SE=COD$model_PRD_SE
                  , model_PRD_CI=COD$model_PRD_CI
                  , PRB=COD$PRB
                  , PRB_SE=COD$PRB_SE
                  , PRB_CI=COD$PRB_CI
                  , model_PRB=COD$model_PRB
                  , model_PRB_SE=COD$model_PRB_SE
                  , model_PRB_CI=COD$model_PRB_CI
                  , MAD=COD$MAD
                  , model_MAD=COD$model_MAD
                  , RMSE=COD$RMSE
                  , model_RMSE=COD$model_RMSE
                  , ideal_trees=ideal_trees
                  , morans_i=NA # Moran.I(subset(modeldata, trim=filter_setting_1)$residuals, weights, na.rm = TRUE)
                  , sphere_errors=NA # sr.sphere.test(subset(modeldata, trim=filter_setting_1)$residuals)
                  , proc_time=Sys.time()-start_time
                )
          )
          , form_id!=""
        ))
  if(exists("hold_out_COD")){rm(hold_out_COD)}
  if(exists("median_hold_out_ratio")){rm(median_hold_out_ratio)}
  print(paste0("Estimated ", models$model_id[m], ":"))
  print(paste0("Median ratio= ", round(median(subset(modeldata, filter_1>=1)$ratio,na.rm=TRUE),2)))
  print(paste0("Mean ratio= ", round(mean(subset(modeldata, filter_1>=1)$ratio,na.rm=TRUE),2)))
  print(paste0("IQR for normalized ratio is now ", round(p25,2)," to ", round(p75,2), ", leaving "
               , nrow(subset(subset(modeldata, filter_1>=filter_setting_1+1)))
               , " observations in filter=", filter_setting_1+1))
  print(Sys.time()-start_time)
}
  modeling_results_m <- modeling_results[modeling_results$model_id == models$model_id[m] & modeling_results$modeling_group == models$modeling_group[m], ]
  trim_setting <- modeling_results_m$trim[modeling_results_m$COD == min(modeling_results_m$COD)]
  ind <- rep(0,dim(modeldata)[1])
  ind[modeldata$filter_1 >= trim_setting & modeldata$modeling_group == models$modeling_group[m]] <- 1
  modeldata_trim_levels <- c(modeldata_trim_levels, list(data.frame(DOC_NO = modeldata$DOC_NO, ind = ind)))
  #if (m == 1) names(modeldata_trim_levels) <- models$model_id else names(modeldata_trim_levels) <- c(names(modeldata_trim_levels),models$model_id)
}
names(modeldata_trim_levels) <- paste(paste(models$model_id,"_",sep=''),models$modeling_group,sep='')
file_info <- update_file_info('2_sf_modeling')

print(warnings())