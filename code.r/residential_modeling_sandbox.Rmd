---
title: "CCAO Residential Modeling Sandbox"
output: 
  html_document:
    toc: true
    toc_float:
      toc_collapsed: true

params:
  estimation_date:
    label: "What is the date at which we want to estimate values? (Relevant if adjusting for location factor)"
    value: !r as.Date(paste0(lubridate::year(Sys.time()),'-01-01'))
    input: date
  computer: 
    label: "What kind of operating system are you using?"
    value: "Windows"
    input: select
    choices:
      - MacOS
      - Windows
  path:
    label: "Home Directory file path (where your repositories are)"
    #value: !r paste0("/Users/", Sys.info()[['user']],"/Dropbox/CCAO")
    value: !r paste0("C:/Users/", Sys.info()[['user']],"/Documents")
    input: text
  data_pull1: 
    label: "Model 1 Data: How would you like to pull in data?"
    value: "Cook County Open Data Portal"
    input: select
    choices:
      - SQL Server (Needed if modeling townships different from last time)
      - Data From Last Query Loaded From Temp Directory (Faster)
      - Locally stored sales_sample file
      - Cook County Open Data Portal
  data_pull_local_path1:
    label: "Model 1 Data: If using your own sales sample, specify file path from home directory. Note: it must be an RDS file"
    value: "/ccao_sf_cama_dev/sales_sample.rds"
  data_pull_save1:
    label: "Model 1 Data: If doing an SQL pull, would you like the dataset to be saved locally? (This will speed up modeling if you intend to use the same dataset for multiple models)"
    value: "Yes, save data locally"
    input: select
    choices:
      - Yes, save data locally
      - No, don't save data locally
  modelgroup1:
    label: "Model 1 Data: What properties are you modeling?"
    value: "Single Family Regression Classes"
    input: select
    choices: 
    - Single Family Regression Classes
    - Residential Multi-Family Classes 211, 212
    - Residential Condominiums
  townships1:
    label: "Model 1 Data: What townships are you modeling?"
    value: 
    - Oak Park
    input: select
    choices: 
    - Barrington
    - Berwyn
    - Bloom
    - Bremen
    - Calumet
    - Cicero
    - Elk Grove
    - Evanston
    - Hanover
    - Hyde Park
    - Jefferson
    - Lake
    - Lake View
    - Lemont
    - Leyden
    - Lyons
    - Maine
    - New Trier
    - Niles
    - North Chicago
    - Northfield
    - Norwood Park
    - Oak Park
    - Orland
    - Palatine
    - Palos
    - Proviso
    - Rich
    - River Forest
    - Riverside
    - Rogers Park
    - Schaumburg
    - South Chicago
    - Stickney
    - Thornton
    - West Chicago
    - Wheeling
    - Worth
    multiple: yes
  min_year1:
    label: "Model 1 Data: What years do you want in your sample? Enter the minimum year you want."
    value: 2015
    input: numeric
    min: 2010
    max: 2020  
  max_year1:
    label: "Model 1 Data: What years do you want in your sample? Enter the maximum year you want."
    value: 2019
    input: numeric
    min: 2010
    max: 2020
  data_pull2: 
    label: "Model 2 Data: How would you like to pull in data?"
    value: "Cook County Open Data Portal"
    input: select
    choices:
      - SQL Server (Needed if modeling townships different from last time)
      - Data From Last Query Loaded From Temp Directory (Faster)
      - Locally stored sales_sample file
      - Cook County Open Data Portal
  data_pull_local_path2:
    label: "Model 2 Data: If using your own sales sample, specify file path from home directory. Note: it must be an RDS file"
    value: "/ccao_sf_cama_dev/sales_sample.rds"
  data_pull_save2:
    label: "Model 2 Data: If doing an SQL pull, would you like the dataset to be saved locally? (This will speed up modeling if you intend to use the same dataset for multiple models)"
    value: "Yes, save data locally"
    input: select
    choices:
      - Yes, save data locally
      - No, don't save data locally
  modelgroup2:
    label: "Model 2 Data: What properties are you modeling?"
    value: "Single Family Regression Classes"
    input: select
    choices: 
    - Single Family Regression Classes
    - Residential Multi-Family Classes 211, 212
    - Residential Condominiums
  townships2:
    label: "Model 2 Data: What townships are you modeling?"
    value: 
    - Oak Park
    input: select
    choices: 
    - Barrington
    - Berwyn
    - Bloom
    - Bremen
    - Calumet
    - Cicero
    - Elk Grove
    - Evanston
    - Hanover
    - Hyde Park
    - Jefferson
    - Lake
    - Lake View
    - Lemont
    - Leyden
    - Lyons
    - Maine
    - New Trier
    - Niles
    - North Chicago
    - Northfield
    - Norwood Park
    - Oak Park
    - Orland
    - Palatine
    - Palos
    - Proviso
    - Rich
    - River Forest
    - Riverside
    - Rogers Park
    - Schaumburg
    - South Chicago
    - Stickney
    - Thornton
    - West Chicago
    - Wheeling
    - Worth
    multiple: yes
  min_year2:
    label: "Model 2 Data: What years do you want in your sample? Enter the minimum year you want."
    value: 2015
    input: numeric
    min: 2010
    max: 2020  
  max_year2:
    label: "Model 2 Data: What years do you want in your sample? Enter the maximum year you want."
    value: 2019
    input: numeric
    min: 2010
    max: 2020          
  model_form1: 
    label: "Model 1 Functional Form"
    # value: log(sale_price)~1+log(BLDG_SF)+log(BLDG_SF)+log(HD_SF)+FBATH+BSMT+AIR+FRPL+ROOF_CNST+AGE_DECADE+AGE_DECADE_SQRD+EXT_WALL+ROOF_CNST+HEAT+BEDS+ATTIC_TYPE+CNST_QLTY+SITE+GAR1_SIZE+REPAIR_CND+sale_year+sale_halfofyear+town_nbhd_SF+LOCF1+log(HD_SF):price_tercile
    # 
    value: log(sale_price) ~ 1+log(BLDG_SF)+log(HD_SF)+AGE_DECADE+AGE_DECADE_SQRD+FBATH+HBATH+BEDS+EXT_WALL+GAR1_SIZE+BSMT+AIR+FRPL+ROOF_CNST+HEAT+ATTIC_TYPE+CNST_QLTY+SITE+REPAIR_CND+sale_quarter+sale_quarterofyear+town_nbhd+floodplain+cut_distance
    input: text
  model_adjust_locf1:
    label: "Model 1: Adjust for location factor?"
    value: FALSE
    input: checkbox
  model_estimation_method1:
    label: "Model 1 Estimation Method"
    value: "OLS"
    input: select
    choices:
      - OLS
      - GBM
      - Quantile
      - COMPS
      # - Random Forest
  model_comps_method1:
    label: "Model 1 COMPS Method (only applicable if COMPS chosen as estimation method)"
    value: "f2"
    input: select
    choices:
      - f1
      - f2
      - f3
  model_form2: 
    label: "Model 2 Specification (if different Model 2 not specified, the output will show Model 1 twice"
    # value: log(sale_price)~1+sale_year+sale_halfofyear+town_nbhd_SF+LOCF1+log(HD_SF):price_tercile
    value: log(sale_price) ~ 1+log(BLDG_SF)+log(HD_SF)+AGE_DECADE+AGE_DECADE_SQRD+FBATH+HBATH+BEDS+EXT_WALL+GAR1_SIZE+BSMT+AIR+FRPL+ROOF_CNST+HEAT+ATTIC_TYPE+CNST_QLTY+SITE+REPAIR_CND+sale_quarter+sale_quarterofyear+town_nbhd+floodplain+LOCF1
    input: text
  model_adjust_locf2:
    label: "Model 2: Adjust for location factor?"
    value: FALSE
    input: checkbox
  model_estimation_method2:
    label: "Model 2 Estimation Method"
    value: "OLS"
    input: select
    choices:
      - OLS
      - GBM
      - Quantile
      - COMPS
      # - Random Forest
  model_comps_method2:
    label: "Model 2 COMPS Method (only applicable if COMPS chosen as estimation method)"
    value: "f1"
    input: select
    choices:
      - f1
      - f2
      - f3
  
  time_plot:
    label: "Which time adjustment plot would you like to see?"
    value: "Sale Quarter"
    input: select
    choices:
    - Sale Quarter
    - Sale Quarter of Year
    - Sale Month
    - Sale Month of Year
    - Sale Half Year
    - Sale Half of Year
    - Sale Year
  floor:
    label: "Floor for negative predicted values"
    value: 10000
    input: numeric

  

---

<style>
th, td {
    padding-left: 15px;
    text-align: left;        
}
table {
  margin-top: 10px;
  margin-left: auto;
  margin-right: auto;
}
</style>

```{r setup, include=FALSE}
chooseCRANmirror(graphics=FALSE, ind=1)
knitr::opts_chunk$set(eval = TRUE)
# knitr::opts_chunk$set(eval = FALSE)

# Utility setup
source(paste0(params$path,"/ccao_utility/code.r/99_utility_2.r"))
invisible(check.packages(libs))

# # until this is added to utility
# library("modelr")

source(paste0(params$path,"/ccao_sf_cama_dev/code.r/0_sandbox_ingest.r"))
source(paste0(params$path, "/ccao_sf_cama_dev/code.r/0_sandbox_newvars.r"))
source(paste0(params$path, "/ccao_sf_cama_dev/code.r/2_sandbox_modeling.r"))
source(paste0(params$path, "/ccao_sf_cama_dev/code.r/3_sandbox_analysis.r"))
source(paste0(params$path, "/ccao_sf_cama_dev/code.r/4_sandbox_output.r"))

```

```{r parameter-setup, echo=FALSE}
# Recode params
param <- params_recodes(CCAODATA, params)

# DB setup
# The following commands need to be run if you're connecting to the server and pulling data (as data pull method or if building COMPS)
if(param$data_pull_choice_code1[1]==1 | 
   param$data_pull_choice_code2 == 1 | 
   param$model_estimation_method1=="COMPS" |
   param$model_estimation_method2=="COMPS"){
  database <- 1
  CCAODATA <- try(dbConnect(odbc()
                          , driver   = odbc.credentials("driver",param$path,param$computer)
                          , server   = odbc.credentials("server",param$path,param$computer)
                          , database = odbc.credentials("database",param$path,param$computer)
                          , uid      = odbc.credentials("uid",param$path,param$computer)
                          , pwd      = odbc.credentials("pwd",param$path,param$computer)))

  if(class(CCAODATA)[1]=="simpleError" | class(CCAODATA)=="try-error"){
    message("Failed to connect to SQL server. Common reasons might be: you're not connected to the VPN, or you specified the wrong Operating System parameter.")
  knitr::opts_chunk$set(eval = FALSE)
  }
}
```

```{r build-clean-data, include = FALSE}
# Building data
sales_sample1 <- query_sandbox_data_1(database = param$data_pull_choice_code1, 
                                      path = param$path,
                                      townships = param$town_codes1, 
                                      min_year = param$min_year1, 
                                      max_year = param$max_year1, 
                                      classes = param$modelgroup_classes1,
                                      local_file = param$path_local_file1, 
                                      dataset_num = 1,
                                      save_locally = param$data_pull_save1)
# If both datasets are SQL pulls with identical parameters, sales sample2 is set to be the same as sales sample1. 
# otherwise we call query sandbox data 1 again
if (param$identical_data) {
  sales_sample2 <- samples_sample1
} else {
    sales_sample2 <- query_sandbox_data_1(database = param$data_pull_choice_code2, 
                                        path = param$path,
                                        townships = param$town_codes2, 
                                        min_year = param$min_year2, 
                                        max_year = param$max_year2, 
                                        classes = param$modelgroup_classes2,
                                        local_file = param$path_local_file2, 
                                        dataset_num = 2,
                                        save_locally = param$data_pull_save2)
}

# Build COMPS data 
if(param$model_estimation_method1=="COMPS") {
  COMPS_model1 <- build_comps_data(param$model_comps_method1)
  #COMPS_model1 <- COMPS_model1
}
if(param$model_estimation_method2=="COMPS"){
  if(param$identical_comps) {
    COMPS_model2 <- COMPS_model1
  }else{
    COMPS_model2 <- build_comps_data(param$model_comps_method2)
    #COMPS_model2 <- COMPS_model2
  }
}
```

```{r sales_sample_error_handling, echo=FALSE}
if (class(sales_sample1)=="character") {
  message(paste0("First sample: ",sales_sample1))
  knitr::opts_chunk$set(eval = FALSE)
}

if (class(sales_sample2)=="character") {
  message(paste0("Second sample: ",sales_sample2))
  knitr::opts_chunk$set(eval = FALSE)
}
```


```{r recodes-and-adjustments, include=FALSE}
# Recodes
sales_sample1 <- date_recodes(sales_sample1, sales_sample1$sale_date)
sales_sample2 <- date_recodes(sales_sample2, sales_sample2$sale_date)

# Add vars
sales_sample1 <- add_vars(sales_sample1)
sales_sample2 <- add_vars(sales_sample2)

# Spatial adjustments
# XX should estimation dates be allowed to differ? 
sales_sample1 <- spatial_adjustments(sales_sample1, 
                                     param$model_estimation_method1, 
                                     param$model_form1, 
                                     param$model_adjust_locf1,
                                     param$estimation_date)

sales_sample2 <- spatial_adjustments(sales_sample2, 
                                     param$model_estimation_method2, 
                                     param$model_form2, 
                                     param$model_adjust_locf2,
                                     param$estimation_date)

```

```{r add-new-vars, include=FALSE}
# New vars
sales_sample1 <- new_sandbox_data_1(2, sales_sample1)
sales_sample1 <- new_sandbox_data_2_USER_POPULATE(2, sales_sample1)
sales_sample1 <- new_sandbox_data_3_oak_park(1, 
                                             sales_sample1,
                                            file=paste0(param$path,"/ccao_sf_cama_dev/austin_blvd.geojson"))

sales_sample2 <- new_sandbox_data_1(2, sales_sample2)
sales_sample2 <- new_sandbox_data_2_USER_POPULATE(2, sales_sample2)
sales_sample2 <- new_sandbox_data_3_oak_park(2, 
                                             sales_sample2)

```

```{r run-models, include=FALSE}
# Modeling
set.seed(1234)

m1 <- model_sales(sales_sample1, 
                  param$model_form1, 
                  param$model_estimation_method1_l,
                  param$model_comps_method1,
                  importance = TRUE,
                  tau = 0.5,
                  GBM_params=list(distribution="gaussian", interaction.depth=2, n.trees=100, cv.folds = 3))
m2 <- model_sales(sales_sample2, 
                  param$model_form2, 
                  param$model_estimation_method2_l,
                  param$model_comps_method2,
                  importance = TRUE,
                  tau = 0.5,
                  GBM_params=list(distribution="gaussian", interaction.depth=2, n.trees=100, cv.folds = 3))

```

```{r model-error-handling, echo=FALSE}
if (class(m1) == "try-error" | class(m2) == "try-error") {
  message("One of both of the models did not run successfully. Please check that the variable names are spelled correctly, and that the variables included are present in the selected dataset. For example, not all variables are available in the Cook County Open Data Portal. Another potential issue, if running quantile regression, might be that there is not variation in your variable. This might be an issue in smaller datasets.")
  knitr::opts_chunk$set(eval = FALSE)
}
```

```{r predict-values, include = FALSE}
# Prediction
sales_sample1 <- predict_sales(sales_sample1, m1, 1, COMPS_data=COMPS_model1)
sales_sample2 <- predict_sales(sales_sample2, m2, 2, COMPS_data=COMPS_model2)

# Adjust predictions, calculate ratios and residuals
sales_sample1 <- post_process_fitted(sales_sample1, 1, param$floor)
sales_sample2 <- post_process_fitted(sales_sample2, 2, param$floor)
```

## Which Model Performed Better?
```{r ratio_summary_run, include=FALSE}
ratio_summary <- summary_ratio_analysis(sales_sample1,1,sales_sample2,2) 
```

```{r overall_model_performance_run, include = FALSE}
model_summary <- model_performance_summary(sales_sample1,
                                           1,
                                           m1,
                                           sales_sample2,
                                           2,
                                           m2) 
```

```{r best_model_decision, echo=FALSE}
lowest_COD <- ratio_summary[[2]]
lowest_RMSE <- model_summary[[2]]

kable(rbind(lowest_COD,lowest_RMSE),'html',digits=2,format.args = list(decimal.mark = '.', big.mark = ",")) %>%
    kable_styling("striped", full_width = F) 

```


## Why was this model better?

### Analysis of Sales Ratios

```{r ratio_summary_table, echo=FALSE}
ratio_summary[[1]]
```

### Overall model performance

```{r overall_model_performance_output, echo=FALSE}
model_summary[[1]]
```

### Heteroskedasticity
#### Sales Ratios and OLS Fit to Decile Median Ratio

<style>
#boxplots2 {
  display:none;       
}
#time_boxplots2 {
  display:none;
}
</style>

<script>
function ratio_by_decile() {
    var x = document.getElementById("boxplots1");
    var y = document.getElementById("boxplots2");
    if (x.style.display === "none") {
        x.style.display = "block";
        y.style.display = "none";
    } else {
        x.style.display = "none";
        y.style.display = "block";
    }
}

function ratio_by_time() {
    var x = document.getElementById("time_boxplots1");
    var y = document.getElementById("time_boxplots2");
    if (x.style.display === "none") {
        x.style.display = "block";
        y.style.display = "none";
    } else {
        x.style.display = "none";
        y.style.display = "block";
    }
}
</script>
 
<button onclick="ratio_by_decile()">Switch Between Models</button>

```{r box-plots-prep, echo = FALSE}
# Run sales ratio by decile to get information from m1 and m2 used to determine a common y axis
model1_boxplot_intermediate <- sales_ratio_by_decile(sales_sample1,1)
model1_ylim <- model1_boxplot_intermediate[[1]]

model2_boxplot_intermediate <- sales_ratio_by_decile(sales_sample2,2)
model2_ylim <- model2_boxplot_intermediate[[1]]

common_ylim <- c(min(min(model1_ylim,model2_ylim)),max(max(model1_ylim,model2_ylim)))
```

<div id="boxplots1">
```{r box-plots1, echo = FALSE,message=FALSE, warning=FALSE}
sales_ratio_by_decile_plot(common_ylim, 
                           model1_boxplot_intermediate[[2]], 
                           model1_boxplot_intermediate[[3]], 
                           model1_boxplot_intermediate[[4]], 
                           model1_boxplot_intermediate[[5]],
                           model1_boxplot_intermediate[[6]], 
                           1)
```

</div>

<div id="boxplots2">
```{r box-plots2, echo = FALSE, message=FALSE, warning=FALSE}
sales_ratio_by_decile_plot(common_ylim, 
                           model2_boxplot_intermediate[[2]], 
                           model2_boxplot_intermediate[[3]], 
                           model2_boxplot_intermediate[[4]], 
                           model2_boxplot_intermediate[[5]],
                           model2_boxplot_intermediate[[6]], 
                           2)
```

</div>

### Treatment of time

<button onclick="ratio_by_time()">Switch Between Models</button>

```{r time_box-plots-prep, echo = FALSE}
# Run sales ratio by decile to get information from m1 and m2 used to determine a common y axis
time_var = as.character(param$time_plot_var)

model1_time_boxplot_intermediate <- sales_ratio_by_time_boxplot_prep(sales_sample1,1,time_var=time_var)
model1_ylim <- model1_time_boxplot_intermediate[[1]]

model2_time_boxplot_intermediate <- sales_ratio_by_time_boxplot_prep(sales_sample2,2,time_var=time_var)
model2_ylim <- model2_time_boxplot_intermediate[[1]]

common_ylim <- c(min(min(model1_ylim,model2_ylim)),max(max(model1_ylim,model2_ylim)))
```


<div id="time_boxplots1">
```{r time_box-plots1, echo = FALSE}
sales_ratio_by_time_boxplot(common_ylim, 
                           model1_time_boxplot_intermediate[[2]],
                           1,
                           time_var = time_var)
```

</div>

<div id="time_boxplots2">
```{r time_box-plots2, echo = FALSE}
sales_ratio_by_time_boxplot(common_ylim, 
                           model2_time_boxplot_intermediate[[2]],
                           2,
                           time_var = time_var)
```

</div>


### Disuniformity
```{r disuniformity-max-compare, echo=FALSE, message=FALSE, warning=FALSE}
disuniformity_model1 <- disuniformity(sales_sample1,1)
disuniformity_model2 <- disuniformity(sales_sample2,2)
disuniformity_compare_boxplot(disuniformity_model1,disuniformity_model2)
```

```{r disuniformity-model1, echo=FALSE, message=FALSE}
scroll_box(disuniformity_model1[[1]],height="300px")
```

```{r disuniformity-model2, echo=FALSE, message=FALSE}
scroll_box(disuniformity_model2[[1]],height="300px")
```


### Spatial Autocorrelation 
```{r morans-i, echo=FALSE, warning=FALSE, message = FALSE}
# Spatial autocorrelation
moran_1 <- spatial_autocorrelation(sales_sample1,1)
moran_2 <- spatial_autocorrelation(sales_sample2,2)
```

```{r morans-i-output, echo=FALSE}
table <- spatial_autocorrelation_table(moran_1,moran_2,1,2)
table
```




## Appendix
#### Summary of parameters
```{r show-params, echo=FALSE}
par <- display_params(params)
```

```{r dataset-compare, echo=FALSE}
kable(par[[1]],'html',digits = 2, caption = "Comparison of Datasets Used for Models 1 and 2") %>%
  kable_styling(bootstrap_options = "striped", full_width = F)
```

```{r models-compare, echo=FALSE}
kable(par[[2]],'html',digits = 2, caption = "Comparison of Models 1 and 2") %>%
  kable_styling(bootstrap_options = "striped", full_width = F)
```


```{r sumstats, results='asis',echo=FALSE, fig.align='center',include=FALSE}
# summary statistics of datasets used in models 1 and 2 (even if the dataset is the same, the functional form
# might be different)
m1_data <- sum_stats(sales_sample1,param$model_form1,param$model_estimation_method1)
m2_data <- sum_stats(sales_sample2,param$model_form2,param$model_estimation_method2)

if (identical(m1_data,m2_data)) {
  data.sumstats <- stargazer(m1_data, type="html", header = FALSE, digits = 2, 
                             title="Summary of data and variables used in Models 1 and 2")  
} else {
  data.sumstats <- stargazer(m1_data, type="html", header = FALSE, digits = 2
                             ,title="Summary of data and variables used in Model 1")
  data.sumstats <- stargazer(m2_data, type="html", header = FALSE, digits = 2
                             ,title="Summary of data and variables used in Model 2")
}

# XXX should we hide summ stats if its comps because there are not variables to restrict on?
```

```{r save_model_output, include=FALSE}
# get output from models depending on the estimation method
model_outputs <- model_output(list(m1,m2))

lm_output <- model_outputs[[1]]
gbm_output <- model_outputs[[2]]
rq_output <- model_outputs[[4]]
model_count <- model_outputs[[5]]

# XXX what model output to add for COMPS?
```

```{r model-output, results='asis',echo=FALSE, fig.align='center',include=FALSE}
# output from lm models
if (length(lm_output)>0) {
  cat(paste(lm_output[[1]], collapse = "\n"), "\n")
  cat(paste(lm_output[[2]], collapse = "\n"), "\n")
}

# output from rq models
if (length(rq_output)>0) {
  cat(paste(rq_output[[1]], collapse = "\n"), "\n")
  cat(paste(rq_output[[2]], collapse = "\n"), "\n")
}

# output from gbm models
if (length(gbm_output)>0) {
  gbm_summary <- lapply(gbm_output,summary,plotit=FALSE)  
  
  for (i in 1:length(gbm_summary)) {
    print(kable(gbm_summary[[i]],'html',digits = 2, caption = paste0("Output from GBM Model(s)")) %>%
    kable_styling(bootstrap_options = "striped", full_width = F))
    cat('\n')
  }
}
```


#### Distribution of sales ratios

```{r ratios-dist, echo=FALSE, warning=FALSE}
# Distribution of ratios for two models
ratio_histogram(sales_sample1,sales_sample2)
```

```{r model-sales-ratios, include=FALSE, eval=FALSE}
# IAAO Stats
iaao_stats_1 <- iaao_stats(sales_sample1,1)
iaao_stats_2 <- iaao_stats(sales_sample2,2)
```

```{r model1-sales-ratio, echo=FALSE, eval=FALSE}
kable(iaao_stats_1,'html',digits = 2, caption = "Model 1") %>%
  kable_styling(bootstrap_options = "striped", full_width = F)
```

```{r model2-sales-ratio, echo=FALSE, eval=FALSE}
kable(iaao_stats_2,'html',digits = 2, caption = "Model 2") %>%
  kable_styling(bootstrap_options = "striped", full_width = F)
```

```{r create-ratio-tables, echo=FALSE, eval=FALSE}
# Other Ratio Stats
ratio_summary_1 <- ratio_stats(sales_sample1,1)
ratio_summary_2 <- ratio_stats(sales_sample2,2)
```

```{r model1-ratio-summary, echo=FALSE, include = FALSE, eval=FALSE}
kable_obj <- kable(ratio_summary_1,'html',digits = 2, caption = "Model 1") %>% 
  kable_styling(bootstrap_options = "striped", full_width = F)
scroll_box(kable_obj,height = "200px")
```

```{r model2-ratio-summary, echo=FALSE, include=FALSE, eval=FALSE}
kable(ratio_summary_2,'html',digits = 2, caption = "Model 2") %>% 
  kable_styling(bootstrap_options = "striped", full_width = F)
```



