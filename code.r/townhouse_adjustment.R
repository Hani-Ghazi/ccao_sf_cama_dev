column_existance <- function(df, column){
  # This funtion is used to check if the column used in the pipeline exists in the valuationdata
  # This function will be used in the following functions to check for the existance of the specified columns
  
  # Inputs:
  #    df: (dataframe) valuationdata
  #    column: (string) column specified
  # Return: (Boolean) TRUE if the column exists
  
  # Example: column_existance(valuationdata, "NBHD")
  
  if (column %in% colnames(df)){return(TRUE)}
  else{print(paste0("The column ", paste(column), " specified does not exist in the given dataset."))
    return(FALSE)}
}


predetermined_condition <- function(df, condition_cols, conditions, method="and"){
  # This function is used to provide pre-determined conditions for every rows in the dataframe which can be
  # used as the overall condition to subset the valuationdata.
  
  # Inputs:
  #    df: (dataframe) valuationdata
  #    condition_cols: (vector of strings) specific columns where conditions are depending on
  #    conditions: (list of vectors of booleans) vectors of boolean which demonstrates if the given condition
  #                                              is satisfied by every given rows
  # Returns: (vector of boolean) Specifies whether every given row in the vector satisfies overall conditions
  
  # Example:
  #    condition_cols <- c("most_recent_sale_price", "filter_1")
  #    conditions <- list(ifelse(!is.na(valuationdata$most_recent_sale_price), TRUE, FALSE), 
  #                       ifelse(valuationdata$filter_1>=1, TRUE, FALSE))
  #    pred_cond <- predetermined_condition(valuationdata, group_cols, group_conditions)
  #
  # pred_cond will provide a vector of boolean, for every row, TRUE if it satisfies all the conditions (not null for 
  # most recent sale price and greater or equals to 1 for filter_1 column)
  
  index <- 1
  
  if (method == "and"){df$cumulated_conditions <- TRUE}
  if (method == "or"){df$cumulated_conditions <- FALSE}
  
  for (col in condition_cols){
    if (column_existance(df, col)){
      
      if (method == "and"){
        df$cumulated_conditions <- ifelse(df$cumulated_conditions & conditions[[index]], TRUE, FALSE)}
      if (method == "or"){
        df$cumulated_conditions <- ifelse(df$cumulated_conditions | conditions[[index]], TRUE, FALSE)}
    }
    index <- index + 1 
  }
  return(df$cumulated_conditions)
}

#########################################################################
#                      Townhouse Adjustment                             #
#########################################################################

# identify_townhouse <- function(df, group_cols, condition_cols, conditions){
#   # The function replace the fitted value of the townhouse with the mean value of them regardless of the
#   # square feet of land
# 
#   df$pred_cond <- predetermined_condition(df, condition_cols, conditions)
#   df <- df %>% mutate(group = group_indices(df, !!!syms(group_cols)))
#   count <- 0
#   df$fitted_value_7 <- df$fitted_value_6
#   townhouses <- data.frame()
# 
#   for (group_val in unique(df$group)){
#     sub_df <- subset(df, df$group == group_val & df$pred_cond)
#     if (nrow(sub_df) > 1){
#       print(paste0("adjusting for townhouse of group: ", paste(group_val), " with BLOCK: ", paste(sub_df$BLOCK[1])))
#       df$fitted_value_7 <- ifelse(df$group == group_val & df$pred_cond, mean(sub_df$fitted_value_6), df$fitted_value_7)
#       count <- count + nrow(sub_df)
#       print(paste0("sub_df has columns ", paste(ncol(sub_df), " and townhouses has columns: ", paste(ncol(townhouses)))))
#       townhouses <- rbind(townhouses, sub_df)}
#   }
#   print(paste0("In total, adjust ", paste(count), " townhouses"))
#   return(list(df, townhouses))
# }

suspected_cross_block <- function(df, sub_group_cols, condition_cols, conditions){
  # The function is used to determine the suspected pins which are having suspected characteristics
  # of the townhouses and are located in different blocks
  
  # Inputs:
  #    df: (dataframe) valuationdata
  #    sub_group_cols: (vector of strings) columns used for grouping
  #    condition_cols: (vector of strings) columns used for conditioning
  #    conditions: (list of vector of boolean) conditions for subseting the dataframe
  # Returns:
  #    other_df: (dataframe) the valuationdata without cases of the suspected townhouses
  #    suspected_townhouses: (dataframe) the valuationdata with cases of the suspected townhouses

  df <- df %>% mutate(group = group_indices(df, !!!syms(sub_group_cols)))
  df$fitted_value_7 <- df$fitted_value_6
  # We only consider the cases which satisfy the pre-determined conditions
  df$pred_cond <- predetermined_condition(df, condition_cols, conditions)
  index <- 1
  df$development_id <- 0
  suspected_townhouses <- data.frame()

  for (group_val in unique(df$group)){
    sub_df <- subset(df, df$group == group_val & df$pred_cond)
    
    # only consider cases for townhouses
    if (nrow(sub_df) > 1){
      # only include town houses which cross the blocks
      df$development_id <- ifelse(df$group == group_val & df$pred_cond, index, df$development_id)
      sub_df$development_id <- index
      index <- index + 1
      if (length(unique(sub_df$BLOCK)) > 1){
        suspected_townhouses <- rbind(suspected_townhouses, sub_df)
        }
      else{
        df$fitted_value_7 <- ifelse(df$group == group_val & df$pred_cond, mean(df$fitted_value_6), df$fitted_value_7)
        }
    }
  }
  other_df <- df[!rownames(df) %in% rownames(suspected_townhouses), ]
  return(list(other_df, suspected_townhouses))
}


get_nearest_blocks <- function(df, k){
  # The function is used to determined the nearest blocks of a given block. In the function, we calculate the
  # centroid of the given block by taking average of longitude and latitude from all of the pin locations. We
  # then calculate the distance between blocks with the centroids. The nearest k blocks will be stored in the
  # column "nearests" in the returned dataframe which uses neighborhood and block number as primary key
  
  # Inputs:
  #    df: (dataframe) valuationdata
  #    k: (integer) number of the nearest blocks considered
  # Returns: dataframe includes information of the nearest blocks for every given block
  
  dist_list <- df %>% group_by(df$NBHD, df$BLOCK) %>% summarise(block_centroidx = mean(centroid_x), 
                                                                block_centroidy = mean(centroid_y))
  dist_frame <- data.frame(dist_list)
  dist_frame$NBBLOCK <- paste0(paste(dist_frame$df.NBHD), dist_frame$df.BLOCK) 
  colnames(dist_frame)[colnames(dist_frame) == "NBBLOCK"] <- "BLOCK"
  dist_frame$nearests <- 0
  
  for (block_idx in unique(dist_frame$BLOCK)){
    center_block <- dist_frame[dist_frame$BLOCK == block_idx, ]
    other_blocks <- dist_frame[dist_frame$BLOCK != block_idx, ]
    other_blocks$center_x <- center_block$block_centroidx
    other_blocks$center_y <- center_block$block_centroidy
    other_blocks <- as.matrix(other_blocks)
    
    distance_to_center <- apply(other_blocks, 1, function(obk) distHaversine(c(as.numeric(obk[3]), as.numeric(obk[4])), 
                                                                             c(as.numeric(obk[7]), as.numeric(obk[8]))))
    other_blocks <- as.data.frame(other_blocks)
    other_blocks$distance_to_center <- distance_to_center
    
    nearest_blocks <- list(as.character(other_blocks[order(other_blocks$distance_to_center), ][1:k, ]$BLOCK))
    dist_frame$nearests[dist_frame$BLOCK== block_idx] <- nearest_blocks
  }
  return(dist_frame)
}


define_and_adjust_townhouse <- function(df, sub_group_cols, condition_cols, conditions, k){
  # The function is used to perform the overall adjustment for the townhouses, the townhouse will be
  # labelled with non-zero development id. The fitted value of the pin with the same development id
  # will be replaced with the mean value of cases with the same development id.
  
  # The fitted value of townhouses within the same development should be identical. A group of properties will be defined
  # as a single unit of townhouse if they contains the same age, house square feet and located in the
  # same neighborhood and same block. However, there might be some of them located in the same neighborhood
  # but end up to be in different blocks. In order to account for this situation, we first group the data
  # by its neighborhood, age and house square feet, the group with only one pins will be excluded from 
  # this adjustment. Then, for the groups with more than one pin, if they all locate in the same block,
  # they will be determined as the townhouse. If the group has pins locate in more than one blocks, we will
  # use get_nearest_blocks function to determined if the blocks in the group are mutual nearest neighborhoods.
  # Only if the properties are mutually located in the neighbor blocks and similar to their fitted value will
  # be considered as the same town house. We use count to calculate the total cases changed, 
  # and use tie-breaker with length(blk_list) / 2 to determine if they are considered the same town houses if
  # they have more than 2 suspected properties in the same group
  
  # Inputs:
  #    df: (dataframe) valuationdata
  #    sub_group_cols: (vector of strings) columns used for grouping
  #    condition_cols: (vector of strings) columns used for conditioning
  #    conditions: (list of vector of boolean) conditions for subseting the dataframe
  #    k: (integer) number of the nearest blocks considered
  # Return: 
  #    final_df: (dataframe) updated valuationdata with development ID
  #    changed_df: (dataframe) the case which has been changed with their group (via development ID)
  
  suspected_info <- suspected_cross_block(df, sub_group_cols, condition_cols, conditions)
  other_df <- suspected_info[[1]]
  suspected_townhouses <- suspected_info[[2]]
  suspected_townhouses$NEW_BLOCK <- paste0(paste(suspected_townhouses$NBHD), paste(suspected_townhouses$BLOCK))
  suspected_townhouses$same_house <- 0
  dist_frame <- get_nearest_blocks(df, k)
  
  changed_df <- data.frame()
  count <- 0
  
  for (group_idx in unique(suspected_townhouses$group)){
    sub_df <- subset(suspected_townhouses, suspected_townhouses$group == group_idx)
    blk_list <- unique(sub_df$NEW_BLOCK)
    if (length(blk_list) > 2){print(paste0("group idx: ", paste(group_idx), " has more than 2 blocks: "))
                              print(blk_list)}
    
    index <- 1
    for (blk1 in blk_list[-length(blk_list)]){
      blk1_mean <- mean(subset(suspected_townhouses, group == group_idx & NEW_BLOCK == blk1)$fitted_value_6)
      for (blk2 in blk_list[index+1:length(blk_list)]){
        blk2_mean <- mean(subset(suspected_townhouses, group == group_idx & NEW_BLOCK == blk2)$fitted_value_6)
        if (blk2 %in% dist_frame[dist_frame$BLOCK == blk1,]$nearests[[1]] &
            blk1 %in% dist_frame[dist_frame$BLOCK == blk2,]$nearests[[1]] & 
            abs(blk1_mean - blk2_mean) / blk1_mean < 0.05){
          suspected_townhouses$same_house <- ifelse(suspected_townhouses$group == group_idx, suspected_townhouses$same_house + 1, 
                                                    suspected_townhouses$same_house)}
        }
      index <- index + 1
      suspected_townhouses$fitted_value_7 <- ifelse(suspected_townhouses$group == group_idx & suspected_townhouses$NEW_BLOCK == blk1,
                                                    blk1_mean, suspected_townhouses$fitted_value_7)
    }
    tie_break_conditions <- suspected_townhouses$same_house >= length(blk_list) / 2 & suspected_townhouses$group == group_idx
    reset_index <- subset(suspected_townhouses, tie_break_conditions)$development_id[1]
    print(paste0("reset_index is set to be: ", paste(reset_index)))
    suspected_townhouses$fitted_value_7 <- ifelse(tie_break_conditions,
                                                  mean(subset(suspected_townhouses, (same_house > length(blk_list) / 2) & group == group_idx)$fitted_value_6),
                                                  suspected_townhouses$fitted_value_7)
    suspected_townhouses$development_id <- ifelse(tie_break_conditions, reset_index, suspected_townhouses$development_id)
    changed_df <- rbind(changed_df, subset(suspected_townhouses, tie_break_conditions))
    count <- count + nrow(subset(suspected_townhouses, tie_break_conditions))
  }
  suspected_townhouses$NEW_BLOCK <- suspected_townhouses$same_house <- NULL
  final_df <- rbind(other_df, suspected_townhouses)
  print(paste0("the total numbers of the changes in townhouses is: ", paste(count)))
  print(paste0("it contains ", paste(length(unique(changed_df$development_id))), " different groups of developmets"))
  return(list(final_df, changed_df))
}

# Code for executiion
# PIN, CLASS, NBHD, BLDG_SF, centroid_x, centroid_y, and fitted value are necessary in valuationdata
valuationdata$BLOCK <- substring(valuationdata$PIN, 5, 7)
condition_cols <- c("CLASS")
conditions <- list(ifelse(valuationdata$CLASS %in% c(210, 295), TRUE, FALSE))
#group_cols <- c("NBHD", "AGE", "BLDG_SF", "BLOCK")
sub_group_cols <- c("NBHD", "AGE", "BLDG_SF")

start_time <- Sys.time()
suspected_df <- define_and_adjust_townhouse(valuationdata, sub_group_cols, condition_cols, conditions, 5)
updated_df <- suspected_df[[1]]
# The case in changed df can be checked manually
changed_df <- suspected_df[[2]]
end_time <- Sys.time()
print(paste0("Total processing time is: ", paste(end_time - start_time), " mins"))