# This file supports the residential modeling sandbox (residential_modeling_sanbox.Rmd)
# It contains analysis functions which calculate ratios, residuals, and IAAO stats for input models

post_process_fitted <- function(model_data,model_number,floor) {
  # This function takes data with predicted values from various fitted models. It adjusts predicted values
  # to be no lower than the floor, calculates ratios, and caps ratios to be no higher than 2
  # Using the adjusted fitted values, the function calculates residuals

  variable <- paste0("Predicted Fair Cash Value Model ",model_number)
  
  # drop those missing predicted values - should only be for COMPS
  model_data <- model_data[!is.na(model_data[,variable]),]
  
  fitted_value <- model_data[,variable]
  
  floor_value <- floor
  sale_price <- model_data$sale_price
  # Adjust fitted value to only go as low as specified floor
  fitted_value <- ifelse(fitted_value<floor_value,floor_value,fitted_value)
  
  # Calculate ratio
  ratio <- fitted_value / sale_price
  
  # If ratio is greater than 2, adjust fitted value to cap ratio and recalculate
  fitted_value = ifelse(ratio>2,sale_price * 2, fitted_value)
  ratio = fitted_value / sale_price
  
  # finally calculate residual
  residual <- sale_price - fitted_value
  
  # Save fitted values, ratios, and residuals for each model
  model_data$`Predicted Fair Cash Value Adj` <- fitted_value
  names(model_data)[names(model_data)=="Predicted Fair Cash Value Adj"] <-
    paste0("Predicted Fair Cash Value Adj Model ",model_number)
  
  model_data$`Ratio` <- ratio
  names(model_data)[names(model_data)=="Ratio"] <-
    paste0("Ratio Model ",model_number)
  
  model_data$`Residual` <- residual
  names(model_data)[names(model_data)=="Residual"] <-
    paste0("Residual Model ",model_number)
  
  return(model_data)  
}


  


