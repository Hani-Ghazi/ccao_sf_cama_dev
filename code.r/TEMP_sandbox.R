remove(list = ls())

params <- NA
class(params) <- "Parameters"
# For windows, will need to change this to: paste0("C:/Users/", Sys.info()[['user']],"/Documents")
params$path <- paste0("/Users/", Sys.info()[['user']],"/Dropbox/CCAO")
params$computer <- "MacOS"
#params$computer <- "Windows"
# params$path_local_file <- paste0(params$path, "/ccao_sf_cama_dev/sales_sample.Rda")
params$floor <- 10000

params$estimation_date <- as.Date(paste0(lubridate::year(Sys.time()),'-01-01'))
params$time_plot <- "Sale Quarter"

# Data 1
params$townships1 <- c("Barrington", "Hanover", "Palatine")
params$data_pull1 <- "Cook County Open Data Portal"
params$modelgroup1 <-  "Single Family Regression Classes"
params$min_year1<- 2018
params$max_year1<- 2018
params$data_pull_local_path1 <- "/ccao_sf_cama_dev/sales_sample.rds"
params$data_pull_save1 <- "Yes, save data locally"

# Data 2
params$townships2 <- c("Barrington", "Hanover", "Palatine")
params$data_pull2 <- "Cook County Open Data Portal"
params$modelgroup2 <-  "Residential Multi-Family Classes 211, 212"
params$min_year2<- 2018
params$max_year2<- 2018
params$data_pull_local_path2 <- "/ccao_sf_cama_dev/sales_sample.rds"
params$data_pull_save2 <- "Yes, save data locally"

# Model 1
params$model_form1 <-  "log(sale_price)~1+log(BLDG_SF)+log(BLDG_SF)+log(HD_SF)+FBATH+BSMT"
params$model_estimation_method1 <-  "OLS"
params$model_comps_method1 <- "f1"
params$model_adjust_locf1 <- FALSE

# Model 2
params$model_estimation_method2 <-  "OLS"
params$model_form2 <-  "log(sale_price)~1+log(BLDG_SF)+log(BLDG_SF)+log(HD_SF)+FBATH+BSMT"
params$model_comps_method2 <- "f1"
params$model_adjust_locf2 <- FALSE

source(paste0(params$path,"/ccao_utility/code.r/99_utility_2.r"))
invisible(check.packages(libs))

source(paste0(params$path,"/ccao_sf_cama_dev/code.r/0_sandbox_ingest.r"))
source(paste0(params$path, "/ccao_sf_cama_dev/code.r/0_sandbox_newvars.r"))
source(paste0(params$path, "/ccao_sf_cama_dev/code.r/2_sandbox_modeling.r"))
source(paste0(params$path, "/ccao_sf_cama_dev/code.r/3_sandbox_analysis.r"))
source(paste0(params$path, "/ccao_sf_cama_dev/code.r/4_sandbox_output.r"))

# Recode params
param <- params_recodes(CCAODATA, params)

# DB setup
# The following commands need to be run if you're connecting to the server and pulling data (as data pull method or if building COMPS)
if(param$data_pull_choice_code1[1]==1 | 
   param$data_pull_choice_code2 == 1 | 
   param$model_estimation_method1=="COMPS" |
   param$model_estimation_method2=="COMPS"){
  database <- 1
  CCAODATA <- try(dbConnect(odbc()
                            , driver   = odbc.credentials("driver",param$path,param$computer)
                            , server   = odbc.credentials("server",param$path,param$computer)
                            , database = odbc.credentials("database",param$path,param$computer)
                            , uid      = odbc.credentials("uid",param$path,param$computer)
                            , pwd      = odbc.credentials("pwd",param$path,param$computer)))
  
  if(class(CCAODATA)[1]=="simpleError" | class(CCAODATA)=="try-error"){
    message("Failed to connect to SQL server. Common reasons might be: you're not connected to the VPN, or you specified the wrong Operating System parameter.")
  }}

# Building data
sales_sample1 <- query_sandbox_data_1(database = param$data_pull_choice_code1, 
                                      path = param$path,
                                      townships = param$town_codes1, 
                                      min_year = param$min_year1, 
                                      max_year = param$max_year1, 
                                      classes = param$modelgroup_classes1,
                                      local_file = param$path_local_file1, 
                                      dataset_num = 1,
                                      save_locally = param$data_pull_save1)
# If both datasets are SQL pulls with identical parameters, sales sample2 is set to be the same as sales sample1. 
# otherwise we call query sandbox data 1 again
if (param$identical_data) {
  sales_sample2 <- samples_sample1
} else {
  sales_sample2 <- query_sandbox_data_1(database = param$data_pull_choice_code2, 
                                        path = param$path,
                                        townships = param$town_codes2, 
                                        min_year = param$min_year2, 
                                        max_year = param$max_year2, 
                                        classes = param$modelgroup_classes2,
                                        local_file = param$path_local_file2, 
                                        dataset_num = 2,
                                        save_locally = param$data_pull_save2)
  
}

if (class(sales_sample1)=="character") {
  message(paste0("First sample: ",sales_sample1))
}

if (class(sales_sample2)=="character") {
  message(paste0("Second sample: ",sales_sample2))
}

# Build COMPS data 
if(param$model_estimation_method1=="COMPS") {
  COMPS_model1 <- build_comps_data(param$model_comps_method1)
  #COMPS_model1 <- COMPS_model1
}
if(param$model_estimation_method2=="COMPS"){
  if(param$identical_comps) {
    COMPS_model2 <- COMPS_model1
  }else{
    COMPS_model2 <- build_comps_data(param$model_comps_method2)
    #COMPS_model2 <- COMPS_model2
  }
}

# Recodes
sales_sample1 <- date_recodes(sales_sample1, sales_sample1$sale_date)
sales_sample2 <- date_recodes(sales_sample2, sales_sample2$sale_date)
# Add vars
sales_sample1 <- add_vars(sales_sample1)
sales_sample2 <- add_vars(sales_sample2)

# Spatial adjustments
# XX should estimation dates be allowed to differ? 
sales_sample1 <- spatial_adjustments(sales_sample1, 
                                     param$model_estimation_method1, 
                                     param$model_form1, 
                                     param$model_adjust_locf1,
                                     param$estimation_date)

sales_sample2 <- spatial_adjustments(sales_sample2, 
                                     param$model_estimation_method2, 
                                     param$model_form2, 
                                     param$model_adjust_locf2,
                                     param$estimation_date)

# New vars
sales_sample1 <- new_sandbox_data_1(2, sales_sample1)
sales_sample1 <- new_sandbox_data_2_USER_POPULATE(2, sales_sample1)

sales_sample2 <- new_sandbox_data_1(2, sales_sample2)
sales_sample2 <- new_sandbox_data_2_USER_POPULATE(2, sales_sample2)

# Modeling
set.seed(1234)
m1 <- model_sales(sales_sample1, 
                  param$model_form1, 
                  param$model_estimation_method1_l,
                  param$model_comps_method1,
                  importance = TRUE,
                  tau = 0.5,
                  GBM_params=list(distribution="gaussian", interaction.depth=2, n.trees=100, cv.folds = 3))
m2 <- model_sales(sales_sample2, 
                  param$model_form2, 
                  param$model_estimation_method2_l,
                  param$model_comps_method2,
                  importance = TRUE,
                  tau = 0.5,
                  GBM_params=list(distribution="gaussian", interaction.depth=2, n.trees=100, cv.folds = 3))


if (class(m1) == "try-error" | class(m2) == "try-error") {
  message("One of both of the models did not run successfully. Please check that the variable names are spelled correctly, and that the variables included are present in the selected dataset. For example, not all variables are available in the Cook County Open Data Portal.")
}



# Prediction
sales_sample1 <- predict_sales(sales_sample1, m1, 1, COMPS_data=COMPS_model1)
sales_sample2 <- predict_sales(sales_sample2, m2, 2, COMPS_data=COMPS_model2)

# Adjust predictions, calculate ratios and residuals
sales_sample1 <- post_process_fitted(sales_sample1, 1, param$floor)
sales_sample2 <- post_process_fitted(sales_sample2, 2, param$floor)

## Which Model Performed Better?
ratio_summary <- summary_ratio_analysis(sales_sample1,1,sales_sample2,2) 
model_summary <- model_performance_summary(sales_sample1,
                                           1,
                                           m1,
                                           sales_sample2,
                                           2,
                                           m2) 
lowest_COD <- ratio_summary[[2]]
lowest_RMSE <- model_summary[[2]]

kable(rbind(lowest_COD,lowest_RMSE),'html',digits=2,format.args = list(decimal.mark = '.', big.mark = ",")) %>%
  kable_styling("striped", full_width = F) 

## Why was this model better?
### Analysis of Sales Ratios
ratio_summary[[1]]
### Overall model performance
model_summary[[1]]
### Heteroskedasticity
#### Sales Ratios and OLS Fit to Decile Median Ratio

# Run sales ratio by decile to get information from m1 and m2 used to determine a common y axis
model1_boxplot_intermediate <- sales_ratio_by_decile(sales_sample1,1)
model1_ylim <- model1_boxplot_intermediate[[1]]

model2_boxplot_intermediate <- sales_ratio_by_decile(sales_sample2,2)
model2_ylim <- model2_boxplot_intermediate[[1]]

common_ylim <- c(min(min(model1_ylim,model2_ylim)),max(max(model1_ylim,model2_ylim)))
sales_ratio_by_decile_plot(common_ylim, 
                           model1_boxplot_intermediate[[2]], 
                           model1_boxplot_intermediate[[3]], 
                           model1_boxplot_intermediate[[4]], 
                           model1_boxplot_intermediate[[5]],
                           model1_boxplot_intermediate[[6]], 
                           1)
sales_ratio_by_decile_plot(common_ylim, 
                           model2_boxplot_intermediate[[2]], 
                           model2_boxplot_intermediate[[3]], 
                           model2_boxplot_intermediate[[4]], 
                           model2_boxplot_intermediate[[5]],
                           model2_boxplot_intermediate[[6]], 
                           2)
### Treatment of time
# Run sales ratio by decile to get information from m1 and m2 used to determine a common y axis
time_var = as.character(param$time_plot_var)

model1_time_boxplot_intermediate <- sales_ratio_by_time_boxplot_prep(sales_sample1,1,time_var=time_var)
model1_ylim <- model1_time_boxplot_intermediate[[1]]

model2_time_boxplot_intermediate <- sales_ratio_by_time_boxplot_prep(sales_sample2,2,time_var=time_var)
model2_ylim <- model2_time_boxplot_intermediate[[1]]

common_ylim <- c(min(min(model1_ylim,model2_ylim)),max(max(model1_ylim,model2_ylim)))
sales_ratio_by_time_boxplot(common_ylim, 
                            model1_time_boxplot_intermediate[[2]],
                            1,
                            time_var = time_var)
sales_ratio_by_time_boxplot(common_ylim, 
                            model2_time_boxplot_intermediate[[2]],
                            2,
                            time_var = time_var)

### Disuniformity
disuniformity_model1 <- disuniformity(sales_sample1,1)
disuniformity_model2 <- disuniformity(sales_sample2,2)
disuniformity_compare_boxplot(disuniformity_model1,disuniformity_model2)

scroll_box(disuniformity_model1[[1]],height="300px")
scroll_box(disuniformity_model2[[1]],height="300px")


### Spatial Autocorrelation 
# Spatial autocorrelation
moran_1 <- spatial_autocorrelation(sales_sample1,1)
moran_2 <- spatial_autocorrelation(sales_sample2,2)
table <- spatial_autocorrelation_table(moran_1,moran_2,1,2)
table

## Appendix
#### Summary of parameters
par <- display_params(params)
kable(par[[1]],'html',digits = 2, caption = "Comparison of Datasets Used for Models 1 and 2") %>%
  kable_styling(bootstrap_options = "striped", full_width = F)

kable(par[[2]],'html',digits = 2, caption = "Comparison of Models 1 and 2") %>%
  kable_styling(bootstrap_options = "striped", full_width = F)

#### Distribution of sales ratios

# Distribution of ratios for two models
ratio_histogram(sales_sample1,sales_sample2)

  



